import React from "react";
import { useLocation } from "react-router-dom";

import { Header } from "components/Header";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import {Hero} from "components/Hero";
import heroImg from "assets/images/hero-blog.jpg";
import {HeroBlog} from "components/Hero/Content";
import { WPBlogList } from "components/WPBlog/WPBlogList";
import { Projects } from 'components/Projects';
import { SourceAboutUs } from 'components/SourceAboutUs';
import { CommonMetaTags } from 'components/CommonMetaTags';
import {data} from "components/Projects/homepageProjectsData";


import './BlogListPage.css'

function useQuery() {
  const { search } = useLocation();

  return React.useMemo(() => new URLSearchParams(search), [search]);
}


export const BlogListPage = () => {

  let query = useQuery();
  const catId = query.get('cat_id');
  const pageNo = query.get('page_no');
  
  return(
    <div className='page-wrapper'>
      <CommonMetaTags 
				title={'Blog'}
				description={'Tandem Marketing'}
				url={'https://www.tandem-m.de/blog.html'}
				image={heroImg}
			/>

      <Header />

      <main>
        <h1 className="visuallyHidden">Blog</h1>

        <Hero
          src={heroImg}
          alt="Bürostuhlrennen"
          width={1920}
          height={800}
          content={<HeroBlog />}
        />

        <section styleName='wrp'>
          <WPBlogList postType={`articles`} categoryId={catId ? catId : 0} pageNo={pageNo ? pageNo : 1} />
        </section>

        <Projects data={data} title="Zeit, unsere Arbeit zu zeigen" />

        <div styleName="sources-about-us">
          <SourceAboutUs />
        </div>

        <FeedbackHomePage />
      </main>

      <Footer />
    </div>
  );
}

