import React, { Fragment } from "react";
import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { Article } from "components/Article";
import { ParallaxEffect } from "components/ParallaxEffect";
import { ColorSteps } from "components/ColorSteps";
import { Hr } from "components/Hr";
import { Icon } from "components/Icon";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from "assets/images/hero-drupal.jpg";
import imgArticle from "assets/images/drupal-myle.jpg";
import mobileParallaxImg from "assets/images/drupal-for-mobile-parallax.jpg";
import backendParallaxImg from "assets/images/drupal-backend-parallax.jpg";
import contactParallaxImg from "assets/images/drupal-contact-parallax.jpg";

import "./DrupalPage.css";

export const DrupalPage = () => {
  return (
    <div className="page-wrapper">
      <CommonMetaTags 
				title={'Drupal für Ihre Webseite? Erfahren Sie mehr über die Vorteile'}
				description={'Drupal ist zu Recht eines der beliebtesten CMS weltweit - Tandem Marketing &amp;amp; Partners ist die Internetagentur für Ihr Webprojekt. Wir beraten Sie gern!'}
				url={'https://www.tandem-m.de/webdesign-cms/drupal.html'}
				image={heroImg}
			/>

      <Header />

      <main>
        <Hero
          src={heroImg}
          heght={500}
          content={
            <HeroContent 
              title={<span>Drupal für Ihre<br /> Website</span>}
              text="Drupal"
            />
          }
        />

        <SectionWhite 
          title="Die Vorteile im Überblick"
          text="Das  Content Management System (CMS) Drupal gehört weltweit zu den führenden CMS. Millionen von Websites basieren bereits auf Drupal. Auch in Deutschland setzen bekannte Unternehmen wie Greenpeace, Amnesty International Deutschland, ARD-aktuell und Zeit online auf das CMS."
          textColor="var(--color-сurious-blue)"
        />

        <SectionColor 
          content={
          <Fragment>
            <p className="section-text-200">Ob Unternehmensauftritt, Blog, Intranet oder Onlineshop: Drupal ist sehr vielseitig und lässt sich für Webprojekte in allen Branchen anwenden, weil es dynamischer als andere Content Management Systeme ist. Das CMS ist leicht zu warten, sehr updatefähig und open source: Teure Lizenzgebühren fallen weg. Außerdem setzen Sie mit Drupal auf ein CMS mit sehr hohen Sicherheitsstandards: Sogar die Webseite des Weißen Haus basierte in der Regierungszeit von Obama darauf.</p>
            <p className="section-text-200">Hier erfahren Sie, warum Drupal das richtige Content Management System für Ihr Webprojekt ist.</p>
          </Fragment>
          }
          backgroundColor="var(--color-сurious-blue)"
        />

        <section>
          <h2 className="visuallyHidden">Modularität des Drupal-Systems</h2>
          <Article 
            title="Große Flexibilität"
            content={<p>Drupal ist nach einem Modulsystem aufgebaut, der Core ist dabei sehr schlicht gehalten. So kann das CMS per Baukastensystem genau auf Ihre Bedürfnisse zugeschnitten werden. Mit mehr als 20.000 Zusatzmodulen lässt sich für jeden Anwender die passende Website zusammenstellen: Kalenderfunktion, Datenbanken für Bilder und Dateien, Blogfunktionen – mit dem open source Content Management System lässt sich (fast) jeder Wunsch umsetzen. Dank “Drupal Commerce” lässt sich das Content Management System bei Bedarf sogar als Basis für E-Commerce-Projekte einsetzen. Die weltweite Drupal Community sorgt dafür, dass das CMS permanent nach neuesten Standards weiterentwickelt wird.</p>}
            buttonUrl="/projekte/75-myle-de.html"
            buttonCaption="Fall zeigen"
            img={<img 
              src={imgArticle}
              alt="myle.de-Website auf einem Laptop"
              width={1920} 
              height={800}
              loading="lazy"
            />}
            color="var(--color-сurious-blue)"
          />
          
        </section>

        <section>
          <h2 className="visuallyHidden">Mobile Nutzung</h2>
          <ParallaxEffect
            image={mobileParallaxImg}
            alt="Mann mit Handy"
            children={
              <div className="container">
                <b className="heading-2 parallax-text">Die mobile Optimierung ist heute Ausgangsbasis jeder erfolgreichen Website</b>
              </div>
            }
          />
          <SectionColor 
            content={
              <Fragment>
                <p>Drupal basiert auf der zukunftsfähigen PHP-Programmiersprache, unterstützt HTML5 / CSS3 und Bootstrap. Responsives Webdesign lässt sich daher ohne Probleme umsetzen, sodass Inhalte auch über mobile Endgeräte optimal abgerufen werden können.</p>
                <p>Selbst der Administrationsbereich kann im responsive Design gestaltet werden. Das ist besonders für Website-Betreiber*innen interessant, die ihre Seite auch von unterwegs pflegen wollen.</p>
              </Fragment>
            }
            backgroundColor="var(--color-сurious-blue)"
          />
        </section>

        <section className="section">
          <div className="container-narrow">
            <h2 className="section-text-400">Community-Building</h2>
            <p>Informationen fließen im Web schon längst nicht mehr nur in eine Richtung. Interaktionen zwischen Nutzer*innen und Website-Betreiber*innen nehmen eine immer größere Rolle ein. Drupal trägt dieser Entwicklung Rechnung und bietet sich besonders für Seiten an, die Wert auf Austausch, Interaktion und Kommunikation legen. Nicht ohne Grund setzen immer mehr NGOs und Nachrichtenportale auf Drupal: Die Module bieten eine ganze Reihe von Möglichkeiten, Foren zu integrieren, Informationen auszutauschen, Weblogs anzulegen und Nachrichten zu teilen. Verschiedene Rollen- und Rechtesysteme können an die Nutzer*innen vergeben werden. So behalten Sie den Überblick und können Ihre Seite leichter verwalten.</p>
          </div>
        </section>

        <ColorSteps 
          hiddenTitle="Drupal-Verwaltungsschnittstelle"
					parallax={
						<ParallaxEffect
							image={backendParallaxImg}
							alt="Admin-Anwendung in CMS Drupal"
							children={
								<div className="container">
									<b className="heading-2 parallax-text">Intuitive Pflege</b>
								</div>
							}
						/>
					}
          bgColor={"var(--color-сurious-blue)"}
					textTop={"Bei Webseiten unterscheidet man zwischen Backend, also der Verwaltungsoberfläche, die nur für den Administrator sichtbar ist, und dem Frontend, das über den Browser für alle einsehbar ist. Die meisten Websites lassen sich über das Backend bearbeiten. Hier unterscheidet sich Drupal von anderen CMS: Inhalte und Bilder der Webseite werden hier nach der Anmeldung direkt im Frontend angepasst."}
					textCenter={"Der Vorteil ist, dass Änderungen unmittelbar im tatsächlichen Design gesehen werden. Gerade unerfahrene Administratoren können direkt mit dem Bearbeiten der Website beginnen und müssen sich nicht mit einem komplexen Menübaum im Backend befassen. Das bedeutet ein Plus an Benutzerfreundlichkeit."}
          textBottom={
            <Fragment>
              <h3 className="section-text-400">Unsere Expertise als Drupal-Agentur im Großraum Stuttgart und Berlin</h3>
              <p className="section-text-200">Als Internetagentur aus Stuttgart mit mehr als zehn Jahren Erfahrung beraten wir SIe gerne zum richtigen Content Management System für Ihre Anforderungen. Unsere Entwickler*innen haben bereits zahlreiche komplexe und umfangreiche Webprojekte umgesetzt, z.B. das Portal der Stadt Leinfelden-Echterdingen myle.de. Wir bieten Ihnen:</p>
            </Fragment>}
				/>

        <section className="container section-m" styleName="services">
          <h2 className="visuallyHidden">Unsere Dienstleistungen</h2>
          <Hr color="var(--color-сurious-blue)" />
          <ul>
            <li>
              <h3 className="heading-4">Persönliche Beratung</h3>
              <Icon
                name="message"
                color="var(--color-сurious-blue)"
                figure="circle"
              />
            </li>
            <li>
              <h3 className="heading-4">Planung und Konzeption</h3>
              <Icon
                name="lamp"
                color="var(--color-сurious-blue)"
                figure="square"
              />
            </li>
            <li>
              <h3 className="heading-4">Erfahrene Webdesigner*innen</h3>
              <Icon
                name="cursor-in-square"
                color="var(--color-сurious-blue)"
                figure="triangle-bottom"
              />
            </li>
            <li>
              <h3 className="heading-4">Hochqualifizierte Drupal-Entwickler*innen</h3>
              <Icon
                name="tag-in-window"
                color="var(--color-сurious-blue)"
                figure="square"
              />
            </li>
            <li>
              <h3 className="heading-4">Programmierung von individuellen Erweiterungen und Modulen</h3>
              <Icon
                name="plus-in-window"
                color="var(--color-сurious-blue)"
                figure="triangle-top"
              />
            </li>
            <li>
              <h3 className="heading-4">Drupal-Updates und Modul-Aktualisierung</h3>
              <Icon
                name="droplet"
                color="var(--color-сurious-blue)"
                figure="circle"
              />
            </li>
            <li>
              <h3 className="heading-4">Migration (sensibler) Daten in Drupal</h3>
              <Icon
                name="server"
                color="var(--color-сurious-blue)"
                figure="square"
              />
            </li>
            <li>
              <h3 className="heading-4">SEO und Content Marketing</h3>
              <Icon
                name="search-text"
                color="var(--color-сurious-blue)"
                figure="circle"
              />
            </li>
          </ul>
        </section>

        <section>
          <h2 className="visuallyHidden">Kontaktiere uns</h2>
          <ParallaxEffect
            image={contactParallaxImg}
            alt="Mann und Frau unterhalten sich"
            children={
              <div className="container">
                <b className="heading-2 parallax-text">Wir freuen uns, im Gespräch die passende Lösung für Sie zu finden.</b>
              </div>
            }
          />
          <SectionColor 
            content={
              <p>Haben Sie noch Fragen? Dann kontaktieren Sie uns. Gerne erstellen wir Ihnen als erfahrene Drupal-Agentur ein unverbindliches Angebot.</p>
            }
            backgroundColor="var(--color-сurious-blue)"
          />
        </section>

      </main>

      <FeedbackHomePage />
      <Footer />
    </div>
  );
}
