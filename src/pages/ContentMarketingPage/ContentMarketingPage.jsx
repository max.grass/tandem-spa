import React, { Fragment } from "react";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { ParallaxEffect } from "components/ParallaxEffect";
import { IconList } from "components/IconList";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from "assets/images/hero-contentmarketing.jpg";

import watchParallax from "assets/images/content-marketing-parallax-watch.jpg";
import peopleParallax from "assets/images/content-marketing-parallax-people.jpg";
import womanParallax from "assets/images/content-marketing-parallax-woman.jpg";

import icon1 from "assets/images/content-marketing-list-1.svg";
import icon2 from "assets/images/content-marketing-list-2.svg";
import icon3 from "assets/images/content-marketing-list-3.svg";


import "./ContentMarketingPage.css";


export const ContentMarketingPage = () => {

	return (
		<div className='page-wrapper'>
			<CommonMetaTags 
				title={'PR - Öffentlichkeitsarbeit – wenn PR, dann mit uns'}
				description={'Mit uns als PR-Agentur aus Stuttgart finden Sie einen zuverlässigen Partner, der Sie in allen Fragen der Unternehmenskommunikation berät!'}
				url={'https://www.tandem-m.de/pr.html'}
				image={heroImg}
			/>

			<Header />
			
			<main>
				<Hero
					src={heroImg}
					alt="Content Marketing Page"
					width={1920}
					height={800}
					content={
						<HeroContent 
							title={<span>Tandem Marketing & Partners – wenn PR, dann mit uns</span>}
							text="Content-Marketing"
						/>
					}
				/>

				<section>
					<h2 className="visuallyHidden">Content-Marketing</h2>
					<SectionWhite 
						title="Tandem Marketing & Partners – wenn PR, dann mit uns"
						text="Mit uns als PR-Agentur aus Stuttgart finden Sie einen zuverlässigen Partner, der Sie in allen Fragen der Unternehmenskommunikation berät, Konzepte erstellt und die PR-Arbeit in Ihrem Namen übernimmt. Denn gute, ehrliche Botschaften und eine aussagekräftige Kommunikation sind wesentliche Erfolgsfaktoren für Unternehmen. Sie stärken das Image, erwecken Aufmerksamkeit, bieten den Dialog mit bestehenden und neuen Kunden oder können zukünftige Mitarbeiter gewinnen."
						textColor="var(--color-scarlet-gum)"
					/>
					<div>
						<SectionColor 
							content={
								<Fragment>
									<p className="section-text-200">Wir beraten und unterstützen Sie bei der Konzeption und Umsetzung von Kommunikationsstrategien und besitzen langjährige Erfahrung in diesem Bereich. Wir sind dialogorientiert und verstehen uns als langfristige Partner unserer Kunden.</p>
								</Fragment>
							}
							textColor="var(--color-white)"
							backgroundColor="var(--color-scarlet-gum)"
						/>
					</div>
				</section>
				<section styleName="strengths">
					<ParallaxEffect
						image={watchParallax}
						alt="Tandem Marketing & Partners"
					/>
				</section>

				<section>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Medien-PR trägt wesentlich zu einer erfolgreichen Unternehmensidentität bei und kann das Image Ihres Unternehmens positiv beeinflussen. Für die zielgruppenspezifische Planung und Umsetzung nehmen wir uns die Zeit, Ihr Unternehmen genau kennenzulernen. Wir bauen Ihre Corporate Identity auf beziehungsweise entwickeln sie weiter, begleiten Sie bei Veränderungsprozessen und arbeiten an Ihrer Unverwechselbarkeit im laufenden Wettbewerb. Das Image und die Vorstellungsbilder, die Ihre Zielgruppe von Ihrem Unternehmen oder den Produkten hat, entscheiden über den Erfolg. Ein positives Image kann die Anzahl der Besucher und Kaufentscheidungen beeinflussen, den Wert einer Marke bestimmen und wichtiger Faktor für die Preisgestaltung sein.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
				</section>

				<section>
					<ParallaxEffect
						image={peopleParallax}
						alt="Tandem Marketing & Partners"
						children={
							<div className="section container">
								<b className="heading-3 parallax-text">
									Das Image ist das Ergebnis vielfältiger Informationen und Eindrücke, die aus der Wahrnehmung von Design, Kommunikation und Verhalten eines Unternehmens entstehen.<br/>
									Essentiell für PR ist daher, die Vorstellungsbilder und Erwartungen der Bezugsgruppen zu kennen und die PR-Strategie gezielt danach zu entwickeln. PR kann das Image beeinflussen und die Vorstellungen Ihrer Kunden prägen oder im positiven Sinne ändern.
								</b>
							</div>
						}
					/>
				</section>

				<section>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Wichtig ist, dass die Art der Kommunikation zu Ihrem Unternehmen und Ihrer Marke passt und die Zielgruppen anspricht. Als PR-Experten führen wir Sie über die Pfade der Presse- und Öffentlichkeitsarbeit bis hin zu neuen Wegen der Kommunikation. Wir finden speziell für Sie den richtigen Ton, kommunizieren in Wort und Bild, schärfen Ihr Profil und stärken Ihren Auftritt in der Öffentlichkeit. Ob Artikel für die Presse, Website und Blog, Onlinekommunikation oder Social Media: Ein guter Auftritt braucht überzeugende Geschichten, die jedes Unternehmen zu erzählen hat – man muss sie nur finden. Tandem Marketing & Partners entwickelt starke Texte, aussagekräftige Bilder oder auch kurze Filme, die heute im Internet immer mehr an Bedeutung erlangen – wir kümmern uns um die komplette Gestaltung Ihrer Online-PR.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
				</section>

				<section styleName="iconsList">
					<SectionColor
						content={
							<Fragment>
								<p className="heading-3">Wenn Sie Ihre Kommunikation von Profis überprüfen lassen möchten, bieten wir Ihnen eine Kurzberatung an:</p>
								<IconList
									listData={[
										{
											"iconSrc" : icon1,
											"label" : "Ist-Zustand:",
											"sublabel": "Wo stehen Sie?"
										},
										{
											"iconSrc" : icon2,
											"label" : "Relevante Kanäle:",
											"sublabel": "Wo erreichen Sie Ihre Zielgruppen?"
										},
										{
											"iconSrc" : icon3,
											"label" : "Soll-Zustand:",
											"sublabel": "Welche Potenziale bleiben aktuell ungenutzt?"	
										}
									]}
								/>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-scarlet-gum)"
					/>
				</section>

				<section>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Wir analysieren Ihren Gesamtauftritt (u.a. Website, Design, Texte, Zielgruppen, Konkurrenten), weisen auf Stärken und Schwächen hin, führen Punkte zur Verbesserung auf, geben Hinweise auf mögliche Kooperationspartner oder finden neue Zielgruppen und Wege zur Kundengewinnung. Kurz, wir liefern Ihnen Ergebnisse, die Ausgangspunkt für eine weitere Zusammenarbeit sein können. Profitieren Sie von unserem Know-how: Wir beraten Sie gerne zu allen PR-relevanten Maßnahmen für Ihr Unternehmen.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
				</section>

				<section>
					<div className="empty-parallax">
						<ParallaxEffect
							image={womanParallax}
							alt="Tandem Marketing & Partners"
						/>
					</div>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Als eine PR- und Marketing-Agentur aus Stuttgart mit mehr als 10 Jahren Erfahrung, wir beraten Sie gerne zu unseren Leistungen. Kontaktieren Sie unseren Marketing-Manager, um mehr zu erfahren.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-scarlet-gum)"
					/>
				</section>
				

				<FeedbackHomePage />
			</main>
			
			<Footer />
		</div>
	);
}
