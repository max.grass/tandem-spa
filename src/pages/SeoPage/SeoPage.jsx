import React, { Fragment } from "react";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { ParallaxEffect } from "components/ParallaxEffect";
import { ColorSteps } from "components/ColorSteps";
import { Article } from "components/Article";
import { ListOrderUnorder } from "components/ListOrderUnorder";
import { Icon } from "components/Icon";
import { Hr } from "components/Hr";
import { CheckedList } from "components/CheckedList";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from "assets/images/hero-suchmaschinenoptimierung.jpg";
import parallaxImageCompany from "assets/images/seo-parallax-company.jpg";
import seoArticleImg from "assets/images/marketing-parallax-graph.jpg";
import seoCartImg from "assets/images/seo-parallax-cart.jpg";
import seoPeopleImg from "assets/images/seo-parallax-people.jpg";
import seoBookImg from "assets/images/seo-parallax-book.jpg";
import seoManImg from "assets/images/seo-parallax-man.jpg";

import "./SeoPage.css";

export const SeoPage = () => {

	return (
		<div className='page-wrapper'>
			<CommonMetaTags 
				title={'Suchmaschinenoptimierung'}
				description={'Tandem Marketing'}
				url={'https://www.tandem-m.de/marketing-stuttgart/suchmaschinenoptimierung.html'}
				image={heroImg}
			/>

			<Header />
			
			<main>
				<Hero
					src={heroImg}
					alt="Suchmaschinenoptimierung"
					width={1920}
					height={800}
					styleName="seo-hero"
					content={
						<HeroContent 
							title={<span styleName="seo-hero-header">Wir zeigen Ihnen, wie Sie gefunden werden</span>}
							text="Suchmaschinenoptimierung"
						/>
					}
				/>

				<section>
					<h2 className="visuallyHidden">Suchmaschinenoptimierung</h2>
					<SectionWhite 
						title="Wir machen Sie sichtbar"
						text="Der Bereich Suchmaschinenoptimierung hat sich in den vergangenen Jahren stark gewandelt. Die Suchmaschinen – und Nutzer – erwarten heute vor allem eins: Gute Inhalte. Als Internetagentur aus Stuttgart helfen wir Ihnen durch gezielte SEO-Maßnahmen, Ihre Positionen im World Wide Web auszubauen und zu verbessern."
						textColor="var(--color-gigas)"
					/>
					<div styleName="section-color">
						<SectionColor 
							content={
								<Fragment>
									<p className="section-text-700">Was ist Suchmaschinenoptimierung?</p>
									<p styleName="text">Suchmaschinenoptimierung (Search Engine Optimization, SEO) bezeichnet Maßnahmen, die darauf abzielen, die Sichtbarkeit in Suchmaschinen wie Google zu verbessern.</p>
								</Fragment>
							}
							textColor="var(--color-white)"
							backgroundColor="var(--color-gigas)"
						/>
					</div>
				</section>

				<ColorSteps 
					hiddenTitle="Suchmaschinenoptimierung"
					parallax={
						<ParallaxEffect
							image={parallaxImageCompany}
							alt="Tandem Marketing & Partners"
							children={
								<div className="section container">
									<b className="heading-2 parallax-text" >
										Warum ist eine Suchmaschinenoptimierung (SEO) so wichtig?
									</b>
								</div>
							}
						/>
					}
					textTop={"Mit der richtigen SEO-Strategie können Sie eine bessere Platzierung Ihrer Webpage erzielen und Produktportfolios oder Ihre Dienstleistungen in den Mittelpunkt rücken. Mehr als 97 Prozent aller User recherchieren heutzutage im Internet und nutzen dabei Suchmaschinen wie Google, Bing oder Yahoo."}
					textCenter={"Jeden Monat werden allein in Deutschland mehr als 5,5 Milliarden Suchanfragen getätigt."}
					textBottom={
						<Fragment>
							<p className="section-text-200">In über 90 Prozent aller Fälle konzentrieren sich potenzielle Kunden auf die erste Ergebnisseiten bei Google & Co. Je relevanter eine Homepage für die Nutzer ist, desto höher ist auch ihr Ranking. Eine gut positionierte Website schafft mehr Aufmerksamkeit, wird häufiger besucht und steigert die Popularität eines Unternehmens. Langfristig betrachtet, sind SEO-Maßnahmen günstiger als bezahlte Werbung und somit ein wichtiger Bestandteil des Online Marketing Mix.</p>
						</Fragment>
					}
					bgColor={"var(--color-gigas)"}
				/>

				<Article 
					buttonUrl="/referenzen.html"
					buttonCaption="Fälle zeigen"
					img={<img 
						src={seoArticleImg}
						alt="suchmaschinenoptimierung"
						width={1920} 
						height={800}
						loading="lazy"
					/>}
					color="var(--color-gigas)"
				/>

				<section>
					<div styleName="section-color">
						<SectionColor 
							content={
								<Fragment>
									<p className="section-text-700">Grundlegende SEO-Maßnahmen</p>
									<p className="section-text-200">Es gibt einige grundlegende SEO-Kriterien, die jede Website erfüllen sollte. Sie leiten sich anhand der sogenannten Google Best Practises ab. Dazu gehören zum Beispiel qualitativ hochwertige Inhalte, Meta Tags, ein transparenter Unternehmensstandort bei Google, die einfache und schnelle Abrufbarkeit einer Website auf allen Endgeräten sowie die Sicherheit einer Webpräsenz. Darüber hinaus beeinflussen einige technische Faktoren die Sichtbarkeit einer Website bei Google wie zum Beispiel eine Sitemap oder eine robots.txt-Datei, die der Suchmaschine bei der Indexierung einer Website helfen.</p>
								</Fragment>
							}
							textColor="var(--color-dark)"
							backgroundColor="var(--color-white)"
						/>
					</div>
				</section>

				<section>
					<ParallaxEffect
						image={seoPeopleImg}
						alt="Tandem Marketing & Partners"
						children={
							<div className="container">
								<b className="heading-2 parallax-text" >
									Was ist Indexierung?
								</b>
							</div>
						}
					/>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Die Indexierung bezeichnet die Aufnahme einer Website in den sogenannten “Google Index”. Der “Google Index” ist das Verzeichnis aller erfassten, d.h. gecrawlten, und gespeicherten, d.h. indexierten, Seiten in Google. Bei schweren Verstößen gegen die Google-Richtlinien können Websites aus dem Index entfernt werden, d.h. sie tauchen nicht mehr in den Suchergebnissen auf.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-gigas)"
					/>
					<ParallaxEffect
						image={seoCartImg}
						alt="Tandem Marketing & Partners"
						children={
							<div className="container">
								<b className="section-text-700 parallax-text" >
								Als Internetagentur aus Stuttgart mit über zehn Jahren Erfahrung berücksichtigen wir bereits bei der Konzeption und Programmierung einer Website die Best Practices von Google in puncto Suchmaschinenoptimierung: Sprechen Sie uns an!
								</b>
							</div>
						}
					/>
				</section>

				<section className="section">
					<div className="container-narrow">
						<h2 className="heading-3" styleName="header">Drei Disziplinen der Suchmaschinenoptimierung:</h2>
					</div>
					<div className="container">
						<CheckedList 
							content={
								<Fragment>
									<li>Zur Onpage-Optimierung zählen alle Maßnahmen, die der Suchmaschine – und Nutzern – helfen, die Inhalte einer Website optimal zu verstehen, also gut strukturierter Content, interne Verlinkungen, optimal aufbereitete Bilder, Meta Description etc.</li>
									<li>Zur Offpage-Optimierung zählen SEO-Maßnahmen, die außerhalb der eigenen Website stattfinden, z.B. Social Media, Bewertungen, Content Marketing, Aufbau von Backlinks etc.</li>
									<li>Technische SEO wird häufig zur Onpage-Optimierung gezählt, da es sich um Maßnahmen innerhalb der eigenen Website handelt. Beispiele für technische SEO sind Seitengeschwindigkeit, strukturierte Daten oder das Setzen von bestimmten Tags. In technischer Hinsicht gibt die Google Search Console Aufschluss über Verbesserungspotenziale.</li>
								</Fragment>
							}
							color="var(--color-gigas)"
						/>
					</div>
				</section>

				<section>
					<div className="empty-parallax">
						<ParallaxEffect
							image={seoBookImg}
							alt="Tandem Marketing & Partners"
						/>
					</div>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Moderne Content Management Systeme (CMS) wie Magento, Drupal oder Wordpress verfügen über zusätzliche Plug-Ins, die die Suchmaschinenoptimierung durch ergänzende Einstellungen unterstützen.</p>
								<p className="section-text-200">Neben den SEO-Basics sind strategische SEO-Maßnahmen wichtig, die individuell auf die Bedürfnisse Ihrer Zielgruppe abgestimmt sind: 70% bis 80% der Internetnutzer klicken auf die organischen Suchergebnisse in den Suchmaschinen, während sie bezahlte Anzeigen (Google Ads) ignorieren. Bieten Sie diesen Website-Besuchern eine optimale User Experience, indem Sie ihnen einzigartige und informative Inhalte liefern.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-gigas)"
					/>
				</section>

				<section>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Das Ranking einer Seite, d.h. die Platzierung der Website in der Suchergebnisliste, ist der wichtigste Erfolgsfaktor aller SEO-Maßnahmen. Wichtige Kennzahlen sind die Besucherzahlen, Backlinks oder Shares in den Sozialen Medien. Über Analysetools wie Google Analytics oder Semrush lassen sich die relevanten Kennzahlen laufend kontrollieren.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Bei der SEO-Optimierung sind gewisse Regeln zu beachten: Das, was gestern zu guten Platzierungen verholfen hat, kann morgen als Spam bewertet werden. Suchmaschinen werden ständig weiterentwickelt, was einen Strategiewechsel bei bereits optimierten Websites erfordern kann. Als SEO-Agentur aus Stuttgart behalten wir die wichtigsten Entwicklungen im Blick und passen Strategien laufend an veränderte Voraussetzungen an.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-gigas)"
					/>
					<div className="empty-parallax">
						<ParallaxEffect
							image={seoManImg}
							alt="Tandem Marketing & Partners"
						/>
					</div>
				</section>

				<section className="section">
					<div className="container-narrow">
						<h2 className="heading-3">Zu den SEO-Trends gehören aktuell:</h2>
					</div>
					<div className="container-narrow grid-hr" styleName="advantages">
						<Hr color="var(--color-gigas)" />
						<ListOrderUnorder
							isOrdered={false}
							icon={
								<Icon
									name="checked"
									width="50px"
									height="50px"
									color="var(--color-dark)"
								/>
							}
							items={[
								<Fragment>
									<h3 className="heading-4">Google Core Web Vitals</h3>
									<p>Ab Mai 2021 wird die User Experience zu einem offiziellen Rankingfaktor. Für Websitebetreiber ist jetzt umso wichtiger, relevante und gut strukturierte Inhalte zu bieten, die ihre Zielgruppe einbeziehen. Wichtige Kennzahlen im Bereich User Experience sind z.B. Klickrate und Verweildauer.</p>
								</Fragment>,
								<Fragment>
									<h3 className="heading-4">Google E-A-T-Prinzipien</h3>
									<p>Websites, die qualitativ hochwertige Inhalte bieten, werden mit guten Rankings belohnt. Die Qualität von Inhalten bewertet Google anhand des E-A-T-Scores: Expertise, Authority, Trust (Expertise, Autorität, Vertrauen). Besonders wichtig ist dieser Score bei Unternehmen aus dem sogenannten YMYL-Kontext (“Your Money or Your Life” zu deutsch: Dein Geld oder Dein Leben), deren Inhalte negative Auswirkungen auf das Leben ihrer Kunden haben könnten. Gut belegte, zielgruppenorientierte Inhalte punkten an dieser Stelle.</p>
								</Fragment>,
								<Fragment>
									<h3 className="heading-4">Mobile First</h3>
									<p>2019 wurde die Mobile-First-Indexierung ausgerollt, d.h. Google betrachtet die mobile Version einer Website bevorzugt. Wie wichtig mobile SEO ist, zeigen auch aktuelle Zahlen: 92 Prozent der weltweiten Internetnutzer verwenden mobile Endgeräte und mehr als die Hälfte aller Websiteaufrufe stammen mittlerweile von Smartphones.</p>
								</Fragment>,
								<Fragment>
									<h3 className="heading-4">Ausführliche Inhalte</h3>
									<p>Eine aktuelle Content Marketing Studie hat ergeben, dass Artikel mit mehr als 3.000 Wörtern drei mal häufiger gelesen und vier mal häufiger geteilt werden – natürlich nur, sofern sie qualitativ hochwertig und anhand relevanter Quellen belegt sind.</p>
								</Fragment>,
								<Fragment>
									<h3 className="heading-4">Featured Snippets</h3>
									<p>Ein Featured Snippet – zu deutsch: hervorgehobener Ausschnitt – liefert dem Nutzer eine kurze und knappe Antwort auf seine Suchanfrage innerhalb der Suchergebnisse. Es gibt Text-Snippets, Video-Snippets, oder Listen- und Tabellen-Snippets. Gezeigt werden Featured Snippets vorwiegend, wenn sich die Suchintention des Nutzers in ein bis zwei Sätzen bzw. einer kurzen Liste auf dem Display eines Smartphones befriedigen lässt. Sogenannte Rich Snippets können zusätzliche Elemente wie Bewertungssterne, Preisangaben oder Bilder enthalten. Websites mit Snippets werden laut Semrush über 500 Mal häufiger besucht, als Websites ohne dergleichen.</p>
								</Fragment>,
								<Fragment>
									<h3 className="heading-4">Local SEO</h3>
									<p>Die meisten Menschen verwenden Suchmaschinen, um Angebote in ihrer Region ausfindig zu machen, z.B. ein gutes Restaurant in der Nähe. Umso wichtiger ist es, in der lokalen Suche präsent zu sein.</p>
								</Fragment>
							]}
						/>
					</div>
				</section>

				<section styleName="target">
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Das wichtigste Schlagwort für SEO heute ist: User Experience. Google setzt seine Ressourcen immer gezielter ein, um die Suchergebnisse optimal auf die Intention der Nutzer abzustimmen. Dabei kommen u.a. der Künstliche-Intelligenz-Algorithmus RankBrain und BERT (Bidirectional Encoder Representations from Transformers) zum Einsatz, um Suchanfragen besser zu verstehen und noch passendere Ergebnisse zu liefern. Semantisch verwandte Keywords und für Voice Search optimierte Inhalte rücken damit in den Fokus der SEOs.</p>
								<p className="section-text-200">Sie möchten mit Ihrer Website oder Ihrem Onlineshop sichtbarer werden? Dann sprechen Sie uns an. Gemeinsam entwickeln wir eine SEO-Strategie, die perfekt auf Ihr Unternehmen abgestimmt ist.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-gigas)"
					/>
				</section>

				<FeedbackHomePage />
			</main>
			
			<Footer />
		</div>
	);
}
