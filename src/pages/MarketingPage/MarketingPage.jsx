import React, { Fragment } from "react";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { ParallaxEffect } from "components/ParallaxEffect";
import { ColorSteps } from "components/ColorSteps";
import { MarketingScheme } from "components/MarketingScheme";
import { ListOrderUnorder } from "components/ListOrderUnorder";
import { Icon } from "components/Icon";
import { Hr } from "components/Hr";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from "assets/images/hero-marketing.jpg";
import parallaxImageWomen from "assets/images/marketing-parallax-women.jpg";
import parallaxImageCompany from "assets/images/marketing-parallax-company.jpg";
import parallaxImageMen from "assets/images/marketing-parallax-men.jpg";
import parallaxImageGraph from "assets/images/marketing-parallax-graph.jpg";

import "./MarketingPage.css";

export const MarketingPage = () => {

	return (
		<div className='page-wrapper'>
			<CommonMetaTags 
				title={'Marketing aus Stuttgart –  mit uns erreichen Sie die Zielgruppe'}
				description={'Erfolgreiches Marketing aus Stuttgart für Ihr Unternehmen.Full service Agentur für digitale Medien.'}
				url={'https://www.tandem-m.de/marketing-stuttgart.html'}
				image={heroImg}
			/>

			<Header />
			
			<main>
				<Hero
					src={heroImg}
					alt="Bürostuhlrennen"
					width={1920}
					height={800}
					content={
						<div styleName="hero-content">
							<HeroContent 
								title={<span>Marketing-Agentur aus Stuttgart</span>}
								text="Marketing"
							/>
						</div>
		  		}
				/>

				<section>
					<h2 className="visuallyHidden">Marketing</h2>
					<SectionWhite 
						title="Unternehmen stehen heute oft vor ganz unterschiedlichen Herausforderungen:"
						text="Der Markt verändert sich und der globale Wettbewerb führt zu immer höherem Preisdruck."
					/>
					<div styleName="section-color">
						<SectionColor 
							content={
								<Fragment>
									<p styleName="text">Produkte und Dienstleistungen werden immer austauschbarer. Die Nachfrage nimmt ab. Um Erfolg und Wachstum weiterhin zu sichern, etablieren wir Sie und Ihre Produkte kommunikativ in neuen Märkten – damit Sie Ihre Zielgruppen überzeugend ansprechen können.</p>
								</Fragment>
							}
							textColor="var(--color-white)"
							backgroundColor="#770c36"
						/>
					</div>
				</section>
				
				<section styleName="strengths">
					<h2 className="visuallyHidden">Was sind unsere Stärken</h2>
					<ParallaxEffect
						image={parallaxImageWomen}
						alt="Tandem Marketing & Partners"
					/>
				</section>

				<MarketingScheme />

				<div>
					<ColorSteps 
						hiddenTitle="Unsere Marketingleistungen"
						parallax={
							<ParallaxEffect
								image={parallaxImageCompany}
								alt="Tandem Marketing & Partners"
								children={
									<div className="section container">
										<b className="heading-2 parallax-text">
											Wir untersuchen gemeinsam mit Ihnen den Markt, Ihre Wettbewerber, Ihre Zielgruppen und Ihre Marketingziele.
										</b>
									</div>
								}
							/>
						}
						textTop={"Wir entwickeln eine Markenstrategie, die Ihr Produkt klar vom Wettbewerber abhebt, Ihren Kunden Sicherheit vermittelt und Qualität impliziert - mit Ideen, die überraschen und Ihrer Marke ein unverwechselbares Profil verleihen. Mit klaren Botschaften, die sofort verstanden werden und so die besten Voraussetzungen haben, sich im Kopf der Zielgruppe zu verankern."}
						textCenter={"Dabei lernen wir Ihr Unternehmen samt Philosophie kennen und entwickeln Konzepte, die auf Ihrer Unternehmensstrategie aufbauen."}
						textBottom={<Fragment>
							<p className="section-text-200">Wir beraten Sie auch beim Aufbau von Geschäftsbeziehungen via Social Media. Im Social Web sprechen Nutzer, Kunden und Konsumenten über Marken, Produkte, Dienstleistungen und Neuigkeiten – sie kommentieren, diskutieren, bewerten und empfehlen. Nutzer werden zu Markenfans und fungieren als Multiplikatoren, die bei Facebook, Twitter, YouTube &amp; Co agieren.</p>
							<p className="section-text-200">Social Media-Kanäle sind in der modernen Kommunikation deshalb nicht mehr wegzudenken und bieten für Marken, Unternehmen und Institutionen vollkommen neue Möglichkeiten – insbesondere für Marketing und Kundenbindung.</p>
							<p className="section-text-200">Unser Leistungsspektrum umfasst alle Facetten des Social Media Marketings – von der Strategieentwicklung bis hin zur Durchführung integrierter Social Media Kampagnen. Dazu gehören die Analyse der Zielgruppe, Entwicklung und Umsetzung von Kampagnen und Applikationen, markenbezogene Gewinnspiele oder Contests bei Facebook & Co.</p>
						</Fragment>}
					/>
				</div>

				<section styleName="services">
					<h2 className="visuallyHidden">Was sind unsere Stärken</h2>
					<ParallaxEffect
						image={parallaxImageMen}
						alt=""
						children={
							<div className="section container-narrow" styleName="parallax-container">
								<b className="heading-3" styleName="parallax-text">
									Alle Maßnahmen werden von unseren Experten aus den Bereichen Kreation, Content Marketing, Webentwicklung und Online Marketing betreut. Ihre Marke ist bei uns in guten Händen.
								</b>
							</div>
						}
					/>
					<div styleName="section-color">
						<SectionColor 
							content={
								<Fragment>
									<p styleName="text">Mit uns kommen Sie Ihrer Zielgruppe ein Stück näher: Per E-Mail. Beim modernen E-Mail-Marketing geht es nicht um Massenmailings, sondern um gezielte und personalisierte Kommunikation. Wir erstellen facettenreiche E-Mail-Marketingkampagnen, die perfekt auf Ihre Abonnenten zugeschnitten sind.</p>
									<p styleName="text">Das Hauptziel einer solchen Kommunikation ist, Ihren Kunden relevante und interessante Inhalte zu bieten. So bauen Sie langfristige Beziehungen auf, die auf Vertrauen und Respekt basieren.</p>
								</Fragment>
							}
							textColor="var(--color-dark)"
							backgroundColor="var(--color-white)"
						/>
					</div>
				</section>

				<section styleName="strengths">
					<h2 className="visuallyHidden">Was sind unsere Stärken</h2>
					<ParallaxEffect
						image={parallaxImageGraph}
						alt="Tandem Marketing & Partners"
					/>
				</section>

				<section>
					<div styleName="section-color">
						<SectionColor 
							content={
								<Fragment>
									<p styleName="text">Beim Marketing geht es nicht nur um Werbung. Es geht darum, die Bedürfnisse Ihrer Zielgruppe zu erkennen und zu befriedigen. Mit diesem Ziel vor Augen, entwickeln wir eine umfassende Content-Marketing-Strategie, die Ihrem Publikum einen echten Mehrwert bietet.</p>
									<p styleName="text">Durch originelle, relevante und nützliche Inhalte, stärken Sie nicht nur die Kundenbindung, sondern erreichen auch potenzielle Neukunden. Darüber hinaus steigt Ihre Sichtbarkeit im Netz, wenn Contents gut strukturiert und für Suchmaschinen optimiert sind.</p>
								</Fragment>
							}
							textColor="var(--color-dark)"
							backgroundColor="var(--color-white)"
						/>
					</div>
				</section>

				<section>
					<div styleName="section-color">
						<SectionColor 
							content={
								<Fragment>
									<p styleName="text">Eine erfolgreiche Marketingstrategie basiert immer auf Daten. Web-Analytics sind eine Möglichkeit, Ihre Kunden zu verstehen, die Konkurrenz zu überwachen und den Erfolg Ihrer Marketingaktionen zu bewerten.</p>
									<p styleName="text">Wir verwenden Webanalysedienste wie Google Analytics und Google Search Console sowie Matomo Analytics und SEMrush. Auf diese Weise behalten wir die wichtigsten Kennzahlen (KPI - Key Performance Indicators) im Blick und passen Strategien bei Bedarf an.</p>
								</Fragment>
							}
							textColor="var(--color-white)"
							backgroundColor="var(--color-cherry)"
						/>
					</div>
				</section>

				<section styleName="marketingList">
					<div className="container-narrow">
						<h2 className="heading-3" styleName="header">Online Marketing aus Stuttgart: Ihre Vorteile</h2>
					</div>
					<div className="container-narrow grid-hr" styleName="advantages">
						<Hr color="var(--color-cherry)" />
						<ListOrderUnorder
							isOrdered={false}
							icon={
								<Icon
									name="checked"
									width="50px"
									height="50px"
									color="var(--color-dark)"
								/>
							}
							items={[
								<Fragment>
									<h3 className="heading-4">Hohe Reichweite</h3>
									<p>Durch Online Marketing, beispielsweise Suchmaschinenwerbung, erreichen Sie mehr potenzielle Kunden als durch konventionelle Werbemittel.</p>
								</Fragment>,
								<Fragment>
									<h3 className="heading-4">Größte Effizienz</h3>
									<p>In Onlinemedien wie Google Ads, Facebook oder Xing, lassen sich Zielgruppen sehr genau definieren. So werden Streuverluste vermieden.</p>
								</Fragment>,
								<Fragment>
									<h3 className="heading-4">Optimale Erfolgskontrolle</h3>
									<p>Kosten pro Klick, Anzahl der Klicks, Impressionen, Rankings … Online-Maßnahmen lassen sich durch gängige Kennzahlen optimal kontrollieren.</p>
								</Fragment>,
								<Fragment>
									<h3 className="heading-4">Positives Image</h3>
									<p>Eine zeitgemäße Marketingstrategie zeugt von Innovationskraft und Dynamik – und führt so langfristig zu dauerhaftem Erfolg Ihres Unternehmens.</p>
								</Fragment>,
								<Fragment>
									<h3 className="heading-4">Weltweite Kundenakquise</h3>
									<p>Im Internet können Sie potenzielle Kunden international ansprechen, und zwar unabhängig von Messen und Branchenevents.</p>
								</Fragment>,
								<Fragment>
									<h3 className="heading-4">Hohe Flexibilität</h3>
									<p>Durch Performance Marketing,d.h. datenorientierte Strategien, lassen sich laufende Kampagnen bei Bedarf verbessern und anpassen.</p>
								</Fragment>,
								<Fragment>
									<h3 className="heading-4">Größte Aktualität</h3>
									<p>Onlinebasierte Inhalte punkten durch Aktualität im Vergleich zu gedruckten Werbemitteln.</p>
								</Fragment>,
								<Fragment>
									<h3 className="heading-4">Kundenzufriedenheit und Kundenbindung</h3>
									<p>Chats oder die Kommunikation über soziale Medien sorgen für mehr Nähe zu Ihren Kunden.</p>
								</Fragment>,
								<Fragment>
									<h3 className="heading-4">Multimedialität</h3>
									<p>Erreichen Sie Kunden visuell, auditiv oder interaktiv auf verschiedensten Kanälen.</p>
								</Fragment>
							]}
						/>
					</div>
				</section>

				<section styleName="target">
					<SectionColor 
						content={
							<Fragment>
								<p styleName="target-text" className="heading-3">Als Marketing-Agentur aus Stuttgart haben wir mehr als 10 Jahre Know How im digitalen Bereich. Kontaktieren Sie uns und erfahren, was wir im Bereich Online Marketing für Sie tun können.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-cherry)"
					/>
				</section>

				<FeedbackHomePage />
			</main>
			
			<Footer />
		</div>
	);
}
