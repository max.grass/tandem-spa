import React, { Fragment, useRef, useEffect } from "react";
import gsap from 'gsap';

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { ParallaxEffect } from "components/ParallaxEffect";
import { ColorSteps } from "components/ColorSteps";
import { CheckedList } from "components/CheckedList";
import { IconList } from "components/IconList";
import { Icon } from "components/Icon";
import { Hr } from "components/Hr";
import { ListItemsTween, RoundBackgroundTween } from "components/Animation"
import { ListOrderUnorder } from "components/ListOrderUnorder";
import { MarkedLogos } from "components/MarkedLogos";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from "assets/images/hero-web-analytics.jpg";
import parallaxImageCompany from "assets/images/seo-parallax-company.jpg";
import graphImg from "assets/images/marketing-parallax-graph.jpg";

import waIcon1 from "assets/images/wa-icon-1.svg";
import waIcon2 from "assets/images/wa-icon-2.svg";
import waIcon3 from "assets/images/wa-icon-3.svg";
import waIcon4 from "assets/images/wa-icon-4.svg";

import analyticScheme from "assets/images/web-analytic-scheme.svg"

import galogo from 'assets/images/logos/galogo.svg';
import matomo from 'assets/images/logos/matomo.svg';
import semrush from 'assets/images/logos/semrush.svg';
import mautic from 'assets/images/logos/mautic.svg';
import cleverReach from 'assets/images/logos/cleverReach.svg';


import { advantagesItems } from "./advantagesItems";


import "./WebAnalPage.css";

const logos = [
	{
		"iconSrc": galogo,
		"label": 'Google Analytics'
	},
	{
		"iconSrc": matomo,
		"label": 'Matomo'
	},
	{
		"iconSrc": semrush,
		"label": 'Semrush'
	},
	{
		"iconSrc": mautic,
		"label": 'Mautic'
	},
	{
		"iconSrc": cleverReach,
		"label": 'Clever Reach'
	}
]

export const WebAnalPage = () => {

	const advantagesRef = useRef();
	const factsRef = useRef();
	const roundBackgroundRef = useRef();
	const advantagesSelector = gsap.utils.selector(advantagesRef);

	useEffect(() => {
		let tl = gsap.timeline();
		
		tl.add(
			ListItemsTween(
				advantagesSelector("li"), 
				advantagesRef.current
			)
		).add(RoundBackgroundTween(
			roundBackgroundRef.current,
			factsRef.current,
			"var(--color-gigas)"
		));
	}, []);

	return (
		<div className='page-wrapper'>
			<CommonMetaTags 
				title={'Werbeagentur Stuttgart – Webdesign, Programmierung und PR'}
				description={'Werbeagentur Stuttgart - Ihr Profi im Bereich Webdesign, E-Commerce, Programmierung und PR. Full Service Agentur für digitale Medien!'}
				url={'https://www.tandem-m.de/web-analytics.html'}
				image={heroImg}
			/>

			<Header />
			
			<main>
				<Hero
					src={heroImg}
					alt="Web Analytics"
					width={1920}
					height={800}
					content={
						<HeroContent 
							title={<span>Für messbare<br/>Erfolge</span>}
							text="Web Analytics"
						/>
					}
				/>

				<section>
					<h2 className="visuallyHidden">Für messbare Erfolge</h2>
					<SectionWhite 
						title="Für messbare Erfolge"
						text="Web Analytics, auch bekannt als Webanalyse, Web Controlling oder Traffic-Analyse, sind heute ein wichtiges Mittel, um den Erfolg einer Website sowie aller digitalen Werbemaßnahmen zu kontrollieren."
						textColor="var(--color-gigas)"
					/>
					<div>
						<SectionColor 
							content={
								<Fragment>
									<p className="section-text-200">Webanalyse umfasst die Überwachung, Messung und strategische Ausrichtung auf Basis von Benutzerdaten. Aus den Bereichen Projektmanagement, E-Commerce und Online-Marketing sind Web Analytics heute kaum noch wegzudenken.</p>
								</Fragment>
							}
							textColor="var(--color-white)"
							backgroundColor="var(--color-gigas)"
						/>
					</div>
				</section>
				
				<section styleName="strengths">
					<h2 className="visuallyHidden">Was sind unsere Stärken</h2>
					<ParallaxEffect
						image={graphImg}
						alt="Tandem Marketing & Partners"
					/>
				</section>

				<section className="section">
					<IconList
						listData={[
							{
								"iconSrc" : waIcon1,
								"label" : "Messen"
							},
							{
								"iconSrc" : waIcon2,
								"label" : "Berichten"
							},
							{
								"iconSrc" : waIcon3,
								"label" : "Analysieren"
							},
							{
								"iconSrc" : waIcon4,
								"label" : "Optimieren"
							}
						]}
					/>
				</section>

				<section styleName="colorSteps" className="section">
					<ColorSteps
						hiddenTitle="Suchmaschinenoptimierung"
						parallax={
							<ParallaxEffect
								image={parallaxImageCompany}
								alt="Tandem Marketing & Partners"
								children={
									<div className="section container">
										<b className="heading-2 parallax-text" >
											Warum sind Web Analytics so wichtig?
										</b>
									</div>
								}
							/>
						}
						textTop={"Web Analyse, also die Sammlung und Auswertung von Daten der Website-Besucher, ist eine wichtige Basis für erfolgreiche Marketingstrategien und Website-Konzepte. Unser Ziel ist stets die nachhaltige Entwicklung unserer Kunden."}
						textCenter={"Das erreichen wir, indem wir aktuelle Trends im Web ebenso im Blick behalten wie wichtige Kennzahlen."}
						textBottom={""}
						bgColor={"var(--color-gigas)"}
					/>
				</section>

				<section styleName="checkedListKonnen" className="section">
					<div className="container-narrow">
						<h2 className="heading-3">Dank Web Analytics können wir ...</h2>
					</div>
					<div className="container">
						<CheckedList 
							content={
								<Fragment>
									<li>
										… die Zielgruppe verstehen.<br />
										Besuchsstatistiken helfen uns dabei, ein Profil Ihrer Zielgruppe zu erstellen (demografisch, sozial, geographisch usw.) und geben uns einen Einblick in das Verhalten von Besuchern, zum Beispiel:<br />
											- Welche Geräte verwenden sie?<br />
											- Woher kommen sie?<br />
											- Welche Inhalte bevorzugen sie?<br />
										<b>Wir haben also objektive Daten darüber, welche Strategien erfolgreicher sind als andere.</b>
									</li>
									<li>
										… die User Experience bewerten.<br />
										Anhand der gesammelten Daten können wir starke und schwache Seiten des Systems bewerten, mögliche Stolpersteine im Zusammenhang mit der Benutzerfreundlichkeit oder der Benutzererfahrung identifizieren - und Korrekturen vornehmen.<br />
										<b>Bestehende Konzepte und Strategien – ob inhaltlich, technisch oder optisch – werden laufend optimiert.</b>
									</li>
									<li>
										… spezifische Datenerkenntnisse nutzen.<br />
										Je nach Ziel, gilt es die richtigen Fragen zu stellen. Webanalyse liefert die Antworten, zum Beispiel:<br />
										- Wie oft finden bestimmte Ereignisse (Klicks, Downloads usw.) auf der Website statt?<br />
										- Wie viele Conversions gibt es?<br />
										- Wie ändert sich der Website-Traffic im Laufe der Zeit?<br />
										<b>Wir behalten anhand der Leistungsindikatoren den Überblick darüber, an welcher Stelle angepasst werden sollte.</b>
									</li>
								</Fragment>
							}
							color="var(--color-gigas)"
						/>
					</div>
				</section>

				<section className="section">
					<div className="container-narrow">
						<h2 className="heading-3">Was sind die Hauptziele von Web Analytics?</h2>
						<img src={analyticScheme} alt="web analytics scheme" loading="lazy" />
					</div>
				</section>

				<section className="section">
					<div className="container">
						<h2 className="heading-3">Welche Webanalyse Tools gibt es?</h2>
						<MarkedLogos
							listData = {logos}
						/>
					</div> 
				</section>

				<section>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Tandem Marketing & Partners nutzt die gesamte Bandbreite der Webanalyse Tools, darunter Google Analytics, Matomo, Semrush, Mautic und CleverReach.</p>
								<p className="section-text-200">Die fortschrittlichen Analyse Tools von Google und Matomo helfen uns anhand von Tracking Codes, Einblicke in den Website-Traffic, das Nutzerverhalten, Conversions und vieles mehr zu erhalten. Mit dem Google Tag Manager können Google Analytics und andere Webanalyse Tools einfach auf einer Website implementiert werden.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-gigas)"
					/>
				</section>

				<section>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Tools wie SEMrush liefern wertvolle Daten in den Bereichen SEO, Content Marketing, Wettbewerbsanalyse, Social Media und Online Marketing, sei es in Bezug auf objektive Daten zu Keywords und Trends der organischen Suche, Positionsänderungen in den Suchergebnissen oder Wachstumschancen im Bereich Content Marketing.</p>
								<p className="section-text-200">Für das E-Mail-Marketing verwenden wir die integrierte Berichtsfunktion von Newsletter-Diensten wie Cleverreach, um die Effektivität von E-Mail-Marketing-Kampagnen zu messen und das Engagement der Abonnenten zu verstehen.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
				</section>

				<section>
					<SectionColor 
						content={
							<Fragment>
								<p className="heading-3">Die fünf wichtigsten Web Analytics KPIs</p>
								<p className="section-text-200">Die KPIs (Key Performance Indicators) sind Kennzahlen, anhand derer die Leistung von Aktivitäten in Unternehmen ermittelt werden kann. Welche KPIs relevant sind, um Erfolg und Misserfolg einer Maßnahme zu bewerten, hängt vom Unternehmen den jeweiligen Zielen ab. </p>
								<p className="section-text-200">Zu den wichtigsten allgemeinen Key Performance Indicators in Bezug auf Websites gehören:<br/>
									- Website Traffic (Besucherzahl, neue vs. wiederkehrende Besucher)<br/>
									- Besucherquellen<br/>
									- Besucherverhalten<br/>
									- Verweildauer & Absprungrate<br/>
									- beliebter Benutzerpfad
								</p>
								<p className="section-text-200">Je nach Unternehmensziel wird in einem Tracking-Konzept festgelegt, welche Kennzahlen überwacht werden sollten. Mögliche Ziele sind zum Beispiel die Steigerung der Markenbekanntheit, mehr Verkäufe im E-Commerce oder Neukundengewinnung im B2B-Bereich.</p>
								<p className="section-text-200">Wer Webanalyse maximal für den Erfolg seines Unternehmens nutzen möchte, benötigt viel Erfahrung in der Deutung der blanken Zahlen: Wir haben sie.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-gigas)"
					/>
				</section>

				<section className="section-m" styleName="advantages" ref={advantagesRef}>
					<div className="container">
						<h2 className="heading-3">Um die KPI-Daten korrekt zu verwenden und optimal zu nutzen, beachten wir folgende Grundsätze:</h2>
					</div>
					<div className="container" styleName="advantages-list">
						<Hr color="var(--color-gigas)" />
						<ListOrderUnorder
							isOrdered={true}
							icon={
								<Icon
									name="checked-empty"
									width="50px"
									height="50px"
									color="var(--color-dark)"
								/>
							}
							items={advantagesItems}
						/>
					</div>
				</section>

				<section>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Die korrekte Verwendung von Web Analytics ist ein Schlüssel zum Erfolg im Netz. Sie ist die Grundlage für eine gute User Experience, erfolgreiche E-Commerce-Projekte und lohnende Marketingstrategien. Unsere Webagentur in Großraum Stuttgart berät Sie gerne zu Ihren Anforderungen und bietet Ihnen ein individuelles Angebot.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-gigas)"
					/>
				</section>

				<FeedbackHomePage />
			</main>
			
			<Footer />
		</div>
	);
}
