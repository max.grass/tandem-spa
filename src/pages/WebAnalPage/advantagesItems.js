import React, { Fragment } from "react";

export const advantagesItems = [
  <Fragment>
    <h3 className="heading-4">Der richtige Kontext:</h3>
    <p>Wir berücksichtigen branchenspezifische und historische Referenzdaten, analysieren den Wettbewerb und vergleichen die aktuelle Leistung Ihres Unternehmens mit historischen Daten.</p>
  </Fragment>,
  <Fragment>
    <h3 className="heading-4">Umfassender Überblick:</h3>
    <p>Wir sammeln ausreichend Daten über einen längeren Zeitraum, um den Einfluss vorübergehender Effekte und zufälliger Schwankungen zu minimieren.</p>
  </Fragment>,
  <Fragment>
    <h3 className="heading-4">Fortlaufendes Monitoring:</h3>
    <p>Wir überwachen die KPIs regelmäßig, um wichtige Trends zu erkennen und rechtzeitig darauf zu reagieren. Die KPIs zeigen den aktuellen Status einer Webpräsenz; ebenso wichtig ist es, die Dynamik der Daten im Zeitverlauf zu betrachten. </p>
  </Fragment>,
]