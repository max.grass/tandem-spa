import React from "react";
import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { Footer } from "components/Footer";
import { CommonMetaTags } from 'components/CommonMetaTags';

import heroImg from "assets/images//hero-bildquellen.jpg";

import "./ImageSourcesPage.css";

export const ImageSourcesPage = () => {
  return (
    <div className="page-wrapper">
      <CommonMetaTags 
				title={'Bildquellen'}
				description={'Tandem Marketing'}
				url={'https://www.tandem-m.de/bildquellen.html'}
				image={heroImg}
			/>

      <Header />

      <main>
        <h1 className="visuallyHidden">Über die Bildquellen</h1>
        
        <Hero 
          src={heroImg}
          alt="Image Sources background"
          width={1920}
          height={1281}
          content={
            <HeroContent 
              title={<span>Image Sources</span>}
            />
          }
        />
        
        <div className="container-narrow">
          <div className="section" styleName="textWrapper">
				    <p>Die auf dieser Website veröffentlichten Fotos wurden von Tandem Marketing &amp; Partners erstellt oder stammen aus folgenden Quellen:</p>
            <div styleName="authorWrp">
              <p><b>elenabsl</b> / stock.adobe.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://stock.adobe.com/de/stock-photo/shopping-and-augmented-reality/136020786">https://stock.adobe.com/de/stock-photo/shopping-and-augmented-reality/136020786</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://stock.adobe.com/de/stock-photo/shopping-and-augmented-reality/136020786">https://stock.adobe.com/de/stock-photo/shopping-and-augmented-reality/136020786</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Everett Collection</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/pic-94123438.html">https://www.shutterstock.com/pic-94123438.html</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Roman Motizov</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/pic-340122947.html">https://www.shutterstock.com/pic-340122947.html</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>eugenepartyzan</b> / ru.fotolia.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://ru.fotolia.com/id/93200698">https://ru.fotolia.com/id/93200698</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>canadastock</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/pic-302417708.html">https://www.shutterstock.com/pic-302417708.html</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>BillionPhotos.com</b> / ru.fotolia.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://ru.fotolia.com/id/90082348">https://ru.fotolia.com/id/90082348</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>javier brosch</b> / ru.fotolia.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://ru.fotolia.com/id/69267328">https://ru.fotolia.com/id/69267328</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Ozerina Anna</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/pic-218320543.html">https://www.shutterstock.com/pic-218320543.html</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>konradbak</b> / ru.fotolia.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://ru.fotolia.com/id/92315237">https://ru.fotolia.com/id/92315237</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Bloomicon</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/pic-145915421.html">https://www.shutterstock.com/pic-145915421.html</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Jan Havlicek</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/pic-267055328.html">https://www.shutterstock.com/pic-267055328.html</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Mathias Rosenthal</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/pic-276548081.html">https://www.shutterstock.com/pic-276548081.html</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>JuliusKielaitis</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/pic-176459972.html">https://www.shutterstock.com/pic-176459972.html</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Bluskystudio</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/pic-254918929.html">https://www.shutterstock.com/pic-254918929.html</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Vintage Tone</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/close-finance-business-graph-which-including-530523184">https://www.shutterstock.com/de/image-photo/close-finance-business-graph-which-including-530523184</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>nelea33</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/delicious-toasted-sandwiches-cheddar-cheese-bacon-2115023654">https://www.shutterstock.com/de/image-photo/delicious-toasted-sandwiches-cheddar-cheese-bacon-2115023654</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Followtheflow</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/stylish-dining-room-interior-design-wooden-1814712413">https://www.shutterstock.com/de/image-photo/stylish-dining-room-interior-design-wooden-1814712413</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Jacob Lund</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/group-businesspeople-having-discussion-meeting-room-2140028027">https://www.shutterstock.com/de/image-photo/group-businesspeople-having-discussion-meeting-room-2140028027</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/two-business-professionals-standing-behind-glass-653795557">https://www.shutterstock.com/de/image-photo/two-business-professionals-standing-behind-glass-653795557</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/three-young-friends-sitting-outdoors-looking-491412196">https://www.shutterstock.com/de/image-photo/three-young-friends-sitting-outdoors-looking-491412196</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/smiling-woman-using-smart-phone-modern-705156643">https://www.shutterstock.com/de/image-photo/smiling-woman-using-smart-phone-modern-705156643</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/businesswoman-laptop-explaining-project-details-colleague-1968955966">https://www.shutterstock.com/de/image-photo/businesswoman-laptop-explaining-project-details-colleague-1968955966</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/shot-group-young-business-professionals-having-381098788">https://www.shutterstock.com/de/image-photo/shot-group-young-business-professionals-having-381098788</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/mature-businessman-sitting-office-lobby-laptop-1718759419">https://www.shutterstock.com/de/image-photo/mature-businessman-sitting-office-lobby-laptop-1718759419</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/multiethnic-young-people-racing-shopping-cart-491412199">https://www.shutterstock.com/de/image-photo/multiethnic-young-people-racing-shopping-cart-491412199</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/cropped-shot-barista-using-coffee-maker-482388412">https://www.shutterstock.com/de/image-photo/cropped-shot-barista-using-coffee-maker-482388412</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/pic-362436506.html">https://www.shutterstock.com/pic-362436506.html</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Olivier Le Moal</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/close-safe-lock-on-vault-blur-202526329">https://www.shutterstock.com/de/image-photo/close-safe-lock-on-vault-blur-202526329</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Gorodenkoff</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/close-portrait-software-engineer-working-on-2081374312">https://www.shutterstock.com/de/image-photo/close-portrait-software-engineer-working-on-2081374312</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/portrait-startup-digital-entrepreneur-working-on-2200894173">https://www.shutterstock.com/de/image-photo/portrait-startup-digital-entrepreneur-working-on-2200894173</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/handsome-male-beautiful-female-mobile-application-714655705">https://www.shutterstock.com/de/image-photo/handsome-male-beautiful-female-mobile-application-714655705</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/female-analyst-her-desk-works-on-1013448751">https://www.shutterstock.com/de/image-photo/female-analyst-her-desk-works-on-1013448751</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/factory-office-portrait-beautiful-confident-female-1899126973">https://www.shutterstock.com/de/image-photo/factory-office-portrait-beautiful-confident-female-1899126973</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/ceo-chief-executive-talking-about-company-1919479694">https://www.shutterstock.com/de/image-photo/ceo-chief-executive-talking-about-company-1919479694</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>BongkarnGraphic</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/overhead-shot-businessman-using-his-cellphone-2151992789">https://www.shutterstock.com/de/image-photo/overhead-shot-businessman-using-his-cellphone-2151992789</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/overhead-shot-woman-using-digital-tablet-2126100407">https://www.shutterstock.com/de/image-photo/overhead-shot-woman-using-digital-tablet-2126100407</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/mans-hand-holding-white-screen-mobile-1206109357">https://www.shutterstock.com/de/image-photo/mans-hand-holding-white-screen-mobile-1206109357</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/close-view-young-man-holding-blank-1524014141">https://www.shutterstock.com/de/image-photo/close-view-young-man-holding-blank-1524014141</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/top-view-businessman-working-digital-tablet-1927265054">https://www.shutterstock.com/de/image-photo/top-view-businessman-working-digital-tablet-1927265054</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Prostock-studio</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/unrecognizable-man-using-smartphone-blank-white-2025603860">https://www.shutterstock.com/de/image-photo/unrecognizable-man-using-smartphone-blank-white-2025603860</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>YAKOBCHUK VIACHESLAV</b> / shutterstock.com</p>
              <ul>
                <li>
                <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/bearded-male-business-expert-glasses-working-1750224341">https://www.shutterstock.com/de/image-photo/bearded-male-business-expert-glasses-working-1750224341</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Adil.Mehmood</b> / shutterstock.com</p>
              <ul>
                <li>
                <a rel="nofollow" href="https://www.shutterstock.com/de/image-illustration/computer-generated-image-green-office-interior-2185806145">https://www.shutterstock.com/de/image-illustration/computer-generated-image-green-office-interior-2185806145</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>RossHelen</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/young-creative-woman-working-web-designer-1683775780">https://www.shutterstock.com/de/image-photo/young-creative-woman-working-web-designer-1683775780</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/creative-architect-projecting-pencil-on-big-241267624">https://www.shutterstock.com/de/image-photo/creative-architect-projecting-pencil-on-big-241267624</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/girlfriends-caress-dog-while-sitting-on-2051190755">https://www.shutterstock.com/de/image-photo/girlfriends-caress-dog-while-sitting-on-2051190755</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Andrey_Popov</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/lost-something-looking-things-searching-keys-1845418159">https://www.shutterstock.com/de/image-photo/lost-something-looking-things-searching-keys-1845418159</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Dean Drobot</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/image-young-caucasian-businesslike-man-woman-1614007153">https://www.shutterstock.com/de/image-photo/image-young-caucasian-businesslike-man-woman-1614007153</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/happy-beautiful-girl-using-mobile-phone-1923577088">https://www.shutterstock.com/de/image-photo/happy-beautiful-girl-using-mobile-phone-1923577088</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/unshaven-happy-man-earphones-using-mobile-1937158507">https://www.shutterstock.com/de/image-photo/unshaven-happy-man-earphones-using-mobile-1937158507</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/beautiful-young-blonde-woman-sitting-cafe-1429275893">https://www.shutterstock.com/de/image-photo/beautiful-young-blonde-woman-sitting-cafe-1429275893</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/happy-young-couple-talking-drinking-coffee-370955465">https://www.shutterstock.com/de/image-photo/happy-young-couple-talking-drinking-coffee-370955465</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/smiling-mid-aged-man-working-on-1952025481">https://www.shutterstock.com/de/image-photo/smiling-mid-aged-man-working-on-1952025481</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/portrait-happy-smiling-businessman-eyeglasses-using-531833692">https://www.shutterstock.com/de/image-photo/portrait-happy-smiling-businessman-eyeglasses-using-531833692</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Pixel-Shot</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/woman-holding-mobile-phone-cup-coffee-2079008377">https://www.shutterstock.com/de/image-photo/woman-holding-mobile-phone-cup-coffee-2079008377</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Fotokostic</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/tractor-spraying-pesticides-on-corn-field-1726189876">https://www.shutterstock.com/de/image-photo/tractor-spraying-pesticides-on-corn-field-1726189876</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>hecke61</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/horse-farm-altefeld-germany-1380742223">https://www.shutterstock.com/de/image-photo/horse-farm-altefeld-germany-1380742223</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Maria Markevich</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/young-stylish-handsome-hipster-man-vintage-608149010">https://www.shutterstock.com/de/image-photo/young-stylish-handsome-hipster-man-vintage-608149010</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Just Life</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/young-architect-working-on-project-handsome-1636262314">https://www.shutterstock.com/de/image-photo/young-architect-working-on-project-handsome-1636262314</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Monkey Business Images</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/businesswoman-on-phone-using-digital-tablet-305434448">https://www.shutterstock.com/de/image-photo/businesswoman-on-phone-using-digital-tablet-305434448</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/woman-viewed-through-window-using-mobile-243770773">https://www.shutterstock.com/de/image-photo/woman-viewed-through-window-using-mobile-243770773 </a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/group-elementary-school-kids-running-corridor-388657198">https://www.shutterstock.com/de/image-photo/group-elementary-school-kids-running-corridor-388657198</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/busy-high-school-corridor-during-recess-1112561213">https://www.shutterstock.com/de/image-photo/busy-high-school-corridor-during-recess-1112561213</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/business-people-work-busy-luxury-office-796346002">https://www.shutterstock.com/de/image-photo/business-people-work-busy-luxury-office-796346002</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Kaspars Grinvalds</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/blank-screen-tablet-mockup-your-own-1831870078">https://www.shutterstock.com/de/image-photo/blank-screen-tablet-mockup-your-own-1831870078</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>best pixels</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/home-office-tablet-devise-on-table-1366196849">https://www.shutterstock.com/de/image-photo/home-office-tablet-devise-on-table-1366196849</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Prathankarnpap</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/top-view-mock-smart-phone-digital-1924901885">https://www.shutterstock.com/de/image-photo/top-view-mock-smart-phone-digital-1924901885</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>LeonidKos</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/group-office-employees-coworking-center-business-1555635677">https://www.shutterstock.com/de/image-photo/group-office-employees-coworking-center-business-1555635677</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Zoran Zeremski</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/male-nurse-measures-blood-pressure-senior-1817431535">https://www.shutterstock.com/de/image-photo/male-nurse-measures-blood-pressure-senior-1817431535</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>wavebreakmedia</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/front-view-happy-diverse-school-kids-1352249762">https://www.shutterstock.com/de/image-photo/front-view-happy-diverse-school-kids-1352249762</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>elwynn</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/yellow-cosmos-flowers-farm-outdoor-under-1812083710">https://www.shutterstock.com/de/image-photo/yellow-cosmos-flowers-farm-outdoor-under-1812083710</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>TREKPix</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/using-digital-touchscreen-tablet-stylus-pen-1769333753">https://www.shutterstock.com/de/image-photo/using-digital-touchscreen-tablet-stylus-pen-1769333753</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Prathankarnpap</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/overhead-shot-stylish-man-hands-holding-1895836315">https://www.shutterstock.com/de/image-photo/overhead-shot-stylish-man-hands-holding-1895836315</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Farknot Architect</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/top-view-mockup-image-woman-holding-1721591668">https://www.shutterstock.com/de/image-photo/top-view-mockup-image-woman-holding-1721591668</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/top-view-mockup-image-woman-holding-1718238892">https://www.shutterstock.com/de/image-photo/top-view-mockup-image-woman-holding-1718238892</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Dariusz Jarzabek</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/modern-house-nice-lawn-surrounded-by-442046731">https://www.shutterstock.com/de/image-photo/modern-house-nice-lawn-surrounded-by-442046731</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Pressmaster</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/joyous-young-businesswoman-headphones-listening-dynamic-1149710558">https://www.shutterstock.com/de/image-photo/joyous-young-businesswoman-headphones-listening-dynamic-1149710558</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>G-Stock Studio</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/analyzing-data-top-view-young-colleagues-1079431568">https://www.shutterstock.com/de/image-photo/analyzing-data-top-view-young-colleagues-1079431568</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/successful-team-work-group-young-business-572805178">https://www.shutterstock.com/de/image-photo/successful-team-work-group-young-business-572805178</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/who-four-young-cheerful-business-people-594117614">https://www.shutterstock.com/de/image-photo/who-four-young-cheerful-business-people-594117614</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/business-professionals-group-young-confident-people-1068519821">https://www.shutterstock.com/de/image-photo/business-professionals-group-young-confident-people-1068519821</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>bbernard</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/team-colleagues-brainstorming-together-while-working-527845618">https://www.shutterstock.com/de/image-photo/team-colleagues-brainstorming-together-while-working-527845618</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>sirtravelalot</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/businessmen-analyzing-documents-on-wall-office-226774441">https://www.shutterstock.com/de/image-photo/businessmen-analyzing-documents-on-wall-office-226774441</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>one photo</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/man-holding-blank-screen-tablet-design-1969259395">https://www.shutterstock.com/de/image-photo/man-holding-blank-screen-tablet-design-1969259395</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Blue Titan</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/mockup-image-woman-holding-digital-tablet-1946818168">https://www.shutterstock.com/de/image-photo/mockup-image-woman-holding-digital-tablet-1946818168</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Rawpixel.com</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/young-woman-using-smartphone-subway-1060222451">https://www.shutterstock.com/de/image-photo/young-woman-using-smartphone-subway-1060222451</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/businessman-thinking-planning-strategy-working-laptop-303167630">https://www.shutterstock.com/de/image-photo/businessman-thinking-planning-strategy-working-laptop-303167630</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/office-worker-working-on-laptop-mockup-1403231063">https://www.shutterstock.com/de/image-photo/office-worker-working-on-laptop-mockup-1403231063</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/pic-218755333.html">https://www.shutterstock.com/pic-218755333.html</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/pic-210974548.html">https://www.shutterstock.com/pic-210974548.html</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/pic-225288178.html">https://www.shutterstock.com/pic-225288178.html</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Ground Picture</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/female-small-business-owner-using-mobile-1832160496">https://www.shutterstock.com/de/image-photo/female-small-business-owner-using-mobile-1832160496</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/group-executives-sitting-working-together-using-289837931">https://www.shutterstock.com/de/image-photo/group-executives-sitting-working-together-using-289837931</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/happy-young-african-american-woman-brainstorming-2007080264">https://www.shutterstock.com/de/image-photo/happy-young-african-american-woman-brainstorming-2007080264</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/nurse-explaining-medicine-dosage-old-patient-2024209253">https://www.shutterstock.com/de/image-photo/nurse-explaining-medicine-dosage-old-patient-2024209253</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>SFIO CRACHO</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/portrait-handsome-bearded-man-wearing-glassesheadphones-562748524">https://www.shutterstock.com/de/image-photo/portrait-handsome-bearded-man-wearing-glassesheadphones-562748524</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/closeup-view-banking-finance-analyst-eyeglasses-608938496">https://www.shutterstock.com/de/image-photo/closeup-view-banking-finance-analyst-eyeglasses-608938496</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>GaudiLab</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/back-view-businessman-sitting-office-desktop-1276085386">https://www.shutterstock.com/de/image-photo/back-view-businessman-sitting-office-desktop-1276085386</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Lordn</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/senior-woman-using-digital-tablet-home-1492696199">https://www.shutterstock.com/de/image-photo/senior-woman-using-digital-tablet-home-1492696199</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Farknot Architect</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/mockup-image-woman-holding-using-black-1427404919">https://www.shutterstock.com/de/image-photo/mockup-image-woman-holding-using-black-1427404919</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/mockup-image-woman-using-typing-on-1741065653">https://www.shutterstock.com/de/image-photo/mockup-image-woman-using-typing-on-1741065653</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>GENETTICA</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/man-sitting-on-desk-videoconferencing-doctor-541161583">https://www.shutterstock.com/de/image-photo/man-sitting-on-desk-videoconferencing-doctor-541161583</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>antoniodiaz</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/cropped-image-woman-using-smartphone-while-1504288832">https://www.shutterstock.com/de/image-photo/cropped-image-woman-using-smartphone-while-1504288832</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>panitanphoto</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/mockup-image-blank-white-screen-cell-1627542232">https://www.shutterstock.com/de/image-photo/mockup-image-blank-white-screen-cell-1627542232</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/computer-screencell-phone-blank-mockup-woman-1912029943">https://www.shutterstock.com/de/image-photo/computer-screencell-phone-blank-mockup-woman-1912029943</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/mockup-image-blank-white-screen-cell-1570066222">https://www.shutterstock.com/de/image-photo/mockup-image-blank-white-screen-cell-1570066222</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/computercell-phone-mockup-image-blank-screen-1661760793">https://www.shutterstock.com/de/image-photo/computercell-phone-mockup-image-blank-screen-1661760793</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/mockup-image-blank-white-screen-cell-1576303171">https://www.shutterstock.com/de/image-photo/mockup-image-blank-white-screen-cell-1576303171</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/mockup-image-blank-white-screen-cell-1548777059">https://www.shutterstock.com/de/image-photo/mockup-image-blank-white-screen-cell-1548777059</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/cell-phone-blank-white-screen-mockup-1881301396">https://www.shutterstock.com/de/image-photo/cell-phone-blank-white-screen-mockup-1881301396</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/mockup-image-blank-screen-computercell-phone-1492959275">https://www.shutterstock.com/de/image-photo/mockup-image-blank-screen-computercell-phone-1492959275</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/cell-phone-blank-white-screen-mockup-1879629337">https://www.shutterstock.com/de/image-photo/cell-phone-blank-white-screen-mockup-1879629337 </a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>best pixels</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/home-office-tablet-devise-on-table-1366196675">https://www.shutterstock.com/de/image-photo/home-office-tablet-devise-on-table-1366196675</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>PaeGAG</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/businessman-working-on-desk-office-using-1932435317">https://www.shutterstock.com/de/image-photo/businessman-working-on-desk-office-using-1932435317</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Drazen Zigic</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/young-manual-worker-using-digital-tablet-1557241355">https://www.shutterstock.com/de/image-photo/young-manual-worker-using-digital-tablet-1557241355</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>4 PM production</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/students-studying-library-young-people-spending-739173847">https://www.shutterstock.com/de/image-photo/students-studying-library-young-people-spending-739173847</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Stock 4you</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/closeup-black-male-hand-swiping-on-1982546711">https://www.shutterstock.com/de/image-photo/closeup-black-male-hand-swiping-on-1982546711</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Zivica Kerkez</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/two-designers-sitting-meeting-table-by-1411159163">https://www.shutterstock.com/de/image-photo/two-designers-sitting-meeting-table-by-1411159163</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/men-renovating-kitchen-shopping-construction-material-1139179313">https://www.shutterstock.com/de/image-photo/men-renovating-kitchen-shopping-construction-material-1139179313</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>TippaPatt</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/work-home-man-using-blank-screen-1740162935">https://www.shutterstock.com/de/image-photo/work-home-man-using-blank-screen-1740162935</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/mockup-image-man-hand-holding-watching-1683156460">https://www.shutterstock.com/de/image-photo/mockup-image-man-hand-holding-watching-1683156460</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/casual-business-man-stylus-pen-writing-1793086768">https://www.shutterstock.com/de/image-photo/casual-business-man-stylus-pen-writing-1793086768</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Pressmaster</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/man-cellphone-418805146">https://www.shutterstock.com/de/image-photo/man-cellphone-418805146</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>baranq</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/young-woman-eating-salad-restaurant-texting-318311855">https://www.shutterstock.com/de/image-photo/young-woman-eating-salad-restaurant-texting-318311855</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>fizkes</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/caucasian-businessman-sitting-table-cafe-modern-1364479901">https://www.shutterstock.com/de/image-photo/caucasian-businessman-sitting-table-cafe-modern-1364479901</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/diverse-young-people-talking-having-fun-1079701145">https://www.shutterstock.com/de/image-photo/diverse-young-people-talking-having-fun-1079701145</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Kostsov</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-illustration/modern-laptop-blank-screen-canvas-draftings-1627785037">https://www.shutterstock.com/de/image-illustration/modern-laptop-blank-screen-canvas-draftings-1627785037</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-illustration/metallic-silver-coffee-machine-white-cups-1381002500">https://www.shutterstock.com/de/image-illustration/metallic-silver-coffee-machine-white-cups-1381002500</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-illustration/tablet-blank-white-screen-mouse-keyboard-1293984631">https://www.shutterstock.com/de/image-illustration/tablet-blank-white-screen-mouse-keyboard-1293984631</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-illustration/realistic-laptop-blank-screen-mobile-phone-1120537991">https://www.shutterstock.com/de/image-illustration/realistic-laptop-blank-screen-mobile-phone-1120537991</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-illustration/laptop-blank-white-screen-lamp-on-1293984610">https://www.shutterstock.com/de/image-illustration/laptop-blank-white-screen-lamp-on-1293984610</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-illustration/realistic-black-laptop-blank-screen-on-1120537979">https://www.shutterstock.com/de/image-illustration/realistic-black-laptop-blank-screen-on-1120537979</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-illustration/coffee-cup-tablet-blank-white-screen-1381002530">https://www.shutterstock.com/de/image-illustration/coffee-cup-tablet-blank-white-screen-1381002530</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-illustration/laptop-blank-screen-on-cafe-table-510838630">https://www.shutterstock.com/de/image-illustration/laptop-blank-screen-on-cafe-table-510838630</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>LightField Studios</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/smiling-young-florists-aprons-using-digital-700005292">https://www.shutterstock.com/de/image-photo/smiling-young-florists-aprons-using-digital-700005292</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/side-view-young-woman-using-smartphone-1115046023">https://www.shutterstock.com/de/image-photo/side-view-young-woman-using-smartphone-1115046023</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/casual-businesswoman-working-desk-colleagues-motion-1237479052">https://www.shutterstock.com/de/image-photo/casual-businesswoman-working-desk-colleagues-motion-1237479052</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/high-angle-view-handsome-stylish-african-1074888302">https://www.shutterstock.com/de/image-photo/high-angle-view-handsome-stylish-african-1074888302</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/high-angle-view-young-casual-business-1211027242">https://www.shutterstock.com/de/image-photo/high-angle-view-young-casual-business-1211027242</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/casual-businessman-heaphones-sitting-on-chair-1237479049">https://www.shutterstock.com/de/image-photo/casual-businessman-heaphones-sitting-on-chair-1237479049</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/businessman-smoking-cigar-multicultural-business-team-645763882">https://www.shutterstock.com/de/image-photo/businessman-smoking-cigar-multicultural-business-team-645763882 </a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Aruta Images</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/happy-multiethnic-people-using-mobile-phone-1950140176">https://www.shutterstock.com/de/image-photo/happy-multiethnic-people-using-mobile-phone-1950140176</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Tinxi</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/young-handsome-asian-model-dressed-red-276616139">https://www.shutterstock.com/de/image-photo/young-handsome-asian-model-dressed-red-276616139</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>KucherAV</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/woman-make-purchase-easy-comfort-order-1928110799">https://www.shutterstock.com/de/image-photo/woman-make-purchase-easy-comfort-order-1928110799</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>1000 Words</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/scenic-summertime-view-beautiful-english-style-1475050415">https://www.shutterstock.com/de/image-photo/scenic-summertime-view-beautiful-english-style-1475050415</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>OlegRi</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/black-brown-horses-eating-hay-1695210331">https://www.shutterstock.com/de/image-photo/black-brown-horses-eating-hay-1695210331</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Sergey Tinyakov</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/beautiful-girl-binoculars-hand-standing-by-1081734533">https://www.shutterstock.com/de/image-photo/beautiful-girl-binoculars-hand-standing-by-1081734533</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>SeventyFour</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/directly-above-view-content-enterprising-young-1356197765">https://www.shutterstock.com/de/image-photo/directly-above-view-content-enterprising-young-1356197765</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/portrait-carefree-young-woman-listening-music-1569484579">https://www.shutterstock.com/de/image-photo/portrait-carefree-young-woman-listening-music-1569484579</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>LStockStudio</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/man-sitting-coffee-shop-using-tablet-534389323">https://www.shutterstock.com/de/image-photo/man-sitting-coffee-shop-using-tablet-534389323</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>kanvag</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/new-palace-das-neue-schloss-which-93335431">https://www.shutterstock.com/de/image-photo/new-palace-das-neue-schloss-which-93335431</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>GaudiLab</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/blurred-back-view-young-woman-working-1375629638">https://www.shutterstock.com/de/image-photo/blurred-back-view-young-woman-working-1375629638</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/young-pondering-female-person-beautiful-curly-747373468">https://www.shutterstock.com/de/image-photo/young-pondering-female-person-beautiful-curly-747373468</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/cropped-image-woman-holding-digital-tablet-1171214533">https://www.shutterstock.com/de/image-photo/cropped-image-woman-holding-digital-tablet-1171214533</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/cheerful-caucasian-hipster-guy-trendy-eyewear-1751360807">https://www.shutterstock.com/de/image-photo/cheerful-caucasian-hipster-guy-trendy-eyewear-1751360807</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/dark-skinned-female-graphic-designer-pointing-1272462163">https://www.shutterstock.com/de/image-photo/dark-skinned-female-graphic-designer-pointing-1272462163</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/young-stylish-female-hipster-standing-near-755936482">https://www.shutterstock.com/de/image-photo/young-stylish-female-hipster-standing-near-755936482</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Roman Samborskyi</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/good-day-mood-build-builder-construction-1224288868">https://www.shutterstock.com/de/image-photo/good-day-mood-build-builder-construction-1224288868</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Dusan Petkovic</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/young-satisfied-stylish-charming-couple-testing-1046342155">https://www.shutterstock.com/de/image-photo/young-satisfied-stylish-charming-couple-testing-1046342155</a>
                </li>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/charming-beautiful-couple-love-making-bed-772822510">https://www.shutterstock.com/de/image-photo/charming-beautiful-couple-love-making-bed-772822510</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Bobex-73</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/this-watch-great-young-woman-surprised-1494272324">https://www.shutterstock.com/de/image-photo/this-watch-great-young-woman-surprised-1494272324</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Juassawa</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/mockup-image-man-working-laptop-blank-1715579557">https://www.shutterstock.com/de/image-photo/mockup-image-man-working-laptop-blank-1715579557</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>New Africa</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/young-man-pajamas-pillow-on-gray-1149498635">https://www.shutterstock.com/de/image-photo/young-man-pajamas-pillow-on-gray-1149498635</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Rymden</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/man-working-laptop-mock-screen-739454041">https://www.shutterstock.com/de/image-photo/man-working-laptop-mock-screen-739454041</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>bezikus</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/round-wooden-tables-gray-armchairs-on-1667839735">https://www.shutterstock.com/de/image-photo/round-wooden-tables-gray-armchairs-on-1667839735</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Look Studio</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/outside-photo-happy-girls-stylish-outfits-1663058266">https://www.shutterstock.com/de/image-photo/outside-photo-happy-girls-stylish-outfits-1663058266</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>ouh_desire</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/mock-man-holding-device-touching-screen-588989423">https://www.shutterstock.com/de/image-photo/mock-man-holding-device-touching-screen-588989423</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Foxy burrow</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/young-smiling-bearded-businessman-white-shirt-1105349987">https://www.shutterstock.com/de/image-photo/young-smiling-bearded-businessman-white-shirt-1105349987</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Tetiana Yurchenko</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-vector/laptop-email-many-hands-holding-envelopes-1161069625">https://www.shutterstock.com/de/image-vector/laptop-email-many-hands-holding-envelopes-1161069625</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>indira's work</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/beautiful-young-sexy-brunette-girl-portrait-1114293701">https://www.shutterstock.com/de/image-photo/beautiful-young-sexy-brunette-girl-portrait-1114293701</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Ionov Artem</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/fashion-man-trousers-leather-jacket-dark-603174827">https://www.shutterstock.com/de/image-photo/fashion-man-trousers-leather-jacket-dark-603174827</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Jens Goepfert</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/stuttgart-germany-castle-square-city-center-138854639 ">https://www.shutterstock.com/de/image-photo/stuttgart-germany-castle-square-city-center-138854639 </a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>brizmaker</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/set-plates-bouquet-flowers-glass-jar-2015073251">https://www.shutterstock.com/de/image-photo/set-plates-bouquet-flowers-glass-jar-2015073251</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Cast Of Thousands</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/top-view-fashionable-femle-student-learns-1012047976">https://www.shutterstock.com/de/image-photo/top-view-fashionable-femle-student-learns-1012047976</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Image Supply</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/luxurious-penthouse-balcony-130853948">https://www.shutterstock.com/de/image-photo/luxurious-penthouse-balcony-130853948</a>
                </li>
              </ul>
            </div>
            <div styleName="authorWrp">
              <p><b>Olena Yakobchuk</b> / shutterstock.com</p>
              <ul>
                <li>
                  <a rel="nofollow" href="https://www.shutterstock.com/de/image-photo/elegant-stylish-mature-man-smoking-cigar-1530864827">https://www.shutterstock.com/de/image-photo/elegant-stylish-mature-man-smoking-cigar-1530864827</a>
                </li>
              </ul>
            </div>
            <p>Tandem Marketing &amp; Partners ist bestrebt, in allen Publikationen die Urheberrechte der verwendeten Bilder, Grafiken, Tondokumente, Videosequenzen und Texte zu beachten, von ihr selbst erstellte Bilder, Grafiken, Tondokumente, Videosequenzen und Texte zu nutzen oder auf lizenzfreie Grafiken, Tondokumente, Videosequenzen und Texte zurückzugreifen.</p>
            <p>Alle innerhalb des Internetangebotes genannten und ggf. durch Dritte geschützten Marken- und Warenzeichen unterliegen uneingeschränkt den Bestimmungen des jeweils gültigen Kennzeichenrechts und den Besitzrechten der jeweiligen eingetragenen Eigentümer.</p>
            <p>Allein aufgrund der bloßen Nennung ist nicht der Schluss zu ziehen, dass Markenzeichen nicht durch Rechte Dritter geschützt sind.<br/>Das Copyright für veröffentlichte, von Tandem Marketing &amp; Partners selbst erstellte Objekte bleibt allein bei ihr. Eine Vervielfältigung oder Verwendung solcher Grafiken, Tondokumente, Videosequenzen und Texte in anderen elektronischen oder gedruckten Publikationen ist ohne ausdrückliche Zustimmung von Tandem Marketing &amp; Partners nicht gestattet.</p>
          </div>
        </div>

      </main>
      
      <Footer content="bottom"/>
    </div>
  );
}