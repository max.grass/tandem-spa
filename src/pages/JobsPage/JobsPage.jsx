import React, {Fragment} from "react";
import { Header } from "components/Header";
import { Hero } from "components/Hero";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";
import heroImg from "assets/images/hero-contact.jpg";
import {HeroPage} from "components/Hero/Content";
import {CheckedList} from "components/CheckedList";

import './JobsPage.css';

export const JobsPage = () => {
  return (
    <div className="page-wrapper">
      <CommonMetaTags 
				title={'Karriere bei Tandem Marketing &amp; Partners'}
				description={'Tandem Marketing'}
				url={'https://www.tandem-m.de/jobs.html'}
				image={heroImg}
			/>

      <Header />

      <main>

        <Hero
          src={heroImg}
          alt="Leute sitzen mit Laptops im Büro"
          width={1920}
          height={300}
          content={<HeroPage title="Karriere bei Tandem Marketing" />}
        />
        
        <div className="container-narrow">
          <div className="section">
            <p>Tandem Marketing &amp; Partners ist eine junge Werbeagentur mit Standorten in Stuttgart und Berlin. Wir
              sind auf den digitalen Bereich spezialisiert, kennen die Trends und Entwicklungen im Internet und zeigen
              unseren Kunden, wie man Potenziale im Online-Bereich am besten nutzt. Ob Website, Corporate Design oder
              Online-Marketing-Strategie – wir bieten alle Leistungen aus einer Hand.&nbsp;<br />Was uns am Herzen
                liegt? Maßgeschneiderte Lösungen für jeden Kunden. Das schaffen wir durch kritische Analysen,
                strategisches Denken und persönliche Beratung rund um E-Commerce, Marketing und PR. Zu unseren Kunden
                zählen kleine und mittelständische Unternehmen aus ganz Deutschland ebenso wie gemeinnützige
                Organisationen.&nbsp;</p>
            <p>Als Online Marketing Manager/in unterstützen Sie uns mit Ihrem feinen Gespür fürs Texten, wirken an der
              Entwicklung von Strategien mit und betreuen laufende Kampagnen. Sie sprechen potenzielle Kunden an und
              entwickeln bestehende Maßnahmen zur Akquise strategisch weiter. Nach der Einarbeitungsphase leiten Sie
              selbstständig erste kleinere Projekte.</p>
            <p>Zum nächstmöglichen Zeitpunkt suchen wir im Großraum Stuttgart Unterstützung im Bereich PR und Online
              Marketing.&nbsp;</p>
            <p><strong>Ihr Profil:</strong></p>

            <div className="container" styleName="checkedList">
              <CheckedList
                content={
                  <Fragment>
                    <li>Abgeschlossenes (Fach-)Hochschulstudium der Medien- oder Kommunikationswissenschaften oder eine vergleichbare Qualifikation</li>
                    <li>Erste Erfahrungen in den Bereichen Online Marketing und Suchmaschinenoptimierung</li>
                    <li>Interesse und Gespür für Entwicklungen im digitalen Bereich</li>
                    <li>Erfahrung im Umgang mit Content-Management-Systemen (TYPO3, Drupal, Wordpress, Magento)</li>
                    <li>idealerweise Kenntnisse über die Nutzung von Web-Analyse-Tools wie Google Analytics und wichtige KPI im Online Marketing</li>
                    <li>Spaß am Texten und die Fähigkeit, sich schnell in neue Themengebiete einzuarbeiten</li>
                    <li>Verständnis und Erfahrung im strategischen Einsatz und Nutzen von Sozialen Medien aus Unternehmenssicht</li>
                    <li>Konzeptionsstärke und zielorientierte Arbeitsweise, Organisationstalent und Genauigkeit</li>
                    <li>Teamfähigkeit und Kommunikationsstärke</li>
                    <li>gute Englischkenntnisse in Wort und Schrift</li>
                  </Fragment>
                }
                color="var(--color-darkredwine)"
              />
            </div>

            <p>Wir bieten Ihnen eine abwechslungsreiche Tätigkeit in einem modernen und interessanten Arbeitsumfeld,
              ein hohes Maß an Gestaltungsfreiheit und Verantwortung, Spaß bei der Arbeit, flexible Arbeitszeiten
              sowie einen attraktiven Arbeitsplatz in Leinfelden-Echterdingen.</p>
            <p>Wir freuen uns darauf, Sie kennen zu lernen! Bitte senden Sie Ihre Bewerbungsunterlagen unter Angabe
              Ihrer Gehaltsvorstellung und des möglichen Eintrittstermins per E-Mail an <a href="mailto:info@tandem-m.de">info@tandem-m.de</a>
            </p>
          </div>
        </div>
      </main>
      
      <Footer content="bottom"/>
    </div>
  );
}
