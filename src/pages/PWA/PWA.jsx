import React, { Fragment, useRef, useEffect } from "react";
import { Link } from 'react-router-dom';
import gsap from "gsap";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { ListOrderUnorder } from "components/ListOrderUnorder";
import { Hr } from "components/Hr";
import { Icon } from "components/Icon";
import { ParallaxEffect } from "components/ParallaxEffect";
import { Article } from "components/Article";
import { ColorSteps } from "components/ColorSteps";

import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { ListItemsTween, ListItemsTween2 } from "components/Animation"
import { LineChartPWA } from "components/Charts";
import { CommonMetaTags } from "components/CommonMetaTags";
import { IconList } from "components/IconList";
import { VideoPlayer } from "components/VideoPlayer";

import heroImg from "assets/images/hero-pwa.jpg";
// import stats from "assets/images/pwa-stats-1.svg";
import stats2 from "assets/images/pwa-stats-2.svg";
import stats3 from "assets/images/pwa-stats-3.svg";
import stats4 from "assets/images/pwa-stats-4.svg";
// import parallaxImg from "assets/images/pwa-parallax-1.jpg";
import parallaxImg2 from "assets/images/pwa-parallax-2.jpg";
import parallaxImg3 from "assets/images/pwa-parallax-3.jpg";
import parallaxImg4 from "assets/images/pwa-parallax-4.jpg";
import parallaxImg5 from "assets/images/pwa-parallax-5.jpg";
import parallaxImg6 from "assets/images/pwa-parallax-6.jpg";
import parallaxImg7 from "assets/images/pwa-parallax-7.jpg";
import parallaxImg8 from "assets/images/pic-pwa-06-b.jpg";

import imgArticle1 from "assets/images/pwa-article-1.jpg";
import imgArticle2 from "assets/images/pwa-article-2.jpg";

import videoPreview from "assets/images/video-hershortt.jpg";

import logo1 from "assets/images/logo-pwa-1.svg";
import logo2 from "assets/images/logo-pwa-2.svg";
import logo3 from "assets/images/logo-pwa-3.svg";
import logo4 from "assets/images/logo-pwa-4.svg";
import logo5 from "assets/images/logo-pwa-5.svg";
import logo6 from "assets/images/logo-pwa-6.svg";
import logo7 from "assets/images/logo-pwa-7.svg";
import logo8 from "assets/images/logo-pwa-8.svg";
import logo9 from "assets/images/logo-pwa-9.svg";
import logo10 from "assets/images/logo-pwa-10.svg";

import "./PWA.css";

export const PWA = () => {
  const aboutRef = useRef();
  const bqStatsRef = useRef();
  const advantagesRef = useRef();
  const structureRef = useRef();
  
  useEffect(() => {
    let tl = gsap.timeline();
    
    tl
    .add(
      ListItemsTween(
        aboutRef.current,
        0.2
      )
    )
    .add(
      ListItemsTween(
        bqStatsRef.current,
        0.2
      )
    )
    .add(
      ListItemsTween2(
        advantagesRef.current,
        0.2
      )
    )
    .add(
      ListItemsTween(
        structureRef.current,
        0.2
      )
    );
  }, []);

  return (
    <div className="page-wrapper">
      <CommonMetaTags 
				title={'PWA: Neuer Standard im mobilen E-Commerce'}
				description={'Progressive Web Apps sind ein bedeutender Fortschritt in der Entwicklung des mobilen Webs'}
				url={'https://www.tandem-m.de/pwa-react.html'}
				image={heroImg}
			/>

      <Header />
      <main>
        <Hero
          src={heroImg}
          heght={500}
          content={
            <HeroContent 
              title={<span>Neuer Standard im mobilen <br /> E-Commerce</span>}
              text="Progressive Web App"
            />
          }
        />

        <section>
          <h2 className="visuallyHidden">Einführung in PWA</h2>
        
          <SectionWhite 
            title="PWA: Neuer Standard im mobilen E-Commerce"
            text="Dank responsive Webdesign und Web-Apps konnte sich der E-Commerce vom Desktop lösen: Immer mehr Internetnutzer shoppen regelmäßig online."
            textColor="var(--color-medium-teal-blue)"
          />

          <SectionColor 
            content={
              <p className="section-text-200">An dieser Stelle kommt ein neues Konzept ins Spiel: Die Progressive Web App, kurz PWA, die eine ultraschnelle und nahtlose Benutzererfahrung ermöglicht.</p>
            }
            backgroundColor="var(--color-medium-teal-blue)"
          />
        </section>

        {/* About */}
        <section ref={aboutRef}>
          <h2 className="visuallyHidden">Über die PWA-Technologie.</h2>
          <Article 
            title="Projects hershortt"
            buttonUrl="/projekte/84-hershortt.html"
            buttonCaption="Fall zeigen"
            img={
              <img 
                src={imgArticle1}
                alt=""
                width={1920} 
                height={800}
                loading="lazy"
              />
            }
            color="var(--color-medium-teal-blue)"
          />

          <div className="section-m" styleName="services">
            <IconList
              listData={[
                {
                  "iconSrc" : logo1,
                  "label" : "Responsive"
                },
                {
                  "iconSrc" : logo2,
                  "label" : "Linkable"
                },
                {
                  "iconSrc" : logo3,
                  "label" : "Discoverable"
                },
                {
                  "iconSrc" : logo4,
                  "label" : "Installable"
                },
                {
                  "iconSrc" : logo5,
                  "label" : "App-like"
                },
                {
                  "iconSrc" : logo6,
                  "label" : "Connectivity Independent"
                },
                {
                  "iconSrc" : logo7,
                  "label" : "Fresh"
                },
                {
                  "iconSrc" : logo8,
                  "label" : "Safe"
                },
                {
                  "iconSrc" : logo9,
                  "label" : "Re-engageable"
                },
                {
                  "iconSrc" : logo10,
                  "label" : "Progressive"
                }
              ]}
            />
            {/* <div styleName="services-slider">
              <ul>
                <li>
                  <p>Responsive</p>
                  <Icon
                    name="responsive"
                    color="var(--color-medium-teal-blue)"
                    figure="triangle-top"
                  />
                </li>
                <li>
                  <p>Linkable</p>
                  <Icon
                    name="linkable"
                    color="var(--color-medium-teal-blue)"
                    figure="square"
                  />
                </li>
                <li>
                  <p>Discoverable</p>
                  <Icon
                    name="discoverable"
                    color="var(--color-medium-teal-blue)"
                    figure="circle"
                  />
                </li>
                <li>
                  <p>Installable</p>
                  <Icon
                    name="installable"
                    color="var(--color-medium-teal-blue)"
                    figure="circle"
                  />
                </li>
                <li>
                  <p>App-like</p>
                  <Icon
                    name="app_like"
                    color="var(--color-medium-teal-blue)"
                    figure="triangle-bottom"
                  />
                </li>
                <li>
                  <p>Connectivity Independent</p>
                  <Icon
                    name="connectivity_independent"
                    color="var(--color-medium-teal-blue)"
                    figure="triangle-top"
                  />
                </li>
                <li>
                  <p>Fresh</p>
                  <Icon
                    name="fresh"
                    color="var(--color-medium-teal-blue)"
                    figure="circle"
                  />
                </li>
                <li>
                  <p>Safe</p>
                  <Icon
                    name="safe"
                    color="var(--color-medium-teal-blue)"
                    figure="square"
                  />
                </li>
                <li>
                  <p>Re-engageable</p>
                  <Icon
                    name="reengageable"
                    color="var(--color-medium-teal-blue)"
                    figure="triangle-bottom"
                  />
                </li>
              </ul>
            </div> */}
          </div>

          <div className="section-m container-narrow grid-hr" styleName="about">
            <b className="heading-3">Was ist eine Progressive Web App?</b>
            <Hr color="var(--color-medium-teal-blue)"/>

            <div styleName="about-text">
              <p className="section-text-200">Eine Progressive Web App ist eine Website, die benutzerfreundliche Features und Fähigkeiten bietet, vergleichbar mit einer nativen App. Native Apps sind die klassischen Apps aus dem Store, die speziell für ein Betriebssystem (iOS, Android) entwickelt wurden.</p>
              <p className="section-text-200">PWA werden als das App-Modell der Zukunft gehandelt, denn sie bieten viele Vorteile gegenüber klassischen Apps und Websites:</p>
            </div>
            <ListOrderUnorder
              icon={
                <Icon
                  name="checked"
                  width="50px"
                  height="50px"
                  color="var(--color-medium-teal-blue)"
                />
              }
              textColor="var(--color-medium-teal-blue)"
              items={[
                "PWA werden wie eine gewöhnliche App auf dem Smartphone, Tablet oder Desktop installiert",
                "Die Ladezeit von PWA ist bedeutend kürzer als bei normalen Websites",
                "PWA lassen sich offline verwenden im Gegensatz zu normalen Websites",
                "PWA werden über die Google-Suche gefunden (unabhängig von App-Stores)",
                "PWA benötigen keine Updateprozesse wie native Apps",
                "Im Vergleich zu nativen Apps sind PWA kostengünstiger",
              ]}
            />
          </div>
          <div className="section" styleName="about-contacts">
            <p className="container-narrow section-text-700">Interessiert? <a href="/contact">Kontaktieren Sie uns!</a></p>
          </div>
        </section>

        {/* Facts */}
        <section styleName="facts">
          <div styleName="factsImgWrapper">
            {/* <img src={parallaxImg} width="1920" height="800" alt="facts" loading="lazy"/> */}
              <VideoPlayer
                playColor={"var(--color-medium-teal-blue)"}
                poster={videoPreview}
                videoUid={"E981N-7gmfA"}
              />
          </div>
          <div  className="section-m container-narrow">
            <h2 className="heading-3">Die Zukunft ist da, die Zukunft ist mobil</h2>
            <p className="section-text-200">Ganz gleich ob B2B oder B2C – mobile Commerce ist von der digitalen Bühne nicht mehr wegzudenken. Bevor wir also zu dem Punkt kommen, warum eine Progressive Web App die richtige Lösung für Ihr Unternehmen ist, erst einmal einige Fakten:</p>
          </div>
          <ParallaxEffect
            image={parallaxImg2}
            alt=""
            children={
              <div className="section container-narrow parallax-text">
                <p className="section-text-700">Nach einer Studie der <a href="https://initiatived21.de/" target="_blank" rel="noopener noreferrer">Initiative D21 e.V.</a> sind derzeit 88 Prozent der Deutschen online, 80 Prozent auch mobil. Die Internetnutzung via mobiler Endgeräte nähert sich kontinuierlich dem Niveau der allgemeinen Internetnutzung. Mittelfristig werden die beiden Werte gleichauf sein – wer das Internet nutzt, wird dann auch mobil online sein:</p>
              </div>
            }
          />

          <div styleName="line-chart-wrapper">
            <div className="section container-narrow" styleName="line-chart-container">
              <LineChartPWA />
            </div>
          </div>

          <div className="section-m container-narrow">
            <p className="section-text-200">Fast alle Deutschen besitzen ein Smartphone. Damit ergibt sich ein riesiger digitaler Markt voller Potenzial. Auch unsere eigenen Analysen bestätigen den Trend stetig wachsender Zahlen bei der mobilen Internetnutzung:</p>
          </div>
          <ParallaxEffect
            image={parallaxImg3}
            alt=""
            children={
              <div className="section container-narrow parallax-text" styleName="facts-traffic-stats">
                <h3 className="section-text-400">Traffic Verteilung nach Gerät</h3>
                <p>(watch.de Onlineshop, durchschn. 590 000 Nutzer pro Jahr)</p>
                <div styleName="factsStatsImgWrapper">
                  <img src={stats2}  width="1020" height="378" alt="Statistiken zur Nutzung des mobilen Internets" loading="lazy"/>
                </div>
              </div>
            }
          />

          <div className="section-m container-narrow">
            <p className="section-text-200">Für moderne Internetnutzer ist das mobile Internet bereits der neue Standard. Für Unternehmen sind das gute Nachrichten: Heute können sie ihre Kunden überall und jederzeit erreichen. Als Internetagentur aus Stuttgart helfen wir Ihnen gerne, das Potenzial der Digitalisierung voll auszuschöpfen.</p>
          </div>
        </section>

        {/* BQ Stats */}
        <section className="section-m">
          <h2 className="visuallyHidden">BQ (Statistics)</h2>
          <Article 
            title="Magento Webshop: Innovative E-Commerce Lösung"
            buttonUrl="/projekte/84-hershortt.html"
            buttonCaption="Fall zeigen"
            img={<img 
              src={imgArticle2}
              alt=""
              width={1920} 
              height={800} 
            />}
            color="var(--color-medium-teal-blue)"
          />
          <div className="container-narrow" styleName="bq-stats" ref={bqStatsRef}>
            <p className="section-text-200">Der allgemeine Trend zur mobilen Internetnutzung spiegelt sich auch im Online-Shopping wider, wie eine aktuelle <a href="https://www.adzine.de/2020/01/e-commerce-eine-von-drei-bestellungen-erfolgt-ueber-mobile/" target="_blank" rel="noopener noreferrer">Studie</a> des Bundesverbandes E-Commerce und Versandhandel e.V. (BEVH) zeigt:</p>
            <figure>
              <img src={stats4}  width="1152" height="640" alt="" loading="lazy"/>
            </figure>
          </div>
        </section>

        {/* Advantages */}
        <section className="section-m ">
          <h2 className="visuallyHidden">PWA-Vorteile</h2>
          <ParallaxEffect
            image={parallaxImg4}
            alt=""
            children={
              <div className="section container-narrow parallax-text">
                <b className="heading-2">Warum sollten Sie auf PWA setzen?</b>
              </div>
            }
          />
          <div styleName="advantages-color">
            <div className="section container-narrow">
              <p styleName="advantages-text">Progressive Web Apps sind ein bedeutender Fortschritt in der Entwicklung des mobilen Webs, den sich Unternehmen zunutze machen können. Das Verzeichnis <a href="https://pwa.bar/" target="_blank" rel="noopener noreferrer">pwa.bar</a> zeigt eine Auswahl der besten Progressive Web Apps. Unternehmen wie BMW und Starbucks setzen ebenso auf PWA wie der eingetragene Verein Tourismous NRW e.V. oder E-Commerce-Plattformen wie AliExpress und Petlove.</p>
            </div>
          </div>
          <div className="section-m container-narrow">
            <div className="grid-hr" styleName="advantages">
              <h3>Die Effizienz von PWA</h3>
              <Hr color="var(--color-medium-teal-blue)"/>
              <ul styleName="advantages-list" ref={advantagesRef}>
                <li>
                  <h4>2,75 Sekunden</h4>
                  <p>Die durchschnittliche Ladezeit für PWAs beträgt nur 2,75 Sekunden und ist damit achtmal schneller als bei einer durchschnittlichen mobilen Zielseite</p>
                  <Icon
                    name="timer"
                    figure="square"
                    color="var(--color-medium-teal-blue)"
                  />
                </li>
                <li>
                  <h4>42,86 Prozent</h4>
                  <p>Die durchschnittliche Absprungrate einer PWA ist um 42,86 Prozent niedriger als bei einer ähnlichen mobilen Website</p>
                  <Icon
                    name="search-text"
                    figure="circle"
                    color="var(--color-medium-teal-blue)"
                  />
                </li>
                <li>
                  <h4>um fast 80 Prozent</h4>
                  <p>Mobile Sitzungen auf PWAs werden um fast 80 Prozent erhöht</p>
                  <Icon
                    name="mobile-arrow-up"
                    figure="triangle-bottom"
                    color="var(--color-medium-teal-blue)"
                  />
                </li>
                <li>
                  <h4>um 137 Prozent</h4>
                  <p>Bei einigen Marken ist das Engagement insgesamt um 137 Prozent höher</p>
                  <Icon
                    name="mobile-and-desktop"
                    figure="triangle-top"
                    color="var(--color-medium-teal-blue)"
                  />
                </li>
              </ul>
            </div>
          </div>

          <div className="section-m" styleName="advantages-color advantages-color2">
            <div className="section container-narrow">
              <p className="section-text-700">Gleichzeitig haben PWAs einen Vorteil gegenüber herkömmlicher reaktionsfähiger Websites oder nativen Apps. Progressive Web Apps sind frei von einer Reihe technischer Einschränkungen, erfordern keine Updates und sind SEO-freundlich.</p>
              <p className="section-text-700" styleName="middleText">
                <a href="/blog/e-commerce-der-zukunft-warum-sind-pwa-das-werkzeug-der-wahl" target="_blank" rel="noopener noreferrer">E-Commerce der Zukunft: Warum sind PWA das Werkzeug der Wahl?</a>
              </p>
              <div styleName="btn-wrapper">
                <Link
                  to="/blog/e-commerce-der-zukunft-warum-sind-pwa-das-werkzeug-der-wahl"
                  className="button"
                  style={{
                  "--stroke": "var(--color-white)",
                  "backgroundColor": "var(--color-science-blue)"
                  }}
                >Jetzt lesen</Link>
              </div>
            </div>
          </div>
          <div className="section-m container-narrow" styleName="advantages-stats">
            <p className="section-text-200">Die hochflexible Webarchitektur der Progressive Web Apps ermöglicht schnellere Leistung und bessere Benutzererfahrung als eine klassische mobile Website und lässt sich zugleich kostengünstiger umsetzen, als  eine native App. Ermöglicht wird dies durch einen JavaSript-Code, den sogenannten “Service&nbsp;Worker”.</p>
            <figure>
              <img src={stats3}  width="467" height="467" alt="Statistiken zur Nutzung des mobilen Internets" loading="lazy"/>
            </figure>
          </div>
        </section>

        <section styleName="structure" ref={structureRef}>
          <h2 className="visuallyHidden">Anatomy of a Progressive Web App</h2>
          <div styleName="structureArticle">
            <Article 
              buttonUrl="/projekte/84-hershortt.html"
              buttonCaption="Fall zeigen"
              img={<img 
                src={parallaxImg8}
                alt=""
                width={1920} 
                height={800}
                loading="lazy"
              />}
              color="var(--color-science-blue)"
            />
          </div>
          <div  className="section-m container-narrow grid-hr">
            <b className="heading-3">Warum PWA? Auf einen Blick:</b>
            <Hr color="var(--color-medium-teal-blue)"/>
            <ListOrderUnorder
              isOrdered={true}
              icon={
                <Icon
                  name="checked-empty"
                  width="50px"
                  height="50px"
                  color="var(--color-medium-teal-blue)"
                />
              }
              textColor="var(--color-medium-teal-blue)"
              items={[
                <p><strong>Progressiv</strong> — PWAs funktionieren in jedem Browser, da sie mit der progressiven Verbesserung als Hauptprinzip erstellt wurden.</p>,
                <p><strong>Responsiv</strong> — PWAs funktionieren auf jedem Gerät: Desktop, Smartphone, Tablet und was noch kommen mag.</p>,
                <p><strong>Offline-Funktionalität</strong> — PWAs können im Offline-Modus arbeiten und werden im Hintergrund aktualisiert, sobald Internet verfügbar ist.</p>,
                <p><strong>App-ähnlich</strong> PWAs sind bezüglich Geschwindigkeit und Benutzererfahrung mit nativen Apps vergleichbar, aber kostengünstiger in der Entwicklung.</p>,
                <p><strong>Immer up-to-date</strong> — Bei PWAs sind keine Updates erforderlich, die über den App-Store geladen werden müssen.</p>,
                <p><strong>Sicher</strong> — PWAs werden immer über das sichere Übertragungsprotokoll HTTPS bereitgestellt.</p>,
                <p><strong>SEO-freundlich</strong> — PWAs sind über Suchmaschinen auffindbar wie eine klassische Website.</p>,
                <p><strong>Interaktiv</strong> — Über PWAs lassen sich beispielsweise Push-Benachrichtigungen direkt auf den Bildschirm Ihrer Kunden senden.</p>,
                <p><strong>Installierbar</strong>  — Benutzer können PWA wie eine native App zu ihrem Startbildschirm hinzufügen, ohne einen App-Store zu besuchen. So funktioniert der Einstieg in die Website mit einem einzigen Klick.</p>,
                <p><strong>Verlinkbar</strong> — Eine PWA ist genauso verlinkbar, wie eine Website. So lässt sie sich einfach über Social Media oder Google Ads verbreiten.</p>,
                <p><strong>Geringe Entwicklungskosten</strong> — Die PWA muss nur einmal entwickelt werden: Sie funktioniert auf Smartphones wie eine klassische App oder wird via Desktop als gewöhnlicher Onlineshop aufgerufen.</p>,
                <p><strong>Kurze Ladezeit</strong> —  Ist die PWA einmal installiert, punktet sie durch extrem kurze Ladezeiten. Während die Progressive Web App unbemerkt notwendige Daten synchronisiert, können Nutzer weiterarbeiten.</p>,
              ]}
            />
          </div>
          <div styleName="structure-color">
            <div className="section container-narrow">
              <p>All diese Faktoren führen zu einer guten User Experience und tragen damit zu einer besseren Kundenbindung, erhöhten Conversion Rate und Neukundengewinnung bei.</p>
            </div>
          </div>
        </section>

        {/* BQ */}
        <section>
          <h2 className="visuallyHidden">PWA mit Magento</h2>
          <ParallaxEffect
            image={parallaxImg5}
            alt=""
            children={
              <div className="section container-narrow parallax-text">
                <b className="heading-2">PWA mit Magento: Eine leistungsstarke Lösung für B2B und B2C</b>
              </div>
            }
          />
          <div className="section-m container-narrow">
            <p>Magento gehört zu den fortschrittlichen Softwareanbietern, die PWA bereits seit 2018 unterstützen. Beim Erstellen von PWA für unsere E-Commerce-Kunden greifen unsere Entwickler*innen auf das Magento PWA Studio zu. Dabei handelt es sich um eine umfassende Entwicklerplattform mit einer umfangreichen Suite von Tools zum Erstellen von Online-Shops.</p>
            <p>Die Kombination der leistungsfähigen Shopsoftware Magento mit der innovationen Technologie von Progressive Web Apps ist für Webshopbetreiber ein Schlüssel zum Erfolg. Dabei stehen Flexibilität, Skalierbarkeit und Kundenbindung im Vordergrund.</p>
          </div>
        </section>

        {/* Conclusion */}
        <ColorSteps 
          hiddenTitle="Fazit"
          parallax={
            <ParallaxEffect
              image={parallaxImg6}
              alt=""
              children={
                <p className="section-text-700 parallax-text container-narrow">Sowohl B2B- als auch B2C-Unternehmen profitieren PWA, hier noch einmal an einem kurzen Beispiel verdeutlicht</p>
              }
            />
          }
          bgColor={"var(--color-medium-teal-blue)"}
          textTop={
            <p className="section-text-200">Steigt die Ladezeit für mobile Seiten von einer auf fünf Sekunden, steigt die Wahrscheinlichkeit eines Absprungs um 90 Prozent – eine langsame Kaufabwicklung gefährdet also den Verkauf.</p>
          }
          textCenter={
            <p className="section-text-200">Die verbesserte Benutzererfahrung und die schnelle Seitengeschwindigkeit einer Progressive Web App ermöglichen schnelle und effiziente Kauferlebnisse. PWAs bieten zwei- bis viermal schnellere Seitengeschwindigkeiten.</p>
          }
          textBottom={
            <Fragment>
              <p className="section-text-200">So bleiben Ihnen bestehende Kunden treu und der Neukundengewinnung steht nichts mehr im Wege.</p>
              <p className="section-text-200">Die digitale Zukunft ist bereits da – gehen Sie die digitale Transformation richtig an: Mit innovativen Technologien, die Wachstum und Erfolg sicherstellen.</p>
            </Fragment>
          }
        />

        {/* Contacts */}
        <section styleName="about">
          <h2 className="visuallyHidden">Bleiben Sie mit uns in Kontakt</h2>
          <ParallaxEffect
            image={parallaxImg7}
            alt="Kunden, die uns vertrauen"
            children={
              <div className="section container-narrow parallax-text">
                <p className="section-text-700">Tandem Marketing &amp; Partners ist eine Magento-Agentur aus Stuttgart mit über 10 Jahren Erfahrung im Aufbau fortschrittlicher Weblösungen. <a href="/contact">Kontaktieren Sie uns</a>, um mehr über unsere Dienstleistungen zu erfahren und ein individuelles Angebot zu erhalten.</p>
              </div>
            }
          />
        </section>

      </main>

      <FeedbackHomePage />
      <Footer />
    </div>
  );
}
