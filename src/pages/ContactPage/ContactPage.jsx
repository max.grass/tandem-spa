import React from "react";
import { Link } from "react-router-dom";

import { Header } from "components/Header";
import { Hero } from "components/Hero";
import { HeroContact } from "components/Hero/Content"
import { PhoneNumber } from "components/PhoneNumber";
import { FeedbackForm } from "components/FeedbackForm";
import { ParallaxEffect } from "components/ParallaxEffect";
import { Addresses } from "components/Addresses";
// import { GoogleMap } from "components/GoogleMap";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from "assets/images/hero-contact.jpg";
import parallaxImage from "assets/images/contact-page-parallax.jpg";
import "./ContactPage.css";

export const ContactPage = () => {
  return (
    <div className="page-wrapper">
      <CommonMetaTags 
				title={'Kontakt / Impressum'}
				description={'Tandem Marketing'}
				url={'"https://www.tandem-m.de/werbeagentur-stuttgart/contact.html'}
				image={heroImg}
			/>

      <Header />

      <main>
        <h1 className="visuallyHidden">Kontaktiere uns</h1>
        
        <Hero
          src={heroImg}
          alt="Leute sitzen mit Laptops im Büro"
          width={1920}
          height={300}
          content={<HeroContact />}
        />

        <section className="section">
          <div className="container-narrow">
            <h2 className="visuallyHidden">Wie Sie uns erreichen</h2>
            <p styleName="text">
              Haben Sie Fragen, Anregungen, Bedarf an Informationsmaterial oder möchten ein Angebot anfordern? Dann rufen Sie uns an oder verwenden das Kontaktformular.
            </p>
            <PhoneNumber
              text="Telefonnummer:"
              phoneLink="tel:+49071194552157"
              phoneCaption="+49 (0) 711 9455215"
            />
            <FeedbackForm />
          </div>
        </section>

        <ParallaxEffect
          image={parallaxImage}
          alt="New Palace Stuttgart"
          strength={200}
          children={
            <div className="container-narrow">
              <p className="heading-2 parallax-text">Unsere<br/> Standorte</p>
            </div>
          }
        />

        <section styleName="adresses">
          <h2 className="visuallyHidden">So finden Sie uns</h2>
          <div styleName="adresses-container">
            <Addresses figureColor="#BFC0F9"/>
            {/* <GoogleMap /> */}
          </div>
        </section>

        <section styleName="terms">
          <h2 className="visuallyHidden">Geschäftsbedingungen</h2>
          <div className="container-narrow">
            <ul styleName="terms-list">
              <li>
                <h3 styleName="terms-title">Rechtliche Angaben:</h3>
                <p styleName="terms-text">
                  Umsatzsteuer-Identifikationsnummer gemäß &sect; 27 a Umsatzsteuergesetz: DE26732170
                </p>
                <p styleName="terms-text">
                  Inhaltlich Verantwortlicher gemäß TMG:<br />
                  Tandem Marketing &amp; Partners<br />
                  <a styleName="terms-link" href="mailto:info@tandem-m.de">info@tandem-m.de</a>
                </p>
              </li>

              <li>
                <h3 styleName="terms-title">Alle hier verwendeten Namen, Begriffe, Zeichen und Grafiken</h3>
                <p styleName="terms-text">
                  können Marken- oder Warenzeichen im Besitze ihrer rechtlichen Eigentümer sein. Die Rechte aller erwähnten und benutzten Marken- und Warenzeichen liegen ausschließlich bei deren Besitzern.
                </p>
              </li>

              <li>
                <h3 styleName="terms-title">Weitere rechtliche Angaben:</h3>
                <p styleName="terms-text">
                  <Link styleName="terms-link" to='/datenschutz.html'>
                    Datenschutzerklärung
                  </Link>
                  <br/>
                  <Link styleName="terms-link" to='/bildquellen.html'>
                    Bildquellen
                  </Link>
                </p>
              </li>
            </ul>
          </div>
        </section>

      </main>
      
      <Footer content="bottom" />
    </div>
  );
}
