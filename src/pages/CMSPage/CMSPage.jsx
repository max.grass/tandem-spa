import React, { Fragment } from "react";
import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { ColorSteps } from "components/ColorSteps";
import { ParallaxEffect } from "components/ParallaxEffect";
import { Article } from "components/Article";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from "assets/images/hero-cms.jpg";
import advantagesImg from "assets/images/cms-watch-de.jpg";
import cmsArticleImg from "assets/images/cms-article.jpg";
import cmsParallaxImg from "assets/images/cms-parallax.jpg";
import magentoParallaxImg from "assets/images/magento-orange-girl.jpg";
import magentoInfographicImg from "assets/images/magento-infographic.svg";
import drupalArticleImg from "assets/images/drupal-notebook.jpg";
import drupalParallaxImg from "assets/images/drupal-notebook-parallax.jpg";
import wordpressArticleImg from "assets/images/wordpress-notebook.jpg";
import wordpressParallaxImg from "assets/images/wordpress-parallax.jpg";
import cmsContactsParallaxImg from "assets/images/cms-contacts-parallax.jpg";

import "./CMSPage.css";


export const CMSPage = () => {
  return (
    <div className="page-wrapper">
      <CommonMetaTags 
				title={'Content Management System von kompetenten Partner aus Stuttgart'}
				description={'Wir sind Ihr kompetenter Partner für Content Management Systeme, Webdesign und digitale Medien'}
				url={'https://www.tandem-m.de/webdesign-cms/content-management-system.html'}
				image={heroImg}
			/>

      <Header />

      <main>
        <Hero
          src={heroImg}
          heght={500}
          content={
            <HeroContent 
              title={<span>Den kompletten Service<br/> rund um CMS</span>}
              text="Content Management System"
            />
          }
        />

        <SectionWhite 
          title="Content-Management-System: So bleibt Ihre Website aktuell"
          text="Sie möchten die Inhalte Ihrer Internetpräsenz effizient verwalten?"
        />

        <SectionColor 
          content={
            <p className="section-text-200">Mit einem Content-Management-System (CMS) können Sie Text, Bilder oder Videos auf Ihrer Website selbstständig anpassen. Als Webagentur mit mehr als 10 Jahren Erfahrung, beraten wir Sie gerne rund um CMS.</p>
          }
        />

        <div>
          <ColorSteps 
            hiddenTitle="Vorteile von CMS"
            parallax={
              <ParallaxEffect
                image={advantagesImg}
                alt="Projekt watch.de auf CMS"
                children={
                  <div className="container">
                    <b className="heading-2 parallax-text">
                      Was sind CMS und worin liegen die Vorteile?
                    </b>
                  </div>
                }
              />
            }
            bgColor={"var(--color-scarlet-gum)"}
            textTop={"Ein Content-Management-System ist eine Software zur Erstellung von Websites, die auf der Trennung von Inhalt (Content) und Struktur basiert. Hier grenzt es sich vom rein statischen HTML ab, das Content und Design zusammenfasst. Wer sich für ein modernes CMS entscheidet, kann Inhalt und Navigation seiner Website selbstständig verwalten – ohne Programmier- oder HTML-Kenntnisse. "}
            textCenter={"Die nutzerfreundliche Anwendung und die Tatsache, dass Content und Layout unabhängig voneinander verwaltet werden, sind die großen Vorteile der CMS."}
            textBottom={
              <Fragment>
                <p className="section-text-200">Denn aktuelle Inhalte bieten echten Mehrwert für Ihre Nutzer und werden von Suchmaschinen mit besseren Positionen belohnt.</p>
                <p className="section-text-200">CMS ermöglichen unterschiedliche Zugriffsrechte, das heißt, dass mehrere Anwender unabhängig voneinander Zugriff auf den geschützten Administrationsbereich (Backend) erhalten . Als Frontend („Vorderansicht“) bezeichnet man das Erscheinungsbild einer Webseite, wie es dem Besucher angezeigt wird.</p>
              </Fragment>
            }
          />
        </div>

        <section>
          <h2 className="visuallyHidden">Über CMS</h2>
          <Article 
            title="Varianten von CMS"
            content={
              <div className="section-m" styleName="variants">
                <p>Mittlerweile existieren zahlreiche Content-Management-Systeme mit unterschiedlichen Anwendungsfeldern von klassischen Websites über Blogs bis hin zu Social Communities.</p>
              </div>
            }
            buttonUrl="/referenzen.html"
            buttonCaption="Fälle zeigen"
            img={<img 
              src={cmsArticleImg}
              alt="pps-vertrieb.de - Website auf CMS Drupal"
              width={1920} 
              height={800}
              loading="lazy"
            />}
            color="var(--color-сurious-blue)"
          />

          <ParallaxEffect
						image={cmsParallaxImg}
						alt="Wir sind Profis auf unserem Gebiet"
						children={
							<div className="container-narrow">
								<b className="section section-text-700 parallax-text" >
                  Inhalt und Ziel Ihres Webprojekts bestimmen, welches CMS sich am besten für Ihre Anforderungen eignet.
								</b>
							</div>
						}
					/>

          <div className="section container">
            <p className="section-text-200">Viele der weltweit am meisten genutzten CMS sind Open-Source, das heißt der Quellcode ist frei zugänglich. Beispiele für Open-Source-Angebote sind Drupal, Magento, Wordpress oder Typo3, deren professionelle Templates und Module sich schon längst für anspruchsvolle Internetpräsenzen eignen.</p>
            <p className="section-text-200">Daneben gibt es sogenannte proprietäre CMS, die von einem bestimmten Unternehmen entwickelt und über kostenpflichtige Lizenzen vertrieben werden. Für viele Unternehmen sind Open-Source-Lösungen schon allein deswegen attraktiver, weil keine Lizenzkosten anfallen.</p>
          </div>

          <SectionColor 
            content={
              <p className="section-text-200">In Bezug auf die Technik sind Open-Source- und proprietäre CMS gleichauf: Bei etablierten Systemen wie Magento oder Drupal sorgen starke Entwickler-Communities für hohes Niveau. Ein weiterer Vorteil von Software mit frei zugänglichem Quellcode ist, dass Sicherheitslücken und Bugs schnellstmöglich aufgedeckt und entfernt werden.</p>
            }
            backgroundColor="var(--color-сurious-blue)"
          />

          <div className="section container">
            <p className="section-text-200">Im folgenden stellen wir Ihnen drei CMS vor, die die zentralen Anwendungsfelder von Content-Management-Systemen abdecken: Drupal für Web-Content-Management und Social Publishing/Communities, Wordpress für Blog-Publishing und News sowie Magento, eine der beliebtesten quelloffenen Webshop-Lösungen.</p>
          </div>
        </section>

        <ColorSteps 
          hiddenTitle="Über Magento CMS"
					parallax={
						<ParallaxEffect
							image={magentoParallaxImg}
							alt="Projekt watch.de auf CMS"
							children={
								<div className="container">
									<b className="heading-2 parallax-text">Magento</b>
								</div>
							}
						/>
					}
          bgColor={"var(--color-flamingo)"}
					textTop={"Magento ist ein innovatives E-Commerce-System, das neue Maßstäbe in Bezug auf Flexibilität, Skalierbarkeit und Sicherheit setzt. Mit der Unterstützung einer großen internationalen Gemeinschaft hat sich Magento zu einer zuverlässigen CMS-Lösung entwickelt, die auf alle spezifischen E-Commerce-Anforderungen zugeschnitten werden kann."}
					textCenter={"Laut der EHI-Statista-Studie von 2020 führt Magento die Liste der Top-1.000-Onlineshops in Deutschland an: "}
          textBottom={
            <Fragment>
              <div styleName="infographicImgWrapper">
                <img src={magentoInfographicImg} width={1011} height={401} alt="Online-Shops auf Magento führen die Top 1000 in Deutschland an." loading="lazy"/>
              </div>
              <p className="section-text-200">Bei Magento haben Sie durch den Einsatz neuester Technologien immer volle Kontrolle über das Erscheinungsbild, den Inhalt und die Funktionalität Ihres Online-Shops. Umfangreiche E-Commerce- und Berichtsfunktionen ermöglichen den Aufbau erfolgreicher Verkaufs- und Marketingprozesse für Ihr Unternehmen.</p>
            </Fragment>
          }
				/>

        <section>
          <h2 className="visuallyHidden">Über Drupal CMS</h2>
          <Article 
            title="Drupal"
            content={
              <Fragment>
                <p>Anwender ohne Vorkenntnisse wissen die simple Bedienung dieses Open Source Enterprise-CMS zu schätzen. Drupal eignet sich für zahlreiche Projekte von einfachen Web-Visitenkarten bis hin zu Firmenportalen mit eigenem Intranet. Designern garantiert Drupal volle Layout-Kontrolle zur Verwirklichung kreativer Ideen. Das flexible Template-System unterstützt alle modernen Techniken und ist bereit für HTML5.</p>
              </Fragment>
            }
            buttonUrl="/referenzen.html"
            buttonCaption="Fälle zeigen"
            img={
              <img 
                src={drupalArticleImg}
                alt="Drupal-Content-Management-System auf Laptop-Bildschirm"
                width={1920} 
                height={800}
                loading="lazy"
              />}
            color="var(--color-сurious-blue)"
          />

          <ParallaxEffect
						image={drupalParallaxImg}
						alt="Die Verwaltung von Inhalten über das CMS Drupal ist einfach"
						children={
							<div className="container-narrow">
								<b className="section section-text-700 parallax-text">Als besonders anwenderfreundlich gilt Drupal, weil sich Inhalte direkt aus der Seite heraus bearbeiten lassen. Ein weiteres Plus ist die umfangreiche Dokumentation in verschiedenen Sprachen sowie der solide Support durch die Drupal Community.</b>
							</div>
						}
					/>

          <div className="section container">
            <p className="section-text-200" style={{ marginBottom: 0 }}>Drupal ging 2001 an den Markt und wird mittlerweile bei unterschiedlichsten Websites weltweit eingesetzt. So wurde beispielsweise die Onlinepräsenz des Duden mit Drupal aufgebaut. Die Leser-Kommentare auf der Website der Wochenzeitung Die Zeit laufen ebenfalls über Drupal und selbst das Weiße Haus setzte in den Jahren 2009 bis 2017 auf die Open-Source-Software.</p>
          </div>
        </section>

        <section>
          <h2 className="visuallyHidden">Über WordPress-CMS</h2>
          <Article 
            title="WordPress"
            content={
              <Fragment>
                <p>WordPress ist die weltweit am häufigsten genutzte Open-Source-Software und bietet sich insbesondere für kleine, einfach strukturierte Webauftritte an, beispielsweise Blogs. In dem CMS mit intuitiv nutzbarer Weboberfläche lassen sich Beiträge einfach kategorisieren und bei Bedarf unmittelbar in die Navigation aufnehmen.</p>
              </Fragment>
            }
            buttonUrl="/referenzen.html"
            buttonCaption="Fälle zeigen"
            img={
              <img 
                src={wordpressArticleImg}
                alt="WordPress-Content-Management-System auf Laptop-Bildschirm"
                width={1920} 
                height={800}
                loading="lazy"
              />}
            color="var(--color-masala)"
          />

          <ParallaxEffect
						image={wordpressParallaxImg}
						alt="Zwei Männer an einem Laptop"
						children={
							<div className="container-narrow">
								<b className="section section-text-700 parallax-text">WordPress ermöglicht Leserkommentare, zentrale Linkverwaltung, die Verwaltung von Benutzerrollen und -rechten sowie die Möglichkeit externer Plug-ins.</b>
							</div>
						}
					/>

          <div className="section container">
            <p className="section-text-200" style={{ marginBottom: 0 }}>Komplexe Web-Projekte, die beispielsweise Mehrsprachigkeit erfordern, lassen sich allerdings besser – und sicherer – mit CMS wie Drupal umsetzen.</p>
          </div>
        </section>

        <ColorSteps 
          hiddenTitle="Wir helfen Ihnen bei der Auswahl eines CMS"
					parallax={
						<ParallaxEffect
							image={cmsContactsParallaxImg}
							alt="Projekt watch.de auf CMS"
							children={
								<div className="container">
									<b className="heading-2 parallax-text">Wie wählen Sie das richtige CMS?</b>
								</div>
							}
						/>
					}
					textTop={"Als erfahrene Webagentur haben wir bereits mit vielen verschiedenen Content-Management-Systemen gearbeitet und beraten Sie gerne persönlich. Gemeinsam ergründen wir Ihre Unternehmensziele und entwickeln daraus eine Strategie. Welches CMS sich am besten für Sie eignet, hängt unter anderem von der Firmengröße, dem erwarteten Traffic sowie gewünschten Funktionen ab."}
					textCenter={"Viele unserer Kunden vertrauen auf die oben beschriebenen umfangreichen und qualitativ hochwertigen "}
          textBottom={
            <Fragment>
              <p className="section-text-200">Open-Source-Systeme, die allesamt eine einfache Verwaltung von Inhalten ermöglichen. Open-Source-CMS eignen sich für Unternehmen mit hohen Ansprüchen an die Technologie ebenso gut wie für Anwender, die nach einer einfachen Lösung suchen.</p>
              <p className="section-text-200">Tandem Marketing &amp; Partners ist eine Internetagentur aus Stuttgart, spezialisiert auf die Bereiche Online Marketing, Social Media und Webentwicklung. Wenn Sie auf dem Weg zur Digitalisierung Ihres Unternehmens sind, stehen wir Ihnen gerne mit unserem Know How zur Seite.</p>
            </Fragment>
          }
				/>
      </main>
      <FeedbackHomePage />
      <Footer />
    </div>
  );
}
