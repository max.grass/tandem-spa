import React, { Fragment } from "react";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { ParallaxEffect } from "components/ParallaxEffect";
import { Article } from "components/Article";
import { CheckedList } from "components/CheckedList";
import { ColorSteps } from "components/ColorSteps";
import { AboutPageStrengths } from "components/AboutPage-OurStrengths";
import { data as RelaunchPageStrengthsData } from "components/AboutPage-OurStrengths/RelaunchPageStrengthsData";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from "assets/images/hero-relaunch.jpg";
import parallaxHershortt from "assets/images/parallax-relaunch-1.jpg";
import parallax2 from "assets/images/parallax-relaunch-02.jpg";
import parallax3 from "assets/images/parallax-relaunch-03.jpg";
import parallax4 from "assets/images/parallax-relaunch-04.jpg";
import parallax5 from "assets/images/parallax-relaunch-05.jpg";
import parallax6 from "assets/images/parallax-relaunch-06.jpg";
import parallaxK from "assets/images/relaunch-parallax-k.jpg";

import scheme1 from "assets/images/webrelaunch-01.svg";
import scheme2 from "assets/images/webrelaunch-02.svg";


import "./RelaunchPage.css";

export const RelaunchPage = () => {

	return (
		<div className='page-wrapper'>
			<CommonMetaTags 
				title={'Website relaunch'}
				description={'Tandem Marketing'}
				url={'https://www.tandem-m.de/website-relaunch.html'}
				image={heroImg}
			/>
			
			<Header />
			
			<main>
				<Hero
					src={heroImg}
					alt="Website Relaunch"
					width={1920}
					height={800}
					content={
						<HeroContent 
							title={<span>In 10 Schritten zu einer modernen Website</span>}
							text="Website Relaunch"
						/>
					}
				/>

				<section>
					<h2 className="visuallyHidden">Website Relaunch</h2>
					<SectionWhite 
						title="Wann ist ein Website Relaunch nötig?"
						text="Wenn die eigene Website in die Jahre gekommen ist und weder die eigenen Ansprüche noch die der Zielgruppe erfüllen kann."
						textColor="var(--color-scarlet-gum)"
					/>
					<div>
						<SectionColor 
							content={
								<Fragment>
									<p className="section-text-200">Mit über zehn Jahren Erfahrung in der Webentwicklung, führen wir Sie in zehn Schritten durch den Website Relaunch: Vom ersten Wunsch nach Neugestaltung zu einer funktionsfähigen und modernen Website.</p>
								</Fragment>
							}
							textColor="var(--color-white)"
							backgroundColor="var(--color-scarlet-gum)"
						/>
					</div>                    
                    <div className="empty-parallax">
						<ParallaxEffect
							image={parallaxHershortt}
							alt="Tandem Marketing & Partners"
						/>
					</div>
				</section>

                <section className="section" styleName="graph-wrp">
                    <div className="container">
                        <img src={scheme1} alt="Website Relaunch Scheme" loading="lazy"/>
                    </div>
                </section>

                <section>
                    <ParallaxEffect
                        image={parallax2}
                        alt="Tandem Marketing & Partners"
                        children={
                            <div className="container">
                                <b className="heading-2 parallax-text">
                                    1. Vorbereitung
                                </b>
                            </div>
                        }
                    />
                    <SectionColor 
                        content={
                            <Fragment>
                                <p className="section-text-200">Die erste und wichtigste Phase beim Website Relaunch ist die Analyse der bestehenden Website. In dieser Phase, sollte man einen Soll-Ist-Abgleich durchführen, d.h., den aktuellen Stand beurteilen und überlegen, was verbessert werden muss.</p>
                            </Fragment>
                        }
                        textColor="var(--color-white)"
                        backgroundColor="var(--color-scarlet-gum)"
                    />
                </section>

                <section>
                    <AboutPageStrengths
                        header={'Website Audit, Soll-Ist-Abgleich'}
                        data={RelaunchPageStrengthsData}
                        hrColor={"var(--color-scarlet-gum)"}
                    />
                </section>

				<section>
					<Article 
						buttonUrl="/referenzen.html"
						buttonCaption="Fälle zeigen"
						img={<img 
							src={parallaxK}
							alt="Website Relaunch"
							width={1920} 
							height={800}
							loading="lazy"
						/>}
						color="var(--color-scarlet-gum)"
					/>
					<SectionColor 
                        content={
                            <Fragment>
								<p className="section-text-700">Erstens sollte die bestehende Website inklusive ihrer Vor- und Nachteile geprüft werden.</p>
								<ul className="section-text-200 simple-list">
									<li>Sieht das Design veraltet aus?</li>
									<li>Vielleicht ist die Website-Struktur nicht effizient oder entspricht nicht Ihren Geschäftszielen?</li>
									<li>Müssen Sie Ihre Corporate Identity überarbeiten?</li>
									<li>Welche Funktionen sind gut und sollten erhalten bleiben?</li>
								</ul>
                                <p className="section-text-200">Sehen Sie sich am besten auch die Websites Ihrer Mitbewerber an und überlegen, was Ihnen daran gefällt.</p>
								<p className="section-text-700">Zweitens sollte die Zielgruppe analysiert werden.</p>
								<ul className="section-text-200 simple-list">
									<li>Wer ist Ihre Zielgruppe in Bezug auf Alter, Geschlecht, Standort? Welche Interessen haben sie?</li>
									<li>Welche Technologien und Geräte nutzen Ihre Besucher?</li>
									<li>Wie verhalten sie sich auf der Website? Welcher Inhalt ist am beliebtesten? Welche Seiten haben eine hohe Absprungrate und warum?</li>
								</ul>
								<p className="section-text-200">Antworten auf diese grundlegenden Fragen geben Google Analytics oder ähnliche Tools. </p>
								<p className="section-text-700">Drittens sollten die objektiven Zahlen analysiert werden.</p>
								<ul className="section-text-200 simple-list">
									<li>Wie hoch ist Ihr aktueller Website-Traffic?</li>
									<li>Wie ist die Leistung der aktuellen Website laut Google Search Console?</li>
									<li>Wie ranken Ihre Keywords? Welche Keywords sind am beliebtesten und bringen die meisten potenziellen Kunden auf Ihre Website?</li>
								</ul>
								<p className="section-text-200">Es gibt eine Reihe leistungsstarker Dienste wie SEMRush oder SE Ranking, mit denen sich Ihre Positionen in der organischen Suche verstehen und überwachen lassen.</p>
								<p className="section-text-700">Schließlich geht es um den technischen Zustand der Website.</p>
								<ul className="section-text-200 simple-list">
									<li>Ist Ihr aktueller Server schnell und leistungsstark genug?</li>
									<li>Ist Ihr Content Management System (CMS) modern, sicher und ermöglicht eine effiziente SEO-Optimierung?</li>
								</ul>
								<p className="section-text-200">Die technischen Aspekte beeinflussen die Ladegeschwindigkeit, die einer der wichtigsten Faktoren für eine gute Website ist. Wenn die Ladezeit zu lang ist, springen Ihre Besucher ab.</p>
								<p className="section-text-200">Zusammenfassend muss eine moderne Website schnell, sicher und in Bezug auf Struktur und Web Design auf dem neuesten Stand sein.</p>
                            </Fragment>
                        }
                        textColor="var(--color-dark)"
                        backgroundColor="var(--color-white)"
                    />
				</section>

                <section className="section" styleName="graph-wrp" style={{ backgroundColor: "var(--color-scarlet-gum)"}}>
                    <div className="container">
                        <img src={scheme2} alt="Website Relaunch Scheme" loading="lazy"/>
                    </div>
                </section>

				<section>
					<SectionColor 
                        content={
                            <Fragment>
								<p className="section-text-700">2 Einrichtung, Zielsetzung</p>
                                <p className="section-text-200">Nach der ersten Phase haben wir ein Verständnis des aktuellen Zustands und eine klare Vorstellung davon, was verbessert werden sollte.</p>
								<p className="section-text-200">Im nächsten Schritt sollten Ziele und KPIs festgelegt werden: Nach dem Website Relaunch möchten Sie Ihre Positionen in der organischen Suche vermutlich beibehalten oder sogar verbessern und Ihren Traffic steigern. Ein anderes Ziel wäre, die Reichweite Ihres Publikums zu erweitern.</p>
                            </Fragment>
                        }
                        textColor="var(--color-dark)"
                        backgroundColor="var(--color-white)"
                    />
					<SectionColor 
                        content={
                            <Fragment>
								<p className="section-text-200">Eine wichtige Entscheidung im Rahmen eines Website Relaunches ist, die zu verwendende Technologie. Eine Internetagentur kann Ihnen je nach Budget und Geschäftszielen mehrere Optionen anbieten.</p>
                            </Fragment>
                        }
                        textColor="var(--color-white)"
                        backgroundColor="var(--color-scarlet-gum)"
                    />
				</section>

				<section>
					<Article 
						title="3. Prototyping"
						content={
							<Fragment>
								<p>Vor dem eigentlichen Website Redesign sollte ein Prototyp erstellt sowie die Struktur und Funktionalität der Elemente festgelegt werden.</p>
								<p>Im Anschluss entsteht das moderne Webdesign, natürlich unter Berücksichtigung aktueller Webdesign Trends und einer optimalen User Experience (kurz: UX, deutsch: Nutzungserlebnis), auch auf mobilen Geräten.  Basis für die gestalterische Umsetzung ist das Corporate Design Handbuch des Kunden.</p>
							</Fragment>
						}
						buttonUrl="/projekte/48-mr-gruppe.html"
						buttonCaption="Fall zeigen"
						img={<img 
							src={parallax4}
							alt="Website Relaunch"
							width={1920} 
							height={800}
							loading="lazy"
						/>}
						color="var(--color-scarlet-gum)"
					/>
					<div style={{ textAlign: "center"}}>
						<SectionColor 
							content={
								<Fragment>
									<p className="section-text-700">Eine finale Überprüfung der UX stellt sicher, dass alle Blöcke funktionsfähig, einfach zu bedienen und zielführend sind.</p>
								</Fragment>
							}
							textColor="var(--color-white)"
							backgroundColor="var(--color-scarlet-gum)"
						/>
					</div>
				</section>

				<section>
					<SectionColor 
                        content={
                            <Fragment>
								<p className="section-text-700">4. Freigabe</p>
                                <p className="section-text-200">In dieser Phase wird dem Kunden das endgültige Design- und Relaunch-Konzept präsentiert. Wir besprechen mögliche Verbesserungen und Ziele für die gesamte Website, um sicher zu gehen, dass alle an einem Strang ziehen. Nach der Freigabe beginnt die technische Implementierung (HTML CSS). </p>
                            </Fragment>
                        }
                        textColor="var(--color-dark)"
                        backgroundColor="var(--color-white)"
                    />
				</section>

				<section>
					<ColorSteps
						hiddenTitle="Website Relaunch"
						parallax={
							<ParallaxEffect
								image={parallax3}
								alt="Tandem Marketing & Partners"
								children={
									<div className="container">
										<b className="heading-2 parallax-text" >
                                            5. Implementierung +<br />
                                            6. Überarbeitung des Inhalts
										</b>
									</div>
								}
							/>
						}
						textTop={"Der sichtbare Teil der Website (Frontend) und ihre technische Architektur (Backend) werden parallel entwickelt. Zugleich beginnen wir mit der inhaltlichen Arbeit: Bestehende Texte müssen normalerweise verbessert und erweitert werden, zum Beispiel indem Meta-Tags und Meta-Beschreibungen hinzugefügt werden."}
						textCenter={"Über spezielle Landing Pages oder Call to Actions kann die Zielgruppe stärker einbezogen werden."}
						textBottom={"Doch auch visuelle Elemente wie Bilder und Videos werden geprüft. Um Website Besuchern eine optimales Nutzererlebnis zu ermöglichen, sollten Frontend-Struktur, Texte und visuelle Inhalte eine harmonische Einheit bilden."}
						bgColor={"var(--color-scarlet-gum)"}
					/>
				</section>


				<section>
					<ParallaxEffect
                        image={parallax6}
                        alt="Tandem Marketing & Partners"
                        children={
                            <div className="container">
                                <b className="heading-2 parallax-text">
									7. Inhaltsintegration
                                </b>
                            </div>
                        }
                    />
					<SectionColor
                        content={
                            <Fragment>
                                <p className="section-text-200">Wenn sowohl Front- als auch Backend fertig sind, füllen wir die Website mit Inhalten. Bei einer veränderten Struktur ist es besonders wichtig, Weiterleitungen einzurichten. So lassen sich 404 Fehler, eine erhöhte Absprungrate und damit eine Verschlechterung der Positionen in der organischen Suche <b>effektiv vermeiden</b>.</p>
                            </Fragment>
                        }
                        textColor="var(--color-white)"
                        backgroundColor="var(--color-scarlet-gum)"
                    />
				</section>

				<section className="section" styleName="nomgl-list">
					<div className="container-narrow">
						<p className="section-text-700">8. Testen</p>
						<p className="section-text-200">Wenn alles fertig ist, sind wir fast bereit, live zu gehen. Aber zuerst führen wir die abschließenden Tests durch:</p>					
						<CheckedList 
							content={
								<Fragment>
									<li>Qualitätskontrolle von Inhalten, um mögliche Fehler zu beseitigen</li>
									<li>Testen von Kontakt und anderen Formularen</li>
									<li>Testen anderer Funktionselemente, um sicherzustellen, dass alles funktionsfähig und benutzerfreundlich ist</li>
									<li>Überprüfung aller Links, auch externe</li>
								</Fragment>
							}
							color="var(--color-scarlet-gum)"
						/>
					</div>
				</section>

				<section>
					<div className="empty-parallax">
						<ParallaxEffect
							image={parallax5}
							alt="Tandem Marketing & Partners"
						/>
					</div>
					<SectionColor
                        content={
                            <Fragment>
								<p className="section-text-700">9 Live gehen +<br/>10 Monitoring</p>
                                <p className="section-text-200">Die neue Website wird von der Entwicklungsumgebung auf den Live-Server übertragen.</p>
								<p className="section-text-200">Wir überprüfen die Funktionalität noch einmal. Anschließend beginnt die Überwachung der neuen Website, um sicherzustellen, dass wir die Ziele erreichen, die in der Phase der Zielsetzung festgelegt wurden. Mithilfe von Analysetools wie Google Analytics oder der Search-Console können wir uns ein umfassendes Bild über die Leistung der Website machen und mögliche Probleme rechtzeitig erkennen und lösen.</p>
                            </Fragment>
                        }
                        textColor="var(--color-dark)"
                        backgroundColor="var(--color-white)"
                    />
					<SectionColor
                        content={
                            <Fragment>
								<p className="section-text-200">Ihre Ziele und unser Fachwissen in den Bereichen Webentwicklung und SEO, sind die besten Zutaten für einen erfolgreichen Website Relaunch. Kontaktieren Sie uns gerne per E-Mail oder über das Kontaktformular für eine erste Beratung. Gemeinsam mit Tandem Marketing & Partners halten Sie mit den Internet-Trends Schritt.</p>
                            </Fragment>
                        }
                        textColor="var(--color-white)"
                        backgroundColor="var(--color-scarlet-gum)"
                    />
				</section>

				<FeedbackHomePage />

			</main>
			
			<Footer />
		</div>
	);
}
