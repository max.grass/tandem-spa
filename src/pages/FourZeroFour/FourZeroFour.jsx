import React, { Fragment } from "react";
import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { Blog } from 'components/Blog';
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from "assets/images/hero-fourzerofour.jpg";

export const FourZeroFour = () => {
  return (
    <div className="page-wrapper">
      <CommonMetaTags 
				title={'404 - Fehler: 404'}
				description={'Tandem Marketing'}
				// url={url}
				image={heroImg}
			/>

      <Header />

      <main>
        <Hero
          src={heroImg}
          heght={500}
          content={
            <HeroContent 
              title={<span>Oops! An Error Occurred</span>}
              text='The server returned a "404 Not Found".'
            />
          }
        />

        <Blog />
      </main>
      <Footer />
    </div>
  );
}
