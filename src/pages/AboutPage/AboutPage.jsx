import React, { Fragment, useRef, useEffect, useLayoutEffect, useState } from "react";
import gsap from 'gsap';
import { Link } from 'react-router-dom';

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { ImageList } from "components/ImageList";
import { ImageListTween } from "components/ImageList";
import { SectionWhite } from "components/SectionWhite";
// import { ColorSteps } from "components/ColorSteps";
import { SectionColor } from "components/SectionColor";
import { ParallaxEffect } from "components/ParallaxEffect";
import { Worktable } from "components/Worktable";
// import { AboutPageStrengths } from "components/AboutPage-OurStrengths";
// import { data as AboutPageStrengthsData } from "components/AboutPage-OurStrengths/AboutPageStrenghsData";
import { SourceAboutUs } from "components/SourceAboutUs";
import { AccordionTabs } from 'components/AccordionTabs';
// import { Article } from "components/Article";
// import { Team } from "components/Team";
import { Reviews } from "components/Reviews";
import { Blog } from "components/Blog";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
// import {Helmet} from "react-helmet";
import Slider from 'react-slick';
import { CommonMetaTags } from 'components/CommonMetaTags'


import heroImg from "assets/images/hero-about.jpg?as=webp";
// import parallaxImageStrengths from "assets/images/about-page-parallax-strengths.jpg";
// import parallaxImageServices from "assets/images/about-page-parallax-services.jpg";
import parallaxImageTrust from "assets/images/about-page-parallax-trust.jpg";
import parallaxImageUs from "assets/images/about-page-parallax-why-us.jpg";
// import parallaxImageResearch from "assets/images/about-page-parallax-research.jpg";
// import parallaxImageWatch from "assets/images/about-parallax-watch.jpg";
// import parallaxImage1 from "assets/images/about-page-parallax-1.jpg";
// import parallaxImageMettler from "assets/images/about-page-parallax-mettler.jpg";
// import parallaxImageWebdesign from "assets/images/about-page-parallax-webdesign.jpg";
// import parallaxImageEntwicklung from "assets/images/about-page-parallax-entwicklung.jpg";
// import parallaxImagePPS from "assets/images/about-page-parallax-pps.jpg";

import logoTesa from 'assets/images/logo-tesa-b.svg';
import logoWatchDe from 'assets/images/logo-watch-de-b.svg';
import logoRitterSport from 'assets/images/logo-ritter-sport-b.svg';
import logoDdsa from 'assets/images/logo-ddsa-b.svg';
import logoSolarWinds from 'assets/images/logo-solarwinds-b.svg';
import logoMyleDe from 'assets/images/logo-myle-de-b.svg';
import logoMettlers from 'assets/images/logo-mettler-b.svg';
import logoGailing from 'assets/images/logo-gailing-b.svg';
import logoHershortt from 'assets/images/logo-hershortt-b.svg';
import logoPps from 'assets/images/logo-pps-b.svg';
import logoKruschina from 'assets/images/logo-kruschina-b.svg';
import logoTabacum from 'assets/images/logo-tabacum-b.svg';


import '../../assets/slick/slick.css';
import '../../assets/slick/slick-theme.css';
import "./AboutPage.css";

export const AboutPage = () => {
  const trustRef = useRef();
  const trustSelector = gsap.utils.selector(trustRef);

  const [isMobile, setMobile] = useState(window.innerWidth < 1024);
  useLayoutEffect(() => {
    function updateSize() {
      setMobile(window.innerWidth < 1024);
    }
    window.addEventListener('resize', updateSize);
    return () => window.removeEventListener('resize', updateSize);
  }, []);

  const images = [
    {
      src: logoTesa,
      alt: "Tesa Logo",
      width: 170,
      loading: "lazy"
    },
    {
      src: logoWatchDe,
      alt: "Watch.de Logo",
      width: 138,
      loading: "lazy"
    },
    {
      src: logoRitterSport,
      alt: "Ritter Sport Logo",
      width: 84,
      loading: "lazy"
    },
    {
      src: logoDdsa,
      alt: "Die Deutsche Schulakademie Logo",
      width: 226,
      loading: "lazy"
    },
    {
      src: logoMyleDe,
      alt: "MyleDe Logo",
      width: 157,
      loading: "lazy"
    },
    {
      src: logoSolarWinds,
      alt: "SolarWinds Logo",
      width: 210,
      loading: "lazy"
    },
    {
      src: logoTabacum,
      alt: "Tabacum Logo",
      width: 152,
      loading: "lazy"
    },
    {
      src: logoGailing,
      alt: "Gailing Logo",
      width: 174,
      loading: "lazy"
    },
    {
      src: logoMettlers,
      alt: "Mettlers Logo",
      width: 236,
      loading: "lazy"
    },
    {
      src: logoHershortt,
      alt: "Hershortt Logo",
      width: 198,
      loading: "lazy"
    },
    {
      src: logoPps,
      alt: "PPS Logo",
      width: 133,
      loading: "lazy"
    },
    {
      src: logoKruschina,
      alt: "Kruschina Logo",
      width: 144,
      loading: "lazy"
    }
  ];

  const settings = {
    arrows: false,
    dots: false,
    infinite: false,
    variableWidth: true,
    slidesToScroll: 2
  };

  useEffect(() => {
    let tl = gsap.timeline();

    tl.add(ImageListTween(trustSelector("li"), trustRef.current));
  }, []);

  return (
    <div className='page-wrapper'>
      <CommonMetaTags 
				title={'Werbeagentur Stuttgart - Wir fördern Ihre Kreativität'}
				description={'Werbeagentur Stuttgart - Sie wollen sich von der Masse abheben? Wir schaffen einen hohen Wiedererkennungswert Ihrer Marke im Internet'}
				url={'https://www.tandem-m.de/werbeagentur-stuttgart.html'}
				image={heroImg}
			/>

      <Header />
      
      <main>
        <h1 className="visuallyHidden">Über unsere Agentur</h1>
        
        <Hero
          src={heroImg}
          alt="Bürostuhlrennen"
          width={1920}
          height={800}
          content={
            <HeroContent 
              title={<span>Wir sind<br/>Tandem Marketing &amp; Partners</span>}
              text="Werbeagentur Stuttgart"
            />
          }
        />

        <section>
          <h2 className="visuallyHidden">Über uns</h2>
          <SectionWhite 
            title="Ihr Fullservice-Dienstleister für digitale Medien"
            text="Seit 2009 setzen wir digitale Projekte um. Die Größten entwickeln sich dank unserer kontinuierlichen Unterstützung stetig weiter."
          />
          <SectionColor 
            content={
              <Fragment>
                <p styleName="text">Im Bereich Webdesign &amp; Webentwicklung bieten wir Ihnen kreative Lösungen und maximale Flexibilität dank professioneller Content Management Systeme für Webseiten, E-Commerce-Portale und Onlineshops.</p>
                <p styleName="text">Im Online Marketing arbeiten wir mit gewinnbringenden Werkzeugen und bieten Leistungen wie Suchmaschinenoptimierung und Google AdWords. Ein weiterer Service unserer Werbeagentur ist der Bereich Online- und Social Media Marketing: Wir schaffen bedarfsweckende Inhalte, die Ihre Zielgruppe ansprechen und entwickeln die passenden Tools und Applikationen. Darüber hinaus setzen wir Aufgaben in den Bereichen E-Mail-Marketing, Newsletter-Aktionen, Tracking, Affiliate Marketing und Web-Controlling um. Hinter unserem Erfolg steckt vor allem viel Arbeit, Einsatzbereitschaft und der Mut, immer wieder Neues zu schaffen.</p>
                <div styleName="buttonGroup">
                  <Link
                    to="/werbeagentur-stuttgart/contact.html"
                    className="button"
                    style={{
                      "--stroke": "var(--color-white)",
                      "backgroundColor": "var(--color-lipstick)"
                    }}
                  >
                    Werden Sie unser Kunde
                  </Link>
                  <Link className="buttonLink" to="/referenzen.html" style={{
                    "--stroke": "var(--color-white)"}}>Referenzen</Link>
                </div>
              </Fragment>
            }
            textColor="var(--color-white)"
            backgroundColor="#770c36"
          />
        </section>

        <section styleName="trust">
          <div className="container" styleName="parallax-container">
            <h3 styleName="title">Diese Kunden vertrauen uns</h3>
            {
              isMobile ? (
                <div styleName="trustSliderWrp">
                  <Slider {...settings}>
                    { images.map(logo => {
                      return <img src={logo.src} />
                    }) }
                  </Slider>
                </div>
              ) : (
                <div ref={trustRef}>
                  <ImageList
                    images={images}
                    rowGap="20px"
                    rowGapDesktop="30px"
                    columnGap="43px"
                    columnGapDesktop="70px"
                  />
                </div>
              )
            }
          </div>
        </section>

        {/* <AboutPageStrengths
          header={'Wie wir arbeiten'}
          data={AboutPageStrengthsData}
          hrColor={"var(--color-blue-marguerite)"}
        /> */}

        <ParallaxEffect
          image={parallaxImageUs}
          alt="Tandem Marketing & Partners"
          children={
            <div className="section container">
              <b className="heading-2 parallax-text" style={{minHeight: "19rem"}}>
                Das bieten wir
              </b>
            </div>
          }
        />
        <AccordionTabs target={"about"} />


        <ParallaxEffect
          image={parallaxImageTrust}
          alt="Tandem Marketing & Partners"
          children={
            <div className="section container">
              <b className="heading-2 parallax-text" style={{minHeight: "19rem"}}>
                Unser Team
              </b>
            </div>
          }
        />
        <Worktable />
        <SectionColor 
          content={
            <Fragment>
              <p className="section-text-400">Unsere Fachkompetenz kombiniert mit der Stärke erfahrener Leistungsträger und der Frische junger Talente gibt uns die kreative Kraft, Projekte zielgesteuert und zeitnah umzusetzen. </p>
            </Fragment>
          }
          textColor="var(--color-white)"
          backgroundColor="var(--color-blue-marguerite)"
        />
        
        {/* <ColorSteps 
						hiddenTitle="Werbeagentur Stuttgart – Das Know-how zählt"
						parallax={
							<ParallaxEffect
								image={parallaxImageTrust}
								alt="Tandem Marketing & Partners"
								children={
									<div className="section container">
										<b className="heading-2 parallax-text">
                      Werbeagentur Stuttgart – Das Know-how zählt
										</b>
									</div>
								}
							/>
						}
            bgColor={"var(--color-blue-marguerite)"}
						textTop={"Im Bereich Webdesign & Webentwicklung bieten wir Ihnen kreative Lösungen und maximale Flexibilität dank professioneller Content Management Systeme für Webseiten, E-Commerce-Portale und Onlineshops."}
						textCenter={"Unsere Fachkompetenz kombiniert mit der Stärke erfahrener Leistungsträger und der Frische junger Talente gibt uns die kreative Kraft, Projekte zielgesteuert und zeitnah umzusetzen."}
						textBottom={<Fragment>
							<p className="section-text-200">Im Online Marketing arbeiten wie mit gewinnbringenden Werkzeugen und bieten Leistungen wie Suchmaschinenoptimierung und Google AdWords. Ein weiterer Service unserer Werbeagentur ist der Bereich Online- und Social Media Marketing: Wir schaffen bedarfsweckende Inhalte, die Ihre Zielgruppe ansprechen und entwicklen die passenden Tools und Applikationen. Darüber hinaus setzen wir Aufgaben in den Bereichen E-Mail-Marketing, Newsletter-Aktionen, Tracking, Affiliate Marketing und Web-Controlling um. Hinter unserem Erfolg steckt vor allem viel Arbeit, Einsatzbereitschaft und der Mut, immer wieder Neues zu schaffen.</p>							
						</Fragment>}
        /> */}

        {/* <Team /> */}
        <section className="section">
          <Reviews />
        </section>

        <div>
          <SourceAboutUs />
        </div>
        
        <Blog />

        <FeedbackHomePage />
      </main>
      
      <Footer />
    </div>
  );
}
