import React, { Fragment } from "react";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { ParallaxEffect } from "components/ParallaxEffect";
import { ColorSteps } from "components/ColorSteps";
import { AboutPageStrengths } from "components/AboutPage-OurStrengths";
import { data as MailPageStrengthsData } from "components/AboutPage-OurStrengths/MailPageStrenghsData";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { LineChartEmailMarketing } from "components/Charts";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from "assets/images/hero-emailmarketing.jpg";
import parallaxImageTel from "assets/images/emailmarketing-parallax-tel.jpg";
import parallaxImageTablet from "assets/images/emailmarketing-parallax-tablet.jpg";
import parallaxImageMan from "assets/images/emailmarketing-parallax-man.jpg";
import parallaxImageWoman from "assets/images/emailmarketing-parallax-woman.jpg";
import parallaxImageMobile from "assets/images/marketing-parallax-mobile.jpg";

// import graph1 from "assets/images/email-graph-1.svg";
import graph2 from "assets/images/email-graph-2.svg";
import graph3 from "assets/images/email-graph-3.svg";

import "./MailPage.css";

export const MailPage = () => {

	return (
		<div className='page-wrapper'>
			<CommonMetaTags 
				title={'E-Mail Marketing – per Newsletter zum Erfolg'}
				description={'Tandem Marketing'}
				url={'https://www.tandem-m.de/marketing-stuttgart/e-mail-marketing.html'}
				image={heroImg}
			/>

			<Header />
			
			<main>
				<Hero
					src={heroImg}
					alt="E-Mail Marketing"
					width={1920}
					height={800}
					content={
						<HeroContent 
							title={<span>Per Newsletter zum Erfolg</span>}
							text="E-Mail Marketing"
						/>
					}
				/>

				<section>
					<h2 className="visuallyHidden">E-Mail Marketing</h2>
					<SectionWhite 
						title="Per Newsletter zum Erfolg"
						text="Laut Marktforschungsergebnissen nutzen mehr als 66 Millionen der Deutschen das Internet."
						textColor="var(--color-cherry-dark)"
					/>
					<div styleName="section-color">
						<SectionColor 
							content={
								<Fragment>
									<p styleName="text">90 Prozent davon kommunizieren über E-Mail, 89 Prozent suchen regelmäßig nach Waren und Dienstleistungen – und 85% haben einen oder mehrere Newsletter abonniert. Für Unternehmen lohnt sich E-Mail Marketing also nach wie vor. Lesen Sie hier, was erfolgreiche Newsletter-Strategien bringen und was Tandem Marketing & Partners für Sie als E-Mail-Marketing-Agentur leisten kann.</p>
								</Fragment>
							}
							textColor="var(--color-white)"
							backgroundColor="var(--color-cherry-dark)"
						/>
					</div>
				</section>
				<div className="empty-parallax">
					<ParallaxEffect
						image={parallaxImageMan}
						alt="Tandem Marketing & Partners"
					/>
				</div>
				<SectionColor 
					content={
						<Fragment>
							<h3 className="section-text-400">Warum E-Mail Marketing?</h3>
							<p className="section-text-200">Die E-Mail ist nach wie vor einer der wichtigsten digitalen Kommunikationskanäle. Laut Einschätzung von Statista wird die Zahl der weltweiten E-Mail-Nutzer bis 2025 voraussichtlich auf 4,6 Milliarden ansteigen.</p>
						</Fragment>
					}
					textColor="var(--color-dark)"
					backgroundColor="var(--color-white)"
				/>

				<section>
					<div styleName="line-chart-wrapper">
						<div className="section container" styleName="line-chart-container">
							<LineChartEmailMarketing />
						</div>
					</div>
				</section>
				
				<section>
					<div styleName="section-color">
						<SectionColor 
							content={
								<Fragment>
									<p styleName="text">Trotz der steigenden Relevanz von Social Media und Instant Messenger, bleibt E-Mail Marketing einer der beliebtesten und effizientesten Kanäle, um Kunden zu erreichen. 87 Prozent der B2B-Marketer nutzen E-Mails als Vertriebsinstrument: </p>
									<img src={graph2} loading="lazy"/>
									<p styleName="text">E-Mail Marketing hilft auch Ihrem Unternehmen, die Kundenbindung zu verbessern, Ihre Marke bekannter zu machen, das Engagement zu steigern und schließlich mehr Verkäufe zu erzielen.</p>
								</Fragment>
							}
							textColor="var(--color-dark)"
							backgroundColor="var(--color-white)"
						/>
					</div>
				</section>

				<ColorSteps 
					hiddenTitle="Suchmaschinenoptimierung"
					parallax={
						<ParallaxEffect
							image={parallaxImageTel}
							alt="Tandem Marketing & Partners"
							children={
								<div className="section container">
									<b className="heading-2 parallax-text" >
										Was ist E-Mail Marketing?
									</b>
								</div>
							}
						/>
					}
					textTop={"E-Mail Marketing bezeichnet einen Bereich des Direktmarketings, bei dem (kommerzielle) E-Mails an einen bestimmten Verteiler gesendet werden. Der Verteiler baut sich aus Personen auf, die sich für Ihren Newsletter angemeldet und dem Erhalt von E-Mails Ihres Unternehmens zugestimmt haben. Kontakte werden in ihrem persönlichen Postfach direkt angesprochen und mit für sie relevanten Informationen versorgt."}
					textCenter={"Ziele von E-Mail Marketing sind normalerweise eine verbesserte Kundenbindung sowie gesteigerte Umsätze."}
					bgColor={"var(--color-cherry-dark)"}
				/>

				<section>
					<div styleName="section-color">
						<SectionColor 
							content={
								<Fragment>
									<p className="section-text-200">Richtig eingesetzt, bietet E-Mail Marketing viele Vorteile. Die wichtigsten auf einen Blick:</p>
									<p className="section-text-700">Breite Zielgruppe</p>
									<div styleName="graph-block">
										<p className="section-text-200">Newsletter-Marketing ist eine Art Allround-Talent, das sich über alle Altersgruppen hinweg effizient einsetzen lässt. Selbst Menschen, die wenig online sind, rufen in den meisten Fällen regelmäßig ihre E-Mails ab.</p>
										<div styleName="graph-block__wrp">
											<img styleName="graph-block__graph" src={graph3} loading="lazy"/>
										</div>
									</div>
								</Fragment>
							}
							textColor="var(--color-dark)"
							backgroundColor="var(--color-white)"
						/>
					</div>
				</section>

				<section styleName="strengths">
					<h2 className="visuallyHidden">Was sind unsere Stärken</h2>
					<ParallaxEffect
						image={parallaxImageTablet}
						alt="Tandem Marketing & Partners"
					/>
				</section>

				<section>
					<div styleName="section-color">
						<SectionColor 
							content={
								<Fragment>
									<p className="section-text-700">Hoher Return on Investment (ROI) und messbare Ergebnisse</p>
									<p className="section-text-200">Newsletter-Marketing ist eine Art Allround-Talent, das sich über alle Altersgruppen hinweg effizient einsetzen lässt. Selbst Menschen, die wenig online sind, rufen in den meisten Fällen regelmäßig ihre E-Mails ab.</p>
									<p className="section-text-700">Markenbekanntheit und Kundenbindung</p>
									<p className="section-text-200">Durch den regelmäßigen E-Mail-Kontakt zu Ihren Kund*innen werden Ihr Unternehmensname, Dienstleistungen und Produkte bewusst wahrgenommen. Das sorgt für hohen Wiedererkennungswert und schafft Vertrauen. Zudem können Sie Ihre Zielgruppe durch persönliche Nachrichten – vom Geburtstagsmailing bis hin zu Erinnerung an Warenkorbabbrüche – an Ihr Unternehmen erinnern. </p>
									<p className="section-text-700">Unabhängigkeit von Dritten</p>
									<p className="section-text-200">Im Fall von E-Mail Marketing bleiben SIe unabhängig von Drittanbietern wie Google, Facebook oder Instagram und Ihren Followern dort, sprich: Ihre Kontaktliste gehört Ihnen. </p>
								</Fragment>
							}
							textColor="var(--color-dark)"
							backgroundColor="var(--color-white)"
						/>
					</div>
				</section>

				<section>
					<ParallaxEffect
						image={parallaxImageWoman}
						alt="Tandem Marketing & Partners"
						children={
							<div className="container">
								<b className="heading-2 parallax-text" >
									Wie funktioniert E-Mail Marketing?
								</b>
							</div>
						}
					/>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Die erste Grundvoraussetzung für erfolgreiches B2C- oder B2B-E-Mail Marketing sind ein passender E-Mail Service Provider (ESP), auch bekannt als E-Mail-Marketing-Tool, -Plattform oder -Software. Neben der Option zur individuellen Gestaltung und dem einfachen Versand von E-Mail-Kampagnen, lassen sich über die Software auch wertvolle Statistiken einsehen. Die zweite Grundvoraussetzung für erfolgreiche Newsletter-Kampagnen ist eine Mailingliste: Diese kann z.B. über ein Anmeldeformular auf der Website, einer speziellen Landingpage oder via Social Media aufgebaut werden. Alles weitere hängt vom Unternehmen, der Branche, der Zielgruppe (B2B oder B2C?) und natürlich den gesetzten Zielen ab.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-cherry-dark)"
					/>
				</section>

				<section>					
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-700">E-Mail Marketing bei Tandem Marketing & Partners</p>
								<p className="section-text-200">Als E-Mail-Marketing Agentur mit Sitz in Stuttgart und Berlin sind wir der richtige Partner für Ihre E-Mail-Marketing-Strategie, die technische Umsetzung und die inhaltliche Umsetzung.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
					<div className="empty-parallax">
						<ParallaxEffect
							image={parallaxImageMobile}
							alt="Tandem Marketing & Partners"
						/>
					</div>
				</section>

				<AboutPageStrengths
					header={'Wir helfen Ihnen dabei ...'}
					data={MailPageStrengthsData}
					hrColor={"var(--color-cherry)"}
				/>

				<section>
					<div styleName="section-color">
						<SectionColor 
							content={
								<Fragment>
									<p styleName="text">Vertrauen Sie auf Tandem Marketing & Partners aus Stuttgart: Unsere Ideen und Lösungen im E-Mail Marketing werden auch Ihre Kunden überzeugen! Kontaktieren Sie uns gerne für ein persönliches Beratungsgespräch oder ein individuelles Angebot.</p>
								</Fragment>
							}
							textColor="var(--color-white)"
							backgroundColor="var(--color-cherry-dark)"
						/>
					</div>
				</section>

				<FeedbackHomePage />
			</main>
			
			<Footer />
		</div>
	);
}
