import React, { Fragment } from "react";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { ParallaxEffect } from "components/ParallaxEffect";
import { CheckedList } from "components/CheckedList";
import { UsedTech } from "components/SourceAboutUs";
import { Blog } from "components/Blog";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from "assets/images/hero-schoolacademie.jpg";
import parallaxImg1 from "assets/images/parallax-schoolacademie-de-1.jpg";
import parallaxImg2 from "assets/images/parallax-schoolacademie-de-2.jpg";
import parallaxImg21 from "assets/images/parallax-schoolacademie-de-21.jpg";
import parallaxImg22 from "assets/images/parallax-schoolacademie-de-22.jpg";
import parallaxImg3 from "assets/images/parallax-schoolacademie-de-3.jpg";

import typo3 from 'assets/images/icon-typo3.svg';
import mailchimp from 'assets/images/mailchimp.svg';
import css from 'assets/images/icon-css3.svg';
import js from 'assets/images/icon-java.svg';
import html from 'assets/images/icon-html5.svg';

const techs = [
  {
    "src" : typo3,
    "width": 157,
    "height": 44,
    "title": "Magento",
    "loading": "lazy"
  },
  {
    "src" : js,
    "width": 59,
    "height": 64,
    "title": "Ecmascript",
    "loading": "lazy"
  },
  {
    "src" : html,
    "width": 46,
    "height": 64,
    "title": "HTML",
    "loading": "lazy"
  },
  {
    "src" : css,
    "width": 45,
    "height": 64,
    "title": "CSS",
    "loading": "lazy"
  },
  {
    "src" : mailchimp,
    "width": 164,
    "height": 44,
    "title": "Wordpress",
    "loading": "lazy"
  }

]

import "./SchoolAcademie.css";

export const SchoolAcademie = () => {
  return (
    <div className="page-wrapper">
      <CommonMetaTags 
				title={'Die Deutsche Schulakademie - Werbeagentur stuttgart'}
				description={'Die Deutsche Schulakademie ist eine unabhängige Institution für Schulentwicklung und Lehrerfortbildung. Sie wurde zum 1. Januar 2015 gegründet, um das Wissen und Können hervorragender Schulen...'}
				url={'https://www.tandem-m.de/projekte/49-die-deutsche-schulakademie.html'}
				image={heroImg}
			/>

      <Header />
      <main>
        <h1 className="visuallyHidden">Die Deutsche Schulakademie</h1>

        <Hero
          src={heroImg}
          alt="Die Deutsche Schulakademie"
          width={1920}
          height={800}
          content={
            <div styleName="hero-content">
              <HeroContent
                title={<span>Die Deutsche Schulakademie</span>}
                text="unabhängige Institution für Schulentwicklung und Lehrerfortbildung"
              />
            </div>
          }
        />
      
        <section>
          <h2 className="visuallyHidden">Über Die Deutsche Schulakademie-Projekt</h2>
          <SectionWhite
            title="Gute Schule in die Breite tragen"
            text="Mit diesem Ziel ging Die Deutsche Schulakademie 2015 an den Start. Tandem Marketing & Partners hat die erste Onlinepräsenz für Die Deutsche Schulakademie umgesetzt. Es folgten ein Website-Relaunch sowie fortlaufende Unterstützung im E-Mail-Marketing und Support."
            textColor="var(--color-green-school)"
          />
          
          <div className="empty-parallax">
            <ParallaxEffect
              image={parallaxImg2}
              alt="Die Deutsche Schulakademie"
            />
          </div>
          <div className="project-gallery">
						<div>
							<img src={parallaxImg21} alt="Die Deutsche Schulakademie" loading="lazy" />
						</div>
						<div>
							<img src={parallaxImg22} alt="Die Deutsche Schulakademie" loading="lazy" />
						</div>
					</div>
        
          <div styleName="section-color">
            <SectionColor
              content={
                <Fragment>
                  <p styleName="text">Tandem Marketing hat die erste Website der Deutschen Schulakademie entwickelt, um ihre Ziele und Erfolge auch im Web optimal zu präsentieren. Nach rund einem Jahr wurde ein Relaunch vorgenommen, um den gewachsenen Anforderungen an die Website gerecht zu werden. Die erste Website der Deutschen Schulakademie wurde innerhalb kürzester Zeit umgesetzt und für die Ansicht auf allen Endgeräten optimiert.</p>
                  <dl styleName="stats-list">
                    <div styleName="stats-item">
                      <dt>NGO:</dt>
                      <dd>mit siebenstelligem Jahresbudget</dd>
                    </div>

                    <div styleName="stats-item">
                      <dt>Preisträgernetzwerk:</dt>
                      <dd>Fast 100 Schulen</dd>
                    </div>
                  </dl>
                </Fragment>
              }
              textColor="var(--color-white)"
              backgroundColor="var(--color-green-school)"
            />
          </div>
        </section>

        <div className="empty-parallax">
          <ParallaxEffect
            image={parallaxImg1}
            alt="Die Deutsche Schulakademie"
          />
        </div>

        <SectionColor
          content={
            <Fragment>
              <p className="section-text-200">Die Website der Deutschen Schulakademie lädt Besucher ein, in ihre Welt einzutauchen, daran teilzuhaben, von ihr zu lernen und sich selbst zu engagieren. Zu den besonderen Features der Onlinepräsenz gehören unter anderem Bildergalerien sowie interaktive Deutschlandkarten zu unterschiedlichen Themen (mit JavaScripts umgesetzt, responsiv).</p>
            </Fragment>
          }
          textColor="var(--color-dark)"
          backgroundColor="var(--color-white)"
        />

        <div className="empty-parallax">
          <img src={parallaxImg3} alt="Die Deutsche Projektfotos" loading="lazy" />
        </div>
        <section className="section">
          <div className="container-narrow">
            <h2 className="heading-3">Leistungen</h2>
          </div>
          <div className="container" styleName="checkedList">
            <CheckedList
              content={
                <Fragment>
                  <li>Website-Entwicklung</li>
                  <li>Newsletter-Implementierung und -Versand</li>
                  <li>Laufende Weiterentwicklung</li>
                  <li>Filehosting-System</li>
                  <li>Website-Relaunch</li>
                  <li>Online-Bewerbungsformular</li>
                  <li>Individualisierte Google Maps Extension mit Filteroptionen</li>
                  <li>responsive Deutschlandkarte auf Basis von JavaSriptst</li>
                  <li>Galeriefunktion</li>
                  <li>Suchmaschinenoptimierung</li>
                  <li>Suchmaschinenmarketing</li>
                </Fragment>
              }
              color="var(--color-fungreen)"
            />
          </div>
        </section>

        <section styleName="usedTech">
          <UsedTech
            techs={techs}
          />
        </section>

        <Blog />

        <FeedbackHomePage />
      </main>
      <Footer />
    </div>
  );
};
