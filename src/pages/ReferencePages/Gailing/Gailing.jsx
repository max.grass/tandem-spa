import React, { Fragment } from "react";
import { Helmet } from "react-helmet";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { ParallaxEffect } from "components/ParallaxEffect";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { CheckedList } from "components/CheckedList";
import { UsedTech } from "components/SourceAboutUs";
import { Blog } from "components/Blog";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from 'assets/images/hero-gailing.jpg';

import parallaxImg1 from "assets/images/parallax-gailing-02-2.jpg";
import parallaxImg2 from "assets/images/parallax-gailing-03-3.jpg";
import parallaxImg3 from "assets/images/parallax-05-2-b.jpg";
import parallaxImg4 from "assets/images/parallax-gailing-04-2.jpg";

import html from 'assets/images/icon-html5.svg';
import css from 'assets/images/icon-css3.svg';
import magento from 'assets/images/magento-new-2.svg';
import wordpress from 'assets/images/logo-wordpress.svg';

import "./Gailing.css";

const techs = [
	{
		"src" : magento,
		"width": 131,
		"height": 44,
		"title": "Magento"
	},
    {
		"src" : wordpress,
		"width": 216,
		"height": 44,
		"title": "Wordpress"
	},
	{
		"src" : html,
		"width": 46,
		"height": 64,
		"title": "html"
	},
	{
		"src" : css,
		"width": 45,
		"height": 64,
		"title": "css"
	},
]

export const Gailing = () => {
	return (
		<div className="page-wrapper">
			<CommonMetaTags 
				title={'Gailing Schlafkultur'}
				description={'Vollendete Schlafkultur in Heilbronn und Umgebung'}
				url={'https://www.tandem-m.de/projekte/gailing.html'}
				image={heroImg}
			/>

			<Header />
			<main>
				<h1 className="visuallyHidden">Gailing Schlafkultur - Vollendete Schlafkultur in Heilbronn und Umgebung</h1>

				<Hero
					src={heroImg}
					alt="Gailing Schlafkultur - Vollendete Schlafkultur in Heilbronn und Umgebung"
					width={1920}
					height={800}
					content={
						<div styleName="hero-content">
							<HeroContent 
								title={<span>Gailing Schlafkultur</span>}
								text="Vollendete Schlafkultur in Heilbronn und Umgebung"
							/>
						</div>
					}
				/>

				<section>
					<h2 className="visuallyHidden">Über Gailing Schlafkultur Projekt</h2>
					<SectionWhite 
						title="Optimale Verbindung von offline & online"
						text="Gailing Schlafkultur ist mehr als ein Betten-Fachgeschäft: Seit über 30 Jahren setzen sich die Mitarbeiter*innen von Gailing Schlafkultur für erholsamen Schlaf im Großraum Heilbronn ein."
						textColor="var(--color-gailing-green)"
					/>
					<div className="empty-parallax">
						<ParallaxEffect
							image={parallaxImg4}
							alt="Gailing Schlafkultur Projektfotos"
						/>
					</div>
					<SectionColor 
						content={
							<Fragment>
								<p styleName="text">Dank eines modernen Magento-Systems können Kunden jetzt auch überregional von der Erfahrung der Schlafexperten profitieren. Tandem Marketing & Partners hat den bestehenden Onlineshop von Gailing Schlafkultur in einen modernen, updatefähigen Magento Webshop migriert. Dabei wurden zahlreiche Schnittstellen, etwa zu Marktplätzen wie Amazon, ebenso berücksichtigt wie das bestehende Warenwirtschaftssystem.</p>
                                <p styleName="text">Im Bereich Marketing wird eine umfassende Content Marketing Strategie verfolgt, Newsletter-Kampagnen umgesetzt und die Online-Werbung gesteuert.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-gailing-grey)"
					/>
				</section>

				<section>
					<div className="empty-parallax">
						<img src={parallaxImg1} alt="Gailing Schlafkultur Projektfotos"  loading="lazy" style={{display: 'block'}}/>
					</div>
                    <div className="project-gallery">
						<div>
							<img src={parallaxImg2} alt="Gailing Schlafkultur Projektfotos"  loading="lazy"/>
						</div>
						<div>
							<img src={parallaxImg3} alt="Gailing Schlafkultur Projektfotos"  loading="lazy"/>
						</div>
					</div>
				</section>

				<section className="section">
					<div className="container-narrow">
						<h2 className="heading-3">Leistungen</h2>
					</div>
					<div className="container" styleName="checkedList">
						<CheckedList 
							content={
								<Fragment>
									<li>Corporate Design</li>
									<li>Webshop-Entwicklung und fortlaufende Weiterentwicklung</li>
									<li>Zielgruppenforschung</li>
									<li>Content Marketing</li>
									<li>Eye-Tracking-Analysen</li>
                                    <li>Social Media Marketing</li>
                                    <li>E-Commerce-Beratung</li>
                                    <li>Suchmaschinenoptimierung</li>
                                    <li>Konzeption</li>
								</Fragment>
							}
							color="var(--color-gailing-green)"
						/>
					</div>
				</section>

				<section styleName="usedTech">
					<UsedTech
						techs={techs}
					/>
				</section>

				<Blog />

				<FeedbackHomePage />
			</main>
			<Footer />
		</div>
	);
};