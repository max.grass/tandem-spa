import React, { Fragment } from "react";
import { Helmet } from "react-helmet";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { ParallaxEffect } from "components/ParallaxEffect";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { CheckedList } from "components/CheckedList";
import { UsedTech } from "components/SourceAboutUs";
import { Blog } from "components/Blog";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from 'assets/images/hero-kruschina.jpg';

import parallaxImg1 from "assets/images/parallax-kruschina_02-37.jpg";
import parallaxImg2 from "assets/images/parallax-kruschina_04.jpg";
import parallaxImg3 from "assets/images/parallax-kruschina.jpg";
import parallaxImg4 from "assets/images/parallax-kruschina_05-1d.jpg";

import html from 'assets/images/icon-html5.svg';
import css from 'assets/images/icon-css3.svg';
import drupal from 'assets/images/icon-drupal.png';
import drupalCo from 'assets/images/icon-drupal-commerce.png';

import "./Kruschina.css";

const techs = [
	{
		"src" : drupal,
		"width": 177,
		"height": 44,
		"title": "drupal"
	},
    {
		"src" : drupalCo,
		"width": 195,
		"height": 64,
		"title": "drupal commerce"
	},
	{
		"src" : html,
		"width": 46,
		"height": 64,
		"title": "html"
	},
	{
		"src" : css,
		"width": 45,
		"height": 64,
		"title": "css"
	},
]

export const Kruschina = () => {
	return (
		<div className="page-wrapper">
			<CommonMetaTags 
				title={'Kruschina'}
				description={'Ihr Partner für anspruchsvolle Dienstleistungen'}
				url={'https://www.tandem-m.de/projekte/kruschina.html'}
				image={heroImg}
			/>

			<Header />
			<main>
				<h1 className="visuallyHidden">Kruschina - Ihr Partner für anspruchsvolle Dienstleistungen</h1>

				<Hero
					src={heroImg}
					alt="Kruschina - Ihr Partner für anspruchsvolle Dienstleistungen"
					width={1920}
					height={800}
					content={
						<div styleName="hero-content">
							<HeroContent 
								title={<span>Kruschina</span>}
								text="Ihr Partner für anspruchsvolle Dienstleistungen"
							/>
						</div>
					}
				/>

				<section>
					<h2 className="visuallyHidden">Über Kruschina Projekt</h2>
					<SectionWhite 
						title="Projekt Kruschina"
						text="Als bundesweit operierendes Dienstleistungsunternehmen ist die KRUSCHINA Unternehmensgruppe seit über fünf Jahrzehnten in den Servicebereichen Gebäudereinigung, Personaldienstleistungen, Gastronomie-Services und Betriebsverpflegung aktiv."
						textColor="var(--color-kruschina-blue)"
					/>
					<SectionColor 
						content={
							<Fragment>
								<p styleName="text">Tandem entwickelte für die KRUSCHINA Unternehmensgruppe ein innovatives Web-Portal, das mehrere hochkomplexe Funktionalitäten in einem Content-Management-Framework auf Drupal-Basis integriert. Neben klassischen CMS-Funktionen bietet das Portal eine integrierte Jobbörse mit geografischer Umkreissuche, Bereichssteuerung und Bewerbermanagement. Ebenso ist ein B2B-Online-Shop mit differenzierter Standortzuordnung sowie ein umfassendes Backend-Dashboard für HR-Management integriert.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-kruschina-blue)"
					/>
				</section>

				<section>
					<div className="empty-parallax">
						<ParallaxEffect
							image={parallaxImg1}
							alt="GKruschina Projektfotos"
						/>
					</div>
					<SectionColor 
						content={
							<Fragment>
								<p styleName="text">Das erweiterte Rechtemanagement ermöglicht es der KRUSCHINA Unternehmensgruppe, die Zugriffsberechtigungen individuell zu konfigurieren. So haben Bereichsmanager die Möglichkeit, im Dashboard alle Abteilungen sowie eingehende Bewerbungen zu überblicken, während Sachbearbeiter nur Zugriff auf von ihnen erstellte Stellenanzeigen haben.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
                    <div className="project-gallery">
						<div>
							<img src={parallaxImg2} alt="Kruschina Projektfotos"  loading="lazy"/>
						</div>
						<div>
							<img src={parallaxImg3} alt="Kruschina Projektfotos"  loading="lazy"/>
						</div>
					</div>
				</section>

				<SectionColor 
					content={
						<Fragment>
							<p className="section-text-200">Das Dashboard bietet den Managerinnen und Sachbearbeiterinnen umfangreiche Funktionen zur Bewerberauswahl. Sie können Bewerbungen zu spezifischen Stellenanzeigen sichten, Kandidaten nach verschiedenen Kriterien bewerten, vormerken, Notizen hinterlegen und sogar automatisiert zu Vorstellungsgesprächen einladen. Zudem können Verantwortlichkeiten neu zugewiesen und in der Bewerberdatenbank nach geeigneten Kandidaten gesucht werden.</p>
						</Fragment>
					}
					textColor="var(--color-white)"
					backgroundColor="var(--color-kruschina-blue)"
				/>

				<div className="empty-parallax">
					<img 
						style={{display: 'block'}}
						src={parallaxImg4} alt="Kruschina Projektfotos" loading="lazy"
					/>
				</div>

				<SectionColor 
					content={
						<Fragment>
							<p className="section-text-200">Dank API-Integration in diverse Jobbörsen kann die KRUSCHINA Unternehmensgruppe die Stellenausschreibungen flexibel koordinieren und zentral steuern. Im Bereich des B2B-Shops implementierten wir eine ausgefeilte Standortverwaltung mit unabhängigen Produkt- und Lagerportfolios, wodurch die Servicenehmer bequem Produkte für Events auswählen und diese im Bestellprozess einem zuständigen Standort automatisch zuordnen können.</p>
						</Fragment>
					}
					textColor="var(--color-white)"
					backgroundColor="var(--color-kruschina-blue)"
				/>

				<section className="section">
					<div className="container-narrow">
						<h2 className="heading-3">Leistungen</h2>
					</div>
					<div className="container" styleName="checkedList">
						<CheckedList 
							content={
								<Fragment>
									<li>Konzeption</li>
									<li>Programmierung</li>
									<li>Webdesign</li>
									<li>Qualitätssicherung und Consulting</li>
									<li>Usability</li>
								</Fragment>
							}
							color="var(--color-kruschina-blue)"
						/>
					</div>
				</section>
+
				<section styleName="usedTech">
					<UsedTech
						techs={techs}
					/>
				</section>

				<Blog />

				<FeedbackHomePage />
			</main>
			<Footer />
		</div>
	);
};