import React, { Fragment } from "react";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { ParallaxEffect } from "components/ParallaxEffect";
import { CheckedList } from "components/CheckedList2";
import { UsedTech } from "components/SourceAboutUs";
import { Blog } from "components/Blog";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";
import { VideoPlayer } from "components/VideoPlayer";
import {AuthorQuote} from "components/AuthorQuote";

import heroImg from "assets/images/hero-mettler.jpg";
import galImage from "assets/images/parallax-mettler-gmbh-1.jpg";
import galleryImg1 from "assets/images/gallery-mettler-1.jpg";
import galleryImg2 from "assets/images/gallery-mettler-2.jpg";
import galleryImg3 from "assets/images/gallery-mettler-3.jpg";
import alexMettler from "assets/images/alex-mettler.png";
import galImage4 from "assets/images/ref-mettler-001.jpg";


import drupal from 'assets/images/logos/drupal.svg';
import html from 'assets/images/icon-html5.svg';
import css from 'assets/images/icon-css3.svg';
import js from 'assets/images/icon-java.svg';
import solr from 'assets/images/icon-solr.svg';
import symfony from 'assets/images/icon-symfony.svg';
import mautic from 'assets/images/icon-mautic.svg';

import videoPreview from "assets/images/video-mettler.jpg";

const techs = [
	{
		"src" : drupal,
		"width": 175,
		"height": 44,
		"title": "Magento"
	},
	{
		"src" : symfony,
		"width": 52,
		"height": 52,
		"title": "High Performance PHP Framework for Web"
	},
	{
		"src" : solr,
		"width": 103,
		"height": 52,
		"title": "Solr Search Server"
	},
	{
		"src" : mautic,
		"width": 52,
		"height": 52,
		"title": "Open Source Marketing Automation"
	},
	{
		"src" : html,
		"width": 46,
		"height": 64,
		"title": "html"
	},
	{
		"src" : css,
		"width": 45,
		"height": 64,
		"title": "css"
	},
	{
		"src" : js,
		"width": 59,
		"height": 64,
		"title": "Ecmascript"
	}
]

import "./MettlerGmbh.css";

export const MettlerGmbh = () => {
	return (
		<div className="page-wrapper">
			<CommonMetaTags 
				title={'Mettler GmbH'}
				description={'Die Mettler GmbH ist ein familiengeführtes Unternehmen, mittlerweile schon in dritter Generation.'}
				url={'https://www.tandem-m.de/projekte/48-mr-gruppe.html'}
				image={heroImg}
			/>

			<Header />
			<main>
				<h1 className="visuallyHidden">Mettler GmbH</h1>

				<Hero
					src={heroImg}
					alt="Luxusuhr als Geschenk an die Lieben"
					width={1920}
					height={800}
					content={
						<div styleName="hero-content">
							<HeroContent 
								title={<span>Mettler GmbH</span>}
								text="Über 70 Jahre Familientradition im Handwerk"
							/>
						</div>
					}
				/>

				<section>
					<h2 className="visuallyHidden">Full Service Webentwicklung und Online Marketing Strategien</h2>
					<SectionWhite 
						title="Full Service Webentwicklung und Online Marketing Strategien"
						text="Tandem Marketing & Partners hat für die Mettler GmbH eine Mobile-First-Website entwickelt, die in Kombination mit Online Marketing Konzepten das Unternehmen zu Top-10-Rankings in branchenrelevanten Suchergebnissen führt."
						textColor="var(--color-mettler)"
					/>
					<VideoPlayer
						playColor={"#E7D000"}
						poster={videoPreview}
						videoUid={"YrxXd1BOyWs"}
					/>
					<div styleName="section-color">
						<SectionColor 
							content={
								<p styleName="text">Seit 2011 unterstützen wir die Mettler GmbH in den Bereichen Online Marketing und Web-Entwicklung. Für die neue Website wurde ein Konzept entwickelt, das auf detaillierten UI/UX-Analysen und einer Mobile-First-Strategie basiert. Ziel ist es, Kund*innen bestmöglich über Produkte und Dienstleistungen zu informieren und ihnen eine unkomplizierte Kontaktaufnahme oder Terminbuchung zu ermöglichen.</p>
							}
							textColor="var(--color-white)"
							backgroundColor="var(--color-mettler-dark)"
						/>
					</div>
				</section>

				<section>
					<div className="empty-parallax">
						<ParallaxEffect
							image={galleryImg1}
							alt="Mettler GmbH Projektfotos"
						/>
					</div>
					<div className="project-gallery">
						<div>
							<img src={galleryImg2} alt="Mettler GmbH Projektfotos " loading="lazy" />
						</div>
						<div>
							<img src={galleryImg3} alt="Mettler GmbH Projektfotos " loading="lazy" />
						</div>
					</div>
				</section>

				<section>
					<SectionColor 
						content={
							<p className="section-text-400">Dank innovativem WebDesign und Headless Frontend-Entwicklung auf Drupal-Basis kombiniert mit dem Solr SearchServer, konnten wir ein barrierefreies Nutzererlebnis und eine intuitive Navigation schaffen. Der gestiegene Traffic bei reduzierter Bounce Rate und erhöhter Verweildauer bestätigt unseren Erfolg</p>
						}
						textColor="var(--color-text)"
						backgroundColor="var(--color-white)"
					/>
				</section>

				<section styleName="mt-0">
					<img  className="wide-image"  src={galImage} alt="Mettler GmbH Projektfotos" loading="lazy" />
				</section>

				<section className="section">
					<div className="container-narrow">
						<h2 className="heading-3">Leistungen</h2>
						<div styleName="checked-list">
							<CheckedList
								title="Webentwicklung:"
								color="var(--color-mettler)"
								content={
									<Fragment>
										<li>Server- und DevOps-Umgebungseinrichtung</li>
										<li>Responsive - Headless Frontend-Entwicklung</li>
										<li>Drupal 10 Backend-Entwicklung</li>
										<li>Implementierung des Solr Searchservers</li>
										<li>Datenmigration von der alten Webseite</li>
										<li>Erzeugung von Nginx-Redirects unter Beibehaltung der URL-Struktur</li>
										<li>Integration diverser Erweiterungen, z.B. Termin-Buchungssystem</li>
										<li>Newsletter-Einrichtung und -Versand über Mautic Cloud</li>
										<li>Entwicklung eines Download-Bereichs</li>
										<li>Recruiting-Management für Online-Bewerbungen</li>
									</Fragment>
								}
							/>
							<CheckedList
								title="Online Marketing / UI/UX:"
								color="var(--color-mettler)"
								content={
									<Fragment>
										<li>Eye-Tracking-Analysen</li>
										<li>Zielgruppenanalyse</li>
										<li>Entwicklung des UI/UX Webinterfaces</li>
										<li>Content Marketing</li>
										<li>SEO/SEA</li>
										<li>Social Media Marketing und Videographie</li>
										<li>Newsletter-Marketing</li>
										<li>Retargeting-Kampagnen inklusive Conversion-Tracking</li>
										<li>Commerce Strategien</li>
									</Fragment>
								}
							/>
						</div>

					</div>

				</section>

				<AuthorQuote
					photo={alexMettler}
					authorName="Michael Mettler"
					authorPosition="Geschäftsführer (CEO)"
					text={<p>Wir arbeiten seit über 10 Jahren mit Tandem Marketing zusammen. Neben kreativen Ansätzen für unseren digitalen Unternehmensauftritt, überzeugen Herr Schmidt und sein Team mit weitsichtigen Ideen zur Digitalisierung unserer Geschäftsprozesse.</p>}
					textColor="var(--color-white)"
					backgroundColor="var(--color-mettler-dark)"
				/>

				<div styleName="mt-0">
					<img className="wide-image" width={1920} height={100} src={galImage4} alt="Mettler GmbH Projektfotos 2" loading="lazy" />
				</div>


				<section styleName="usedTech">
					<UsedTech
						techs={techs}
					/>
				</section>

				<Blog />

				<FeedbackHomePage />
			</main>
			<Footer />
		</div>
	);
};
