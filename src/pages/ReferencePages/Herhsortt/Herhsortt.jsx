import React, { Fragment } from "react";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { ParallaxEffect } from "components/ParallaxEffect";
import { CheckedList } from "components/CheckedList";
import { UsedTech } from "components/SourceAboutUs";
import { CommonMetaTags } from "components/CommonMetaTags";
import { Blog } from "components/Blog";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { VideoPlayer } from "components/VideoPlayer";

import heroImg from "assets/images/hero-hershortt.jpg";
import galleryImg1 from "assets/images/gallery-hershortt-1.jpg";
import galleryImg2 from "assets/images/gallery-hershortt-2.jpg";
import galleryImg3 from "assets/images/gallery-hershortt-3.jpg";
import galleryImg4 from "assets/images/gallery-hershortt-4.jpg";


import magento from 'assets/images/magento-new-2.svg';
import html from 'assets/images/icon-html5.svg';
import css from 'assets/images/icon-css3.svg';
import pwa from 'components/Tech/pwa.svg';
import react from 'components/Tech/react.svg';

import videoPreview from "assets/images/video-hershortt.jpg";

const techs = [
	{
		"src" : magento,
		"width": 131,
		"height": 44,
		"title": "Magento"
	},
	{
		"src" : pwa,
		"width": 81,
		"height": 32,
		"title": "Adobe PWA"
	},
	{
		"src" : react,
		"width": 56,
		"height": 56,
		"title": "React"
	},
	{
		"src" : html,
		"width": 46,
		"height": 64,
		"title": "html"
	},
	{
		"src" : css,
		"width": 45,
		"height": 64,
		"title": "css"
	}
]

import "./Herhsortt.css";

export const Herhsortt = () => {
	return (
		<div className="page-wrapper">
			<CommonMetaTags 
				title={'Hershortt - Werbeagentur stuttgart'}
				description={'Designermode zu Outlet-Preisen: Dafür steht das Stuttgarter Unternehmen Hershortt. Als der Verkauf über das Ladengeschäft und Internationale Marktplätze um einen eigenen Onlineshop ergänzt werden sollte,...'}
				url={'https://www.tandem-m.de/projekte/84-hershortt.html'}
				image={galleryImg1}
			/>

			<Header />
			<main>
				<h1 className="visuallyHidden">Hershortt Online-Shop PWA</h1>

				<Hero
					src={heroImg}
					alt="Hershortt Online-Shop PWA"
					width={1920}
					height={800}
					content={
						<div styleName="hero-content">
							<HeroContent 
								title={<span>hershortt.com</span>}
								text="Designermode zu Outlet-Preisen"
							/>
						</div>
					}
				/>

				<section>
					<h2 className="visuallyHidden">Über Hershortt Projekt</h2>
					<SectionWhite 
						title="Unser Vorgehen"
						text="Tandem Marketing & Partners realisierte mit dem Magento-Webshopsystem einen modernen, updatefähigen und skalierbaren Onlineshop, in dem das Angebot von Hershortt.com kompakt und übersichtlich präsentiert werden kann.
Zahlreiche Schnittstellen und Weiterentwicklungen ermöglichen es Hershortt, alle Marktplätze, Lagerbestände, Buchhaltung sowie Auflistungen der Länder nach Performance im Blick zu behalten."
						textColor="var(--color-hershortt-wine)"
					/>
					<VideoPlayer
						playColor={"var(--color-hershortt-wine)"}
						poster={videoPreview}
						videoUid={"E981N-7gmfA"}
					/>
					<div styleName="section-color">
						<SectionColor 
							content={
								<Fragment>
									<p styleName="text">Im Bereich Marketing koordinierten wir Onpage-SEO-Strategien und unterstützten den Shop-Launch durch unterschiedliche Kampagnen, z.B. Remarketing, Newsletter sowie Internationalisierung der Ads. Alle Maßnahmen wurden auf Basis gezielter Analytics-Werkzeuge getroffen.</p>
									{/* <dl styleName="stats-list">
										<div styleName="stats-item">
											<dt>Turnover:</dt>
											<dd>100 500 &#8364;</dd>
										</div>

										<div styleName="stats-item">
											<dt>Traffic:</dt>
											<dd>100 500 unique visitors/day</dd>
										</div>
									</dl> */}
								</Fragment>
							}
							textColor="var(--color-white)"
							backgroundColor="var(--color-hershortt-dark)"
						/>
					</div>
				</section>

				<section>
					<div className="empty-parallax">
						<ParallaxEffect
							image={galleryImg1}
							alt="Tandem Marketing & Partners"
						/>
					</div>
					<div className="project-gallery">
						<div>
							<img src={galleryImg2} alt="Hershortt Projektfotos "  loading="lazy"/>
						</div>
						<div>
							<img src={galleryImg3} alt="Hershortt Projektfotos "  loading="lazy"/>
						</div>
					</div>
				</section>

				<SectionColor 
					content={
						<Fragment>
							<p className="section-text-200">Darüber hinaus gehörte es zu den Aufgaben des Unternehmens, die Website auf Basis der neuesten Adobe Magento PWA und des React-Systems zu aktualisieren. Dadurch wurde die Geschwindigkeit der Seite deutlich erhöht, Nutzer mobiler Geräte konnten die Seite als App installieren, was watch.de bei Google noch konkurrenzfähiger machte.</p>
						</Fragment>
					}
					textColor="var(--color-text)"
					backgroundColor="var(--color-white)"
				/>

				<section className="section">
					<img src={galleryImg4} alt="Hershortt Projektfotos" loading="lazy" />
				</section>

				<section className="section">
					<div className="container-narrow">
						<h2 className="heading-3">Leistungen</h2>
					</div>
					<div className="container" styleName="checkedList">
						<CheckedList 
							content={
								<Fragment>
									<li>Corporate Design</li>
									<li>Zielgruppenforschung</li>
									<li>Eye-Tracking-Analysen</li>
									<li>E-Commerce-Beratung</li>
									<li>Konzeption</li>
									<li>Webshop-Entwicklung und fortlaufende Weiterentwicklung</li>
									<li>Content Marketing</li>
									<li>Social Media Marketing</li>
									<li>Suchmaschinenoptimierung</li>
								</Fragment>
							}
							color="var(--color-hershortt-wine)"
						/>
					</div>
				</section>

				<section styleName="usedTech">
					<UsedTech
						techs={techs}
					/>
				</section>

				<Blog />

				<FeedbackHomePage />
			</main>
			<Footer />
		</div>
	);
};
