import React, { Fragment } from "react";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { ParallaxEffect } from "components/ParallaxEffect";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { CheckedList } from "components/CheckedList";
import { UsedTech } from "components/SourceAboutUs";
import { Blog } from "components/Blog";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from 'assets/images/hero-mymed24.jpg';

import parallaxImg1 from "assets/images/parallax-mymed-1.jpg";
import parallaxImg2 from "assets/images/parallax-mymed-2.jpg";
import parallaxImg3 from "assets/images/parallax-mymed-3.jpg";
import parallaxImg4 from "assets/images/parallax-mymed-4.jpg";

import drupal8 from 'assets/images/icon-drupal-8.svg';
import drupalCommerce from 'assets/images/icon-drupal-commerce.png';
import bootstrap from 'assets/images/icon-b.svg';
import solr from 'assets/images/icon-solr.svg';

import "./Mymed24.css";

const techs = [
	{
		"src" : drupal8,
		"width": 177,
		"height": 44,
		"title": "Drupal 8 CMF"
	},
	{
		"src" : drupalCommerce,
		"width": 208,
		"height": 68,
		"title": "Drupal Commerce"
	},
	{
		"src" : bootstrap,
		"width": 65,
		"height": 52,
		"title": "Bootstrap Framework"
	},
    {
		"src" : solr,
		"width": 103,
		"height": 52,
		"title": "Solr Search Server"
	},
]

export const Mymed24 = () => {
	return (
		<div className="page-wrapper">
			<CommonMetaTags 
				title={'Mymed 24 - internationale, mehrsprachige Plattform für Gesundheitsdienstleister und ihre Patienten'}
				description={'my-med-24.de ist die erste internationale, mehrsprachige Plattform für Gesundheitsdienstleister und ihre Patienten: Ob Kliniken, REHA-Zentren oder Ärzte – my-med-24.de bietet ein umfassendes Netzwerk von medizinischen Dienstleistern aus allen Bereichen.'}
				url={'https://www.tandem-m.de/projekte/85-mymed-24.html'}
				image={heroImg}
			/>
			

			<Header />
			<main>
				<h1 className="visuallyHidden">Mymed 24 - internationale, mehrsprachige Plattform für Gesundheitsdienstleister und ihre Patienten</h1>

				<Hero
					src={heroImg}
					alt="Mymed 24 - internationale, mehrsprachige Plattform für Gesundheitsdienstleister und ihre Patienten"
					width={1920}
					height={800}
					content={
						<div styleName="hero-content">
							<HeroContent 
								title={<span>Mymed 24</span>}
								text="internationale, mehrsprachige Plattform für Gesundheitsdienstleister und ihre Patienten"
							/>
						</div>
					}
				/>

				<section>
					<h2 className="visuallyHidden">Über Mymed 24 Projekt</h2>
					<SectionWhite 
						title="Heute ist watch.de ein komplexes Magento-System"
						text="my-med-24.de ist die erste internationale, mehrsprachige Plattform für Gesundheitsdienstleister und ihre Patienten: Ob Kliniken, REHA-Zentren oder Ärzte – my-med-24.de bietet ein umfassendes Netzwerk von medizinischen Dienstleistern aus allen Bereichen."
						textColor="var(--color-mymed24-light)"
					/>
					<SectionColor 
						content={
							<Fragment>
								<p styleName="text">Tandem Marketing & Partners hat die my-med-24® GmbH & CO. KG von der Idee bis zur finalen Umsetzung der zentralen Plattform für medizinische Dienstleistungen begleitet. Beim Kick-Off wurde herausgearbeitet, dass my-med-24.de sowohl für medizinische Institutionen und Fachkräfte, als auch für Patienten umfassende Services bieten sollte: Wir haben die Herausforderung gerne angenommen. </p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-mymed24-dark)"
					/>
				</section>

				<section>
					<div className="empty-parallax">
						<ParallaxEffect
							image={parallaxImg1}
							alt="Drivelock Projektfotos"
						/>
					</div>
					<SectionColor 
						content={
							<Fragment>
								<p styleName="text">Tandem Marketing & Partners schaffte ein umfassendes Onlineportal, das sowohl unterschiedliche Accounts inklusive verschiedener Abrechnungsmöglichkeiten, Checkouts und Mehrsprachigkeit abdeckt, als auch ein hohes Maß an Sicherheit bietet. Die Wahl fiel auf das Content Management Framework (CMF) Drupal, das technisch allen Anforderungen gerecht wird und zudem besonders übersichtlich und nutzerfreundlich ist. Neben der Programmierung, übernahmen wir die Bereiche Konzeption, Webdesign und Usability.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
					<div className="empty-parallax">
						<ParallaxEffect
							image={parallaxImg4}
							alt="MyMed24 Projektfotos"
						/>
					</div>
					<SectionColor 
						content={
							<Fragment>
								<p styleName="text">Das Angebot wird je nach Abonnement in vier Sprachen veröffentlicht, sodass Teilnehmer mit wenig Aufwand auch internationale Patienten erreichen können. Für arabischsprachige Kunden wurde ein von rechts-nach-links-Algorithmus entwickelt, der alle Inhalte, Grafiken und Funktionsbausteine entsprechend spiegelt.</p>
								<p styleName="text">(Internationale) Patienten wiederum können über das Onlineportal nach Kliniken, Ärzten ,REHA-Zentren und weiteren Medizinischen Kategorien suchen. Ein starkes Filtersystem sowie eine ausgefeilte Suche, basierend auf einem Solr Enterprise Server, ermöglichen die individuelle Suchausgabe durch Filter zu spezifizieren. Die Filter sind dynamisch und passen sich entsprechend der Ergebnisliste an. </p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
                    <SectionColor 
						content={
							<Fragment>
								<p className="section-text-400">Das Ergebnis ist eine moderne übersichtliche und konkurrenzfähige Webseite.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-mymed24-light)"
					/>
					<div className="project-gallery">
						<div>
							<img src={parallaxImg2} alt="MyMed24 Projektfotos"  loading="lazy"/>
						</div>
						<div>
							<img src={parallaxImg3} alt="MyMed24 Projektfotos"  loading="lazy"/>
						</div>
					</div>
				</section>

				<section className="section">
					<div className="container-narrow">
						<h2 className="heading-3">Leistungen</h2>
					</div>
					<div className="container" styleName="checkedList">
						<CheckedList 
							content={
								<Fragment>
									<li>Konzeption</li>
									<li>Programmierung</li>
									<li>Webdesign</li>
									<li>Qualitätssicherung und Consulting</li>
									<li>Usability</li>
								</Fragment>
							}
							color="var(--color-mymed24-light)"
						/>
					</div>
				</section>

				<section styleName="usedTech">
					<UsedTech
						techs={techs}
					/>
				</section>

				<Blog />

				<FeedbackHomePage />
			</main>
			<Footer />
		</div>
	);
};