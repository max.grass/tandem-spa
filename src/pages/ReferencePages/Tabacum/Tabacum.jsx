import React, { Fragment } from "react";
import { Helmet } from "react-helmet";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { ParallaxEffect } from "components/ParallaxEffect";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { OurSteps } from "components/OurSteps";
import { CheckedList } from "components/CheckedList";
import { UsedTech } from "components/SourceAboutUs";
import { Blog } from "components/Blog";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";

import heroImg from 'assets/images/hero-tabacum.jpg';

import parallaxImg1 from "assets/images/parallax-tabacum_02_b.jpg";
import parallaxImg2 from "assets/images/parallax-tabacum_03-ed.jpg";
import parallaxImg3 from "assets/images/parallax-tabacum_04_b.jpg";
import parallaxImg4 from "assets/images/parallax-tabacum_05_b.jpg";

import icon1 from "assets/images/icons/gailing-icon-1.svg";
import icon2 from "assets/images/icons/gailing-icon-2.svg";
import icon3 from "assets/images/icons/gailing-icon-3.svg";
import icon4 from "assets/images/icons/gailing-icon-4.svg";
import icon5 from "assets/images/icons/gailing-icon-5.svg";
import icon6 from "assets/images/icons/gailing-icon-6.svg";
import icon7 from "assets/images/icons/gailing-icon-7.svg";

import html from 'assets/images/icon-html5.svg';
import css from 'assets/images/icon-css3.svg';
import magento from 'assets/images/magento-new-2.svg';
import wordpress from 'assets/images/logo-wordpress.svg';

import "./Tabacum.css";

const techs = [
	{
		"src" : magento,
		"width": 131,
		"height": 44,
		"title": "Magento"
	},
    {
		"src" : wordpress,
		"width": 216,
		"height": 44,
		"title": "Wordpress"
	},
	{
		"src" : html,
		"width": 46,
		"height": 64,
		"title": "html"
	},
	{
		"src" : css,
		"width": 45,
		"height": 64,
		"title": "css"
	},
]

export const Tabacum = () => {
	return (
		<div className="page-wrapper">
			<CommonMetaTags 
				title={'Tabacum'}
				description={'Subheader'}
				url={'https://www.tandem-m.de/projekte/tabacum.html'}
				image={heroImg}
			/>

			<Header />
			<main>
				<h1 className="visuallyHidden">Tabacum - Subheader</h1>

				<Hero
					src={heroImg}
					alt="Tabacum - Subheader"
					width={1920}
					height={800}
					content={
						<div styleName="hero-content">
							<HeroContent 
								title={<span>Tabacum</span>}
								text="Subheader"
							/>
						</div>
					}
				/>

				<section>
					<h2 className="visuallyHidden">Über Tabacum Projekt</h2>
					<SectionWhite 
						title="Lorem ipsum, dolor sit amet consectetur adipisicing elit."
						text="Lorem ipsum, dolor sit amet consectetur adipisicing elit. Facere et quaerat itaque modi, sed esse deserunt officia ea obcaecati tempore, laudantium sint recusandae incidunt exercitationem blanditiis. Aliquam, et. Nulla, obcaecati!"
						textColor="var(--color-tabacum-red)"
					/>
					<SectionColor 
						content={
							<Fragment>
								<p styleName="text">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Facere et quaerat itaque modi, sed esse deserunt officia ea obcaecati tempore, laudantium sint recusandae incidunt exercitationem blanditiis. Aliquam, et. Nulla, obcaecati!</p>
								<p styleName="text">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Facere et quaerat itaque modi, sed esse deserunt officia ea obcaecati tempore, laudantium sint recusandae incidunt exercitationem blanditiis. Aliquam, et. Nulla, obcaecati!</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-tabacum-grey)"
					/>
				</section>

				<section>
					<div className="empty-parallax">
						<ParallaxEffect
							image={parallaxImg1}
							alt="Tabacum Projektfotos"
						/>
					</div>
                    <div className="project-gallery">
						<div>
							<img src={parallaxImg2} alt="Tabacum Projektfotos"  loading="lazy"/>
						</div>
						<div>
							<img src={parallaxImg3} alt="Tabacum Projektfotos"  loading="lazy"/>
						</div>
					</div>
				</section>

				<section className="section">
					<div className="container">
						<h2 className="heading-3">Was wir tun</h2>
					</div>
					<OurSteps
						listData={[
							{
								"iconSrc" : icon1,
								"label" : "Strategische E-Commerce Beratung",
								"figure": "circle"
							},
							{
								"iconSrc" : icon2,
								"label" : "Shop-Konzept",
								"figure": "triangle2",
							},
							{
								"iconSrc" : icon3,
								"label" : "Frontend & Backend",
								"figure": "square"
							},
							{
								"iconSrc" : icon4,
								"label" : "Betreuung",
								"figure": "circle",
                                "description" :
									`<ul>
										<li>Newsletter</li>
										<li>Social Media</li>
                                        <li>Content Marketing</li>
										<li>SEO / SEA</li>
									</ul>`
							}
						]}
					/>
					<OurSteps
						columnGap="5rem"
						listData={[
							{
								"iconSrc" : icon5,
								"label" : "Programmierung",
								"figure": "circle"
							},
							{
								"iconSrc" : icon6,
								"label" : "Qualitätsmanagement",
								"figure": "square"
							},
							{
								"iconSrc" : icon7,
								"label" : "Web-Analyse",
								"figure": "square",
                                "description" :
									`<ul>
										<li>Google Search Console</li>
										<li>Google Analytics</li>
                                        <li>SemRush</li>
										<li>Matomo</li>
									</ul>`
							}
						]}
					/>
					<div className="empty-parallax">
						<img src={parallaxImg4} alt="Tabacum Projektfotos"  loading="lazy"/>
					</div>
				</section>

				<section className="section">
					<div className="container-narrow">
						<h2 className="heading-3">Leistungen</h2>
					</div>
					<div className="container" styleName="checkedList">
						<CheckedList 
							content={
								<Fragment>
									<li>Corporate Design</li>
									<li>Webshop-Entwicklung und fortlaufende Weiterentwicklung</li>
									<li>Zielgruppenforschung</li>
									<li>Content Marketing</li>
									<li>Eye-Tracking-Analysen</li>
                                    <li>Social Media Marketing</li>
                                    <li>E-Commerce-Beratung</li>
                                    <li>Suchmaschinenoptimierung</li>
                                    <li>Konzeption</li>
								</Fragment>
							}
							color="var(--color-tabacum-red)"
						/>
					</div>
				</section>

				<section styleName="usedTech">
					<UsedTech
						techs={techs}
					/>
				</section>

				<Blog />

				<FeedbackHomePage />
			</main>
			<Footer />
		</div>
	);
};