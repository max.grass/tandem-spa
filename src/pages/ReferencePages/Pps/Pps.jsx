import React, { Fragment } from "react";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { ParallaxEffect } from "components/ParallaxEffect";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { Article } from "components/Article";
import { CheckedList } from "components/CheckedList2";
import { UsedTech } from "components/SourceAboutUs";
import { Blog } from "components/Blog";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import {AuthorQuote} from "components/AuthorQuote";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from 'assets/images/hero-pps.jpg';

import parallaxImg1 from "assets/images/parallax-pps_02-b.jpg";
import parallaxImg2 from "assets/images/parallax-pps_03-5.jpg";
import parallaxImg3 from "assets/images/parallax-pps-05_5.jpg";
import parallaxImg4 from "assets/images/parallax-pps_04_2_b.jpg";
import image5 from  "assets/images/pps-06-b-3.jpg";
import image6 from  "assets/images/pps-06-b-4.jpg";
import image7 from  "assets/images/pps-06-b-5.jpg";
import image8 from  "assets/images/pps-06-b-6.jpg";

import markus from "assets/images/markus.png";

import html from 'assets/images/icon-html5.svg';
import css from 'assets/images/icon-css3.svg';
import magento from 'assets/images/magento-new-2.svg';
import js from 'assets/images/icon-java.svg';
import mautic from 'assets/images/icon-mautic.svg';
import varnish from 'assets/images/icon-varnish-cache.svg';

import "./Pps.css";

import {IconMagento} from "pages/ReferencePages/WatchDe/IconMagento";
import pwa from "components/Tech/pwa.svg";
import react from "components/Tech/react.svg";

const techs = [
	{
		"src" : magento,
		"width": 131,
		"height": 44,
		"title": "Magento"
	},
	{
		"src" : pwa,
		"width": 81,
		"height": 32,
		"title": "Adobe PWA"
	},
	{
		"src" : react,
		"width": 56,
		"height": 56,
		"title": "React"
	},
	{
		"src" : mautic,
		"width": 52,
		"height": 52,
		"title": "html"
	},
	{
		"src" : html,
		"width": 46,
		"height": 64,
		"title": "html"
	},
	{
		"src" : css,
		"width": 45,
		"height": 64,
		"title": "css"
	},
]

export const Pps = () => {
	return (
		<div className="page-wrapper">
			<CommonMetaTags 
				title={'PPS Vertrieb'}
				description={'PPS Vertrieb Eins der Größten B2B Fach-Shops Deutschlands'}
				url={'https://www.tandem-m.de/projekte/pps.html'}
				image={heroImg}
			/>

			<Header />
			<main>
				<h1 className="visuallyHidden">PPS Vertrieb - Eins der Größten B2B Fach-Shops Deutschlands</h1>

				<Hero
					src={heroImg}
					alt="PPS Vertrieb"
					width={1920}
					height={800}
					content={
						<div styleName="hero-content">
							<HeroContent 
								title={<span>PPS Vertrieb</span>}
								text="Eins der Größten B2B Fach-Shops Deutschlands"
							/>
						</div>
					}
				/>

				<section>
					<h2 className="visuallyHidden">Der B2B-Shop von PPS-Vertrieb.de</h2>
					<SectionWhite 
						title="Der B2B-Shop von PPS-Vertrieb.de"
						text={
							<Fragment>
								Der B2B-Shop von PPS-Vertrieb.de wurde über viele Jahre in enger Zusammenarbeit mit Tandem Marketing modernisiert und erweitert.
								<br /><br />Seit 2011 unterstützen wir die PPS Vertrieb GmbH im B2B-Commerce und haben gemeinsam mehrere Multi-Webshop-Lösungen entwickelt. Diese Lösungen wurden im Laufe der Jahre kontinuierlich updated und auf die Magento-Version 2.4.x upgraded, einschließlich wiederholter Neugestaltungen. Zusätzlich wurde eine schrittweise Automatisierung umgesetzt.
							</Fragment>
						}
						textColor="var(--color-pps-blue)"
					/>
					<SectionColor 
						content={
							<Fragment>
								<h2 className="heading-3">Unser Vorgehen</h2>
								<p styleName="text">Um den spezifischen Anforderungen im B2B-Commerce gerecht zu werden, haben wir im Laufe der Jahre Benutzererfahrungen gesammelt und zusammen mit PPS Vertrieb GmbH Großkundenbefragungen durchgeführt, um diese Erkenntnisse in die neue Entwicklung einzubeziehen. </p>
								<p styleName="text">Dabei wurden verschiedene Funktionscluster neu entwickelt. Die Arbeit mit verschiedenen Kundengruppen, Preissegmenten, Staffelpreisen, Zubehörbereitstellung, Multi-Wunschlisten, kundenspezifische Rabatte sowie Dashboards für von ERP-Systemen bereitgestellte Rechnungen und andere Unterlagen wurde schrittweise verfeinert. </p>
								<p styleName="text">Darüber hinaus haben wir auch interne Abläufe und Automatisierungen in Zusammenarbeit mit unserem hausinternen ERP-System kontinuierlich erweitert, wodurch der B2B-Commerce immer autonomer wurde.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-pps-grey)"
					/>
				</section>

				<section>
					<div className="empty-parallax">
						<ParallaxEffect
							image={parallaxImg1}
							alt="pps-vertrieb.de Projektfotos"
						/>
					</div>
                    <div className="project-gallery">
						<div>
							<img src={parallaxImg2} alt="pps-vertrieb.de Projektfotos"  loading="lazy"/>
						</div>
						<div>
							<img src={parallaxImg3} alt="pps-vertrieb.de Projektfotos"  loading="lazy"/>
						</div>
					</div>
				</section>

				<section className="section">
					{/*<div className="container">
						<h2 className="heading-3">Was wir tun</h2>
					</div>
					<OurSteps
						listData={[
							{
								"iconSrc" : icon1,
								"label" : "Konzept und Design",
								"figure": "circle"
							},
							{
								"iconSrc" : icon2,
								"label" : "TYPO3 Frontend-Entwicklung",
								"figure": "square"
							},
							{
								"iconSrc" : icon3,
								"label" : "Programmierung der Module",
								"figure": "square"
							},
							{
								"iconSrc" : icon4,
								"label" : "TYPO3 Backend Entwicklung",
								"figure": "circle"
							}
						]}
					/>
					<OurSteps
						columnGap="5rem"
						listData={[
							{
								"iconSrc" : icon5,
								"label" : "Responsive Templateerstellung",
								"figure": "circle"
							},
							{
								"iconSrc" : icon6,
								"label" : "SEO/SEA",
								"figure": "square"
							},
							{
								"iconSrc" : icon7,
								"label" : "Marketing Unterstützung",
								"figure": "square"
							}
						]}
					/>*/}

					<SectionColor
						content={
							<Fragment>
								<h3>PPS erweiterte B2B Vorschlagssuche</h3>
								<p className="section-text-200">Da die Suche im B2B-Commerce eine entscheidende Rolle spielt, haben wir die Suchfunktion von PPS Vertrieb GmbH kontinuierlich verbessert. Dies ermöglichte uns, die Performance durch das Caching häufiger Suchanfragen zu steigern und die Präzision durch tiefgreifende und mehrschichtige Verfeinerung der Elastic Search-Algorithmen zu erhöhen. Unsere Katalogsuche orientiert sich an den Standards globaler Suchmaschinen wie Google: Ein einfaches Eintippen des Namens genügt, und die Suchfunktion schlägt verschiedene Optionen vor. Selbst Tippfehler werden erkannt und automatisch korrigiert.</p>
							</Fragment>
						}
						textColor="var(--color-text)"
						backgroundColor="var(--color-white)"
					/>
					<div className="empty-parallax">
						<img className="wide-image" src={image5} alt="pps-vertrieb.de Projektfotos" width={1920} height={1000} loading="lazy"/>
					</div>

					<SectionColor
						content={
							<Fragment>
								<h3>Erweiterte Produktansichten und Preissegmente</h3>
								<p className="section-text-200">Die Produktgruppen und Preise sind, wie im B2B-Bereich üblich, kundenspezifisch organisiert. Nach dem Einloggen sehen die jeweiligen Kundengruppen individuelle Preise, Rabatte, Staffelpreise und weitere Produktinformationen. Zudem gibt es ein vereinfachtes Zubehörmanagement, das Produktzubehör sowohl einzeln als auch gruppiert verknüpft. Ein komplexes und vollständig automatisiertes Anhangsmanagement sorgt dafür, dass alle Datenblätter in den jeweiligen Produkten stets aktuell sind.</p>
							</Fragment>
						}
						textColor="var(--color-text)"
						backgroundColor="var(--color-white)"
					/>
					<div className="empty-parallax">
						<img className="wide-image" src={image6}  width={1920} height={1000} alt="pps-vertrieb.de Projektfotos"  loading="lazy"/>
					</div>
					<SectionColor
						content={
							<Fragment>
								<h3>Warenkorb und Checkout</h3>
								<p className="section-text-200">Der Warenkorb ermöglicht das schnelle Hinzufügen von Produkten über die Artikelnummer. Je nach Kundengruppe können Produkte nach automatischer Umsatzsteuer-Identifikationsnummerabfrage mehrwertsteuerbefreit bestellt werden. Die Land- und Zeitsteuerung für sämtliche Versanddienstleister ist ebenfalls Teil der Optimierung im B2B-Commerce.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-pps-blue)"
					/>

					<section className="section-m" styleName="buttonLogo">
						<h2 className="visuallyHidden">Über die Magento-Technologie.</h2>
						<Article
							title="Magento Shop für Ihr Geschäftswachstum"
							buttonUrl="/magento-webshop.html"
							buttonCaption={<IconMagento fill="#fff" />}
							img={<img src={image7} style={{display: 'block'}} width={1920} height={1000} alt="pps-vertrieb.de Projektfotos" loading="lazy" />}
							color="var(--color-flamingo)"
						/>
						<section className="section container-narrow">
							<h3 className="section-text-400">Magento 2 Backend</h3>
							<p>Für die Verarbeitung von Bestellungen und Kundenmanagement haben wir ein umfassendes ERP-System entwickelt. Dies unterstützt die Sachbearbeiter dabei, Kaufabwicklungen im Backend effizient und korrekt durchzuführen. Das System umfasst Module wie das Reparatur-Buch, Lieferantenverwaltung, automatische Verknüpfungen für Privatankäufe, Management von Teilzahlungen und Auktionen sowie Druckschnittstellen für Etiketten und Uhrenpässe. Darüber hinaus bietet es erweiterte Bestellmanagement-Funktionen, individualisierte Ansichten, Berichte, eine Inventurfunktion, die Möglichkeit zur POS-Abwicklung und vieles mehr.</p>
						</section>
					</section>

					<div className="empty-parallax">
						<img className="wide-image" src={image8}  width={1920} height={1000} alt="pps-vertrieb.de Projektfotos"  loading="lazy"/>
					</div>

					<SectionColor
						content={
							<Fragment>
								<h3>Online Marketing Strategien</h3>
								<p className="section-text-200">Wir sind auch für das fortlaufende Management der digitalen Marketingkanäle verantwortlich, darunter SEO, Newsletter und Social Media Marketing (SMM). Unsere Strategien zur Steigerung der Bekanntheit von PPS Vertrieb basieren auf klar definierten KPIs. In enger Abstimmung mit der PPS-Leitung passen wir die Kampagnen an die Kundenbedürfnisse an und steigern die Conversion-Raten durch gezieltes Targeting und Überwachung der Conversion-Raten.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-pps-grey)"
					/>

				</section>

				<section className="section-m">
					<div className="container-narrow">
						<h2 className="heading-3">Leistungen</h2>
						<div styleName="checked-list">
							<CheckedList
								title="Webentwicklung:"
								color="var(--color-pps-blue)"
								content={
									<Fragment>
										<li>Server- und DevOps-Umgebungseinrichtung</li>
										<li>Nginx, Docker, Load Balancer und sämtliche Site Reliability Maßnahmen</li>
										<li>Continuous Integration und Continuous Deployment (CI/CD) Prozesse</li>
										<li>Responsive Bootstrap Frontend-Entwicklung </li>
										<li>Magento 2.4.x Backend-Entwicklung</li>
										<li>Migration von Magento 1 (M1) auf Magento 2 (M2)</li>
										<li>Implementierung des Elasticsearch Searchservers mit optimierten Schemen</li>
										<li>Nachprogrammierung zahlreicher Backend-Module</li>
										<li>Umprogrammierung und Integration diverser Erweiterungen</li>
										<li>Erzeugung von Nginx-Redirects unter Beibehaltung der URL-Struktur</li>
										<li>FPC Web Performance Optimierung und Caching Warmer-Lösungen</li>
										<li>Sicherheitsüberprüfungen und -optimierungen</li>
										<li>Integration von Drittanbieter-APIs und -Diensten</li>
										<li>Multi-Site und Multi-Language Implementierungen</li>
										<li>Newsletter-Einrichtung und -Versand über Mautic Cloud</li>
										<li>Omni Channel Integrationen (z.B. m2e)</li>
									</Fragment>
								}
							/>
							<CheckedList
								title="Online Marketing / UI/UX:"
								color="var(--color-pps-blue)"
								content={
									<Fragment>
										<li>Eye-Tracking-Analysen</li>
										<li>Zielgruppenanalyse und Erstellung von B2B Buyer-Personas</li>
										<li>Entwicklung des UI/UX Webinterfaces</li>
										<li>Content Marketing in Kombination mit SEO</li>
										<li>Social Media Marketing und Management</li>
										<li>Newsletter-Marketing</li>
										<li>A/B-Testing und Multivariates Testing</li>
										<li>Usability-Tests</li>
										<li>Retargeting-Kampagnen inklusive Conversion-Tracking</li>
										<li>Analysen von User Journeys und Conversion-Funnel</li>
										<li>Marketing-Automatisierung</li>
										<li>Commerce Strategien</li>
									</Fragment>
								}
							/>
						</div>
					</div>
				</section>

				<AuthorQuote
					photo={markus}
					authorName="Markus Göbbels"
					authorPosition="PPS CTO"
					text={
						<>
							<p>Mit Tandem haben wir einen innovativen und kompetenten Partner gewinnen können.In sehr vielen gemeinsamen Projekten konnten wir eine starke Vertrauensbasis auf Augenhöhe entwickeln.Auch die zukunftsorientierte Entwicklung unserer Onlinedienste wird stark durch die Kreativität von Tandem beflügelt.</p>
						</>
					}
					textColor="var(--color-white)"
					backgroundColor="var(--color-pps-blue)"
				/>

				<div className="empty-parallax">
					<img className="wide-image" src={parallaxImg4} alt="pps-vertrieb.de Projektfotos"  loading="lazy"/>
				</div>
				<section styleName="usedTech">
					<UsedTech
						techs={techs}
					/>
				</section>

				<Blog />

				<FeedbackHomePage />
			</main>
			<Footer />
		</div>
	);
};
