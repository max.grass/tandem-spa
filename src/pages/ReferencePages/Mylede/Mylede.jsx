import React, { Fragment } from "react";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { ParallaxEffect } from "components/ParallaxEffect";
import { CheckedList } from "components/CheckedList";
import { UsedTech } from "components/SourceAboutUs";
import { Blog } from "components/Blog";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from "assets/images/myle-ref-myle-05-3-b.jpg";
import galleryImg1 from "assets/images/gallery-mylede-1.jpg";
import galleryImg2 from "assets/images/myle-ref-myle-06-b.jpg";
import galleryImg3 from "assets/images/myle-36-2.jpg";
import galleryImg4 from "assets/images/gallery-mylede-4.jpg";


import bootstrap from 'assets/images/logos/bootstrap.svg';
import drupal from 'assets/images/logos/drupal.svg';
import html from 'assets/images/icon-html5.svg';
import css from 'assets/images/icon-css3.svg';
import drupalcom from 'assets/images/logos/drupal-commerce.png';

const techs = [
	{
		"src" : drupal,
		"width": 177,
		"height": 44,
		"title": "Drupal"
	},
	{
		"src" : drupalcom,
		"width": 208,
		"height": 68,
		"title": "drupal commerce"
	},
	{
		"src" : bootstrap,
		"width": 65,
		"height": 52,
		"title": "bootstrap"
	},
	{
		"src" : html,
		"width": 46,
		"height": 64,
		"title": "html"
	},
	{
		"src" : css,
		"width": 46,
		"height": 64,
		"title": "css"
	}
]

import "./Mylede.css";

export const Mylede = () => {
	return (
		<div className="page-wrapper">
			<CommonMetaTags 
				title={'myle.de - Interaktiver Online-Marktplatz für Leinfelden-Echterdingen'}
				description={'myle.de ist ein lokaler Online-Marktplatz für Leinfelden-Echterdingen, auf dem sich ansässige Händler und Dienstleister online präsentieren können.'}
				url={'https://www.tandem-m.de/projekte/75-myle-de.html'}
				image={heroImg}
			/>

			<Header />
			<main>
				<h1 className="visuallyHidden">myLE.de</h1>

				<Hero
					src={heroImg}
					alt="myLE.de"
					width={1920}
					height={800}
					content={
						<div styleName="hero-content">
							<HeroContent 
								title={<span>myLE.de</span>}
								text="Lokaler Online-Marktplatz für Leinfelden-Echterdingen"
							/>
						</div>
					}
				/>

				<section>
					<h2 className="visuallyHidden">Eine Stadt geht online</h2>
					<SectionWhite 
						title="Eine Stadt geht online"
						text="myle.de ist ein lokaler Online-Marktplatz für Leinfelden-Echterdingen für ansässige Händler und Dienstleister. Gerade Betriebe, die bisher keinen eigenen Internetauftritt hatten, bekommen so die Chance, sich gegen die Konkurrenz aus dem Internet zu behaupten und auch online Präsenz zu zeigen."
						textColor="var(--color-myle-orange)"
					/>
					<div styleName="section-color">
						<SectionColor 
							content={
								<Fragment>
									<p styleName="text">Hinter der Idee zu myle.de steht der BDS Bund der Selbstständigen Leinfelden-Echterdingen e.V., zu dessen Satzungszielen die Unterstützung mittelständischer Betriebe vor Ort gehört. Tandem Marketing & Partners hat den BDS von der Idee bis zur finalen Umsetzung des Online-Marktplatzes begleitet.</p>
									{/* <dl styleName="stats-list">
										<div styleName="stats-item">
											<dt>Nutzer 2021:</dt>
											<dd>223.271</dd>
										</div>

										<div styleName="stats-item">
											<dt>Seitenaufrufe 2021: </dt>
											<dd>447.437</dd>
										</div>
									</dl> */}
								</Fragment>
							}
							textColor="var(--color-white)"
							backgroundColor="var(--color-myle-dark)"
						/>
					</div>
				</section>

				<section>
					<div className="empty-parallax">
						<ParallaxEffect
							image={galleryImg1}
							alt="Tandem Marketing & Partners"
						/>
					</div>
				</section>

				<SectionColor
					content={
						<Fragment>
							<p className="section-text-200">Seit Onlinegang im Jahr 2016 hat sich myle.de zu einer dynamischen Online-Plattform für die Stadt Leinfelden-Echterdingen – kurz LE – entwickelt. Hier präsentieren sich ansässige Unternehmen, Vereine, Gastro-Betriebe sowie Hotels auf einer Plattform. </p>
							<p className="section-text-200">Mit Multiaccounts und Commerce Funktionen ist es den Betreibern möglich, eigene WebPräsenzen, mit Filialen, Galerien, News und Aktionen, Stellenanzeigen, sowie eigenständigem Shop über ein DashBoard zu verwalten. Dabei Werden alle Kundendaten, Bestellungen, Zahlarten und weitere Features ähnlich wie bei Amazon individuell verwaltet. Abgesehen davon, hat myle.de auch weitere Vorteile, welche die Plattform zur Zentraler Anlaufstelle für die Bürger macht.</p>
							<p className="section-text-200">myle.de verfügt auch über eine Kleinanzeigen Funktion, die ähnlich wie bei ebay Kleinanzeigen allen Bürgern zur verfügung gestellt wird, News rund ums LE, sowie städtische Events wie Krautfest, Gastronomenmap, ViaLE Digital und Social Media Itegrationen machen myle.de zur Plattform Nr. 1 in der Region. </p>
						</Fragment>
					}
					textColor="var(--color-dark)"
					backgroundColor="var(--color-white)"
				/>
				<section>
					<img src={galleryImg4} alt="wacthde Projektfotos" loading="lazy" />
				</section>
				<SectionColor
					content={
						<Fragment>
							<p className="section-text-200">in einem Jahr besuchen über 90% aller Leinfelden Echterdinger einwohner mindestens einmal im Monat myle.de</p>
							<p className="section-text-200">Besucher und Bewohner der Stadt erhalten ein umfassendes Bild von Leinfelden-Echterdingen, können Bewertungen und Kommentare schreiben und sogar an allgemeinen Umfragen und bei den digitalen Events (Kopfschmücken) mitmachen.</p>
							<p className="section-text-200">Kulturelle Highlights wie das Krautfest oder Kunst bewegt LE werden ebenso hervorgehoben wie die Onlineversion des Stadtmagazins viaLE.</p>
						</Fragment>
					}
					textColor="var(--color-dark)"
					backgroundColor="var(--color-white)"
				/>

				<section>
					<SectionColor
						content={
							<Fragment>
								<p className="section-text-400">Tandem Marketing & Partners unterstützt die myle-Macher weiterhin in der Entwicklung (es kommen jährlich immer Mehr funktionen hinzu), Support und Online Marketing.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-myle-orange)"
					/>
					<div className="project-gallery">
						<div>
							<img src={galleryImg2} alt="wacthde Projektfotos" loading="lazy" />
						</div>
						<div>
							<img src={galleryImg3} alt="wacthde Projektfotos" loading="lazy" />
						</div>
					</div>
				</section>

				<section className="section">
					<div className="container-narrow">
						<h2 className="heading-3">Leistungen</h2>
					</div>
					<div className="container" styleName="checkedList">
						<CheckedList 
							content={
								<Fragment>
									<li>Corporate Design</li>
									<li>Zielgruppenforschung</li>
									<li>Eye-Tracking-Analysen</li>
									<li>E-Commerce-Beratung</li>
									<li>Konzeption</li>
									<li>Webshop-Entwicklung und fortlaufende Weiterentwicklung</li>
									<li>Content Marketing</li>
									<li>Social Media Marketing</li>
									<li>Suchmaschinenoptimierung</li>
								</Fragment>
							}
							color="var(--color-myle-orange)"
						/>
					</div>
				</section>

				<section styleName="usedTech">
					<UsedTech
						techs={techs}
					/>
				</section>

				<Blog />

				<FeedbackHomePage />
			</main>
			<Footer />
		</div>
	);
};
