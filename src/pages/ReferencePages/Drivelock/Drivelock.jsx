import React, { Fragment } from "react";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { ParallaxEffect } from "components/ParallaxEffect";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { CheckedList } from "components/CheckedList";
import { UsedTech } from "components/SourceAboutUs";
import { Blog } from "components/Blog";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from 'assets/images/hero-drivelock.jpg';

import parallaxImg1 from "assets/images/parallax-drivelock-01.jpg";
import parallaxImg2 from "assets/images/parallax-drivelock-02.jpg";
import parallaxImg3 from "assets/images/parallax-drivelock-03.jpg";
import parallaxImg4 from "assets/images/parallax-drivelock-04.jpg";

import html from 'assets/images/icon-html5.svg';
import css from 'assets/images/icon-css3.svg';
import typo3 from 'assets/images/icon-typo3.svg';

import "./Drivelock.css";

const techs = [
	{
		"src" : typo3,
		"width": 158,
		"height": 44,
		"title": "typo3"
	},
	{
		"src" : html,
		"width": 46,
		"height": 64,
		"title": "html"
	},
	{
		"src" : css,
		"width": 45,
		"height": 64,
		"title": "css"
	},
]

export const Drivelock = () => {
	return (
		<div className="page-wrapper">
			<CommonMetaTags 
				title={'DriveLock - Security Software'}
				description={'Die CenterTools Software GmbH gehört mit ihrer Security Software DriveLock zu den führenden Spezialisten für IT- und Datensicherheit. Über 2.000 Kunden in mehr als 30 Ländern zeugen davon.'}
				url={'https://www.tandem-m.de/projekte/32-drivelock.html'}
				image={heroImg}
			/>

			<Header />
			<main>
				<h1 className="visuallyHidden">DriveLock - Security Software</h1>

				<Hero
					src={heroImg}
					alt="DriveLock - Security Software"
					width={1920}
					height={800}
					content={
						<div styleName="hero-content">
							<HeroContent 
								title={<span>DriveLock</span>}
								text="Security Software"
							/>
						</div>
					}
				/>

				<section>
					<h2 className="visuallyHidden">Über DriveLock Projekt</h2>
					<SectionWhite 
						title="Das neue DriveLock-Portal"
						text="Die CenterTools Software GmbH gehört mit ihrer Security Software DriveLock zu den führenden Spezialisten für IT- und Datensicherheit. Über 2.000 Kunden in mehr als 30 Ländern zeugen davon."
						textColor="var(--color-drivelock-blue)"
					/>
					<SectionColor 
						content={
							<Fragment>
								<p styleName="text">Die alte Website des Ludwigsburger Unternehmens konnte seine innovative und dynamische Kraft nicht mehr widerspiegeln - hier kam Tandem Marketing ins Spiel.</p>
								<p styleName="text">Tandem Marketing übernahm Konzeption, Design und technische Umsetzung der neuen Website von CenterTools. Um die Website-Usability zu verbessern wurden die umfassenden Inhalte der alten Webpräsenz neu strukturiert und für alle Endgeräte optimiert. Dabei wurde ein starker Fokus auf Call-to-Actions gelegt, um Unternehmen, Privatnutzer und potenzielle Partner gleichermaßen anzusprechen und die Conversion-Rate zu verbessern. Das neue DriveLock-Portal wurde dabei nahtlos mit dem bestehenden Onlineshop sowie dem Help-Desk-System von CenterTools verknüpft.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-drivelock-blue)"
					/>
				</section>

				<section>
					<div className="empty-parallax">
						<ParallaxEffect
							image={parallaxImg1}
							alt="DriveLock Projektfotos"
						/>
					</div>
					<SectionColor 
						content={
							<Fragment>
								<p styleName="text">Die neue Webseite sollte nutzerfreundlich sein, daher wurde eine Download-Steuerung für B2B- und B2C-Nutzer programmiert. Dank dieser Funktion, für die eine Cookie-Steuerung eingesetzt wurde, muss der Kunde nur der Eingabeaufforderung folgen und der Download startet automatisch.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
					<div className="project-gallery">
						<div>
							<img src={parallaxImg2} alt="DriveLock Projektfotos"  loading="lazy"/>
						</div>
						<div>
							<img src={parallaxImg3} alt="DriveLock Projektfotos"  loading="lazy"/>
						</div>
					</div>
					<SectionColor 
						content={
							<Fragment>
								<p styleName="text">Darüber hinaus wurde eine filterbare Händlerliste auf Basis von Google Maps erstellt, mit der Nutzer leicht einen passenden Händler für ihre Ansprüche finden.</p>
								<p styleName="text">Um die Verwaltung der Kundenkontakte für CenterTools effektiver zu gestalten, programmierte Tandem Marketing einen TYPO3-Navision-CRM-Connector, der einen automatischen Lead-Export möglich macht.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
				</section>

				<section className="section">
					<div className="empty-parallax">
						<img src={parallaxImg4} alt="DriveLock Projektfotos" loading="lazy" />
					</div>
				</section>

				<section className="section">
					<div className="container-narrow">
						<h2 className="heading-3">Leistungen</h2>
					</div>
					<div className="container" styleName="checkedList">
						<CheckedList 
							content={
								<Fragment>
									<li>Konzeption</li>
									<li>Laufende Weiterentwicklung</li>
									<li>Webdesign</li>
									<li>Support</li>
									<li>Website-Entwicklung</li>
									<li>Suchmaschinenoptimierung</li>
									<li>Content Marketing</li>
								</Fragment>
							}
							color="var(--color-drivelock-blue)"
						/>
					</div>
				</section>

				<section styleName="usedTech">
					<UsedTech
						techs={techs}
					/>
				</section>

				<Blog />

				<FeedbackHomePage />
			</main>
			<Footer />
		</div>
	);
};
