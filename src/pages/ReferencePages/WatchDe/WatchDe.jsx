import React, { Fragment } from "react";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { ParallaxEffect } from "components/ParallaxEffect";
import { CheckedList } from "components/CheckedList2";
import { UsedTech } from "components/SourceAboutUs";
import { Article } from "components/Article";

import { CommonMetaTags } from "components/CommonMetaTags";
import { Blog } from "components/Blog";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { IconMagento } from "./IconMagento";
import {AuthorQuote} from "components/AuthorQuote";

import heroImg from "assets/images/hero-watch-de.jpg";
import parallaxImg from "assets/images/parallax-watch-de-1-2.jpg";
import galleryImg1 from "assets/images/gallery-watch-de-2-2.jpg";
import galleryImg2 from "assets/images/gallery-watch-de-2.jpg";
import galleryImg3 from "assets/images/ref-watch-de-001-2.jpg";

import image5 from "assets/images/ref-watch-de-005.jpg";
import image6 from "assets/images/ref-watch-de-006.jpg";
import image7 from "assets/images/ref-watch-de-007.jpg";
import image8 from "assets/images/ref-watch-de-008.jpg";

import ralf from "assets/images/rh-photo-min.png";

import magento from 'assets/images/magento-new-2.svg';
import wordpress from 'assets/images/logo-wordpress.svg';
import html from 'assets/images/icon-html5.svg';
import css from 'assets/images/icon-css3.svg';
import pwa from 'components/Tech/pwa.svg';
import react from 'components/Tech/react.svg';

const techs = [
	{
		"src" : magento,
		"width": 131,
		"height": 44,
		"title": "Magento"
	},
	{
		"src" : wordpress,
		"width": 216,
		"height": 44,
		"title": "Wordpress"
	},
	{
		"src" : pwa,
		"width": 81,
		"height": 32,
		"title": "Adobe PWA"
	},
	{
		"src" : react,
		"width": 56,
		"height": 56,
		"title": "React"
	},
	{
		"src" : html,
		"width": 46,
		"height": 64,
		"title": "html"
	},
	{
		"src" : css,
		"width": 45,
		"height": 64,
		"title": "css"
	}
]

import "./WatchDe.css";

const title = 'Watch.de - Werbeagentur stuttgart /projekte/52-watch-de.html';
const description = 'Watch.de bezeichnet sich selbst als größter Internetshop für exklusive Markenuhren und Schmuck. Tatsächlich war watch.de einer der ersten Webshops der Uhrenbranche, als er 1994 online...';
const url = 'https://www.tandem-m.de/projekte/52-watch-de.html';

export const WatchDe = () => {
	return (
		<div className="page-wrapper">
			<CommonMetaTags 
				title={title}
				description={description}
				url={url}
				image={heroImg}
			/>

			<Header />
			<main>
				<h1 className="visuallyHidden">watch.de Online-Shop-Projektseite</h1>

				<Hero
					src={heroImg}
					alt="Luxusuhr als Geschenk an die Lieben"
					width={1920}
					height={800}
					content={
						<div styleName="hero-content">
							<HeroContent 
								title={<span>watch.de</span>}
								text="Größter Internetshop für exklusive Markenuhren und Schmuck"
							/>
						</div>
					}
				/>

				<section>
					<h2 className="visuallyHidden">Über watch.de-Projekt</h2>
					<SectionWhite 
						title="Heute ist watch.de eine komplexe PWA mit einem Magento Commerce Backend"
						text="Seit 2010 betreuen wir für watch.de den gesamten .com Bereich, der jüngste revolutionärer PWA-Relaunch beinhaltet ein Upgrade von Magento 1.9 auf 2.4.x und viele zusätzliche Erweiterungen."
						textColor="var(--color-fungreen)"
					/>
					<div>
						<SectionColor
							content={
								<Fragment>
									<h2 className="heading-3">Unser Vorgehen:</h2>
									<p>Durch Zielgruppenforschung, Eye-Tracking und weitere Marketinganalysen konnte Tandem Marketing präzise ermitteln, welche Aspekte watch.de für einen erfolgreichen PWA-Relaunch berücksichtigen sollte.  Basierend auf diesen Ergebnissen wurde das neue UX/UI-Design entwickelt, welches dank KI-Tools schon in der Prototypen-Phase intensiv getestet wurde. </p>
									<p>Wir haben in mehreren Schritten den alten Magento 1 Webshop sowie die gesamte Warenwirtschaft von watch.de auf Magento 2.4.X umprogrammiert und konnten alle Daten erfolgreich auf das neue System migrieren</p>
									<p>Eine eigens entwickelte Serverarchitektur mit Monitoring-Einheiten und integriertem Prerendering hat nach dem Launch kontinuierlich zur Performance-Optimierung beigetragen.</p>
									{/* <dl styleName="stats-list">
										<div styleName="stats-item">
											<dt>Turnover:</dt>
											<dd>100 500 &#8364;</dd>
										</div>

										<div styleName="stats-item">
											<dt>Traffic:</dt>
											<dd>100 500 unique visitors/day</dd>
										</div>
									</dl> */}
								</Fragment>
							}
							textColor="var(--color-white)"
							backgroundColor="var(--color-fungreen)"
						/>
					</div>
				</section>

				<section>
					<div className="empty-parallax">
						<ParallaxEffect
							image={parallaxImg}
							alt="wacthde Projektfotos"
						/>
					</div>
					<div className="project-gallery">
						<div>
							{/* <div className="video-wrp">
								<iframe className="video-wrp__video" src="https://www.youtube.com/embed/_yE3phfw68o" frameborder="0"></iframe>
							</div> */}
							<img src={galleryImg2} alt="wacthde Projektfotos" loading="lazy" />
						</div>
						<div>
							<img src={galleryImg1} alt="wacthde Projektfotos" loading="lazy" />
						</div>
					</div>
				</section>

				<section>
                    <SectionColor 
                        content={
                            <Fragment>
								<h3>watch.de Auktion</h3>
                                <p className="section-text-200">Watch.de verfügt über eine eigenständige Auktionsplattform, auf der sich Bieter registrieren und an Versteigerungen teilnehmen können. Die Kunden können auf eine Bilderhistorie zugreifen und den gesamten Kauf inklusive Bezahlung über den Webshop abwickeln.  </p>
                            </Fragment>
                        }
                        textColor="var(--color-text)"
                        backgroundColor="var(--color-white)"
                    />
					<img src={galleryImg3} style={{display: 'block'}} alt="wacthde Projektfotos" loading="lazy" />

					<SectionColor 
                        content={
                            <Fragment>
								<h3>Optimierte Kategorie- und Produktansicht</h3>
                                <p className="section-text-200">Die Kategorie- und Produktansichten auf watch.de sind so konzipiert, dass sie trotz der Informationsvielfalt den Benutzer nicht überfordern. Der Fokus liegt stets auf den wesentlichen Produktmerkmalen. Mit Hilfe von schnellen TOP-Kategoriefiltern und einer erweiterten Filternavigation kann der User gezielt nach gewünschten Pretiosen suchen und diese einfach dem Warenkorb hinzufügen.</p>
                            </Fragment>
                        }
                        textColor="var(--color-white)"
                        backgroundColor="var(--color-fungreen)"
                    />

					<img src={image5} style={{display: 'block'}} alt="wacthde Projektfotos" loading="lazy" />

					<SectionColor
						content={
							<>
								<h3>Optimierte Elasticsearch-Suchfunktion für die PWA: </h3>
								<p>In die PWA haben wir die von Magento bevorzugte Elasticsearch-Suche integriert und die GraphQL-Abfragen verfeinert. Die Katalogsuche folgt den Standards globaler Suchmaschinen wie Google: Ein einfaches Eintippen des Namens reicht aus, und die Suchfunktion schlägt verschiedene Optionen vor. Selbst Tippfehler werden erkannt und automatisch korrigiert.</p>
							</>
						}
						textColor="var(--color-text)"
						backgroundColor="var(--color-white)"
					/>

					<img src={image6} style={{display: 'block'}} alt="wacthde Projektfotos" loading="lazy" />


					<SectionColor
						content={
							<Fragment>
								<h3>Warenkorb und Checkout</h3>
								<p>Unser Team hat ein intelligentes Checkout-System entwickelt, das den internationalen Handel erleichtert. Die Steuerung erfolgt nach Regionen und Warengruppen, mit Versandkostenmodellen basierend auf Gewicht und Land. Dazu wurden vielfältige Zahlungsmethoden implementiert, einschließlich Finanzierungsoptionen und Apple Pay.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-fungreen)"
					/>

					<section className="section-m" styleName="buttonLogo">
						<h2 className="visuallyHidden">Über die Magento-Technologie.</h2>
						<Article
							title="Magento Shop für Ihr Geschäftswachstum"
							buttonUrl="/magento-webshop.html"
							buttonCaption={<IconMagento fill="#fff" />}
							img={<img src={image7} style={{display: 'block'}} width={1920} height={1000} alt="wacthde Projektfotos" loading="lazy" />}
							color="var(--color-flamingo)"
						/>
						<section className="section container-narrow">
							<h3 className="section-text-400">Magento 2 Backend</h3>
							<p>Für die Verarbeitung von Bestellungen und Kundenmanagement haben wir ein umfassendes ERP-System entwickelt. Dies unterstützt die Sachbearbeiter dabei, Kaufabwicklungen im Backend effizient und korrekt durchzuführen. Das System umfasst Module wie das Reparatur-Buch, Lieferantenverwaltung, automatische Verknüpfungen für Privatankäufe, Management von Teilzahlungen und Auktionen sowie Druckschnittstellen für Etiketten und Uhrenpässe. Darüber hinaus bietet es erweiterte Bestellmanagement-Funktionen, individualisierte Ansichten, Berichte, eine Inventurfunktion, die Möglichkeit zur POS-Abwicklung und vieles mehr.</p>
						</section>
					</section>

					<SectionColor
						content={
							<>
								<h3>Online Marketing Maßnahmen</h3>
								<p>Zu unseren weiteren Aufgaben zählt die kontinuierliche Betreuung von Online-Marketing-Kanälen wie SEA, SEO, Newsletter, SMM und Videographie. Basierend auf festgelegten KPIs entwickeln und implementieren wir Strategien, um die Bekanntheit von watch.de zu steigern. Durch gezieltes Targeting und Conversion-Rate-Monitoring passen wir das Sortiment gemeinsam mit watch.de Leitung an die Kundenanfragen an und erhöhen so die Conversions.</p>
							</>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-fungreen)"
					/>
					<img src={image8} style={{display: 'block'}} alt="wacthde Projektfotos" loading="lazy" />

                </section>

				<section className="section">
					<div className="container-narrow">
						<h2 className="heading-3">Leistungen</h2>
						<div styleName="checked-list">
							<CheckedList
								title="Webentwicklung:"
								color="var(--color-fungreen)"
								content={
									<Fragment>
										<li>Server- und DevOps-Umgebungseinrichtung</li>
										<li>Nginx, Docker, Load Balancer und sämtliche Site Reliability Maßnahmen</li>
										<li>Continuous Integration und Continuous Deployment (CI/CD) Prozesse</li>
										<li>Responsive - Headless PWA React Frontend-Entwicklung und Service Worker
											Implementierung
										</li>
										<li>Magento 2.4.x Backend-Entwicklung</li>
										<li>Migration von Magento 1 (M1) auf Magento 2 (M2)</li>
										<li>Implementierung des Elasticsearch Searchservers mit optimierten GraphQL
											Schemen
										</li>
										<li>Nachprogrammierung zahlreicher Backend-Module</li>
										<li>Umprogrammierung und Integration diverser Erweiterungen</li>
										<li>Erzeugung von Nginx-Redirects unter Beibehaltung der URL-Struktur</li>
										<li>Prerender-Implementierung für PWA</li>
										<li>Web Performance Optimierung und Caching-Lösungen</li>
										<li>Sicherheitsüberprüfungen und -optimierungen</li>
										<li>Integration von Drittanbieter-APIs und -Diensten</li>
										<li>Multi-Site und Multi-Language Implementierungen</li>
										<li>Newsletter-Einrichtung und -Versand über Mautic Cloud</li>
										<li>Omni Channel Integrationen (z.B. m2e)</li>
									</Fragment>
								}
							/>
							<CheckedList
								title="Online Marketing / UI/UX:"
								color="var(--color-fungreen)"
								content={
									<Fragment>
										<li>Eye-Tracking-Analysen</li>
										<li>Zielgruppenanalyse und Erstellung von Buyer-Personas</li>
										<li>Entwicklung des UI/UX Webinterfaces</li>
										<li>Content Marketing in Kombination mit SEO</li>
										<li>Videographie</li>
										<li>Suchmaschinenmarketing (SEA)</li>
										<li>Social Media Marketing und Management</li>
										<li>Influencer-Marketing</li>
										<li>Affiliate-Marketing</li>
										<li>Newsletter-Marketing</li>
										<li>A/B-Testing und Multivariates Testing</li>
										<li>Usability-Tests</li>
										<li>Retargeting-Kampagnen inklusive Conversion-Tracking</li>
										<li>Analysen von User Journeys und Conversion-Funnel</li>
										<li>Marketing-Automatisierung</li>
										<li>Commerce Strategien</li>
									</Fragment>
								}
							/>
						</div>

					</div>

				</section>

				<AuthorQuote
					photo={ralf}
					authorName="Ralf Häffner"
					authorPosition="Inhaber"
					text={
						<>
							<p>Seit über 13 Jahren arbeiten wir partnerschaftlich zusammen und haben während dieser Zeit bemerkenswerte Fortschritte erzielt. Wir haben nicht nur den technologischen Wandel von Perl auf M1 erfolgreich durchgeführt, sondern auch mehrere Upgrades und Relaunches erfolgreich umgesetzt. Ein herausragender Meilenstein war die gemeinsame Umsetzung der PWA-Erweiterung inklusive Magento 2-Upgrade in Zusammenarbeit mit Tandem.</p>
							<p>Die Zusammenarbeit mit Andreas Schmidt und seinem Tandem-Team bereitet uns Freude. Ihre Fachkompetenz und ihr Gespür für zukünftige Trends haben uns beeindruckt. Gemeinsam haben wir einen fortschrittlichen Onlineshop gestaltet, der sowohl im Design als auch in der technischen Umsetzung wegweisend ist.</p>
							<p>Unser Backend wurde durch maßgeschneiderte Erweiterungen perfekt auf unsere Anforderungen angepasst. Tandem hat eine individuelle Lösung geschaffen, die wir von keiner anderen Agentur zugetraut hätten.</p>
							<p>Neben der Optimierung unserer Webseite und unseres Backends betreut Tandem auch all unsere Marketing-Aktivitäten. Wöchentliche Blogbeiträge, Videoinhalte, Newsletter, Inhalte für Plattformen wie Youtube und Instagram sowie sämtliche Designprojekte werden zeitnah und zur vollsten Zufriedenheit umgesetzt.</p>
							<p>Die Zusammenarbeit mit Andreas Schmidt und seinem Team erfüllt uns mit Freude und wir freuen uns auf kommende Projekte!</p>
						</>
					}
					textColor="var(--color-white)"
					backgroundColor="var(--color-fungreen)"
				/>

				<div styleName="mt-0">
					{/*<img className="wide-image" width={1920} height={100} src={galImage4} alt="Mettler GmbH Projektfotos 2" loading="lazy" />*/}
				</div>

				<section styleName="usedTech">
					<UsedTech
						techs={techs}
					/>
				</section>

				<Blog />

				<FeedbackHomePage />
			</main>
			<Footer />
		</div>
	);
};
