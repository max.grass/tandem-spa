import React, { Fragment } from "react";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { ParallaxEffect } from "components/ParallaxEffect";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { CheckedList } from "components/CheckedList";
import { UsedTech } from "components/SourceAboutUs";
import { Blog } from "components/Blog";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from 'assets/images/hero-8man.jpg';

import parallaxImg1 from "assets/images/parallax-8man-02-b.jpg";
import parallaxImg2 from "assets/images/parallax-8man-05.jpg";
import parallaxImg3 from "assets/images/parallax-8man-04-b.jpg";
import parallaxImg4 from "assets/images/parallax-8man-03-2.jpg";
import parallaxImg5 from "assets/images/parallax-8man-06.jpg";

import html from 'assets/images/icon-html5.svg';
import css from 'assets/images/icon-css3.svg';
import typo3 from 'assets/images/icon-typo3.svg';

import "./EightMan.css";

const techs = [
	{
		"src" : typo3,
		"width": 158,
		"height": 44,
		"title": "typo3"
	},
	{
		"src" : html,
		"width": 46,
		"height": 64,
		"title": "html"
	},
	{
		"src" : css,
		"width": 45,
		"height": 64,
		"title": "css"
	},
]

export const EightMan = () => {
	return (
		<div className="page-wrapper">
			<CommonMetaTags 
				title={'8man'}
				description={'Die Protected Networks GmbH - Solarwinds ist ein junges Dienstleistungsunternehmen im Bereich IT-Berechtigungsmanagement und Identity Governance.'}
				url={'https://www.tandem-m.de/projekte/eight-man.html'}
				image={heroImg}
			/>

			<Header />
			<main>
				<h1 className="visuallyHidden">8man - Protected Networks GmbH - Solarwinds</h1>

				<Hero
					src={heroImg}
					alt="8man - Protected Networks GmbH - Solarwinds"
					width={1920}
					height={800}
					content={
						<div styleName="hero-content">
							<HeroContent 
								title={<span>8man</span>}
								text="Heute SolarWinds ARM"
							/>
						</div>
					}
				/>

				<section>
					<h2 className="visuallyHidden">8MAN</h2>
					<SectionWhite 
						title="8MAN"
						text="8MAN, heute SolarWinds Access Rights Manager, hilft Unternehmen, die täglichen Herausforderungen der IT-Verwaltung zu bewältigen."
						textColor="var(--color-8man-green)"
					/>
					<SectionColor 
						content={
							<Fragment>
								<p styleName="text">Zugriffsrechte verwalten, Einhaltung von Richtlinien sicherstellen, Datenmissbrauch vermeiden: 8MAN, heute SolarWinds ARM, bietet eine Vielzahl an Funktionen für Unternehmen. Die verschiedenen Schwerpunkte des Tools für Berechtigungsmanagement sollten auf einer Website übersichtlich abgebildet werden.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-8man-dark)"
					/>
				</section>

				<section>
					<div className="empty-parallax">
						<ParallaxEffect
							image={parallaxImg1}
							alt="8man Projektfotos"
						/>
					</div>
					<SectionColor 
						content={
							<Fragment>
								<p styleName="text">Tandem Marketing & Partners übernahm sowohl die inhaltliche, also auch die technische Konzeption der neuen Internetpräsenz. Um die Website benutzerfreundlich und intuitiv zu gestalten, sind die neuesten Erkenntnisse im Bereich Usability ins Design eingeflossen. Videos im Hintergrund haben die Benutzererfahrung auf ein noch höheres Level gehoben.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
					<div className="empty-parallax">
						<img src={parallaxImg5} alt="8man Projektfotos"  loading="lazy" style={{display: 'block'}}/>
					</div>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-400">Das Ergebnis: Eine moderne, übersichtliche und verkaufsfördernde Produktpräsentation.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-8man-green)"
					/>
					<div className="project-gallery">
						<div>
							<img src={parallaxImg2} alt="8man Projektfotos"  loading="lazy"/>
						</div>
						<div>
							<img src={parallaxImg3} alt="8man Projektfotos"  loading="lazy"/>
						</div>
					</div>
					<div className="empty-parallax">
						<img src={parallaxImg4} alt="8man Projektfotos"  loading="lazy"/>
					</div>
				</section>

				<section className="section">
					<div className="container-narrow">
						<h2 className="heading-3">Leistungen</h2>
					</div>
					<div className="container" styleName="checkedList">
						<CheckedList 
							content={
								<Fragment>
									<li>Consulting</li>
									<li>Konzeption</li>
									<li>Content Marketing</li>
									<li>Webdesign</li>
									<li>Website-Entwicklung</li>
								</Fragment>
							}
							color="var(--color-8man-green)"
						/>
					</div>
				</section>

				<section styleName="usedTech">
					<UsedTech
						techs={techs}
					/>
				</section>

				<Blog />

				<FeedbackHomePage />
			</main>
			<Footer />
		</div>
	);
};