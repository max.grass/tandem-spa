import React, { Fragment } from "react";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { ParallaxEffect } from "components/ParallaxEffect";
import { CheckedList } from "components/CheckedList";
import { UsedTech } from "components/SourceAboutUs";
import { Blog } from "components/Blog";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from "assets/images/hero-1malx.jpg";
import parallaxImg from "assets/images/parallax-onemaxx-1.jpg";
import galleryImg1 from "assets/images/parallax-onemaxx-2.jpg";
import galleryImg2 from "assets/images/parallax-onemaxx-3.jpg";
import galleryImg3 from "assets/images/parallax-onemaxx-4.jpg";


import bitrix from 'assets/images/icon-bitrix.svg';
import html from 'assets/images/icon-html5.svg';
import css from 'assets/images/icon-css3.svg';

const techs = [
	{
		"src" : bitrix,
		"width": 143,
		"height": 44,
		"title": "Bitrix"
	},
	{
		"src" : html,
		"width": 46,
		"height": 64,
		"title": "html"
	},
	{
		"src" : css,
		"width": 45,
		"height": 64,
		"title": "css"
	}
]

import "./OnemalX.css";

export const OnemalX = () => {
	return (
		<div className="page-wrapper">
			<CommonMetaTags 
				title={'1malX - Karrierenetzwerk für Studenten'}
				description={'Zu Beginn von 1malX stand die Vision, ein virtuelles Karrierenetzwerk für Studenten und Absolventen zu schaffen.'}
				url={'https://www.tandem-m.de/projekte/36-1malx.html'}
				image={heroImg}
			/>

			<Header />
			<main>
				<h1 className="visuallyHidden">1malX - Karrierenetzwerk für Studenten</h1>

				<Hero
					src={heroImg}
					alt="1malX Projektfotos"
					width={1920}
					height={800}
					content={
						<div styleName="hero-content">
							<HeroContent 
								title={<span>1malX</span>}
								text="Karrierenetzwerk für Studenten"
							/>
						</div>
					}
				/> 

				<section>
					<h2 className="visuallyHidden">Über 1malX-Projekt</h2>
					<SectionWhite 
						title="Von der Konzeption über das Corporate Design"
						text="Zu Beginn von 1malX stand die Vision, ein virtuelles Karrierenetzwerk für Studenten und Absolventen zu schaffen."
						textColor="var(--color-1maxx-blue)"
					/>
					<SectionColor 
						content={
							<Fragment>
								<p styleName="text">Studenten sollten ihre eigenen Karriereprofile anlegen und kinderleicht nach passenden Jobangeboten suchen können. Zugleich sollten Unternehmen im Bereich Recruiting entlastet werden und die Chance gewinnen, auf Knopfdruck maßgeschneiderte Bewerber zu finden.</p>
								<p styleName="text">Tandem gab der Vision ein Gesicht - von der Konzeption über das Corporate Design bis hin zur technischen Entwicklung. Für Studenten und Personaler wurden nutzerfreundliche Administrationsoberflächen geschaffen, die den jeweiligen Bedürfnissen der Nutzergruppe entsprechen. Darüber hinaus bietet das Karrierenetzwerk Studenten und Personalern individuelle Profilseiten.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-1maxx-grey)"
					/>
					<section>
						<img src={galleryImg3} alt="OneMalX Projektfotos" loading="lazy" style={{display: 'block'}}/>
					</section>
					<SectionColor 
						content={
							<Fragment>
								<p styleName="text">Studenten können Matching-Profile mit für sie relevanten Auswahlkriterien anlegen, woraufhin das System automatisch nach passenden Stellen sucht. Außerdem kann ein digitaler Lebenslauf inklusive Fotos angelegt werden. Der Bewerber behält zu jedem Zeitpunkt die Kontrolle über seine Daten, da der Lebenslauf erst nach der Bewerbung auf eine Stelle freigegeben wird. Es gibt zudem die Option, den Zugriff auf das eigene Profil sperren zu lassen. Um leicht die Übersicht zu behalten, verfügen die Profilseiten über ein eigenes Postfach und eine Merkfunktion, mit der Stellen inklusive Notizen einfach verwaltet werden können.</p>
								<p styleName="text">Personaler können auf 1malX ein Unternehmensprofil anlegen. Damit das Unternehmen optimal präsentiert wird, verfügen diese Seiten über ein voll funktionsfähiges Redaktions- und Bildbearbeitungsskript. So wird das Anlegen und Pflegen von Unternehmensprofilen kinderleicht.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-1maxx-grey)"
					/>
				</section>

				<section>
					<SectionColor 
						content={
							<Fragment>
								<p styleName="text">Stellenanzeigen können auf dem Portal geschaltet, verwaltet und als Vorlage gespeichert werden. Die übersichtliche Verwaltung der Stellenanzeigen ermöglicht weitere Features, beispielsweise können Jobangebote nur für bestimmte Zeiträume aktiviert werden. Der Personaler behält dabei die vollständige Übersicht über aktuell geschaltete Anzeigen.</p>
								<p styleName="text">Die Stellenanzeigen auf 1malX verfügen über ein ausgeklügeltes Attribute-System, sodass Unternehmen Kandidaten nach spezifischen Kriterien wie Studienfach, Region oder gewünschter Beschäftigungsart suchen können. Um das Bewerbermanagement zu erleichtern und übersichtlich zu gestalten, verfügen Personaler über ein separates Postfach, in dem Bewerbernachrichten pro Stellenanzeige gebündelt angezeigt werden. Zu jedem Bewerber können Notizen und Antwortvorlagen hinterlegt werden. Auf diese Art können Nachrichten wie Absagen oder Einladungen zum Vorstellungsgespräch bequem verschickt werden.</p> 
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
					<div className="empty-parallax">
						<ParallaxEffect
							image={parallaxImg}
							alt="OneMalX Projektfotos"
						/>
					</div>
					<SectionColor 
						content={
							<Fragment>
								<p styleName="text">Studenten können Matching-Profile mit für sie relevanten Auswahlkriterien anlegen, woraufhin das System automatisch nach passenden Stellen sucht. Außerdem kann ein digitaler Lebenslauf inklusive Fotos angelegt werden. Der Bewerber behält zu jedem Zeitpunkt die Kontrolle über seine Daten, da der Lebenslauf erst nach der Bewerbung auf eine Stelle freigegeben wird. Es gibt zudem die Option, den Zugriff auf das eigene Profil sperren zu lassen. Um leicht die Übersicht zu behalten, verfügen die Profilseiten über ein eigenes Postfach und eine Merkfunktion, mit der Stellen inklusive Notizen einfach verwaltet werden können.</p>
								<p styleName="text">Die 1malX-Katalogssuche zeichnet sich insbesondere durch ein ausgeklügeltes Matching-System sowie die Bündelung der Stellenanzeigen pro Unternehmen aus: Jedes Unternehmen erhält den gleichen Raum in der Katalogübersicht, unabhängig davon, wieviele Anzeigen geschaltet sind. So haben kleinere Unternehmen, die nur wenige offene Stellen anbieten, die gleiche Chance gefunden zu werden wie große. In die Katalogssuche wurde eine Geo-Datenbank eingebunden, damit auch nach Ort und Umkreis gesucht werden kann.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-400">Die Betreiber von 1malX erhielten eine erweiterte Administration inklusive Management- und Controlling-Funktionen. Bis heute hostet Tandem Marketing das Karrierenetzwerk 1malX und übernimmt Aufgaben im Bereich Qualitätsmanagement.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-1maxx-green)"
					/>
					<div className="project-gallery">
						<div>
							<img src={galleryImg1} alt="OneMalX Projektfotos " loading="lazy" />
						</div>
						<div>
							<img src={galleryImg2} alt="OneMalX Projektfotos " loading="lazy" />
						</div>
					</div>
				</section>

				<section className="section">
					<div className="container-narrow">
						<h2 className="heading-3">Leistungen</h2>
					</div>
					<div className="container" styleName="checkedList">
						<CheckedList 
							content={
								<Fragment>
									<li>Corporate Design</li>
									<li>Zielgruppenforschung</li>
									<li>Eye-Tracking-Analysen</li>
									<li>E-Commerce-Beratung</li>
									<li>Konzeption</li>
									<li>Webshop-Entwicklung und fortlaufende Weiterentwicklung</li>
									<li>Content Marketing</li>
									<li>Social Media Marketing</li>
									<li>Suchmaschinenoptimierung</li>
								</Fragment>
							}
							color="var(--color-1maxx-blue)"
						/>
					</div>
				</section>

				<section styleName="usedTech">
					<UsedTech
						techs={techs}
					/>
				</section>

				<Blog />

				<FeedbackHomePage />
			</main>
			<Footer />
		</div>
	);
};
