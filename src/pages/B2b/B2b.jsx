import React, { Fragment, useRef, useEffect } from "react";
import gsap from "gsap";

import { Link } from 'react-router-dom';

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { ParallaxEffect } from "components/ParallaxEffect";
import { ColorSteps } from "components/ColorSteps";
import { ListOrderUnorder } from "components/ListOrderUnorder";
import { Icon } from "components/Icon";
import { Hr } from "components/Hr";
import { Article } from "components/Article";
import { CommonMetaTags } from "components/CommonMetaTags";

import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";

import { ListItemsTween, ListItemsTween2 } from "components/Animation"

import heroImg from "assets/images/hero-b2b.jpg";
import statsParallax from "assets/images/b2b-parallax.jpg";
import statsParallax2 from "assets/images/b2b-parallax-2.jpg";
import parallaxImg3 from "assets/images/b2b-parallax-3.jpg";
import parallaxImg4 from "assets/images/b2b-parallax-4.jpg";
import parallaxImg5 from "assets/images/b2b-parallax-5.jpg";
import parallaxImgContact from "assets/images/b2b-parallax-contact.jpg";
import statsInfographic from "assets/images/b2b-infographic.svg";
import statsInfographic2 from "assets/images/b2b-infographic-2.svg";
import statsInfographic3 from "assets/images/b2b-infographic-3.svg";
import imgArticle from "assets/images/b2b-article.jpg";

import "./B2b.css";
// import {Helmet} from "react-helmet";

export const B2b = () => {
  const factsRef = useRef();
  const advantagesRef = useRef();

  useEffect(() => {
    let tl = gsap.timeline();
    
    tl
    .add(
      ListItemsTween(
        factsRef.current
      )
    )
    .add(
      ListItemsTween2(
        advantagesRef.current,
        0.1
      )
    );
  }, []);

  return (
    <div className="page-wrapper">
      <CommonMetaTags 
				title={'B2B-Shop: Ihr Einstieg in den digitalen Vertrieb'}
				description={'Tandem Marketing'}
				url={'https://www.tandem-m.de/b2b-commerce.html'}
				image={heroImg}
			/>

      <Header />
      <main>
        <Hero
          src={heroImg}
          heght={500}
          content={
            <HeroContent 
              title={<span>Ihr Einstieg in den <br /> digitalen Vertrieb</span>}
              text="B2B-Shop"
            />
          }
        />

        <section>
          <h2 className="visuallyHidden">Was ist B2B Shop und brauchen wir ihn?</h2>
          <SectionWhite 
            title="B2B-Shop"
            text="Die Nachfrage nach B2B-Shops und B2B-Portalenen wächst weiter: Mit dem allgemeinen Trend zur Digitalisierung suchen auch Nutzer im Geschäftsbereich nach dem Komfort und der Funktionalität, die sie aus B2C Shops gewohnt sind. Mit über 10 Jahren Erfahrung in der Webentwicklung, schaffen wir gemeinsam mit Ihnen den perfekten B2B-Shop für Ihre Kunden. "
            textColor="var(--color-chathams-blue)"
          />

          <SectionColor 
            content={
              <Fragment>
                <h3>B2B-Shop: Benötigen wir das wirklich?</h3>
                <p className="section-text-200">In vielen Unternehmen läuft der Vertrieb nach wie vor über Excel-Tabellen und schwere Papierkataloge. Individuelle Rabatte werden persönlich am Telefon oder per E-Mail ausgehandelt – natürlich während der Geschäftszeiten. Dies ist sowohl für die eigenen Vertriebsmitarbeiter als auch für Kunden langwierig und zeitaufwendig.</p>
                <p className="section-text-200" style={{ marginBottom: 0 }}>Dazu kommt, dass Ihre B2B-Kunden privat ganz selbstverständlich in B2C Shops einkaufen sowie Produkte konfigurieren – und das in nur wenigen Klicks. Damit entsteht der Wunsch nach einfachen Lösungen, auch im Berufsalltag.</p>
               
              </Fragment>
            }
            backgroundColor="var(--color-chathams-blue)"
          />

          <div className="section-m container-narrow">
            <p className="section-text-200" style={{ marginBottom: 0 }}>Die Antwort auf die oben stehende Frage ist daher eindeutig: Ja. Wer seine Geschäftskunden auf Dauer halten möchte, benötigt einen B2B-Shop oder ein B2B-Portal. Durch den Einstieg in den Onlinehandel lassen sich Einkaufsprozesse für B2B Kunden komfortabel, weniger fehleranfällig und rund um die Uhr gestalten. Ein nutzerfreundlicher, moderner Bestellprozess macht Kunden zufriedener: Das B2B-Portal ist ein wichtiges Instrument zur Kundenbindung.</p>
          </div>
        </section>

        <ColorSteps 
          hiddenTitle="B2B-E-Commerce-Statistiken"
          parallax={
            <ParallaxEffect
              image={statsParallax}
              alt="Handynutzung"
              children={
                <div className="container">
                  <h3 className="heading-2 parallax-text">Wachstumsmarkt: <br/> B2B-E-Commerce</h3>
                </div>
              }
            />
          }
          bgColor={"var(--color-chathams-blue)"}
          textTop={<p className="section-text-200">Auch die bloßen Zahlen sprechen für den Einstieg in den B2B-Onlinehandel: Das <a href="https://www.ifhkoeln.de/b2b-e-commerce-auf-der-ueberholspur/" styleName="stats-link" target="_blank" rel="noopener noreferrer">Institut für Handelsforschung (IFH Köln)</a> fand heraus, dass der Umsatz im B2B-E-Commerce seit 2012 jedes Jahr durchschnittlich um mehr als 15 Prozent gestiegen ist.</p>}
          textCenter={ <p className="section-text-200">Der gesamte B2B-E-Commerce-Markt erzielte bereits im Jahr 2018 einen Anteil von fast 25 Prozent am Gesamtumsatz aller betrachteten Wirtschaftszweige. Das bedeutet, dass knapp 1.300 Milliarden Euro zwischen Geschäftskunden online umgesetzt wurden.</p>}
          textBottom={
            <Fragment>
              <h3>Klares Argument für einen B2B-Shop:</h3>
              <p>Rund ein Viertel der Umsätze im B2B-Commerce wurden über B2B-Onlineshops, Websites oder Marktplätze erzielt (Umsatzvolumen 320 Milliarden Euro).</p>
            </Fragment>
          }
        />

        <section styleName="onlineMarketing-stats">
          <h2 className="visuallyHidden">Verkaufsstatistiken</h2>
          <div className="container-narrow" styleName="ecommerce-stats">
            <figure>
              <img src={statsInfographic3} width="930" height="360" alt="statsInfographic3" loading="lazy" />
            </figure>
          </div>
          <div className="section-m container-narrow">
            <p>Eigentlich ist es keine Frage mehr, ob B2B-Verkäufer die digitale Transformation durchlaufen sollen. Das digitale Zeitalter ist schon längst da:</p>
          </div>

          <ParallaxEffect
            image={statsParallax2}
            alt="Handynutzung"
            children={
              <div className="container-narrow section-m parallax-text">
                <h3 className="section-text-700">Wie hoch ist der Anteil der Verkäufe über Online-Shops oder Marktplätze am Gesamt-Jahresumsatzes Ihres Unternehmens?</h3>
                <img 
                  src={statsInfographic}
                  width={1011}
                  height={401}
                  alt="Prozentsatz der Online-Verkäufe pro Jahr"
                  loading="lazy"
                />
              </div>
            }
          />

          <div className="section container-narrow">
            <h3 className="section-text-700">Welche Lösung nutzt Ihr Unternehmen für den Verkauf?</h3>
            <img 
              src={statsInfographic2}
              width={1011}
              height={401}
              alt="Vertriebslösungen"
              loading="lazy"
            />
          </div>
        </section>

        <section className="section-m">
          <Article 
            title=""
            buttonUrl="/referenzen.html"
            buttonCaption="Fälle zeigen"
            img={
              <img 
                src={imgArticle}
                alt=""
                width={1920} 
                height={800}
                loading="lazy"
              />
            }
            color="var(--color-chathams-blue)"
          />
          <div className="container-narrow grid-hr" styleName="facts">
            <h2 className="heading-3">Magento Webshop verfügt über diverse leistungsstarke Funktionen und Fähigkeiten:</h2>
            <Hr color="var(--color-chathams-blue)" />
            <div ref={factsRef}>
              <ListOrderUnorder
                isOrdered={true}
                icon={
                  <Icon
                    name="checked-empty"
                    width="50px"
                    height="50px"
                    color="var(--color-dark)"
                  />
                }
                items={[
                  <Fragment>
                    <h3 className="heading-4">Kundenbindung wird herausfordernd</h3>
                    <p><strong>67 Prozent</strong> der B2B-Verkäufer sehen die Notwendigkeit, digitale Fähigkeiten zu entwickeln. Digitale Kanäle ermöglichen es B2B-Käufern, autonome Entscheidungen zu treffen. Kundenbindung kann nur mittelfristig durch bequeme Beschaffungsprozesse und -dienstleistungen erreicht werden.</p>
                    <p>Wenn etablierte Unternehmen keine digitale Kundenbindung aufbauen, müssen sie in den nächsten drei Jahren erhebliche Umsatzverluste hinnehmen.</p>
                  </Fragment>,
                  <Fragment>
                    <h3 className="heading-4">Wachstum nur durch digitalisierung</h3>
                    <p>Die Zahl der kleinen Großhändler ist in den letzten Jahren bereits um <strong>9 Prozent</strong> gesunken.</p>
                  </Fragment>,
                  <Fragment>
                    <h3 className="heading-4">Informationstransparenz wird zum standard</h3>
                    <p><strong>Mindestens 50</strong> Prozent der etablierten Unternehmen werden in den nächsten Jahren erhebliche Marktanteile an neue Marktteilnehmer mit digitaler DNA verlieren.</p>
                    <p>Die mangelnde Sichtbarkeit der Großhändler im Onlinebereich sowie das bequeme Serviceangebot (Bewertungen, Verfügbarkeit usw.) der Digital Player führen zu Kundenverlusten im klassischen Großhandel.</p>
                    <p>Aufgrund der zunehmenden Preistransparenz online, übernehmen plattformbasierte Geschäftsmodelle relevante Marktanteile und Kundenbeziehungen.</p>
                  </Fragment>,
                  <Fragment>
                    <h3 className="heading-4">Serviceerlöse sind zukünftig erfolgskritisch</h3>
                    <p><strong>Etwa 40 Prozent</strong> der Großhändler erkennen nicht, dass profitables Wachstum in Zukunft nur durch neue serviceorientierte Erlösmodelle möglich sein wird. Neue Strategien zur Monetarisierung von Dienstleistungen sind erforderlich, um weiterhin gute Margen zu erzielen.</p>
                    <p>Die Zahlungsbereitschaft für B2B-Services liegt bereits bei <strong>mehr als 60 Prozent</strong>.</p>
                  </Fragment>,
                  <Fragment>
                    <h3 className="heading-4">Prozessproduktivität wird zum wettbewerbsvorteil</h3>
                    <p>Um am E-Commerce-Wachstum im B2B-Bereich von rund <strong>15 Prozent p.a.</strong> teilnehmen zu können, müssen effiziente, kanalübergreifende Prozesse eingerichtet werden.</p>
                    <p>Die Mehrheit der Einzelhändler erfüllt die Kundenerwartungen für exzellente Logistikdienstleistungen (noch) nicht. Die mangelnde digitale Ausrichtung der Bestell-, Zahlungs- und Logistikprozesse im Großhandel verhindert, dass die tatsächliche Geschäftstätigkeit des B2B-Kunden in den Mittelpunkt rückt.</p>
                  </Fragment>,
                ]}
              />
            </div>
          </div>
				</section>

        {/* advantages */}
        <section>
          <h2 className="visuallyHidden">Vorteile digitaler Technologien</h2>
          <ParallaxEffect
            image={parallaxImg3}
            alt=""
            children={
              <div className="container-narrow section-m parallax-text">
                <b className="heading-2">Was gewinnen Sie, wenn Sie digital gehen?</b>
              </div>
            }
          />

          <div className="section-m container-narrow grid-hr" styleName="advantages">
            <p>Basierend auf den Erfahrungen zahlreicher Industrieunternehmen lassen sich folgende Vorteile für B2B-Verkäufer ausmachen, wenn sie digital gehen – und zwar in jeder Phase des Verkaufsprozesses:</p>
            <Hr color="var(--color-chathams-blue)"/>
            <ul styleName="advantages-list" ref={advantagesRef}>
              <li>
                <h4>Presales-Phase optimieren via Online Marketing</h4>
                <ul>
                  <li>Agiles Online Marketing</li>
                  <li>Bezahlte Suche verbessern (SEM)</li>
                  <li>Organische Reichweite erhöhen (SEO)</li>
                  <li>Personalisierte Produktempfehlungen</li>
                </ul>
                <Icon
                  name="mobile-and-desktop-rect"
                  figure="square"
                  color="var(--color-chathams-blue)"
                />
              </li>
              <li>
                <h4>Kundenerfahrung verbessernt</h4>
                <ul>
                  <li>Agiles Online Marketing</li>
                  <li>Bezahlte Suche verbessern (SEM)</li>
                  <li>Organische Reichweite erhöhen (SEO)</li>
                  <li>Personalisierte Produktempfehlungen</li>
                </ul>
                <Icon
                  name="globe"
                  figure="square"
                  color="var(--color-chathams-blue)"
                />
              </li>
              <li>
                <h4>Optimierte Preisgestaltung</h4>
                <ul>
                  <li>Agiles Online Marketing</li>
                  <li>Bezahlte Suche verbessern (SEM)</li>
                  <li>Organische Reichweite erhöhen (SEO)</li>
                  <li>Personalisierte Produktempfehlungen</li>
                </ul>
                <Icon
                  name="label-euro"
                  figure="circle"
                  color="var(--color-chathams-blue)"
                />
              </li>
              <li>
                <h4>IoT bei Händlern und Einzelhändlern ermöglichen</h4>
                <ul>
                  <li>Agiles Online Marketing</li>
                  <li>Bezahlte Suche verbessern (SEM)</li>
                  <li>Organische Reichweite erhöhen (SEO)</li>
                  <li>Personalisierte Produktempfehlungen</li>
                </ul>
                <Icon
                  name="medium"
                  figure="triangle-top"
                  color="var(--color-chathams-blue)"
                />
              </li>
            </ul>
          </div>

          <div className="section" styleName="advantages-color">
            <div className="container-narrow">
              <p className="section-text-200">Häufig wird unterschätzt, wie stark sich negative Kundenerlebnisse auf die Leadgenerierung auswirken, zum Beispiel eine nicht optimierte Websites oder die übermäßige Abhängigkeit von veralteten Vertriebskanälen. </p>
              <p className="section-text-200">Unternehmen, die verbraucherorientierte Strategien in Richtung Onlinehandel entwickeln, werden laut Schätzungen führender Marktforscher  belohnt und zwar mit konstanten Gewinnquoten, die weit über dem Durchschnitt liegen.</p>
              <p className="section-text-200">Richtig eingesetzt, ermöglicht die Umstellung auf B2B-E-Commerce, interne Prozesse und die Interaktion mit Kunden nachhaltig und effektiv zu verbessern und langfristig Positionen zu stärken. Es geht also um weitaus mehr, als technische Fähigkeiten.</p>
            </div>
          </div>
        </section>

        {/* differences */}
        <section>
          <h2 className="visuallyHidden">Unterschiede zwischen B2B und B2C.</h2>
          <div className="section-m container-narrow grid-hr" styleName="diffs">
            <b className="heading-3">B2B-Shop: Wir digitalisieren Ihren Vertrieb</b>
            <p className="section-text-200" styleName="diffs-text">Ein B2B-Shop sollte natürlich genauso schnell und nutzerfreundlich sein wie ein normaler Onlineshop; zugleich gilt es, die bestehenden Geschäftsprozesse abzubilden. Es gibt einige wichtige Unterschiede zwischen B2B und B2C, die wir als erfahrene Werbeagentur in Stuttgart (und deutschlandweit) im Entwicklungsprozess berücksichtigen:</p>
            <Hr color="var(--color-chathams-blue)" />
            <ListOrderUnorder
              isOrdered={true}
              icon={
                <Icon
                  name="checked-empty"
                  width="50px"
                  height="50px"
                  color="var(--color-chathams-blue)"
                />
              }
              items={[
                <Fragment>
                  <h3 className="heading-4">Reibungslose Verbindung zu bestehenden Geschäftsprozessen</h3>
                  <p>Bei der Erstellung eines B2B-Shops oder B2B-Portalen ist wichtig, dass alle bestehenden Geschäftsprozesse berücksichtigt werden. Der gesamte Online-Workflow muss sowohl für Verkäufer als auch für Kunden klar, schnell und einfach sein.</p>
                </Fragment>,
                <Fragment>
                  <h3 className="heading-4">Umfangreiche Produktdarstellung</h3>
                  <p>Das B2B-Segment hat zum Teil sehr komplexe Produkte. Durch umfassende Produktbeschreibungen inklusive aller technischen Daten, aussagekräftige Produktfotos sowie ggf. 360-Grad-Ansichten, finden Geschäftskunden einfach das passende Produkt. Erklärvideos oder Dokumentationen helfen, Ihrer Zielgruppe ein vollständiges Bild zu vermitteln – so funktioniert B2B online.</p>
                </Fragment>,
                <Fragment>
                  <h3 className="heading-4">Rechte- und Rollenmanagement im B2B-Shop</h3>
                  <p>Im Gegensatz zum B2C-Handel sind im B2B-Commerce viele Personen für die Entscheidungsfindung verantwortlich. Ein durchdachtes Rechte- und Rollenmanagement ermöglicht B2B-Kunden einfach festzulegen, wer welche Funktionen nutzen darf: So sieht beispielsweise ein Einkäufer Preise, darf Rabatte aushandeln und Bestellungen tätigen – aber nur bis zu einer bestimmten Summe. Auch Genehmigungsverfahren können im B2B-Shop automatisch hinterlegt werden.</p>
                </Fragment>
              ]}
            />
          </div>
          <ParallaxEffect
            image={parallaxImg4}
            alt=""
            children={
              <div className="container-narrow parallax-text">
                <p className="section-text-700">Was ein B2B-Shop alles können sollte, lesen Sie auch in <a styleName="diffs-link" href="https://www.tandem-m.de/blog/59-e-commerce-im-b2b-6-tipps-fuer-einen-erfolgreichen-b2b-shop.html" target="_blank" rel="noopener noreferrer">E-Commerce im B2B: 6 Tipps für einen erfolgreichen B2B-Shop.</a> Eine Voraussetzung für einen guten B2B-Shop ist natürlich eine leistungsstarke B2B Shop Software wie Magento.</p>
              </div>
            }
          />
        </section>

        {/* Magento */}
        <section className="section-m container-narrow" styleName="magento">
          <h2 className="heading-3">Magento als B2B-Shop-Lösung</h2>
          <Hr color="var(--color-flamingo)"/>
          <p>Ihr B2B-Shop oder Ihr B2B-Portal muss schnell und flexibel sein, um auf Marktveränderungen reagieren zu können. <a href="/magento-webshop/warum-magento.html">Magento</a> ist eine der besten derzeit verfügbaren Lösungen:</p>
          <ul>
            <li>Ihre B2B-Kunden können ihre Unternehmenskonten selbst verwalten</li>
            <li>Sie können mehrere Käuferebenen mit bestimmten Rollen und Berechtigungen einrichten</li>
            <li>Sie können Angebote einfach verfolgen und die detaillierte Bestellhistorie anzeigen</li>
            <li>Es gibt über 5.000 Magento-Module, die an Ihre spezifischen Geschäftsanforderungen angepasst werden können</li>
            <li styleName="no-dot">
              <Link
                to="/magento-webshop.html"
                className="button"
                style={{
                "--stroke": "var(--color-white)",
                "backgroundColor": "var(--color-flamingo)"
                }}
              >Weitere Informationen zu Magento</Link>
            </li>
          </ul>
        </section>

        {/* Services */}
        <section className="section-m">
          <h2 className="visuallyHidden">Unsere Online-Trading-Services</h2>
          <ParallaxEffect
            image={parallaxImg5}
            alt=""
            children={
              <div className="container-narrow parallax-text">
                <b className="heading-2">Mit Tandem Marketing &amp; Partners zum B2B-Shop</b>
              </div>
            }
          />
          <div className="section-m container-narrow" styleName="services">
            <p className="sectiom-text-200">Als erfahrene Marketing Agentur begleiten wir Sie gerne bei Ihrem Weg in den <Link to="/marketing-stuttgart/e-commerce.html" target="_blank" rel="noopener noreferrer">Onlinehandel</Link>: Wir unterstützen Sie von der Umsetzung Ihres B2B-Portals bis hin zu einer langfristigen Marketingstrategie.</p>
            <ListOrderUnorder
              icon={
                <Icon
                  name="checked"
                  width="50px"
                  height="50px"
                  color="var(--color-dark)"
                />
              }
              items={[
                <Fragment>
                  <h3 className="heading-4">Analyse und Entwicklung</h3>
                  <ul>
                    <li>Wir analysieren den aktuellen Workflow Ihres Unternehmens und erarbeiten gemeinsam mit Ihnen Ziele</li>
                    <li>Auf Basis Ihres Corporate Design Handbuchs entsteht ein modernes, benutzerfreundliches Webdesign</li>
                    <li>Wir starten mit der Entwicklung Ihres B2B-Shops. Immer im Fokus: Effizienz und eine großartige Benutzererfahrung</li>
                  </ul>
                </Fragment>,
                <Fragment>
                  <h3 className="heading-4">Online Marketing</h3>
                  <p>Als Online-Marketing Agentur entwickeln wir eine umfassende Strategie für Ihren Einstieg in den B2B-Onlinehandel:</p>
                  <ul>
                    <li>Durch Suchmaschinenoptimierung fällt Ihr B2B-Shop in den organischen Suchergebnissen auf und Sie erreichen weitere potenzielle Kunden</li>
                    <li>Content Marketing in Form von Blog-Artikeln, Online-Magazinen oder Newslettern trägt zur Kundenbindung bei, stärkt Ihre Corporate Identity und trägt Ihre Botschaft an die Zielgruppe heran</li>
                    <li>Durch Online-Werbekampagnen steigern Sie Ihre Zielgruppenreichweite, um den Traffic und die Sichtbarkeit Ihres B2B-Shops zu erhöhen</li>
                  </ul>
                </Fragment>,
                <Fragment>
                  <h3 className="heading-4">Monitoring &amp; Beratung</h3>
                  <p>Wir behalten Ihre wichtigsten Leistungsindikatoren im Auge und geben Empfehlungen, was getan werden muss, um Ihre Positionen zu verbessern, zu halten oder eine höhere Conversion-Rate zu erzielen.</p>
                </Fragment>
              ]}
            />
          </div>
        </section>

        {/* Contacts */}
        <section className="section-m">
          <h2 className="visuallyHidden">Bleiben Sie mit uns in Kontakt</h2>
          <ParallaxEffect
            image={parallaxImgContact}
            alt=""
            children={
              <div className="container-narrow parallax-text">
                <b className="heading-2">Marketing Agentur mit Fachwissen und individuellem Ansatz</b>
              </div>
            }
          />
          <div className="section" styleName="contacts-color">
            <div className="container-narrow">
              <p className="section-text-200">Mit über 10 Jahren Erfahrung im Online Marketing verfügt unsere Webagentur in Stuttgart (und deutschlandweit) nicht nur über Fachwissen, sondern auch über einen aufmerksamen und individuellen Ansatz für die Geschäftsanforderungen unserer Kunden, ob B2B oder B2C. Sprechen Sie uns an, wir überzeugen Sie gern von unseren Produkten und Dienstleistungen.</p>
            </div>
          </div>
        </section>
      </main>
      <FeedbackHomePage />
      <Footer />
    </div>
  );
}
