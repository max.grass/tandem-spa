import React, { Fragment, useRef } from "react";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { ProjectsRef } from 'components/ProjectsRef';
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from "assets/images/hero-webdesign.jpg";
import {data} from "components/ProjectsRef/projectsDataRef";

// import "./ReferencePage.css";

export const ReferencePage = () => {

	const feedbackForm = useRef(0);

	const scrollToForm = () => {
		feedbackForm.current.scrollIntoView({ behavior: "smooth" });
	}

	return (
		<div className='page-wrapper'>
			<CommonMetaTags 
				title={'Referenzen - Unsere Arbeit spricht für sich'}
				description={'Tandem Marketing'}
				url={'https://www.tandem-m.de/referenzen.html'}
				image={heroImg}
			/>

			<Header />
			
			<main>
        		<Hero
					src={heroImg}
					alt="Referenzen"
					width={1920}
					height={800}
					content={
						<HeroContent 
							title={<span>Referenzen</span>}
							text="Referenzen"
						/>
					}
				/>

				<ProjectsRef
					title="Zeit, unsere Arbeit zu zeigen"
					data={data}
					referenzePage={true}
				/>

				<section className="section" style={{textAlign: 'center'}}>
					<button
						onClick={() => scrollToForm()}
						className="button"
						style={{
							"--stroke": "var(--color-white)",
							"backgroundColor": "var(--color-cherry)"
						}}
					>
						Werden Sie unser Kunde
					</button>
				</section>

				<div ref={feedbackForm}>
					<FeedbackHomePage />
				</div>
			</main>
			
			<Footer />
		</div>
	);
}
