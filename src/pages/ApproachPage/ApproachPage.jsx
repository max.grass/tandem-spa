import React, { Fragment, useRef, useEffect, useLayoutEffect, useState } from "react";
import gsap from 'gsap';
import { Link } from 'react-router-dom';

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
// import { ImageList } from "components/ImageList";
import { ImageListTween } from "components/ImageList";
import { SectionWhite } from "components/SectionWhite";
import { ColorSteps } from "components/ColorSteps";
import { SectionColor } from "components/SectionColor";
import { ParallaxEffect } from "components/ParallaxEffect";
import { Worktable } from "components/Worktable";
// import { AboutPageStrengths } from "components/AboutPage-OurStrengths";
// import { data as AboutPageStrengthsData } from "components/AboutPage-OurStrengths/AboutPageStrenghsData";
import { SourceAboutUs } from "components/SourceAboutUs";
// import { AccordionTabs } from 'components/AccordionTabs';
import { Article } from "components/Article";
// import { Team } from "components/Team";
// import { Reviews } from "components/Reviews";
import { Blog } from "components/Blog";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import {Helmet} from "react-helmet";
// import Slider from 'react-slick';


import heroImg from "assets/images/hero-about.jpg?as=webp";
// import parallaxImageStrengths from "assets/images/about-page-parallax-strengths.jpg";
// import parallaxImageServices from "assets/images/about-page-parallax-services.jpg";
// import parallaxImageTrust from "assets/images/about-page-parallax-trust.jpg";
// import parallaxImageUs from "assets/images/about-page-parallax-why-us.jpg";
import parallaxImageResearch from "assets/images/about-page-parallax-research.jpg";
import parallaxImageWatch from "assets/images/about-parallax-watch.jpg";
import parallaxImage1 from "assets/images/about-page-parallax-1.jpg";
import parallaxImageMettler from "assets/images/about-page-parallax-mettler.jpg";
import parallaxImageWebdesign from "assets/images/about-page-parallax-webdesign.jpg";
import parallaxImageEntwicklung from "assets/images/about-page-parallax-entwicklung.jpg";
import parallaxImagePPS from "assets/images/about-page-parallax-pps.jpg";

import logoTesa from 'assets/images/logo-tesa-b.svg';
import logoWatchDe from 'assets/images/logo-watch-de-b.svg';
import logoRitterSport from 'assets/images/logo-ritter-sport-b.svg';
import logoDdsa from 'assets/images/logo-ddsa-b.svg';
import logoSolarWinds from 'assets/images/logo-solarwinds-b.svg';
import logoMyleDe from 'assets/images/logo-myle-de-b.svg';
import logoMettlers from 'assets/images/logo-mettler-b.svg';
import logoGailing from 'assets/images/logo-gailing-b.svg';
import logoHershortt from 'assets/images/logo-hershortt-b.svg';
import logoPps from 'assets/images/logo-pps-b.svg';
import logoKruschina from 'assets/images/logo-kruschina-b.svg';
import logoTabacum from 'assets/images/logo-tabacum-b.svg';


import '../../assets/slick/slick.css';
import '../../assets/slick/slick-theme.css';
import "./ApproachPage.css";

export const ApproachPage = () => {
  const trustRef = useRef();
  const trustSelector = gsap.utils.selector(trustRef);

  const [isMobile, setMobile] = useState(window.innerWidth < 1024);
  useLayoutEffect(() => {
    function updateSize() {
      setMobile(window.innerWidth < 1024);
    }
    window.addEventListener('resize', updateSize);
    return () => window.removeEventListener('resize', updateSize);
  }, []);

  // const images = [
  //   {
  //     src: logoTesa,
  //     alt: "Tesa Logo",
  //     width: 170,
  //     loading: "lazy"
  //   },
  //   {
  //     src: logoWatchDe,
  //     alt: "Watch.de Logo",
  //     width: 138,
  //     loading: "lazy"
  //   },
  //   {
  //     src: logoRitterSport,
  //     alt: "Ritter Sport Logo",
  //     width: 84,
  //     loading: "lazy"
  //   },
  //   {
  //     src: logoDdsa,
  //     alt: "Die Deutsche Schulakademie Logo",
  //     width: 226,
  //     loading: "lazy"
  //   },
  //   {
  //     src: logoMyleDe,
  //     alt: "MyleDe Logo",
  //     width: 157,
  //     loading: "lazy"
  //   },
  //   {
  //     src: logoSolarWinds,
  //     alt: "SolarWinds Logo",
  //     width: 210,
  //     loading: "lazy"
  //   },
  //   {
  //     src: logoTabacum,
  //     alt: "Tabacum Logo",
  //     width: 152,
  //     loading: "lazy"
  //   },
  //   {
  //     src: logoGailing,
  //     alt: "Gailing Logo",
  //     width: 174,
  //     loading: "lazy"
  //   },
  //   {
  //     src: logoMettlers,
  //     alt: "Mettlers Logo",
  //     width: 236,
  //     loading: "lazy"
  //   },
  //   {
  //     src: logoHershortt,
  //     alt: "Hershortt Logo",
  //     width: 198,
  //     loading: "lazy"
  //   },
  //   {
  //     src: logoPps,
  //     alt: "PPS Logo",
  //     width: 133,
  //     loading: "lazy"
  //   },
  //   {
  //     src: logoKruschina,
  //     alt: "Kruschina Logo",
  //     width: 144,
  //     loading: "lazy"
  //   }
  // ];

  // const settings = {
  //   arrows: false,
  //   dots: false,
  //   infinite: false,
  //   variableWidth: true,
  //   slidesToScroll: 2
  // };

  useEffect(() => {
    let tl = gsap.timeline();

    tl.add(ImageListTween(trustSelector("li"), trustRef.current));
  }, []);

  return (
    <div className='page-wrapper'>
      <CommonMetaTags 
				title={'Werbeagentur Stuttgart - Dienstleistungen'}
				description={'Werbeagentur Stuttgart - Wir sind eine Full-Service-Agentur und bieten eine breite Palette von Dienstleistungen an, von der Analyse und Markenentwicklung bis hin zur Umsetzung an allen Berührungspunkten mit der Zielgruppe.'}
				url={'https://www.tandem-m.de/werbeagentur-stuttgart.html'}
				image={heroImg}
			/>

      <Header />
      
      <main>
        <h1 className="visuallyHidden">Dienstleistungen</h1>
        
        <Hero
          src={heroImg}
          alt="Bürostuhlrennen"
          width={1920}
          height={800}
          content={
            <HeroContent 
              title={<span>Dienstleistungen</span>}
              text="Werbeagentur Stuttgart"
            />
          }
        />

        <section>
          <h2 className="visuallyHidden">Über uns</h2>
          <SectionWhite 
            title="Was tun wir?"
            text="Wir sind eine Full-Service-Agentur und bieten eine breite Palette von Dienstleistungen an, von der Analyse und Markenentwicklung bis hin zur Umsetzung an allen Berührungspunkten mit der Zielgruppe."
          />
        </section>

        <Worktable />

        <ColorSteps 
						hiddenTitle="Qualitative Forschung"
						parallax={
							<ParallaxEffect
								image={parallaxImageResearch}
								alt="Tandem Marketing & Partners"
								children={
									<div className="section container">
										<b className="heading-2 parallax-text">
                      Qualitative Forschung
										</b>
									</div>
								}
							/>
						}
            // bgColor={"var(--color-blue-marguerite)"}
						textTop={"Wir bilden die KPIs zu Beginn eines Projekts nach der Untersuchung der Benutzererfahrung durch Anytics, Metrix und Expiriance. Während der Ausarbeitung dieser verstehen wir die Bedürfnisse der Zielgruppe und wie sie mit dem (Use Cases) Produkt (Webportal, Commerce System, Website)  interagieren."}
						textCenter={"Wir glauben, dass jedes Projekt messbare KPIs haben sollte, auf deren Grundlage es erstellt und entwickelt wird."}
						textBottom={<Fragment>
							<p className="section-text-200">Gleichzeitig befragen wir wenn möglich Kundenbetreuer im Unternehmen des Kunden in allen betroffenen Abteilungen, um einen vollständigen Prozessablauf zu verstehen.</p>
              <p className="section-text-200">Auf diese Weise schaffen wir Projekte, die gleichzeitig die Probleme aller Geschäftsbereiche des Kunden lösen, ohne dass der Nutzen für die Endkunden dabei verloren geht.</p>							
						</Fragment>}
        />

        <section>
          <SectionColor 
            content={
              <Fragment>
                <p styleName="text">Bei der qualitativen Forschung erstellen wir eine Kundenerfahrungskarte (CJM). Dazu können wir auf Wunsch Tagebuchstudien durchführen, um Hypothesen für die Optimierung des gesamten Zyklus der Benutzerinteraktion mit dem Produkt zu ermitteln. Usability-Tests von von bestehenden Websites und mobilen Anwendungen werden durchgeführt, um Schwierigkeiten bei der Kundeninteraktion mit Online-Produkten zu identifizieren und bei der Neuentwicklung diese Aspekte gleich zu verbessern.</p>
                <div styleName="buttonGroup">
                  <Link className="buttonLink" to="/referenzen" style={{
                    "--stroke": "var(--color-white)"}}>Zielgruppenanalyse</Link>
                  <Link className="buttonLink" to="/referenzen" style={{
                    "--stroke": "var(--color-white)"}}>Produktanalyse</Link>
                  <Link className="buttonLink" to="/referenzen" style={{
                    "--stroke": "var(--color-white)"}}>Split-testing</Link>
                </div>
              </Fragment>
            }
            textColor="var(--color-white)"
            backgroundColor="var(--color-cherry-dark)"
          />
        </section>

        <section>
					<Article 
						title="Systemanalyse"
						content={
							<Fragment>
								<p>Welches System eignet sich besser für unsere Kunden anhand der vorab gesetzten KPIs und der Zielgruppen- Produktanalyse. Dabei steht der Kunde im Vordergrund und nicht nur unser Produktportfolio. Wir ermitteln gemeinsam das best geeignetste System (CMS, CMF, Framework), welches sich für die Anforderungen des Kunden am besten eignet.</p>
							</Fragment>
						}
						buttonUrl="#"
						buttonCaption="Fall zeigen"
						img={<img 
							src={parallaxImageWatch}
							alt=""
							width={1920} 
							height={800}
							loading="lazy"
						/>}
						color="var(--color-cherry-dark)"
            narrowText={true}
					/>
				</section>

        <ParallaxEffect
          image={parallaxImage1}
          alt="Tandem Marketing & Partners"
          strength={200}
          children={
            <div className="container-narrow">
              <h2 className="parallax-text" styleName="parallax-text">
                Wie erreichen wir es?
              </h2>
            </div>
          }
        />

        <section>
          <SectionColor 
            content={
              <p styleName="text">Auf Basis der beschriebenen Voruntersuchung, ermitteln wir die Grenzen und Beschränkungen der jeweiligen Systeme. Auf der Grundlage der Umfrage und des Analyseberichts erstellen wir eine einheitliche Liste mit priorisierten und sich aus unserer Sicht geeigneten Lösungen (Drupa, Typo3, Wordpress, Magento, Framework, Shopify, Apps (SPA)) bereit.</p>
            }
            textColor="var(--color-white)"
            backgroundColor="var(--color-scarlet-gum)"
          />
          <SectionColor 
            content={
              <Fragment>
                <p styleName="text">Anschließend planen wir die Navigation des künftigen Projekts auf der Grundlage der ermittelten Szenarien für den Besuch der Website und der mobilen Anwendung durch verschiedene Benutzergruppen. </p>
                <p styleName="text">Dann beginnen wir mit der Erstellung eines Prototyps - wir visualisieren das künftige Produkt, bauen eine Struktur von Seiten aus logischen Blöcken ohne des finalen Screen Desigs. Der Prototyp ermöglicht es uns, uns auf die Logik und den Inhalt zu konzentrieren, ohne vom Design abgelenkt zu werden. In der Entwurfsphase legen wir auch die Präsentation der Informationen fest. Wir schreiben Texte, machen Fotos und Videos und bereiten Illustrationen vor.</p>
              </Fragment>
            }
            textColor="var(--color-dark)"
            backgroundColor="var(--color-white)"
          />
          <SectionColor 
            content={
              <p styleName="text" style={{textAlign: 'center'}}><b>Der inhaltsreiche Prototyp wird getestet, um festzustellen, wie gut er die Geschäftsziele erfüllt. Die Tests werden mit echten Benutzern durchgeführt, die zur Zielgruppe des Projekts gehören. Tests helfen, Fehler in den frühen Phasen der Arbeit zu vermeiden.</b></p>
            }
            textColor="var(--color-white)"
            backgroundColor="var(--color-scarlet-gum)"
          />
        </section>

        <section>
					<Article 
						title="Aufgabenstellung"
						content={
							<Fragment>
								<p>Nachdem unsere Systemanalytiker die Anforderungen an das Produkt und die Systeme erhalten haben, beginnen sie mit der Ausarbeitung der Aufgabenstellung.</p>
                <p>Darin beschreiben sie die funktionale Logik aller Abschnitte und Blöcke des künftigen Projekts sowie die nichtfunktionalen Anforderungen daran. Auf dieser Grundlage entwickelt das Team die Software-Architektur, die Struktur der Datenspeicherung und die Integrationsspezifikationen. In der gleichen Phase legen unsere Spezialisten für Informationssicherheit die Sicherheitsanforderungen für die Systeme fest. Die Aufgabe wird mit dem Kunden besprochen und genehmigt.</p>
							</Fragment>
						}
						buttonUrl="#"
						buttonCaption="Fall zeigen"
						img={<img 
							src={parallaxImageMettler}
							alt=""
							width={1920} 
							height={800}
							loading="lazy"
						/>}
						color="var(--color-blue-marguerite)"
            narrowText={true}
					/>
				</section>

        <ColorSteps 
						hiddenTitle="Webdesign (UX/UI)"
						parallax={
							<ParallaxEffect
								image={parallaxImageWebdesign}
								alt="Tandem Marketing & Partners"
								children={
									<div className="section container">
										<b className="heading-2 parallax-text">
                      Webdesign (UX/UI)
										</b>
									</div>
								}
							/>
						}
            bgColor={"var(--color-gigas)"}
						textTop={"Die Designarbeit beginnt mit einem allgemeinen visuellen Konzept, in dem wir den Stil des künftigen Projekts festlegen (Startseite, globale Seiten, Landings). Das Projektkonzept wird von unserem Creative Director in Zusammenarbeit mit dem Art Director erstellt.<br><br>Für die Präsentation erstellen wir ein Video mit Mikro-Animation und Demonstration der Interface-Elemente."}
						textCenter={"Bei großen Projekten erstellen wir ein komplettes Designsystem - die visuelle Sprache des Projekts (Hunderte individuelle Ebenen mit allen Elementen)."}
						textBottom={<Fragment>
							<p className="section-text-200">Auf der Grundlage der Regeln und Standardelemente können die Designer in Zukunft schnell neue Produkte entwerfen und dabei die visuelle Integrität des Projekts bewahren.</p>
						</Fragment>}
        />

        <section>
          <ParallaxEffect
            image={parallaxImageEntwicklung}
            alt="Tandem Marketing & Partners"
            strength={200}
            children={
              <div className="container-narrow">
                <h2 className="parallax-text" styleName="parallax-text">
                  Entwicklung
                </h2>
              </div>
            }
          />
          <SectionColor 
            content={
              <Fragment>
                <p styleName="text">Unsere Entwickler arbeiten an Projekten in Teams unter der Leitung von Teamleads. Wir lösen selbstständig komplexe technische Aufgaben: hochskalierbare fehlertolerante Portale, PWA, SPA und native mobile Anwendungen, Integration mit den Back-Systemen des Kunden und weitere Integrationen.</p>
                <p styleName="text">Wir automatisieren und standardisieren die Entwicklung im Haus. Wir prüfen alle Projekte anhand von Checklisten und decken sie mit Tests ab. Wir kennen den Umgang mit personenbezogenen Daten und entwickeln Projekte gemäß DSGVO.</p>
              </Fragment>
            }
            textColor="var(--color-white)"
            backgroundColor="var(--color-blue-marguerite)"
          />
          <SectionColor 
            content={
              <p styleName="text">Jedes Projekt wird von einem oder mehreren KeyAccount Managern betreut, die als Owner fungieren. Sie beteiligen sich an der Konzeption, führen Tests durch und sind für die Integrität des Projekts verantwortlich. Die Owner kommunizieren mit den technischen Spezialisten des Kunden und leiten das Projektentwicklungsteam.</p>
            }
            textColor="var(--color-dark)"
            backgroundColor="var(--color-white)"
          />
        </section>

        <section>
					<Article 
						title="Support und Weiterentwicklung"
						content={
							<Fragment>
								<p>Wir übergeben nach Finalisierung das fertige Produkt nicht einfach an den Kunden, ohne Support und Weiterentwicklung zu gewährleisten. Unsere Aufgabe ist es, den Kunden mit dem Projekts auch nach dem Start zu begleiten und bei Bedarf zu allumfänglich zu unterstützen.</p>
							</Fragment>
						}
						buttonUrl="#"
						buttonCaption="Fall zeigen"
						img={<img 
							src={parallaxImagePPS}
							alt=""
							width={1920} 
							height={800}
							loading="lazy"
						/>}
						color="var(--color-scarlet-gum)"
            narrowText={true}
					/>
				</section>

        <div>
          <SourceAboutUs />
        </div>
        
        <Blog />

        <FeedbackHomePage />
      </main>
      
      <Footer />
    </div>
  );
}
