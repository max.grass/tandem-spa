import React, { Fragment } from "react";

import { Link } from 'react-router-dom';

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { ParallaxEffect } from "components/ParallaxEffect";
import { ColorSteps } from "components/ColorSteps";
import { CheckedList } from "components/CheckedList";
import { AboutPageStrengths } from "components/AboutPage-OurStrengths";
import { data as UtilityPageStrengthsData } from "components/AboutPage-OurStrengths/UtilityPageStrengthsData";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from "assets/images/hero-usability.jpg";

import parallax1 from "assets/images/parallax-usability-1.jpg";
import parallax2 from "assets/images/parallax-usability-2.jpg";
import parallax3 from "assets/images/parallax-usability-3.jpg";
import parallax4 from "assets/images/parallax-usability-4.jpg";
import parallax5 from "assets/images/parallax-usability-5.jpg";
import parallax6 from "assets/images/parallax-usability-6.jpg";
import parallax7 from "assets/images/parallax-usability-7.jpg";
import parallax8 from "assets/images/parallax-usability-8.jpg";

import scheme from "assets/images/usability-s-01.svg";

import "./UsabilityPage.css";

export const UsabilityPage = () => {

	return (
		<div className='page-wrapper'>
			<CommonMetaTags 
				title={'Usability und Webdesign Spezialisten aus Stuttgart'}
				description={'Dank Usability zu Erfolg und höherer Conversion Rate Ihrer Webpräsenz. Vertrauen Sie auf unsere Kompetenz!'}
				url={'https://www.tandem-m.de/webdesign-cms/usability.html'}
				image={heroImg}
			/>

			<Header />
			
			<main>
				<Hero
					src={heroImg}
					alt="Bürostuhlrennen"
					width={1920}
					height={800}
					content={
						<div>
							<HeroContent 
								title={<span>Mit Usability und UX Kunden gewinnen</span>}
								text="Usability"
							/>
						</div>
		  		}
				/>

				<section>
					<h2 className="visuallyHidden">Marketing</h2>
					<SectionWhite 
						title="Mit Usability und UX Kunden gewinnen"
						text="Als Spezialisten für Usability und User Experience (UX) sind wir Ihr Ansprechpartner für messbaren Erfolg im Netz."
						textColor="var(--color-scarlet-gum)"
					/>
					<div>
						<SectionColor 
							content={
								<Fragment>
									<p className="section-text-200">Ob Analyse, Usability-Tests, Konzeption, Website-Entwicklung oder Webdesign: Tandem Marketing & Partners optimiert Ihren Internetauftritt, Apps oder andere digitale Produkte mit praktischem Knowhow. Eine wichtige Voraussetzung dafür sind gute Usability und User Experience.</p>
								</Fragment>
							}
							textColor="var(--color-white)"
							backgroundColor="var(--color-scarlet-gum)"
						/>
					</div>
					<div className="empty-parallax">
						<ParallaxEffect
							image={parallax1}
							alt="Tandem Marketing & Partners"
						/>
					</div>
				</section>

				<section>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-700">Usability</p>
								<p className="section-text-200">Usability ist die Benutzerfreundlichkeit von Websites, Onlineshops oder Portalen. Dazu gehören beispielsweise kurze Ladezeiten, ein ansprechendes, modernes Webdesign und eine übersichtliche Navigationsstruktur.</p>
								<p className="section-text-200">Besucher sollten sich intuitiv auf einer Website bewegen und Informationen schnell finden können.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
					<ParallaxEffect
						image={parallax4}
						alt="Tandem Marketing & Partners"
						children={
							<div className="container-narrow" styleName="parallaxText">
								<p className="section-text-700 parallax-text">Die internationale Norm DIN ISO 9241, 11 definiert Usability folgendermaßen:<br />“Usability ist das Ausmaß, in dem ein System durch bestimmte Benutzer in einem bestimmten Nutzungskontext genutzt werden kann, um bestimmte Ziele effektiv, effizient und zufriedenstellend zu erreichen.” (DIN ISO 9241, 11)</p>
							</div>
						}
					/>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-700">User Experience</p>
								<p className="section-text-200">UX bezieht auch emotionale Aspekte in das Nutzungserlebnis einer Onlinepräsenz ein: Der Nutzer soll schnell und effektiv ans Ziel kommen und dabei im besten Fall auch noch Spaß haben. Das Webdesign sollte das Vertrauen fördern und positive Gefühle wecken (Joy of Use).</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Der Begriff User Experience umfasst also das gesamte Nutzungserlebnis.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-scarlet-gum)"
					/>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Die internationale Norm DIN ISO 9241,210 definiert User Experience so:<br />“Wahrnehmungen und Reaktionen einer Person, die aus der tatsächlichen und/oder der erwarteten Benutzung eines Produkts, eines Systems oder einer Dienstleistung resultieren. [...] Dies umfasst alle Emotionen, Vorstellungen, Vorlieben, Wahrnehmungen, physiologischen und psychologischen Reaktionen, Verhaltensweisen und Leistungen, die sich vor, während und nach der Nutzung ergeben.” (DIN ISO 9241, 210)</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
					<div className="container">
						<img src={scheme} alt="scheme" loading="lazy"/>
					</div>
					<ParallaxEffect
						image={parallax5}
						alt="Tandem Marketing & Partners"
						children={
							<div className="container-narrow" styleName="parallaxText">
								<p className="section-text-700 parallax-text">Als Internetagentur aus Stuttgart schaffen wir positive Erfahrungen – für Sie und Ihre Kunden.</p>
							</div>
						}
					/>
				</section>

				<section styleName="section-nopdb">
					<div className="container-narrow">
						<h2 className="heading-3">Unsere Leistungen im Bereich Usability und User Experience:</h2>
					</div>
					<div className="container-narrow">
						<CheckedList 
							content={
								<Fragment>
									<li>Usability-Test</li>
									<li>Zielgruppenanalyse</li>
									<li>UX-Design</li>
									<li>UX-Consulting</li>
								</Fragment>
							}
							color="var(--color-scarlet-gum)"
						/>
					</div>
				</section>

				<section styleName="section-nopdb">
					<ParallaxEffect
						image={parallax6}
						alt="Tandem Marketing & Partners"
						children={
							<div className="container-narrow" styleName="parallaxText">
								<p className="heading-2 parallax-text">Warum sind gute Usability und User Experience so wichtig?</p>
							</div>
						}
					/>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Besucher entscheiden innerhalb von 2,3 Sekunden, ob ihnen eine Seite gefällt oder nicht. Farbgebung, Bilder und Typographie spielen dabei eine entscheidende Rolle. Ein einheitliches Corporate Design wirkt vertrauensbildend, seriös und führt zu einer subjektiven Qualitätssteigerung Ihrer Produkte. Damit einher geht eine erhöhte Kaufbereitschaft. Doch zunächst kommt es darauf an, dass Ihre Inhalte auch geladen werden, und zwar schnell.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-scarlet-gum)"
					/>
					<div className="empty-parallax">
						<ParallaxEffect
							image={parallax7}
							alt="Tandem Marketing & Partners"
						/>
					</div>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-700">Gute Usability ist die Voraussetzung für positive User Experience</p>
								<p className="section-text-200">Neben kurzen Ladezeiten sollte ein Internetauftritt eine einfache und übersichtliche Navigationsstruktur aufweisen. Je angenehmer eine Website gestaltet ist, desto besser können Benutzer sich auf die Inhalte konzentrieren und das Angebot verstehen. Eine übersichtliche, aufgeräumte Website macht es Kunden leicht, sich zurecht zu finden. Ein zufriedener Kunde kommt gerne wieder und ist eher bereit zu persönlichen Weiterempfehlungen.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
					<div className="empty-parallax">
						<ParallaxEffect
							image={parallax3}
							alt="Tandem Marketing & Partners"
						/>
					</div>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-700">Usability und UX sind Rankingfaktoren</p>
								<p className="section-text-200">Usability und UX spielen eine entscheidende Rolle für Ihren Erfolg im Internet – auch in puncto Suchmaschinenoptimierung. Denn die sogenannten Core Web Vitals sind seit Mai 2021 Rankingfaktoren. Sie spiegeln die Nutzererfahrung einer Website anhand von Ladezeit, Interaktivität und visueller Stabilität wider.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
					<ParallaxEffect
						image={parallax8}
						alt="Tandem Marketing & Partners"
						children={
							<div className="container-narrow" styleName="parallaxText">
								<p className="heading-2 parallax-text">Wir gestalten Ihre digitalen Produkte: Einfach und intuitiv</p>
							</div>
						}
					/>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">In Bezug auf die Nutzerfreundlichkeit und das Nutzungserlebnis Ihrer Onlinepräsenz verfolgen wir einen ganzheitlichen Ansatz: Dazu gehört die intuitive Bedienung ebenso wie ein einheitlicher, vertrauensbildender optischer Eindruck und emotionale Faktoren.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-scarlet-gum)"
					/>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Als Experten im Bereich Usability und UX analysieren wir Ihren Internetauftritt und machen ungenutzte Potenziale ausfindig. Wir beraten Sie zu aktuellen Trends im Webdesign wie beispielsweise „Mobile First“, <Link to={'/webdesign-cms/responsives-webdesign.html'}>„Responsive Design“</Link> oder „Parallax Scrolling“, stellen Ihnen neueste Technologien vor und entwickeln individuelle, auf Ihr Unternehmen zugeschnittene Lösungen. So bieten Sie Ihren Kunden einen echten Mehrwert!</p>
								<Link
									to="/werbeagentur-stuttgart/contact.html"
									className="button"
									style={{
									"color": "var(--color-white)",
									"--stroke": "var(--color-white)",
									"backgroundColor": "var(--color-scarlet-gum)"
									}}
								>Jetzt Beratungstermin vereinbaren</Link>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
				</section>

				<section>
					<ColorSteps
						hiddenTitle="Usability-Test"
						parallax={
							<ParallaxEffect
								image={parallax2}
								alt="Tandem Marketing & Partners"
								children={
									<div className="section container">
										<b className="heading-2 parallax-text">
											Usability-Test: Wir prüfen Ihre Website 
										</b>
									</div>
								}
							/>
						}
						textTop={"Im Rahmen unseres Usability-Consultings beraten wir Sie gerne projektbegleitend zu den Themen Usability und User Experience. Auf Wunsch nehmen wir ein sogenanntes Website-Testing vor. Dabei analysieren wir u.a., wie effektiv Besucher Ihre Seite nutzen, wie lange sie verweilen und ob sie einkaufen. Auf Basis der relevanten Kennzahlen erstellen wir für Sie einen individuellen Plan zur Conversion-Optimierung. Die Ergebnisse werden in einem persönlichen Termin (ggf. per Video oder Telefon) präsentiert."}
						textCenter={"Im Anschluss stehen wir Ihnen gerne weiterhin beratend oder auch aktiv zur Seite."}
						textBottom={""}
						bgColor={"var(--color-scarlet-gum)"}
					/>
				</section>

				<AboutPageStrengths
					header={'Der Usability-Test beinhaltet:'}
					data={UtilityPageStrengthsData}
					hrColor={"var(--color-scarlet-gum)"}
				/>

				<section>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Unser Ziel ist es, die Wettbewerbsfähigkeit Ihres Unternehmens zu stärken und zwar durch herausragende Benutzerfreundlichkeit und ein einmaliges Nutzungserlebnis.</p>
								<p className="section-text-200">Kontaktieren Sie uns gerne für weitere Informationen oder vereinbaren direkt einen Termin.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-scarlet-gum)"
					/>
				</section>
				
				

				<FeedbackHomePage />
			</main>
			
			<Footer />
		</div>
	);
}
