import React, { Fragment, useEffect, useRef } from "react";
import gsap from 'gsap';

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { Article } from "components/Article";
import { ParallaxEffect } from "components/ParallaxEffect";
import { Icon } from "components/Icon";
import { ColorSteps } from "components/ColorSteps";
import { Hr } from "components/Hr";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import { ListItemsTween2 } from "components/Animation"

import heroImg from "assets/images/magento-orange-girl.jpg";
import featuresArticleImg from "assets/images/hs-tablet.jpg";
import communityImg from "assets/images/magento-orange-women.jpg";
import communityStatsImg from "assets/images/warum-magento-stats-community.svg";
import hershorttImg from "assets/images/magento-hershortt.jpg";
import imgIntegrationArticle from "assets/images/watchde-desktop-mobile.jpg";
import modulesParallaxImg from "assets/images/warum-magento-pps.jpg";
import modulesStatsImg from "assets/images/warum-magento-stats-modules.svg";
import conclusionImg from "assets/images/warum-magento-conclusion.jpg";

import "./WarumMagentoPage.css";

export const WarumMagentoPage = () => {
  const customersRef = useRef();

  useEffect(() => {
    let tl = gsap.timeline();
    
    tl.add(
      ListItemsTween2(
        customersRef.current,
        0.1
      )
    );
  }, []);

  return (
    <div className="page-wrapper">
      <CommonMetaTags 
				title={'Warum Magento'}
				description={'Tandem Marketing'}
				url={'https://www.tandem-m.de/magento-webshop/warum-magento.html'}
				image={heroImg}
			/>

      <Header />
      <main>
        <Hero
          src={heroImg}
          heght={500}
          content={
            <HeroContent 
              title={<span>Magento Vorteile <br/> auf einen Blick</span>}
              text="Warum Magento"
            />
          }
        />

        <section>
          <h2 className="visuallyHidden">Einleitung</h2>
          <SectionWhite 
            title="Magento Vorteile auf einen Blick"
            text="Bei der Suche nach einem passenden Webshopsystem sollte auf einiges geachtet werden. Ein wichtiger Punkt — gerade bei größeren Vorhaben — ist die Skalierbarkeit des Systems. "
            textColor="var(--color-flamingo)"
          />

          <SectionColor 
            content={
              <p className="section-text-200">So ist bei hohem Bestell- und Traffic-Aufkommen eine proportionale Leistungssteigerung möglich. Um das Überleben einer neuen Plattform auf dem Markt langfristig zu garantieren, ist auch die Updatefähigkeit des Quellcodes von entscheidender Bedeutung. Magento erfüllt diese Voraussetzungen. Lesen Sie hier, warum Magento das richtige Shopsystem für Ihr Unternehmen ist.</p>
            }
            backgroundColor="var(--color-flamingo)"
          />
        </section>

        <section className="section-m" styleName="features">
          <h2 className="visuallyHidden">Funktionen von Magento</h2>
          <Article 
            buttonUrl="/projekte/84-hershortt.html"
            buttonCaption="Fall zeigen"
            img={<img 
              src={featuresArticleImg}
              alt="hershortt.com project on tablet"
              width={1920} 
              height={800}
              loading="lazy"
            />}
            color="var(--color-flamingo)"
          />
          <div className="container-narrow" styleName="features-content">
            <h3 className="section-text-400">Flexibel, skalierbar, updatefähig</h3>
            <p styleName="features-text">Eine flexible und hohe Skalierbarkeit wird dadurch erreicht, dass die Architektur von Magento das Aufsetzen von Datenbank- und Web-Server-Clustern unterstützt. Ein Magento Webshop ist modular aufgebaut und lässt sich nahezu uneingeschränkt erweitern. Der Programmkern ist von Modulen und Design getrennt. Genauer gesagt heißt das, dass Updates eingespielt werden können, ohne individuelle Änderungen oder Erweiterungen zu überschreiben. So ist die Updatefähigkeit des Systems über Jahre hinweg garantiert. Voraussetzung dafür ist eine professionelle Umsetzung.</p>
          </div>

        </section>
        
        <section className="section-m" styleName="community">
          <h3 className="visuallyHidden">Weltweite Community</h3>
          <ParallaxEffect
            image={communityImg}
            alt="Dame mit Tablet"
            children={
              <p className="container-narrow section parallax-text section-text-700">Mit einer schnell wachsenden Community von über 150.000 Mitgliedern und einem Kernteam von ca. 290 Mitarbeitern gehört Magento weltweit zu den führenden E-Commerce-Lösungen.</p>
              }
          />

          <div className="section-m container-narrow" styleName="community-stats">
            <img src={communityStatsImg} width="1260" height="428" alt="Magento hat weltweit 30% Marktanteil; 150.000 Magento-Entwickler; 300 Solution-Partner; 50 Millionen User; 250.000 Onlineshops auf Magento-Basis; über 100 Milliarden Dollar Umsatz/" loading="lazy" />
          </div>
          <SectionColor 
            content={
              <p className="section-text-200">Die Community Edition ist open source, d.h. kostenlos. Daneben gibt es mit Magento Commerce auch eine kostenpflichtige Enterprise Edition. Wichtig zu wissen ist: Beide Versionen nutzen den gleichen Magento-Core und sind in der Struktur identisch.</p>
            }
            backgroundColor="var(--color-flamingo)"
          />
        </section>

        <section className="section-m container-narrow" styleName="customers" ref={customersRef}>
          <h3 className="section-text-400">Von Marktanalysten empfohlen</h3>
          <p>2020 wurde Magento zum vierten Mal in Folge von Gartner als führendes Shopsystem ausgezeichnet. Als Gründe wurden u.a. die große Anzahl an Neukunden im B2B- und B2C-Bereich, der Funktionsumfang sowie die Partnerschaft mit Adobe genannt. Magento Shops dominieren gerade bei mittelständischen Unternehmen, doch auch die großen Player schätzen die Commerce-Plattform.</p>
          <ul>
            <li>
              <h5>Coca-Cola</h5>
              <Icon
                name="logo-coca-cola"
                width="164px"
                height="54px"
                figure="circle"
              />
            </li>
            <li>
              <h5>Rebecca <br/> Minkoff</h5>
              <Icon
                name="logo-rebecca-minkoff"
                width="164px"
                height="11px"
                figure="square"
              />
            </li>
            <li>
              <h5>Nike</h5>
              <Icon
                name="logo-nike"
                width="164px"
                height="59px"
                figure="triangle-bottom"
              />
            </li>
            <li>
              <h5>Christian <br/> Louboutin</h5>
              <Icon
                name="logo-christian-louboutin"
                width="140px"
                height="108px"
                figure="circle"
              />
            </li>
            <li>
              <h5>Rubiks</h5>
              <Icon
                name="logo-rubik-s-cube"
                width="164px"
                height="61px"
                figure="triangle-top"
              />
            </li>
            <li>
              <h5>Nestle <br /> Nespresso</h5>
              <Icon
                name="logo-nestle-nespresso"
                width="164px"
                height="30px"
                figure="square"
              />
            </li>
            <li>
              <h5>Olympus</h5>
              <Icon
                name="logo-olympus"
                width="146px"
                height="28px"
                figure="square"
              />
            </li>
            <li>
              <h5>Ford</h5>
              <Icon
                name="logo-ford"
                width="164px"
                height="63px"
                figure="triangle-top"
              />
            </li>
            <li>
              <h5>Warby <br/> Parker</h5>
              <Icon
                name="logo-warby-parker"
                width="164px"
                height="14px"
                figure="circle"
              />
            </li>
            <li>
              <h5>Fox Connect</h5>
              <Icon
                name="logo-fox-connect"
                width="148px"
                height="69px"
                figure="square"
              />
            </li>
            <li>
              <h5>Jaguar</h5>
              <Icon
                name="logo-jaguar"
                width="164px"
                height="88px"
                figure="circle"
              />
            </li>
            <li>
              <h5>Vizio</h5>
              <Icon
                name="logo-vizio"
                width="164px"
                height="37px"
                figure="triangle-bottom"
              />
            </li>
          </ul>
        </section>

        <ColorSteps 
          hiddenTitle="Multi-Domain"
          parallax={
            <ParallaxEffect
              image={hershorttImg}
              alt="Hershortt PWA-Projekt auf Magento"
              children={
                <div className="container">
                  <b className="heading-2 parallax-text">Dynamisches Templatesystem</b>
                </div>
              }
            />
          }
          bgColor={"var(--color-flamingo)"}
          textTop={"Das Shopsystem Magento trennt Funktionalität und Layout: So ist ein individuelles Design nach Vorgaben eines bestehenden Corporate Designs einfach umsetzbar."}
          textCenter={"Da die E-Commerce-Plattform multishop-fähig ist, können je nach Projektgröße auch mehrere unterschiedliche Onlineshops über ein Administrationsmenü verwaltet werden."}
          textBottom={
            <p className="section-text-200">Das dynamische Templatesystem ermöglicht verschiedene Designs für die Webshops, CMS-Seiten, Kategorien, Produktansichten, Store Views pro Sprache und vieles mehr. Auf diese Weise können die Bedürfnisse der jeweiligen Zielgruppe optimal erfüllt werden.</p>
          }
        />

        <Article 
          title="Nahtlos integrierbar"
          buttonUrl="/projekte/52-watch-de.html"
          buttonCaption="Fall zeigen"
          img={<img 
            src={imgIntegrationArticle}
            alt="Watch.de project on tablet"
            width={1920} 
            height={800}
            loading="lazy"
          />}
          color="var(--color-flamingo)"
        />
        <div className="container-narrow" styleName="article-content">
          <h3 className="section-text-400">Nahtlos integrierbar</h3>
          <p>Die Magento-Programmierschnittstelle (application programming interface, kurz API) unterstützt sowohl das Netzwerkprotokoll SOAP als auch REST, was eine nahtlose, bidirektionale Integration in diverse ERP-Systeme erlaubt. Der Abgleich von Produktstammdaten, Bildern, Bestellungen, Kundenkreisen, Statistiken wird damit ein Leichtes.</p>
          <p>Durch die hohe Kompatibilität mit IT-Infrastrukturen aller Art, lässt sich Magento auch auf Basis modernster Technologien wie Headless und PWA aufbauen. Damit sind Magento Onlineshops der Konkurrenz in puncto Technik und User Experience voraus.</p>
          <h3 className="section-text-400">Produktorientiert</h3>
          <p>Ganz gleich, ob Sie einzelne Artikel, Bundles, Sets oder downloadbare Produkte verkaufen möchten: Der Magento Onlineshop unterstützt vielfältige Produktarten. Dazu kommt die flexible Attributverwaltung. Shop-Betreiber können Attribute und Attributsets frei definieren und in Kategorien zusammenfassen.  Für Kunden wird so die Suche im Onlineshop durch verschiedene FIlter erleichtert; darüber hinaus können sie Produkte vor der Bestellung konfigurieren oder vergleichen.</p>
        </div>

        <ColorSteps 
          hiddenTitle="Erweiterungen und Module"
          parallax={
            <ParallaxEffect
              image={modulesParallaxImg}
              alt="Magento-Dashboard"
              children={
                <div className="container">
                  <b className="heading-2 parallax-text">Erweiterungen und Module</b>
                </div>
              }
            />
          }
          bgColor={"var(--color-flamingo)"}
          textTop={
            <div styleName="modules">
              <Hr color="var(--color-flamingo)"/>
              <p className="section-text-400" styleName="modules-text">Über den Magento Marketplace sind eine Vielzahl an Erweiterungen und Modulen für Magento Onlineshops verfügbar. Dazu gehören z.B.</p>
              <ul>
                <li>Magento Shop Community Edition</li>
                <li>Magento Shop Enterprise Edition</li>
                <li>Magento Shop Enterprise Cloud Edition </li>
                <li>Magento Commerce Order Management (MCOM) </li>
                <li>Magento Business Intelligence </li>
                <li>Magento Services</li>
              </ul>
            </div>
          }
          textCenter={
            <div styleName="modules-stats">
              <img src={modulesStatsImg} width="810" height="222" alt="Insgesamt: über 5.000 Erweiterungen, über 3.800 Open Source" loading="lazy" />
            </div>}
          textBottom={null}
        />

        <section className="section-m container-narrow">
          <h2 className="section-text-400">Marketing-Features</h2>
          <p>Die Marketing- und Promotion-Features von Magento ermöglichen Shop-Betreiber*innen Aktionen schnell und effektiv umzusetzen. So können z.B. für bestimmte Produkte Preisregeln festgelegt werden oder sie können mit anderen Artikeln im Paket angeboten werden (Product-Bundles).</p>
          <p>Cross-Selling-Features auf Artikelseiten („Sie könnten an folgenden Produkten interessiert sein“) regen Besucher*innen zum längeren Verweilen an. Upselling-Funktionen im Warenkorb sowie die individuelle Konfiguration von Gutscheincodes gehören ebenso zu dem umfangreichen Angebot von Magento.</p>
          <p>Darüber hinaus ermöglicht ein einfaches Newsletter-Modul, Neuheiten, Aktionen oder das aktuelle Angebot zu bewerben.</p>
        </section>

        <section className="section-m" styleName="conclusion">
          <h2 className="visuallyHidden">Fazit</h2>
          {/* <ParallaxEffect
            image={conclusionImg}
            alt="Dame mit Tablet"
          /> */}
          <div styleName="conclusionArticle">
            <Article 
              buttonUrl="/projekte/84-hershortt.html"
              buttonCaption="Fall zeigen"
              img={<img 
                src={conclusionImg}
                alt="Dame mit Tablet"
                width={1920} 
                height={800}
                loading="lazy"
              />}
              color="var(--color-flamingo)"
            />
          </div>

          <div className="container-narrow section-m" styleName="conclusion-content">
            <h3>Magento Shop: Die richtige Onlineshop-Software für Sie</h3>
            <p className="section-text-200">Neben den oben genannten Gründen, gibt es einen weiteren, warum Magento die richtige Lösung für Ihr Commerce-Projekt ist; Das Webshopsystem ist sehr nutzerfreundlich und übersichtlich. Selbst Administratoren, die vorher noch mit keinem Shopsystem gearbeitet haben, finden sich in der klaren Struktur des Magento-Backends schnell zurecht und haben maximalen Überblick über Kauf und Verkauf.</p>
            <p className="section-text-200">ber Sales-Reports sind Shop-Betreiber in der Lage, Verkaufsreports zu generieren und Statistiken, z. B. über abgebrochene Warenkörbe, Suchanfragen, Wunschzettel, Topseller etc. zu kontrollieren. Darüber hinaus ist Magento optimal für die Suchmaschinenoptimierung gewappnet – eine wichtige Voraussetzung für einen erfolgreichen Onlineshop.</p>
          </div>
        </section>

        <section styleName="contacts">
          <h2 className="visuallyHidden">Bleiben Sie mit uns in Kontakt</h2>
          <div styleName="contacts-img"></div>
          <div className="container-narrow section"> 
            <p className="section-text-200">Die Marketing- und Promotion-Features von Magento ermöglichen Shop-Betreiber*innen Aktionen schnell und effektiv umzusetzen. So können z.B. für bestimmte Produkte Preisregeln festgelegt werden oder sie können mit anderen Artikeln im Paket angeboten werden (Product-Bundles).</p>
            <p className="section-text-200">Cross-Selling-Features auf Artikelseiten („Sie könnten an folgenden Produkten interessiert sein“) regen Besucher*innen zum längeren Verweilen an. Upselling-Funktionen im Warenkorb sowie die individuelle Konfiguration von Gutscheincodes gehören ebenso zu dem umfangreichen Angebot von Magento.</p>
            <p className="section-text-200">Darüber hinaus ermöglicht ein einfaches Newsletter-Modul, Neuheiten, Aktionen oder das aktuelle Angebot zu bewerben.</p>
          </div>
        </section>
      </main>
      <FeedbackHomePage />
      <Footer />
    </div>
  );
}
