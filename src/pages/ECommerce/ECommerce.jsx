import React, { Fragment, useRef, useEffect } from "react";
import gsap from "gsap";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { Hr } from "components/Hr";
import { ParallaxEffect } from "components/ParallaxEffect";
import { ListOrderUnorder } from "components/ListOrderUnorder";
import { Icon } from "components/Icon";
import { Article } from "components/Article";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { ListItemsTween, ListItemsTween2, RoundBackgroundTween } from "components/Animation";
import { LineChartEcommerce, TinyBarChartEcommerce } from "components/Charts";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from "assets/images/hero-ecommerce.jpg";
import statsParallaxInternet from "assets/images/e-commerce-stats-parallax1.jpg";
// import statsInfographicInternet from "assets/images/e-commerce-stats-internet.svg";
import statsInfographicShopping from "assets/images/e-commerce-stats-shopping.svg";
import statsParallaxEurostat from "assets/images/e-commerce-stats-parallax2.png";
import statsInfographicEurostat from "assets/images/e-commerce-stats-eurostat.svg";
import statsInfographicInteraction from "assets/images/e-commerce-stats-interactions.svg";
import offersParallax from "assets/images/e-commerce-offers.jpg";

import magentoArticle from "assets/images/e-commerce-about-magento.jpg";
import parallaxImageContact from "assets/images/about-page-parallax-trust.jpg";

import "./ECommerce.css";

export const ECommercePage = () => {
  const advantagesRef = useRef();
  const offersRef = useRef();
  const magentoServicesRef = useRef();
  const servicesRef = useRef();
  const roundBackgroundRef = useRef();
  
  const servicesSelector = gsap.utils.selector(servicesRef);

  useEffect(() => {
    let tl = gsap.timeline();
    
    tl
    .add(
      ListItemsTween(
        advantagesRef.current
      )
    )
    .add(
      ListItemsTween2(
        offersRef.current,
        0.2
      )
    )
    .add(
      ListItemsTween(
        magentoServicesRef.current
      )
    )
    .set(servicesRef.current, {
      "--bgColor": "var(--color-white)",
    })
    .set(servicesSelector("figure"), {
      "--secondary-color": "var(--color-white)",
    })
    .add(RoundBackgroundTween(
      roundBackgroundRef.current,
      servicesRef.current,
      "var(--color-darkredwine)"
      )
    );
  }, []);

  return (
    <div className="page-wrapper">
      <CommonMetaTags 
				title={'E-Commerce - Vertriebsweg der Zukunft'}
				description={'Tandem Marketing'}
				url={'https://www.tandem-m.de/marketing-stuttgart/e-commerce.html'}
				image={heroImg}
			/>

      <Header />
      <main>
        <Hero
          src={heroImg}
          heght={500}
          content={
            <HeroContent 
              title={<span>Wir begleiten Ihren Einstieg in <br /> den Onlinehandel</span>}
              text="E-Commerce"
            />
          }
        />

        <section>
          <h2 className="visuallyHidden">Einleitung</h2>
          <SectionWhite 
            title="Ihr Erfolg im Netz!"
            text="Als Magento-Agentur aus Stuttgart haben wir langjährige Erfahrung in den Bereichen Konzeption, Design und Webentwicklung."
          />

          <SectionColor 
            content={
              <Fragment>
              <p className="section-text-200">Gemeinsam mit Ihnen erstellen wir einen erfolgreichen Onlineshop und erweitern Ihr potenzielles Publikum durch effektives Online Marketing. Darüber hinaus bieten wir die gesamte Palette von Analysemöglichkeiten, um Ihren Traffic und Ihren Umsatz zu steigern.</p>
              <p className="section-text-200">Die Experten der Webagentur Stuttgart haben das Ziel klar vor Augen: Ihren Erfolg im Netz!
              </p>
              </Fragment>
            }
          />
        </section>

        <section styleName="stats">
          <div className="section-m container-narrow grid-hr">
            <h2 className="heading-3">Onlineshop erstellen: Nutzen Sie Ihre Chance</h2>
            <Hr color="var(--color-gigas)" />
            <div styleName="stats-content">
              <p className="section-text-200">Die Anzahl der Internetnutzer steigt stetig. Für die meisten von ihnen ist Onlineshopping mittlerweile zu einer Selbstverständlichkeit geworden:</p>
            </div>


            <div className="container-narrow" styleName="stats-infographic stats-infographic-shopping">
              <figure>
                <img src={statsInfographicShopping}  width="301" height="373" alt="Internetnutzung in Deutschland, % (Quelle: D21)" loading="lazy"/>
                <figcaption><em>Quellen:</em> D21, AGOF.de, McKinsey &amp; Company</figcaption>
              </figure>
              <figure>
                <img src={statsInfographicInteraction}  width="280" height="280" alt="Durchschnittlicher Anteil der Kundeninteraktionen, die digital sind." loading="lazy"/>
                <figcaption>Durchschnittlicher Anteil der Kundeninteraktionen, die digital sind.</figcaption>
              </figure>
            </div>
          </div>
          <ParallaxEffect
            image={statsParallaxInternet}
            alt="Fröhliche Gesellschaft junger Menschen"
            children={
              <div className="container-narrow section parallax-text">
                <p className="section-text-400">
                  <span className="heading-2">0,36 Millionen</span><br/>
                  Menschen waren in Deutschland 2020 im Internet
                </p>
                <p>
                  <b>87,4%</b> der deutschen Wohnbevölkerung ab 16 Jahren
                  <br/>
                  (860.000 User mehr als 2019)
                </p>
              </div>
            }
          />
            
          <div styleName="line-chart-wrapper">
            <div className="section container" styleName="line-chart-container">
              <LineChartEcommerce />
            </div>
          </div>


          <div className="section-m container-narrow">
            <div styleName="stats-content">
              <p className="section-text-200">In der modernen Welt haben die Menschen gelernt, digital zu interagieren. Auch Menschen älterer Generationen werden zu aktiven Internetnutzern. Vertreter der Generationen Y und Z können sich ihr Leben ohne das Internet nicht vorstellen. E-Commerce ist für sie einfach und natürlich: Sie legen Wert auf schnelle, bequeme und sichere Lösungen, umfassende Informationen zu Produkten und die Möglichkeit, Bewertungen zu lesen und Optionen zu vergleichen.</p>
            </div>
          </div>

          {/* <div className="section container-narrow" styleName="stats-infographic stats-infographic-shopping">
            <div className="section container-narrow" styleName="stats-infographic stats-infographic-shopping">
              <TinyBarChartEcommerce />
            </div>
            
            <div className="section container-narrow" styleName="stats-infographic stats-infographic-shopping">
              <TinyBarChartEcommerce />
            </div>
          </div> */}


          <ParallaxEffect
            image={statsParallaxEurostat}
            alt="Fröhliche Gesellschaft junger Menschen"
            children={
              <div className="container-narrow section parallax-text">
                <h3 className="section-text-400">Number and share <br/> of Millennials in Europe (2020)</h3>
                <figure styleName="stats-infographic">
                  <img src={statsInfographicEurostat}  width="954" height="300" alt="Internetnutzung in Deutschland, % (Quelle: D21)" loading="lazy"/>
                  <figcaption><em>Quellen:</em> Eurostat, McKinsey &amp; Company</figcaption>
                </figure>
              </div>
            }
          />
				</section>
        

        <section className="section-m container-narrow grid-hr" styleName="advantages">
					<h2 className="heading-3">Vorteile eines Webshops</h2>
          <p className="section-text-200" styleName="advantages-text">Die Vorteile von Onlineshops gegenüber einem klassischen Geschäft liegen auf der Hand. Selbst wer fest im stationären Handel verwurzelt ist, profitiert von einem eigenen Webshop; schließlich bietet dieser langfristig die Möglichkeit weitere potenzielle Kunden zu erreichen – auch außerhalb der Öffnungszeiten.</p>
          <Hr color="var(--color-blue-marguerite)"/>
          <div ref={advantagesRef}>
            <ListOrderUnorder
              items={[
                <Fragment>
                  <h3 className="heading-4">Barrierefreiheit &amp; Sicherheit</h3>
                  <p>Mit modernen mobile-first E-Commerce-Lösungen (wie z.B. Magento Webshop) können Sie Ihre Kunden jederzeit und überall erreichen.</p>
                  <p>Flexible und sichere Zahlungsmethoden ermöglichen erfolgreiche Geschäftsabschlüsse rund um die Uhr.</p>
                </Fragment>,

                <Fragment>
                  <h3 className="heading-4">Größeres Publikum</h3>
                  <p>Durch Beruf und erhöhten Leistungsdruck in der Gesellschaft, haben die Menschen zunehmend weniger Zeit zum Einkaufen. Das stationäre Shopping wird somit immer mehr zum Luxus. Mit unseren Online-Lösungen ermöglichen wir jedem stationären Geschäft eine überregionale Web-Präsenz</p>
                </Fragment>,

                <Fragment>
                  <h3 className="heading-4">Flexible Sortimentsgestaltung und Kostenreduktion</h3>
                  <p>Onlineshops benötigen häufig nur einen kleinen oder gar keinen Lagerraum, da sie die Ware direkt vom Hersteller an den Kunden ausliefern lassen (Drop-Shopping) oder die Produkte erst bei Bedarf bestellen.</p>
                </Fragment>,

                <Fragment>
                  <h3 className="heading-4">Effizientere Geschäftsprozesse</h3>
                  <p>E-Commerce optimiert die Effizienz der Geschäftsprozesse, zum Beispiel durch umfassende Produktbeschreibungen, die eine informationsgestützte Kaufentscheidung ermöglichen. So haben Mitarbeiter mehr Zeit für andere Aufgaben.</p>
                </Fragment>,
              ]}
              iconColor="var(--color-scarlet-gum)"
            />
          </div>
        </section>

        <section className="section-m">
          <h2 className="visuallyHidden">Was wir anbieten:</h2>
          <ParallaxEffect
            image={offersParallax}
            alt=""
            children={
              <div className="container-narrow section parallax-text">
                <b className="heading-2">Wir erfüllen Ihre E&nbsp;-&nbsp;Commerce-Anforderungen</b>
              </div>
            }
          />
          <SectionColor
            backgroundColor="var(--color-blue-marguerite)"
            content={
              <p>Als Internetagentur in Stuttgart bieten wir Ihnen den gesamten Online Marketing Mix von Display Advertising über Content Marketing bis hin zu E-Mail Marketing.</p>
            }
          />
          <div className="section-m container-narrow grid-hr" styleName="offers">
            <p styleName="offers-text">Wir können zahlreiche erfolgreiche Magento-Projekte vorweisen und unterstützen auch Sie gern beim Aufbau Ihrer Commerce-Plattform in allen Bereichen:</p>
            <Hr color="var(--color-blue-marguerite)"/>
            <div ref={offersRef}>
              <ul styleName="offers-list">
                <li>
                  <h3 className="heading-4">Design und <br/> Entwicklung von <br/> E-Commerce-Websites</h3>
                  <Icon
                    name="cursor-in-square"
                    figure="triangle-bottom"
                    color="var(--color-blue-marguerite)"
                  />
                </li>
                <li>
                  <h3 className="heading-4">SEO-Optimierung <br/> für Ihren Onlineshop</h3>
                  <Icon
                    name="search-text"
                    figure="circle"
                    color="var(--color-blue-marguerite)"
                  />
                </li>
                <li>
                  <h3 className="heading-4">E&nbsp;-&nbsp;Commerce <br/> Marketing</h3>
                  <Icon
                    name="target"
                    figure="circle"
                    color="var(--color-blue-marguerite)"
                  />
                </li>
                <li>
                  <h3 className="heading-4">E&nbsp;-&nbsp;Commerce <br/> PPC-Werbung</h3>
                  <Icon
                    name="badge-in-window"
                    figure="square"
                    color="var(--color-blue-marguerite)"
                  />
                </li>
                <li>
                  <h3 className="heading-4">Integration sicherer <br/> Zahlungsgateways</h3>
                  <Icon
                    name="security"
                    figure="square"
                    color="var(--color-blue-marguerite)"
                  />
                </li>
                <li>
                  <h3 className="heading-4">E&nbsp;-&nbsp;Commerce <br/> Analytics</h3>
                  <Icon
                    name="ring-sector"
                    figure="circle"
                    color="var(--color-blue-marguerite)"
                  />
                </li>
              </ul>
            </div>
          </div>
        </section>

        <section className="section-m">
          <h2 className="visuallyHidden">Über die Magento-Technologie.</h2>
          <Article 
            title="Magento Shop für Ihr Geschäftswachstum"
            buttonUrl="/projekte/52-watch-de.html"
            buttonCaption="Fall zeigen"
            img={<img 
              src={magentoArticle}
              alt=""
              width={1920} 
              height={800}
              loading="lazy"
            />}
            color="var(--color-flamingo)"
          />
          <div className="container-narrow" styleName="about-magento-content">
            <h3 className="section-text-400">Magento Shop für Ihr Geschäftswachstum</h3>
            <p>Unsere Arbeit mit verschiedenen Onlineshop- und Content Management Systemen hat immer wieder gezeigt: Magento ist eine der besten derzeit verfügbaren E-Commerce Lösungen und sowohl für den B2C- als auch B2B-Onlinehandel geeignet. Die Onlineshop-Software ist flexibel, skalierbar, wird von einer großen internationalen Gemeinschaft unterstützt und verfügt über eine Vielzahl an Modulen, die genau auf Ihre Geschäftsanforderungen zugeschnitten werden können.</p>
          </div>

          <section className="container-narrow" styleName="magentoServices" ref={magentoServicesRef}>
            <h3 className="section-text-400">Unsere Dienstleistungen im Bereich Magento Commerce:</h3>
            <Hr color="var(--color-flamingo)"/>
            <ListOrderUnorder
              items={[
                <p>Wir analysieren Ihre Geschäftsanforderungen und passen die Magento-Funktionen an, um die effizienteste Lösung zu konfigurieren und Ihren Umsatz zu steigern.</p>,
                <p>Wir erstellen ein responsives Magento-Thema, das auf mobilen Endgeräten ebenso wie Desktop-Computern perfekt funktioniert.</p>,
                <p>Wir bieten Magento-Support und -Wartung, um Ihre E-Commerce-Leistung zu optimieren und mögliche Probleme rechtzeitig zu lösen.</p>,
                <p>Magento 2 ist die neueste Version der Onlineshop-Software, die hinsichtlich Leistung und Sicherheit weiter optimiert wurde. Bei Bedarf übernehmen wir die Migration von Magento 1 zu Magento 2 mit minimaler Unterbrechungszeit</p>,
                <p>Magento unterstützt auch neueste Technologien wie Progressive Web Apps (PWA), eine der vielversprechendsten Neuerungen für E-Commerce-Anwendungen.</p>,
              ]}
            />
          </section>
        </section>

        <section styleName="services" ref={servicesRef}>
          <div className="container section">
            <h2 className="heading-3">Unsere E-Commerce-Dienstleistungen</h2>
            <ul>
              <li>
                <h3 className="heading-4">Design und Entwicklung von Onlineshops</h3>
                <p className="section-text-100">Wir analysieren Ihre Geschäftsziele, Werte und Anforderungen und entwickeln eine ansprechende Webshop-Oberfläche mit den höchsten UX-Standards. Unser Schwerpunkt liegt auf Benutzerfreundlichkeit, Effizienz und Sicherheit.</p>
                <Icon
                  name="cursor-in-square"
                  color="var(--color-white)"
                  figure="triangle-bottom"
                  figureColor="rgba(255, 255, 255, 0.13)"
                />
              </li>
              <li>
                <h3 className="heading-4">Online Marketing Maßnahmen</h3>
                <p className="section-text-100">Wir entwickeln eine umfassende Marketingstrategie, um die Sichtbarkeit Ihres Onlineshops zu verbessern, den Traffic zu erhöhen und die Conversions zu steigern.</p>
                <Icon
                  name="target"
                  color="var(--color-white)"
                  figure="circle"
                  figureColor="rgba(255, 255, 255, 0.13)"
                />
              </li>
              <li>
                <h3 className="heading-4">Integration sicherer Zahlungsmethoden</h3>
                <p className="section-text-100">Wir statten Ihren Onlineshop mit beliebten Zahlungsmethoden aus, um Ihren Kunden ein schnelles und sicheres Einkaufserlebnis zu bieten.</p>
                <Icon
                  name="security"
                  color="var(--color-white)"
                  secondaryColor="var(--color-flamingo)"
                  figure="square"
                  figureColor="rgba(255, 255, 255, 0.13)"
                />
              </li>
              <li>
              <h3 className="heading-4">E-Commerce PPC-Werbung</h3>
                <p className="section-text-100">Wir setzen datengesteuerte PPC-Werbung (Pay-per-Click) ein, also Onlinewerbung, die die Reichweite Ihres Unternehmens steigert und relevante Leads gewinnt.</p>
                <Icon
                  name="badge-in-window"
                  color="var(--color-white)"
                  figure="square"
                  figureColor="rgba(255, 255, 255, 0.13)"
                />
              </li>
              <li>
              <h3 className="heading-4">E-Commerce Analysen</h3>
                <p className="section-text-100">Durch leistungsstarke Webanalyse-Tools wie Google Analytics überwachen wir Ihre wichtigsten Leistungsindikatoren, erkennen aktuelle Trends und ermöglichen die nachhaltige Entwicklung Ihres E-Commerce-Geschäfts.</p>
                <Icon
                  name="ring-sector"
                  color="var(--color-white)"
                  figure="circle"
                  figureColor="rgba(255, 255, 255, 0.13)"
                />
              </li>
            </ul>
            <span styleName="services-round" ref={roundBackgroundRef}></span>
          </div>
        </section>

        <section styleName="contacts">
          <h2 className="visuallyHidden">Bleiben Sie mit uns in Kontakt</h2>
          <ParallaxEffect
            image={parallaxImageContact}
            alt="Kunden, die uns vertrauen"
            children={
              <div className="container parallax-text">
                <b className="heading-2">Webagentur Stuttgart: Starke Partner im E-Commerce</b>
                <p className="section-text-200">Unsere E-Commerce-Manager beraten Sie gerne und finden die optimale Lösung für Ihr Unternehmen.</p>
              </div>
            }
          />
          
        </section>
      </main>
      <FeedbackHomePage />
      <Footer />
    </div>
  );
}
