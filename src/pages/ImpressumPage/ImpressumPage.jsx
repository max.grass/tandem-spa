import React from "react";
import { Link } from 'react-router-dom';

import { Header } from "components/Header";
import { HeroPage } from "components/Hero/Content"
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";
import {Hero} from "components/Hero";
import heroImg from "assets/images/hero-contact.jpg";

export const ImpressumPage = () => {
  return (
    <div className="page-wrapper">
      <CommonMetaTags 
				title={'Impressum'}
				description={'Tandem Marketing'}
				url={'https://www.tandem-m.de/impressum.html'}
				image={heroImg}
			/>

      <Header />

      <main>
        <Hero
          src={heroImg}
          alt="Leute sitzen mit Laptops im Büro"
          width={1920}
          height={300}
          content={<HeroPage title="Impressum" h1={true} />}
        />
        
        <div className="container-narrow">
          <div className="section">
            <h1>Impressum</h1>
            <p>
              <strong>Rechtliche Angaben:</strong>

              <br/> Umsatzsteuer-Identifikationsnummer
              <br/> gemäß § 27 a Umsatzsteuergesetz:
              <br/> DE267321709
              <br/>
              <br/> Inhaltlich Verantwortlicher gemäß TMG:
              <br/> Tandem Marketing & Partners
              <br/> info@tandem-m.de
            </p>
            <p>
              <strong>Weitere rechtliche Angaben:</strong>
              <br/>
              <Link to="/datenschutz.html">Datenschutzerklärung</Link>
            </p>
            <p>
              <strong>Alle hier verwendeten Namen, Begriffe, Zeichen und Grafiken</strong>
              <br/>können Marken- oder Warenzeichen im Besitze ihrer rechtlichen Eigentümer sein. Die Rechte aller erwähnten und benutzten Marken- und Warenzeichen liegen ausschließlich bei deren Besitzern.
            </p>
          </div>
        </div>
      </main>
      
      <Footer content="bottom"/>
    </div>
  );
}
