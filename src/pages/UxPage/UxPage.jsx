import React, { Fragment } from "react";
import { Link } from 'react-router-dom';

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { ParallaxEffect } from "components/ParallaxEffect";
import { CheckedList } from "components/CheckedList";
import { IconList } from "components/IconList";
import { ColorSteps } from "components/ColorSteps";
import { AboutPageStrengths } from "components/AboutPage-OurStrengths";
import { data as UXPageStrengthsData } from "components/AboutPage-OurStrengths/UXPageStrengthsData";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from "assets/images/hero-ux.jpg";
import parallaxPeople from "assets/images/parallax-ux.jpg";
import parallaxWomen from "assets/images/parallax-ux-women.jpg";
import parallaxHershortt from "assets/images/pic-ux-05-b.jpg";
import parallaxWomen2 from "assets/images/parallax-ux-women-2.jpg";
import parallaxWomen3 from "assets/images/parallax-ux-women-3.jpg";

import icon1 from "assets/images/ux-list-1.svg";
import icon2 from "assets/images/ux-list-2.svg";
import icon3 from "assets/images/ux-list-3.svg";
import icon4 from "assets/images/ux-list-4.svg";
import icon5 from "assets/images/ux-list-5.svg";
import icon6 from "assets/images/ux-list-6.svg";
import icon7 from "assets/images/ux-list-7.svg";
import icon8 from "assets/images/ux-list-8.svg";

import "./UxPage.css";

export const UxPage = () => {

	return (
		<div className='page-wrapper'>
			<CommonMetaTags 
				title={'Herausragende UX dank Empathie und Daten'}
				description={'Bei User Experience (UX oder Benutzererfahrung) geht es darum, die Kundenzufriedenheit und -loyalität durch den Nutzen.'}
				url={'https://www.tandem-m.de/ux.html'}
				image={heroImg}
			/>

			<Header />
			
			<main>
				<Hero
					src={heroImg}
					alt="Ux"
					width={1920}
					height={800}
					content={
						<HeroContent 
							title={<span>User<br/>Experience</span>}
							text="UX"
						/>
					}
				/>

				<section>
					<h2 className="visuallyHidden">User Experience</h2>
					<SectionWhite 
						title="Herausragende UX dank Empathie und Daten"
						text="Bei User Experience (UX oder Benutzererfahrung) geht es darum, die Kundenzufriedenheit und -loyalität durch den Nutzen, die Benutzerfreundlichkeit und Freude an der Interaktion mit einem Produkt zu verbessern. User Experience umfasst die Gefühle und das Verhalten von Benutzer*innen während der gesamten Customer Journey."
						textColor="var(--color-cherry-dark)"
					/>
				</section>

				<section className="section">
					<IconList
						listData={[
							{
								"label" : "Nützlich"
							},
							{
								"label" : "Erwünscht"
							},
							{
								"label" : "Verwendbar"
							},
							{
								"label" : "Wertvoll"
							},
							{
								"label" : "Barrierefrei"
							},
							{
								"label" : "Auffindbar"
							},
							{
								"label" : "Glaubwürdig"
							}
						]}
					/>
				</section>

				<section>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Wer mit einer Website, einem Onlineshop oder einem anderen digitalen Produkt eine positive Erfahrung ermöglichen möchte, kann sich an Peter Morvilles Wabenschema orientieren, aus dem sich folgende Fragen ableiten:</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-cherry-dark)"
					/>
				</section>

				<section styleName="strengths">
					<h2 className="visuallyHidden">UX</h2>
					<ParallaxEffect
						image={parallaxWomen}
						alt="Tandem Marketing & Partners"
					/>
				</section>

				<section styleName="no-bullits-list">
					<AboutPageStrengths
						data={UXPageStrengthsData}
						hrColor={"var(--color-cherry-dark)"}
					/>
				</section>

				<section>
					<ColorSteps
						hiddenTitle="Webdesign aus Stuttgart"
						parallax={
							<ParallaxEffect
								image={parallaxPeople}
								alt="Tandem Marketing & Partners"
								children={
									<div className="section container">
										<b className="heading-2 parallax-text" >
											Benutzerzentriertes Design: Erfolgreiche Web-Projekte durch Empathie
										</b>
									</div>
								}
							/>
						}
						textTop={"Wer sich mit dem Begriff ‘User Experience’ befasst, wird auch schnell mit dem Begriff ‘benutzerzentriertes Design’ in Berührung kommen. Benutzerzentriertes Design heißt, dass bestehende und potenzielle Kund*innen im Zentrum der Design-, Entwicklungs- und Marketingaktivitäten stehen."}
						textCenter={"Alle Schnittstellen, Dienste und Kampagnen werden unter Berücksichtigung der Zielgruppe geplant."}
						textBottom={""}
						bgColor={"var(--color-cherry-dark)"}
					/>
				</section>

				<section className="section">
					<div className="container-narrow">
						<h2 className="heading-3">Unternehmen, die</h2>
					</div>
					<div className="container">
						<CheckedList 
							content={
								<Fragment>
									<li>Bedürfnisse</li>
									<li>Wünsche</li>
									<li>Gewohnheiten</li>
									<li>Erwartungen und</li>
									<li>Denkprozesse</li>
								</Fragment>
							}
							color="var(--color-cherry-dark)"
						/>
					</div>
				</section>

				<section styleName="afterCheckedList">
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">ihrer Endbenutzer antizipieren und beachten, werden im Wettbewerb langfristig am erfolgreichsten sein. Da Website, Onlineshop oder -portal häufig der Erstkontakt zu potenziellen Kund*innen sind, ist sind User Experience und Usability an dieser Stelle besonders wichtig.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
					<div className="empty-parallax">
						<ParallaxEffect
							image={parallaxHershortt}
							alt="Tandem Marketing & Partners"
						/>
					</div>
				</section>


				<section className="section" styleName="UxIconListIcons">
					<IconList
						listData={[
							{
								"iconSrc": icon1,
								"label" : "Informations-architektur"
							},
							{
								"iconSrc": icon2,
								"label" : "Visuelles Design"
							},
							{
								"iconSrc": icon3,
								"label" : "Funktionalität"
							},
							{
								"iconSrc": icon4,
								"label" : "Benutzerfreund-lichkeit"
							},
							{
								"iconSrc": icon5,
								"label" : "Typographie"
							},
							{
								"iconSrc": icon6,
								"label" : "Benutzer-schnittstelle"
							},
							{
								"iconSrc": icon7,
								"label" : "Content-Strategie"
							},
							{
								"iconSrc": icon8,
								"label" : "Interaktionsdesign"
							}
						]}
					/>
				</section>


				<section>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Eine Onlinepräsenz sollte unter Berücksichtigung des Kundenkomforts gestaltet, entwickelt und optimiert werden. Das bedeutet zum einen, Grundlagen der Benutzerpsychologie zu beachten. Zum anderen geht es um die spezifischen Besonderheiten und Bedürfnisse der Zielgruppe. </p>
								<p className="section-text-200">Als Webagentur aus Stuttgart mit mehr als zehn Jahren Erfahrung sorgen wir dafür, dass die Customer Journey Ihrer Kund*innen effizient und zufriedenstellend verläuft – und Joy of Use (deutsch: Freunde an der Nutzung) entsteht. Ihr Vorteil? Conversions, Vertrauen und Loyalität Ihrer Kund*innen.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
				</section>

				<section styleName="strengths">
					<ParallaxEffect
						image={parallaxWomen2}
						alt="UX"
						children={
							<div className="section container">
								<b className="heading-2">
									...aber nicht nur Empathie: Data-driven Design
								</b>
							</div>
						}
					/>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Eine wichtige Voraussetzung für empathische UX-Strategien und erfolgversprechendes User Experience Design sind Daten, Stichwort: Data-driven Design. Data-driven Design bedeutet, dass Informationen über Verhalten, Einstellung und Bedürfnisse der Nutzer*innen in Designentscheidungen einfließen. Ebenso sollte erfasst werden, wie Kund*innen mit dem Design interagieren – und ob das User Interface Design seinen Zweck erfüllt.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-cherry-dark)"
					/>
				</section>

				<section className="section">
					<div className="container-narrow">
						<h2 className="heading-3">Fragen in diesem Zusammenhang sind zum Beispiel:</h2>
					</div>
					<div className="container">
						<CheckedList 
							content={
								<Fragment>
									<li>Erhält eine Call-To-Action-Schaltfläche ausreichend Klicks? Ist sie sichtbar genug?</li>
									<li>Verstehen Onlineshop-Nutzer*innen den Kaufprozess?</li>
									<li>Verlassen Benutzer*innen die Website an einem bestimmten Punkt der Customer Journey? Warum?</li>
									<li>Gibt es beim Zugriff über mobile Endgeräte mehr Ausstiege? Warum?</li>
								</Fragment>
							}
							color="var(--color-cherry-dark)"
						/>
					</div>
				</section>

				<section>
					<SectionColor 
						content={
							<Fragment>
								<p className="heading-3">Was verstehen wir unter ‘Daten’?</p>
								<p className="section-text-200">Daten sind nicht nur Zahlen. Es können quantitative Angaben sein, zum Beispiel Antworten auf die Fragen: “Wie viele?” oder “Wie oft?”, aber auch qualitative Angaben, zum Beispiel Antworten auf die Frage “Warum?”, die auf die Motivation oder Absicht der Benutzer*innen abzielen.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-cherry-dark)"
					/>
				</section>

				<section>
					<SectionColor 
						content={
							<div styleName="table-data">
								<div>
									<p className="heading-4">QUANTITATIVE DATEN</p>
									<ul className="section-text-200 simple-list">
										<li>A/B- oder multivariate Tests</li>
										<li>Website-Analyse</li>
										<li>Heatmaps aus Eye-Tracking-Studien</li>
										<li>Klick-Karten</li>
										<li>Umfragen mit großen Stichproben</li>
									</ul>
								</div>
								<div>
									<p className="heading-4">QUALITATIVE DATEN</p>
									<ul className="section-text-200 simple-list">
										<li>Interviews</li>
										<li>Konkurrenzanalyse</li>
										<li>Usability-Studien</li>
										<li>Fokusgruppen</li>
										<li>Tagebuchstudien</li>
									</ul>
								</div>
							</div>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
				</section>

				<section>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Daten helfen, das Verhalten des Nutzers bei der Interaktion besser zu verstehen und fundierte Entscheidungen über UX Design und Benutzerfreundlichkeit zu treffen. Die Verwendung objektiver Daten dient als Ergänzung zur Intuition und Vision der Designerin: Sie gibt dem Optimierungsprozess einen wertvollen Impuls, der zu mehr Effizienz und Conversions führt.</p>
								<p className="section-text-200">Unsere Datenanalysten arbeiten eng mit Designerinnen zusammen, um herauszufinden, was für den Endbenutzer am besten funktioniert: So erzielen wir eine optimale User Experience.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-cherry-dark)"
					/>
				</section>

				<section styleName="strengths">
					<h2 className="visuallyHidden">UX</h2>
					<ParallaxEffect
						image={parallaxWomen3}
						alt="Tandem Marketing & Partners"
					/>
				</section>
				
				<section styleName="UxContacts">
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-700">Tandem Marketing & Partners ist eine Webagentur aus Stuttgart mit mehr als 10 Jahren Erfahrung in der Entwicklung benutzerzentrierter Internetlösungen. Kontaktieren Sie uns gerne für mehr Informationen oder ein individuelles <Link to="/werbeagentur-stuttgart/contact.html">Angebot</Link>.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-cherry-dark)"
					/>
				</section>
				

				<FeedbackHomePage />

			</main>
			
			<Footer />
		</div>
	);
}
