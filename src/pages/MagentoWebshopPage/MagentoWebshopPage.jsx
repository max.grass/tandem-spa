import React, { Fragment, useEffect, useRef } from "react";
import gsap from "gsap";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { Article } from "components/Article";
import { ListOrderUnorder } from "components/ListOrderUnorder";
import { Icon } from "components/Icon";
import { Hr } from "components/Hr";
import { ParallaxEffect } from "components/ParallaxEffect";
import { Blog } from "components/Blog";
import { IconList } from "components/IconList";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { ListItemsTween, ListItemsTween2, RoundBackgroundTween } from "components/Animation";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from "assets/images/magento-orange-girl.jpg";
import imgArticle from "assets/images/magento-webshop.jpg";
import advantagesParallaxImg from "assets/images/magento-webshop-parallax.jpg";
import usageParallaxImg from "assets/images/magento-usage-stats.png";
import usageStatsImg from "assets/images/magento-usage-stats.svg";
import intercalationImg from "assets/images/magento-webshop-intercalation.jpg";

import "./MagentoWebshopPage.css";

import icon1 from "assets/images/icons/magento-icon-1.svg";
import icon2 from "assets/images/icons/magento-icon-2.svg";
import icon3 from "assets/images/icons/magento-icon-3.svg";
import icon4 from "assets/images/icons/magento-icon-4.svg";
import icon5 from "assets/images/icons/magento-icon-5.svg";
import icon6 from "assets/images/icons/magento-icon-6.svg";

export const MagentoWebshopPage = () => {
  const advantagesRef = useRef();
  const factsRef = useRef();
  const roundBackgroundRef = useRef();
  const servicesRef = useRef();
  const factsSelector = gsap.utils.selector(factsRef);

  useEffect(() => {
    let tl = gsap.timeline();
    
    tl
    .set(factsRef.current, {
      "--bgColor": "var(--color-white)",
    })
    .set(factsSelector("figure"), {
      "--secondary-color": "var(--color-white)",
    })
    .add(
      ListItemsTween(
        advantagesRef.current
      )
    )
    .add(RoundBackgroundTween(
      roundBackgroundRef.current,
      factsRef.current,
      "var(--color-flamingo)"
      )
    ).add(
      ListItemsTween2(
        servicesRef.current
      )
    );
  }, []);

  return (
    <div className="page-wrapper">
      <CommonMetaTags 
				title={'Magento Webshop Agentur aus Stuttgart'}
				description={'Magento Webshop Agentur aus  dem Großraum Stuttgart . Magento Design, Programmierung, Erweiterungen ERP + Support. Alles aus einer Hand!'}
				url={'https://www.tandem-m.de/magento-webshop.html'}
				image={heroImg}
			/>

      <Header />

      <main>
        <Hero
          src={heroImg}
          heght={500}
          content={
            <HeroContent 
              title={<span>Webshop-Lösung für Ihr<br /> Online-Business</span>}
              text="Magento Webshop"
            />
          }
        />

        <SectionWhite 
          title="Magento Shop"
          text="Ein Großteil aller Verkaufsaktivitäten wird heutzutage über das Internet abgewickelt – Tendenz stark steigend. Wenn Ihr Unternehmen auch Produkte und / oder Dienstleistungen anbietet, so empfiehlt sich ein leistungsstarker und nachhaltiger Webshop als Teil der Unternehmensstrategie. Mit Magento E-Commerce erhalten Sie eine effektive Webshop-Lösung für Ihr Online-Business."
          textColor="var(--color-flamingo)"
        />

        <SectionColor 
          content={
            <p className="section-text-200" style={{ marginBottom: 0 }}>Magento Webshop steht für Flexibilität und Leistungsfähigkeit im Opensource-Segment. Eine offene Struktur, modernste Technologie und eine vielfältige Erweiterungsfähigkeit garantieren die optimale Basis für Ihre Aktivitäten im E-Commerce.</p>
          }
          backgroundColor="var(--color-flamingo)"
        />

        <section className="section-m" styleName="innovation">
          <h2 className="visuallyHidden">Magento-Vorteile</h2>
          <Article 
            title="Magento Webshop: Innovative E-Commerce Lösung"
            buttonUrl="/referenzen.html"
            buttonCaption="Fälle zeigen"
            img={<img 
              src={imgArticle}
              alt="Fälle zeigen"
              width={1920} 
              height={800}
              loading="lazy"
            />}
            color="var(--color-flamingo)"
          />
          <div className="container-narrow" styleName="innovation-content">
            <h3 className="section-text-400">Magento Webshop: Innovative E-Commerce Lösung</h3>
            <p>Der Magento Webshop ist eine innovative E-Commerce Software, die neue Standards setzt. Magento-Commerce überzeugt durch langjährige Erfahrung im Einsatz und eine Fülle modernster Technologien. Sie haben immer die volle Kontrolle über Aussehen, Inhalt und Funktionalität Ihres Onlineshops.</p>
          </div>
        </section>

        <section styleName="facts" ref={factsRef}>
          <div className="container section">
            <h2 className="heading-3">Magento Zahlen &amp; Fakten</h2>
            <ul>
              <li>
                <p className="section-text-100">
                  Magento is powering 
                  <strong className="heading-4">Over 25% </strong>
                  of online stores
                </p>
                <Icon
                  name="cart-in-window"
                  color="var(--color-white)"
                  figure="square"
                  figureColor="rgba(255, 255, 255, 0.33)"
                />
              </li>
              <li>
                <p className="section-text-100">
                  Magento has a market share 
                  <strong className="heading-4"><span className="visuallyHidden">of </span>19,64% </strong>
                  in the top 1M websites
                </p>
                <Icon
                  name="sector"
                  color="var(--color-white)"
                  figure="circle"
                  figureColor="rgba(255, 255, 255, 0.33)"
                />
              </li>
              <li>
                <p className="section-text-100">
                  Magento handled 
                  <strong className="heading-4">$155B </strong>
                  worth of online transactions in 2018
                </p>
                <Icon
                  name="credit-card"
                  color="var(--color-white)"
                  secondaryColor="var(--color-flamingo)"
                  figure="triangle-top"
                  figureColor="rgba(255, 255, 255, 0.33)"
                />
              </li>
              <li>
                <p className="section-text-100">
                  Magento is downloaded nearly 5K 
                  <strong className="heading-4">5000 </strong>
                  times every day
                </p>
                <Icon
                  name="cloud"
                  color="var(--color-white)"
                  figure="square"
                  figureColor="rgba(255, 255, 255, 0.33)"
                />
              </li>
              <li>
                <p className="section-text-100">
                  <strong className="heading-4">Over 250.000 </strong>
                  Merchants worldwide used Magento to power their online business in 2018.
                </p>
                <Icon
                  name="magento-in-window"
                  color="var(--color-white)"
                  figure="square"
                  figureColor="rgba(255, 255, 255, 0.33)"
                />
              </li>
            </ul>
            <span styleName="facts-round" ref={roundBackgroundRef}></span>
          </div>
        </section>

        {/* <section className="section-m container-narrow grid-hr" styleName="countries">
          <h2 className="heading-3">Country numbers of websites</h2>
          <Hr color="var(--color-flamingo)"/>
          <ul>
            <li>
              <h3 className="section-text-100">US</h3>
              <p className="heading-4">23.187</p>
              <Icon
                name="flag-us"
                width="90px"
                height="90px"
                figure="circle"
              />
            </li>
            <li>
              <h3 className="section-text-100">Netherlands</h3>
              <p className="heading-4">7.767</p>
              <Icon
                name="flag-netherlands"
                width="90px"
                height="90px"
                figure="circle"
              />
            </li>
            <li>
              <h3 className="section-text-100">Germany</h3>
              <p className="heading-4">11.035</p>
              <Icon
                name="flag-germany"
                width="90px"
                height="90px"
                figure="circle"
              />
            </li>
            <li>
              <h3 className="section-text-100">Brazil</h3>
              <p className="heading-4">7.523</p>
              <Icon
                name="flag-brazil"
                width="90px"
                height="90px"
                figure="circle"
              />
            </li>
            <li>
              <h3 className="section-text-100">UK</h3>
              <p className="heading-4">10.645</p>
              <Icon
                name="flag-uk"
                width="90px"
                height="90px"
                figure="circle"
              />
            </li>
          </ul>
        </section> */}

        <section styleName="usage">
          <ParallaxEffect
            image={usageParallaxImg}
            alt="Handynutzung"
            children={
              <div className="container parallax-text" styleName="usage-content">
                <h2 className="section-text-700">Magento 2 <br />web usage statistic</h2>
                <img 
                  src={usageStatsImg}
                  width={1010}
                  height={301}
                  alt="Website categories usage (popularity). Shopping: 25.38%; Business and Industry: 18.31%; Clothing: 5.28%; Arts and Entertainment: 3.53%; Others: 47.50%."
                  loading="lazy"
                />
            </div>
            }
          />
        </section>

        <section styleName="extensions">
          <div className="section container-narrow">
            <h2 className="visuallyHidden">Erweiterungen für CMS Magento</h2>
            <p className="section-text-400">Für Magento sind über 5.000 Erweiterungen verfügbar, von denen viele kostenlos sind.</p>
            <p>Die Preise für kostenpflichtige Module beginnen bei <strong>50$</strong>.</p>
          </div>
        </section>

        <section className="section-m" styleName="services" ref={servicesRef}>
          <h2 className="visuallyHidden">Als Magento-Agentur aus dem Großraum Stuttgart bieten wir Ihnen folgende Leistungen an:</h2>
          <IconList
            listData={[
              {
                "iconSrc" : icon1,
                "label" : "Beratung"
              },
              {
                "iconSrc" : icon2,
                "label" : "Projektierung"
              },
              {
                "iconSrc" : icon3,
                "label" : "Design"
              },
              {
                "iconSrc" : icon4,
                "label" : "Programmierung"
              },
              {
                "iconSrc" : icon5,
                "label" : "Usability"
              },
              {
                "iconSrc" : icon6,
                "label" : "Support"
              }
            ]}
          />
          {/* <div styleName="services-slider">
            <ul>
              <li>
                <p>Beratung</p>
                <Icon
                  name="message"
                  color="var(--color-flamingo)"
                  figure="circle"
                />
              </li>
              <li>
                <p>Projektierung</p>
                <Icon
                  name="lamp"
                  color="var(--color-flamingo)"
                  figure="square"
                />
              </li>
              <li>
                <p>Design</p>
                <Icon
                  name="cursor-in-square"
                  color="var(--color-flamingo)"
                  figure="triangle-bottom"
                />
              </li>
              <li>
                <p>Programmierung</p>
                <Icon
                  name="tag-in-window"
                  color="var(--color-flamingo)"
                  figure="square"
                />
              </li>
              <li>
                <p>Usability</p>
                <Icon
                  name="mobile-and-desktop"
                  color="var(--color-flamingo)"
                  figure="triangle-top"
                />
              </li>
              <li>
                <p>Support</p>
                <Icon
                  name="gear"
                  color="var(--color-flamingo)"
                  figure="circle"
                />
              </li>
            </ul>
          </div> */}
        </section>

        <section className="section-m container-narrow" styleName="tools">
          <h2 className="visuallyHidden">Tools und Services</h2>
          <p className="heading-3" styleName="tools-title">In der Zwischenzeit bietet Magento zahlreiche Tools und Services an,</p>
          <Hr color="var(--color-flamingo)"/>
          <p className="section-text-400" styleName="tools-text">mit denen B2B- und B2C-Verkäufer die richtigen Lösungen für sich auswählen können:</p>
          <ul>
            <li>Magento Shop Community Edition</li>
            <li>Magento Shop Enterprise Edition</li>
            <li>Magento Shop Enterprise Cloud Edition </li>
            <li>Magento Commerce Order Management (MCOM) </li>
            <li>Magento Business Intelligence </li>
            <li>Magento Services</li>
          </ul>
        </section>

        <section styleName="intercalation">
          {/* <ParallaxEffect
            image={intercalationImg}
            alt=""
            content={null}
          />
          <Button 
            caption={'buttonCaption'}
            url={'/'}
            color={'#fff000'}
          /> */}
          <Article 
            title=""
            buttonUrl="/referenzen.html"
            buttonCaption="Fälle zeigen"
            img={<img 
              src={intercalationImg}
              alt="Fälle zeigen"
              width={1920} 
              height={800}
              loading="lazy"
            />}
            color="var(--color-flamingo)"
          />
        </section>

        <section className="section-m container-narrow grid-hr" styleName="features">
          <h2 className="heading-3">Magento für Online-Händler</h2>
          <Hr color="var(--color-flamingo)"/>
          <div>
            <p>Magento ist aufgrund seiner zahlreichen Funktionen und einer Vielzahl von Modulen ein effizientes Tool für den Online-Handel.</p>
            <ul styleName="features-list">
              <li>
                <h3 className="heading-4">Exzellenter Kundenservice</h3>
                <ul>
                  <li>Separater Kundenkonto-Bereich für registrierte Kunden: Übersicht über alle Aktionen</li>
                  <li>Individuelle Gutscheine oder allgemeine Gutscheincodes möglich</li>
                  <li>unterschiedliche Kundenkonten, Gruppen, Berechtigungen</li>
                </ul>
                <Icon
                  name="globe"
                  width="90px"
                  height="90px"
                  figure="square"
                  color="var(--color-flamingo)"
                />
              </li>
              <li>
                <h3 className="heading-4">Umfangreiches Bestellmanagement</h3>
                <ul>
                  <li>Warenkorbüberwachung</li>
                  <li>Integrierte Wunschliste, die direkt in den Einkaufswagen übertragen oder mit Freunden geteilt werden kann</li>
                  <li>Meinungen / Produktbewertungen durch Kunden</li>
                </ul>
                <Icon
                  name="items-check"
                  width="90px"
                  height="90px"
                  figure="triangle-top-right"
                  color="var(--color-flamingo)"
                />
              </li>
              <li>
                <p className="heading-4" styleName="features-title">Unterschiedliche Zahlungsmöglichkeiten<br/>
                  <span styleName="features-span">sowie automatisierte Magento Versandmodule</span>
                </p>
                <Icon
                  name="cart-in-window"
                  width="90px"
                  height="90px"
                  figure="square"
                  color="var(--color-flamingo)"
                />
              </li>
              <li>
                <h3 className="heading-4">Mehrsprachigkeit und Magento Store Views</h3>
                <ul>
                  <li>einfach zu verwalten</li>
                </ul>
                <Icon
                  name="magento-in-window"
                  width="90px"
                  height="90px"
                  figure="circle"
                  color="var(--color-flamingo)"
                />
              </li>
              <li>
                <h3 className="heading-4">Produktkatalogmanagement</h3>
                <ul>
                  <li>mit übersichtlicher Navigation, Kategorien, Produkten</li>
                  <li>Attributverwaltung</li>
                  <li>Filter nach Preis, Relevanz, Marke, Farbe oder anderen Merkmalen</li>
                </ul>
                <Icon
                  name="credit-card"
                  width="90px"
                  height="90px"
                  figure="triangle-top"
                  color="var(--color-flamingo)"
                />
              </li>
              <li>
                <p className="heading-4">Magento Webshop Sitemanagement<br/>
                  <span styleName="features-span">mit eigenständigem Content Management System</span>
                </p>
                <Icon
                  name="search-arc"
                  width="90px"
                  height="90px"
                  figure="triangle-bottom"
                  color="var(--color-flamingo)"
                />
              </li>
              <li>
                <h3 className="heading-4">Professionelle Marketing- und Analysewerkzeuge, z.B.:</h3>
                <ul>
                  <li>upsell</li>
                  <li>cross-sell</li>
                  <li>Aktionen</li>
                  <li>Bestellanalyse</li>
                </ul>
                <Icon
                  name="hieroglyph"
                  width="90px"
                  height="90px"
                  figure="circle"
                  color="var(--color-flamingo)"
                />
              </li>
              <li>
                <h3 className="heading-4">Intelligente Suchfunktionen</h3>
                <ul>
                  <li>Kunden finden das notwendige Produkt schneller und präziser</li>
                  <li>Verbesserte Benutzerfreundlichkeit und erhöhte Conversion Rate</li>
                </ul>
                <Icon
                  name="ring-sector"
                  width="90px"
                  height="90px"
                  figure="circle"
                  color="var(--color-flamingo)"
                />
              </li>
              <li>
                <h3 className="heading-4">Magento Suchmaschinenoptimierung onpage (SEO)</h3>
                <ul>
                  <li>suchmaschinenfreundliche Internet-Adressen (URLs) für alle Produkte und Kategorien</li>
                  <li>die wichtigsten Metatags einer Seite können individuell vergeben werden (Seitentitel, Description und Keywords)</li>
                  <li>suchmaschinenfreundlicher Seitenaufbau</li>
                </ul>
                <Icon
                  name="search-text"
                  width="90px"
                  height="90px"
                  figure="circle"
                  color="var(--color-flamingo)"
                />
              </li>
            </ul>
          </div>
        </section>

        <section className="section-m" ref={advantagesRef}>
          <ParallaxEffect
            image={advantagesParallaxImg}
            alt=""
            children={
              <div className="container-narrow parallax-text">
                <h2>Magento Webshop verfügt über diverse leistungsstarke Funktionen und Fähigkeiten:</h2>
            </div>
            }
          />
          <div className="container-narrow grid-hr" styleName="advantages">
            <Hr color="var(--color-flamingo)" />
            <ListOrderUnorder
              isOrdered={true}
              icon={
                <Icon
                  name="checked-empty"
                  width="50px"
                  height="50px"
                  color="var(--color-dark)"
                />
              }
              items={[
                <Fragment>
                  <h3 className="heading-4">Zuverlässig, sicher, updatefähig</h3>
                  <p>Mit einer weltweiten Community von über 150 000 Magento-Entwicklern und 300 Lösungspartnern ist Magento eines der vielversprechendsten und zuverlässigsten Commerce Systems auf dem Markt. Es wird ständig verbessert, erweitert und regelmäßig auf die höchsten Sicherheitsstandards aktualisiert.</p>
                </Fragment>,
                <Fragment>
                  <h3 className="heading-4">Commerce Experiences</h3>
                  <p>Magento setzt verstärkt auf Commerce Experience, um die Conversions zu maximieren. Es gibt zahlreiche individuelle Lösungen für Suche, Checkout-Prozesse, Kommunikation, Zahlungs-Gateways und Versand. Elastic Search oder Integration in gängige Online-Zahlungssysteme (Apple Pay, Google Pay oder PayPal) ermöglichen es die Magento Shops barrierefrei an die jeweiligen Gewohnheiten der Zielgruppe anzupassen.</p>
                </Fragment>,
                <Fragment>
                  <h3 className="heading-4">Mobile First (PWA), React und die Zukunft</h3>
                  <p>Magento 2.4 ist für den mobile-first-Einsatz optimiert, zum Beispiel mit dem PWA Studio. Der Anteil des mobilen E-Commerce hat im Jahr 2021 72,9 Prozent erreicht: Nutzen Sie diesen Wettbewerbsvorteil, wenn Sie einen neuen Onlineshop erstellen lassen.</p>
                  <p>Die fortschrittliche Architektur gewährleistet eine hohe Ladegeschwindigkeit, Offline Caches sowie native Funktionalitäten, die man sonst von Apps kennt: Ein Schlüsselfaktor für die User Experience.</p>
                </Fragment>,
                <Fragment>
                  <h3 className="heading-4">Content-Marketing und Analyse leicht gemacht</h3>
                  <p>Mit Magento profitieren Ladenbesitzer und Marketing-Manager von effizienten Berichten und benutzerfreundlichen Content-Management-Funktionen (wie Page Builder in Magento Enterprise). Die Software hilft bei der Analyse und Visualisierung von Informationen zu Bestellungen, Kundendaten usw. Zahlreiche Magento-Module können auf spezifische Geschäftsanforderungen zugeschnitten werden, um maximale Effizienz zu gewährleisten.</p>
                </Fragment>,
                <Fragment>
                  <h3 className="heading-4">Integrationsfähigkeit in ERP - PIM - CRM </h3>
                  <p>Magento verfügt über alle erforderlichen Schnittstellen, die eine vollständige Verbindung verschiedener Geschäftssysteme ermöglichen, einschließlich eines PIM-, ERP- oder CRM-Systems. Dies ermöglicht den Austausch von Preisen, Bestelldaten, Kundendaten und Produktinformationen über verschiedene Programme, Tools und Abteilungen in der E-Commerce-Infrastruktur des Unternehmens.</p>
                </Fragment>,
              ]}
            />
          </div>
				</section>

      </main>
      <Blog />
      <FeedbackHomePage />
      <Footer />
    </div>

  );
}
