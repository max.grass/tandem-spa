import React from "react";
import { Header } from "components/Header";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { WPBlogArticle } from "components/WPBlog/WPBlogArticle"


export const BlogArticlePage = (props) => {
  const slug = props.match.params.slug;

  return(
    <div className='page-wrapper'>
      <Header />

      <main>

        <WPBlogArticle slug={slug} />
        <FeedbackHomePage />
      </main>

      <Footer />
    </div>
  );
}

