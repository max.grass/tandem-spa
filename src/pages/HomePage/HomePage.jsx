import React, { Fragment, useRef, useEffect } from 'react';
import gsap from 'gsap';

import { Link } from 'react-router-dom';

import { Header } from 'components/Header';
import { Hero } from 'components/Hero';
import { HeroHome } from "components/Hero/Content";
import { SectionWhite } from 'components/SectionWhite';
import { SectionColor } from 'components/SectionColor';
import { Tech } from 'components/Tech';
import { ParallaxEffect } from "components/ParallaxEffect";
import { AccordionTabs } from 'components/AccordionTabs';
import { Icon } from "components/Icon";
import { Projects } from 'components/Projects';
import { SourceAboutUs } from 'components/SourceAboutUs';
import { Blog } from 'components/Blog';
import { FeedbackHomePage } from 'components/FeedbackHomePage';
import { Footer } from 'components/Footer';
import { RoundBackgroundTween } from "components/Animation"
import { CommonMetaTags } from "components/CommonMetaTags";

import {data} from "components/Projects/homepageProjectsData";

import heroImg from "assets/images/hero-home.jpg";
import parallaxServicesImg from "assets/images/home-page-services-parallax.jpg";

import './HomePage.css';

export const HomePage = () => {
  const aboutWorkRef = useRef();
  const roundBackgroundRef = useRef();
  const aboutWorkSelector = gsap.utils.selector(aboutWorkRef);

  useEffect(() => {
    let tl = gsap.timeline();

    tl
    .set(aboutWorkRef.current, {
      "--bgColor": "var(--color-white)",
    })
    .set(aboutWorkSelector("figure"), {
      "--secondary-color": "var(--color-white)",
    })
    .add(RoundBackgroundTween(
      roundBackgroundRef.current,
      aboutWorkRef.current,
      "var(--color-blue-marguerite)"
    ));
}, []);

  return (
    <div className='page-wrapper'>
      <CommonMetaTags 
				title={'Werbeagentur Stuttgart – Webdesign, Programmierung und PR'}
				description={'Werbeagentur Stuttgart - Ihr Profi im Bereich Webdesign, E-Commerce, Programmierung und PR. Full Service Agentur für digitale Medien!'}
				url={'https://www.tandem-m.de/'}
				image={heroImg}
			/>

      <Header/>

      <main>
        <h1 className='visuallyHidden'>Tandem-Marketing und Partner-Werbeagentur-Website</h1>
       
        <Hero
          src={heroImg}
          alt="Ein Mann liest am Tisch ein Buch"
          width={1920}
          height={1281}
          content={<HeroHome />}
        />

        <section styleName="about-us">
          <h2 className="visuallyHidden">Über uns</h2>
          <SectionWhite 
            title={`Werbeagentur – Am${"\u00A0"}Puls${"\u00A0"}des${"\u00A0"}digitalen Zeitalters`}
            text="Als professionelle Werbeagentur aus dem Großraum Stuttgart entwickeln wir seit 2009 innovative Lösungen im Bereich digitale Medien."
          />
          <SectionColor 
            content={
              <Fragment>
                <p className="section-text-200" styleName="text">
                  Wir bieten unseren Kunden einen ganzheitlichen Ansatz, der ihre Bedürfnisse mit unserer Expertise vereint. Die Säulen unserer Arbeit sind Beratung, Konzeption, Design und Technologie.
                </p>
                <p className="section-text-200" styleName="text">
                  Zu unseren Stärken gehört die Umsetzung von Webportalen und Webshops, Leistungen rund um E-Commerce sowie die Betreuung aller Online-Marketing-Kanäle.
                </p>
                <p className="section-text-200" styleName="text">
                  Um unseren Kunden optimale Ergebnisse zu ermöglichen, setzen wir moderne Technologien ein und gehen jedes Projekt voller Motivation, Leidenschaft und Einsatzbereitschaft an.
                </p>
                <div styleName="buttonGroup">
                  <Link
                    to="/werbeagentur-stuttgart/contact.html"
                    className="button"
                    style={{
                      "--stroke": "var(--color-white)",
                      "backgroundColor": "var(--color-lavender)"
                    }}
                  >
                    Werden Sie unser Kunde
                  </Link>
                  <Link
                    to="/werbeagentur-stuttgart.html"
                    className="buttonLink"
                    style={{
                      "--stroke": "var(--color-white)"
                    }}
                  >
                    Über uns
                  </Link>
                  <Link className="buttonLink" to="/referenzen.html" style={{
                    "--stroke": "var(--color-white)"}}>Referenzen</Link>
                </div>
              </Fragment>
            }
            backgroundColor="var(--color-scarlet-gum)"
          />
        </section>

        <Tech />

        <ParallaxEffect
          image={parallaxServicesImg}
          alt="Mitarbeiter im Büro"
          strength={200}
          children={
            <div className="section container-narrow">
              <p className="heading-2 parallax-text"
                style={{minHeight: "auto"}}
              >
                Unsere<br/> Leistungen
              </p>
            </div>
          }
        />

        <AccordionTabs target={"main"} />

        <section styleName="about-work" ref={aboutWorkRef}>
          <div className="container section">
            <h2 className="heading-3">Wie wir arbeiten</h2>
            <ul>
              <li>
                <h3 className="heading-4">Beratung</h3>
                <p className="section-text-100">Vor Projektstart beraten wir Sie eingehend, gehen auf Ihre Wünsche und Bedürfnisse ein und empfehlen Ihnen erste Lösungsmöglichkeiten.</p>
                <Icon
                  name="message"
                  color="var(--color-white)"
                  secondaryColor="var(--color-blue-marguerite)"
                  figure="circle"
                  figureColor="rgba(255, 255, 255, 0.33)"
                />
              </li>
              <li>
                <h3 className="heading-4">Konzeption</h3>
                <p className="section-text-100">Nach dem Briefing sowie der eingehenden Konkurrenz- und Marktanalyse entwickeln wir ein stimmiges Konzept, das optimal auf Ihre Zielgruppe ausgerichtet ist.</p>
                <Icon
                  name="lamp"
                  color="var(--color-white)"
                  secondaryColor="var(--color-blue-marguerite)"
                  figure="square"
                  figureColor="rgba(255, 255, 255, 0.33)"
                />
              </li>
              <li>
                <h3 className="heading-4">Design</h3>
                <p className="section-text-100">Die komplexe Gestaltung erfolgt dabei in einzelnen Schritten – von den ersten Skizzen auf Papier bis hin zur onlinetauglichen Digitalisierung.</p>
                <Icon
                  name="cursor-in-square"
                  color="var(--color-white)"
                  secondaryColor="var(--color-blue-marguerite)"
                  figure="triangle-bottom"
                  figureColor="rgba(255, 255, 255, 0.33)"
                />
              </li>
              <li>
                <h3 className="heading-4">Programmieren</h3>
                <p className="section-text-100">Unsere Entwickler wenden ausschließlich zukunftsfähige Web-Technologien an, die in moderne Content Management Systeme (CMS) implementiert werden.</p>
                <Icon
                  name="tag-in-window"
                  color="var(--color-white)"
                  secondaryColor="var(--color-blue-marguerite)"
                  figure="square"
                  figureColor="rgba(255, 255, 255, 0.33)"
                />
              </li>
              <li>
                <h3 className="heading-4">Support</h3>
                <p className="section-text-100">Nach dem erfolgreichen Abschluss eines Webprojekts stehen wir unseren Kunden auf Wunsch weiterhin in den Bereichen Entwicklung, Marketing und Support zur Seite.</p>
                <Icon
                  name="gear"
                  color="var(--color-white)"
                  secondaryColor="var(--color-blue-marguerite)"
                  figure="circle"
                  figureColor="rgba(255, 255, 255, 0.33)"
                />
              </li>
            </ul>
            <span styleName="about-work-round" ref={roundBackgroundRef}></span>
          </div>
        </section>

        <Projects data={data} title="Zeit, unsere Arbeit zu zeigen" />

        <div styleName="sources-about-us">
          <SourceAboutUs />
        </div>

        {/* <CustomCursorProvider value={{ cursorType, setCursorType }}> */}
          <Blog />
        {/* </CustomCursorProvider> */}

        <FeedbackHomePage />

        {/* <Chat /> */}
      </main>
      
      <Footer content="full"/>
    </div>
  );
}
