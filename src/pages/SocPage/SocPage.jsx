import React, { Fragment } from "react";

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionColor } from "components/SectionColor";
import { ParallaxEffect } from "components/ParallaxEffect";
import { ColorSteps } from "components/ColorSteps";
import { ListOrderUnorder } from "components/ListOrderUnorder";
import { Icon } from "components/Icon";
import { Hr } from "components/Hr";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from "assets/images/soc-parallax-man.jpg";
import parallaxImageWomen from "assets/images/soc-parralax-women.jpg";
import parallaxImageTablet from "assets/images/soc-parallax-tablet.jpg";
import parallaxWatchSoc from "assets/images/watch-de-soc.jpg";


import "./SocPage.css";

export const SocPage = () => {

	return (
		<div className='page-wrapper'>
			<CommonMetaTags 
				title={'Social Media Marketing aus Stuttgart - Erfolgreich Kunden gewinnen'}
				description={'Tandem Marketing'}
				url={'https://www.tandem-m.de/marketing-stuttgart/social-media-marketing.html'}
				image={heroImg}
			/>

			<Header />
			
			<main>
				<Hero
					src={heroImg}
					alt="Social "
					width={1920}
					height={800}
					styleName="seo-hero"
					content={
						<HeroContent 
							title={<span styleName="seo-hero-header">Social Media Marketing für Unternehmen</span>}
							text="Social media marketing"
						/>
					}
				/>

				<section>
					<h2 className="visuallyHidden">Social media marketing</h2>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Social Media gehört heute zu den wichtigsten und dynamischsten Bestandteilen des Online Marketing Mix: Mittlerweile nutzen über 67,80 Millionen der Deutschen jeden Tag Soziale Medien. Viele Unternehmen setzen heute selbstverständlich auf soziale Plattformen, um den direkten Draht zu den Kunden zu nutzen, die Kundenbindung zu stärken und die Werte des eigenen Unternehmens offen zu kommunizieren. Als Webagentur in Stuttgart unterstützen wir Sie bei Ihrem Einstieg in die Welt der sozialen Medien und entwickeln erfolgreiche Social Media Strategien.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-blue)"
					/>
					<div className="empty-parallax">
						<ParallaxEffect
							image={parallaxWatchSoc}
							alt="Tandem Marketing & Partners"
						/>
					</div>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-700">Was ist Social Media Marketing?</p>
								<p className="section-text-200">Unter Social Media Marketing (SMM) versteht man die Nutzung von Sozialen Netzwerken wie Facebook, Instagram, Twitter u.v.m. zu Marketingzwecken. Eine gezielte Social Media Marketing Strategie trägt dazu bei, die Bekanntheit eines Unternehmens oder einer Marke zu steigern und die Konversionsraten zu erhöhen. Auf welche Soziale Plattform(en) gesetzt wird, hängt stark von der Zielgruppe und den Unternehmenszielen ab. </p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
				</section>

				<ColorSteps 
					hiddenTitle="Suchmaschinenoptimierung"
					parallax={
						<ParallaxEffect
							image={parallaxImageWomen}
							alt="Tandem Marketing & Partners"
							children={
								<div className="section container">
									<b className="heading-2 parallax-text" >
										Warum Social Media Marketing?
									</b>
								</div>
							}
						/>
					}
					textTop={"Es gibt viele Gründe für Unternehmen, in den Sozialen Medien präsent zu sein, z.B. um die Markenbekanntheit steigern: 67,80 Millionen Menschen in Deutschland bewegen sich täglich in Sozialen Netzwerken. Durch Inhalte, die spezifisch auf Ihre Zielgruppe zugeschnitten sind, bringen Sie Ihre Marke oder Ihre Produkte ins Gespräch. Kommentare, Likes, Shares und Reposts sind wertvolle Mund-zu-Mund-Propaganda für Ihr Unternehmen. Interessierte Nutzer gelangen über richtig platzierte Links direkt auf Ihre Website. So lassen sich langfristig neue Leads über Soziale Medien gewinnen. Zudem können Sie das Vertrauen in Ihr Unternehmen und die Kundenbindung stärken, indem Sie unmittelbar mit Ihren Nutzern interagieren, online Fragen beantworten und auch Raum für Kritik geben."}
					textCenter={"Social Media bedeutet weit mehr, als bei Facebook präsent zu sein: Es ist wichtig, die Meinungen der Interessenten zu hören, mit Ihnen in Dialog zu treten, gemeinsam an Optimierungen zu arbeiten und letztlich neue Markenfans zu gewinnen."}
					bgColor={"var(--color-blue)"}
				/>

				<section>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-700">Die Nutzerzahlen: Ein Argument für sich</p>
								<p className="section-text-200">Wer jetzt noch überlegt, ob es sich lohnt im Social Web aktiv zu werden, sollte vielleicht einen Blick auf die Nutzerzahlen werfen. Die meisten Deutschen nutzen die Sozialen Medien ganz selbstverständlich, und zwar täglich: Auf Facebook kurz die Neuigkeiten scannen und mit Freunden kommunizieren. Den Schnappschuss auf Instagram hochladen. Neue Rezeptideen auf Pinterest entdecken. In der Mittagspause relevante Artikel aus der Branche lesen und so auf dem Laufenden bleiben. Die Liste beispielhafter Aktivitäten im Web 2.0 könnten wir endlos fortführen. Wer die Bekanntheit seines Unternehmens erhöhen möchte, kommt an den sozialen Medien nicht vorbei!</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Welche Netzwerke für Ihr Unternehmensziele besonders relevant sind, hängt stark von der Zielgruppe ab: Während sich jüngere Nutzer eher auf den relativ neuen Plattformen wie Snapchat (sechs Millionen wöchentlich) und TikTok (zwei Millionen wöchentlich) bewegen, nutzt eine breite Masse nach wie vor Facebook (18 Millionen wöchentlich) und Instagram (14 Millionen wöchentlich), gefolgt von Twitter (3,5 Millionen wöchentlich). Für den B2B-Bereich können auch Xing und LinkedIn (jeweils 2,8 Millionen wöchentlich) interessant sein. Die Nutzerzahlen stammen aus der <a href="https://www.ard-zdf-onlinestudie.de/" target="_blank">ARD/ZDF-Onlinestudie</a>.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-blue)"
					/>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-700">Wie wichtig ist Social Media für Unternehmen?</p>
								<p className="section-text-200">Social Media hat sich in den vergangenen Jahren zu einem selbstverständlichen Marketing Tool entwickelt. Mehr als 75% Prozent der Unternehmen in Deutschland nutzen heute ein oder mehrere soziale Netzwerke. Ein Blick auf die Konkurrenz und deren Social Media Strategie hilft einzuschätzen, wo Sie als Unternehmen potenzielle Kunden am besten erreichen und welche Taktiken sich bewährt haben – oder das Gegenteil.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
				</section>

				<section styleName="soc-parallax">
					<ParallaxEffect
						image={parallaxImageTablet}
						alt=""
						children={
							<div className="section container-narrow" styleName="parallax-container">
								<b className="heading-2" styleName="parallax-text">
									Unsere Dienstleistungen<br/>im Web 2.0
								</b>
							</div>
						}
					/>
				</section>

				<section>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-700">Social Media Marketing - Beratung und Strategie</p>
								<p className="section-text-200">Als Internetagentur in Stuttgart mit deutschlandweiten Projekten unterstützen Sie gerne dabei, die Potenziale des Social Webs voll auszunutzen – mit einer ausgeklügelten Social Media Marketing Strategie. Dabei behalten wir Ihre Unternehmensziele im Blick und setzen den Fokus auf Ihre Nutzer.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
					<div className="container-narrow">
						<h2 className="heading-3">Denn ohne engagierte Fans gibt es kein erfolgreiches Social Media Marketing:</h2>
					</div>
					<div className="container-narrow grid-hr" styleName="advantages">
						<Hr color="var(--color-blue)" />
						<ListOrderUnorder
							isOrdered={false}
							icon={
								<Icon
									name="checked"
									width="50px"
									height="50px"
									color="var(--color-dark)"
								/>
							}
							items={[
								<Fragment>
									<h3 className="heading-4">Zielgruppe definieren</h3>
									<p>Auf Basis von Markt- und Konkurrenzanalysen definieren wir Ihre Zielgruppe auf Facebook & Co.</p>
								</Fragment>,
								<Fragment>
									<h3 className="heading-4">Geeignete Social Media Plattform auswählen</h3>
									<p>Wir analysieren, welche sozialen Netzwerke für Ihre potenziellen Kunden am relevantesten sind und entwickeln auf dieser Basis eine Strategie.</p>
								</Fragment>,
								<Fragment>
									<h3 className="heading-4">Einzigartige, interessante Inhalte schaffen</h3>
									<p>Wir schaffen einzigartige Contents, mit denen Sie sich von der Konkurrenz abheben. Gute Inhalte sind Ihre Daseinsberechtigung im Social Web.</p>
								</Fragment>,
								<Fragment>
									<h3 className="heading-4">Social Media Analyse</h3>
									<p>Wir analysieren, was Ihre Social Media Aktivitäten bewirken und decken Verbesserungspotenzial auf.</p>
								</Fragment>
							]}
						/>
					</div>
				</section>

				<section styleName="lastSection">
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-700">Social Media Advertising</p>
								<p className="section-text-200">Ein unverzichtbares Element einer erfolgreichen Social Media Strategie ist die Werbung. Ohne Social Ads lässt sich – selbst bei brillianten Inhalten – kaum die Reichweite erzielen, die Ihr Unternehmen verdient. Dies hängt damit zusammen, dass die organische Reichweite für Unternehmen in den vergangenen Jahren seitens der verschiedenen Netzwerke stark eingeschränkt wurde.</p>
								<p className="section-text-200">Ein großer Vorteil von Werbung in Social Media Kanälen ist, dass sie sich sehr genau auf die Zielgruppe einstellen lässt – mögliche Streuverluste werden minimiert. Wer auf Facebook, Twitter und Co wirbt, kann neue potenzielle Kunden erreichen. Aber Werbung in den verschiedenen Social Media Kanälen dient auch dazu, bestehende Kunden zu halten und die Kundenbindung zu stärken. </p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-blue)"
					/>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-700">Social Media Analyse</p>
								<p className="section-text-200">Welche Inhalte kommen bei Ihren Fans am besten an? Wo ist noch Optimierungsbedarf? Wir werten die Ergebnisse Ihrer Beiträge regelmäßig aus und passen die laufende Strategie kontinuierlich anhand Ihrer Unternehmensziele und der Nutzerinteressen an. So stellen wir sicher, Ihre Ziele zu erreichen.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-700">Social Media Betreuung</p>
								<p className="section-text-200">Social Media kostet Zeit, die im hektischen Unternehmensalltag oft fehlt. Gerne betreuen wir Ihre Social Media Kanäle und überraschen Sie mit einzigartigen Ideen für Posts und fortlaufende Kampagnen für Ihre Produkte und Dienstleistungen. Dabei kümmern wir uns um übersichtliche Arbeitsabläufe, klare Zuständigkeiten und stimmige Inhalte, sowohl inhaltlich als auch optisch in Bezug auf Ihr Corporate Design. Unser gemeinsames Ziel ist, die sozialen Medien nahtlos in Ihren Marketing-Mix einzubetten.</p>
								<p className="section-text-200">Sollten Sie interessiert an einer Social Media Beratung oder fortlaufenden Betreuung Ihrer Kanäle sein, sprechen Sie uns an. Als Werbeagentur in Stuttgart erstellen wir Ihnen gerne ein individuelles Angebot, das auf Ihr Unternehmen zugeschnitten ist.</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-blue)"
					/>
				</section>

				<FeedbackHomePage />
			</main>
			
			<Footer />
		</div>
	);
}
