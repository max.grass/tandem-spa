import React, { Fragment } from "react";

import { Link } from 'react-router-dom';

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { CommonMetaTags } from "components/CommonMetaTags";
import { SectionColor } from "components/SectionColor";
import { ParallaxEffect } from "components/ParallaxEffect";
import { ColorSteps } from "components/ColorSteps";
// import { MarketingScheme } from "components/MarketingScheme";
import { CheckedList } from "components/CheckedList";
import { Article } from "components/Article";
// import { AboutPageStrengths } from "components/AboutPage-OurStrengths";
// import { data as UtilityPageStrengthsData } from "components/AboutPage-OurStrengths/UtilityPageStrengthsData";
import { ListOrderUnorder } from "components/ListOrderUnorder";
import { Icon } from "components/Icon";
import { Hr } from "components/Hr";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";

import heroImg from "assets/images/hero-responsive.jpg";

import parallax1 from "assets/images/parallx-responsive-1.jpg";
import parallax2 from "assets/images/parallax-responsive-2.jpg";
import parallax3 from "assets/images/parallax-responsive-3.jpg";
import parallax4 from "assets/images/parallax-responsive-4.jpg";
import parallax5 from "assets/images/parallax-responsive-5.jpg";
import parallax6 from "assets/images/parallax-responsive-6.jpg";
import parallax7 from "assets/images/parallax-responsive-7.jpg";

import "./ResponsivePage.css";

export const ResponsivePage = () => {

	return (
		<div className='page-wrapper'>
			<CommonMetaTags 
				title={'Responsives Webdesign Sind Sie für die Zukunft gut gerüstet?'}
				description={'Responsive Webdesign ist keine Zukunftsmusik, sondern eine zeitgemäße Lösung. Mit uns haben Sie einen zuverlässigen Partner für die Umsetzung.'}
				url={'https://www.tandem-m.de/responsives-webdesign.html'}
				image={heroImg}
			/>


			<Header />
			
			<main>
				<Hero
					src={heroImg}
					alt="Bürostuhlrennen"
					width={1920}
					height={800}
					content={
						<div>
							<HeroContent 
								title={<span>Responsive Webdesign Agentur: </span>}
								text="Das heutige Must-Have"
							/>
						</div>
		  		}
				/>

				<section>
					<h2 className="visuallyHidden">Responsive Webdesign</h2>
					<SectionWhite 
                        title="Responsive Webdesign Agentur"
						text="Sie möchten, dass Ihre Website optimal angezeigt wird, schnell läuft und bei Google rankt? Setzen Sie auf Responsive Webdesign."
						textColor="var(--color-gigas)"
					/>
                    <SectionColor 
                        content={
                            <Fragment>
                                <p className="section-text-200">Das flexible Layout für Websites und Onlineshops hat sich heute längst zum Standard entwickelt. Als Webdesign Agentur aus Stuttgart haben wir uns von Beginn an auf diese Technologie spezialisiert: Tandem Marketing & Partners entwickelt ansprechende Layouts, individuelles Webdesign und erfolgreiche Webprojekte, die genau auf Ihr Unternehmen und Ihre Zielgruppe zugeschnitten sind.</p>
                            </Fragment>
                        }
                        textColor="var(--color-white)"
                        backgroundColor="var(--color-gigas)"
                    />
                </section>

                <section styleName="nopdb">
                    <div className="container-narrow">
                        <h2 className="heading-3">Wir bieten:</h2>
                    </div>
                    <div className="container">
                        <CheckedList 
                            content={
                                <Fragment>
                                    <li>Anpassung Ihres aktuellen Designs</li>
                                    <li>Konzeption von responsiven Websites und Webshops</li>
                                    <li>technische Umsetzung gängiger CMS in responsive Design (Magento, Drupal, Wordpress)</li>
                                </Fragment>
                            }
                            color="var(--color-gigas)"
                        />
                    </div>
                </section>

                <section className="section">
                    <ParallaxEffect
                        image={parallax5}
                        alt="Tandem Marketing & Partners"
                        children={
                            <div className="container">
                                <b className="heading-2 parallax-text">
                                    Was ist responsive Webdesign?
                                </b>
                            </div>
                        }
                    />
                    <SectionColor 
                        content={
                            <Fragment>
                                <p className="section-text-200">Egal ob Nutzer eine Website mit dem Smartphone, Tablet oder PC besuchen: Durch responsive Webdesign wird ihnen stets ein optimales Nutzererlebnis (User Experience) ermöglicht. Responsives Design passt sich der jeweiligen Bildschirmgröße und/oder dem genutzten Endgerät an. Responsiv bedeutet so viel wie ‘reagierend’, das heißt Inhalte (Texte, Bilder, Videos etc.) reagieren flexibel auf das genutzte Device und/oder die Größe des Browserfensters.</p>
                            </Fragment>
                        }
                        textColor="var(--color-white)"
                        backgroundColor="var(--color-gigas)"
                    />				
					<Article 
                        buttonUrl="/projekte/52-watch-de.html"
                        buttonCaption="Fälle zeigen"
                        img={<img 
                            src={parallax1}
                            alt="Responsive Webdesign"
                            width={1920} 
                            height={800}
							loading="lazy"
                        />}
                        color="var(--color-gigas)"
                    />
                </section>

                <section className="section">
                    <div className="container-narrow">
						<h2 className="heading-3">Der technische Hintergrund</h2>
					</div>
					<div className="container-narrow grid-hr" styleName="advantages">
						<Hr color="var(--color-gigas)" />
						<ListOrderUnorder
							isOrdered={false}
							icon={
								<Icon
									name="checked"
									width="50px"
									height="50px"
									color="var(--color-dark)"
								/>
							}
							items={[
								<Fragment>
									<h3 className="heading-4">Technische Basis</h3>
									<p>Technische Basis für responsives Design sind Webstandards wie HTML5, CSS3 und JavaScript. Sogenannte Media Queries dienen dazu, bestimmte Eigenschaften der Ausgabegeräte abzufragen, darunter die Orientierung (Hoch- oder Querformat), Verbindungsqualität, Bildschirmauflösung, Browser, Größe des Geräts sowie Eingabemöglichkeiten (Tastatur, Touchscreen, Sprache).</p>
								</Fragment>,
								<Fragment>
									<h3 className="heading-4">Responsive CSS-Grids</h3>
									<p>Für eine responsive Seite verwenden Webdesigner*innen keine starren Gestaltungsraster (Grids) mehr. Die Anordnung der Elemente auf einer Seite richtet sich vielmehr nach flexiblen prozentualen Pixelwerten. Es gibt auch keine festen Bild- und Schriftgrößen mehr. Für gewöhnlich ist die Anzahl der Pixel, die der Browser in der Breite zur Verfügung hat, ausschlaggebend für die Darstellung einer Website.</p>
								</Fragment>,
								<Fragment>
									<h3 className="heading-4">JavaScript Plug-Ins</h3>
									<p>JavaScript Plug-Ins oder HTML-Optionen skalieren die Größe von Headlines, Bildern und Grafiken, damit Inhalte auch auf mobilen Geräte optimal abrufbar sind und schnell laden.<br/>Unsere Experten für Webdesign in Stuttgart sind erfahren im Umgang mit flexiblen Layout-Grids und führen die responsive Auslegung Ihrer Unternehmensseite professionell durch.</p>
								</Fragment>
							]}
						/>
					</div>
				</section>

				<section>
					<ColorSteps
						hiddenTitle="Responsive Webdesign"
						parallax={
							<ParallaxEffect
								image={parallax2}
								alt="Tandem Marketing & Partners"
								children={
									<div className="section container">
										<b className="heading-2 parallax-text">
                                            Warum ist responsive Webdesign so wichtig?
										</b>
									</div>
								}
							/>
						}
						textTop={"Ein Großteil der Deutschen besitzt heute ein Smartphone, die Mobilfunktarife werden immer günstiger. Dies führt dazu, dass die mobile Internetnutzung in Deutschland immer gängiger wird. Bereits im Jahr 2018 überstieg die Anzahl der Internetnutzer über mobile Endgeräte die Anzahl derer, die über PC oder Notebook online gingen. Der Anteil der mobilen Internetnutzer in Deutschland lag im Jahr 2020 bei 80 Prozent. Zu einer der beliebtesten Aktivitäten im Netz gehört die Suche nach Waren oder Dienstleistungen (Quelle: Statista)"}
						textCenter={"Ist die Website oder der Onlineshop Ihres Unternehmens noch nicht für Mobilgeräte optimiert? Dann reagieren Sie jetzt."}
						textBottom={
                            <Fragment>
                                <p className="section-text-200">Als Responsive Design Agentur aus Stuttgart unterstützen wir Sie gerne bei der Planung und Umsetzung Ihres Internetauftritts.</p>
                                <Link
                                    to="/werbeagentur-stuttgart.html"
                                    className="button"
                                    style={{
                                        "--stroke": "var(--color-white)",
                                        "backgroundColor": "var(--color-gigas)"
                                    }}
                                >Jetzt kontaktieren</Link>
                            </Fragment>
                        }
						bgColor={"var(--color-gigas)"}
					/>
				</section>

                <section>
                    <SectionColor 
                        content={
                            <Fragment>
                                <p className="section-text-200" styleName="blue-links">Internetbesucher heute erwarten, dass sie eine Website, ein Onlineportal oder einen Webshop auch von unterwegs einfach erreichen, dass sie dort alle Informationen vorfinden und die Seite schnell lädt, kurz: gute Usability bzw. <a href="/usability">Benutzerfreundlichkeit</a>. Wichtige Faktoren dafür sind eine übersichtliche Navigation, intuitive Bedienbarkeit sowie optische Usability-Faktoren: Websites sollen auf allen Ausgabegeräten gut aussehen.</p>
                            </Fragment>
                        }
                        textColor="var(--color-white)"
                        backgroundColor="var(--color-gigas)"
                    />
                    <ParallaxEffect
                        image={parallax6}
                        alt="Tandem Marketing & Partners"
                        children={
                            <div className="container parallax-text">
                                <p className="section-text-700">Wer seinen Kunden jetzt noch eine mobile Webseite zumutet, die weder den gesamten Inhalt noch den vollen Funktionsumfang bietet, verliert auf Dauer Kunden an die besser aufgestellte Konkurrenz. Benutzerfreundlichkeit ist für den Erfolg im Internet entscheidend. Aus diesem Grund ist Responsive Webdesign so wichtig.</p>
                                <p className="section-text-200" style={{marginBottom:0}}><i>Andreas Schmidt, Gründer von Tandem Marketing & Partners</i></p>
                            </div>
                        }
                    />
                    <SectionColor 
                        content={
                            <Fragment>
                                <p className="section-text-200">Responsive Websites punkten auch in Bezug auf den Pflegeaufwand. Der gesamte Inhalt (Texte, Grafiken, Videos etc.) wird über Content Management Systeme wie Magento oder Drupal zentral verwaltet und steht im Anschluss auf allen Endgeräten gleichermaßen zur Verfügung. Dies ist ein großes Plus gegenüber Websites, bei denen die Mobil- und Desktop-Version getrennt sind: Diese müssen doppelt gepflegt werden, was sich auch auf der Kostenseite niederschlägt. Aus diesem Grund haben responsive Websites mittlerweile mobile Versionen — beispielsweise gekennzeichnet durch Subdomains mit vorangestelltem „m“ — abgelöst.</p>
                            </Fragment>
                        }
                        textColor="var(--color-dark)"
                        backgroundColor="var(--color-white)"
                    />
                    <div className="empty-parallax">
						<ParallaxEffect
							image={parallax3}
							alt="Tandem Marketing & Partners"
						/>
					</div>
                    <SectionColor 
                        content={
                            <Fragment>
                                <p className="section-text-700">Mobile First Indexing</p>
                                <p className="section-text-200">Auch Google wertet die mobile Ansicht einer Seite mittlerweile als die wichtigste, Stichwort: Mobile First Indexing. Das bedeutet, dass Google die mobile Ansicht eines Internetauftritts als Grundlage der Bewertung für das Ranking nimmt. Damit ist responsives Design auch im Hinblick auf die Suchmaschinenoptimierung absolut empfehlenswert.</p>
                            </Fragment>
                        }
                        textColor="var(--color-white)"
                        backgroundColor="var(--color-gigas)"
                    />
                </section>

                <section>
                    <ParallaxEffect
                        image={parallax7}
                        alt="Tandem Marketing & Partners"
                        children={
                            <div className="container">
                                <p className="section-text-700 parallax-text">Responsive Webdesign mit Tandem Marketing & Partners: Projektablauf und Umsetzung</p>
                            </div>
                        }
                    />
                    <SectionColor
                        content={
                            <Fragment>
                                <p className="section-text-200">Durch unsere überregionale Arbeitsweise realisieren wir responsives Webdesign in Stuttgart und deutschlandweit. Der Ablauf sieht folgendermaßen aus:</p>
                            </Fragment>
                        }
                        textColor="var(--color-dark)"
                        backgroundColor="var(--color-white)"
                    />
                </section>

                <section styleName="checklistPart">
					<div className="container-narrow">
						<h2 className="heading-3">1. Konzeption</h2>
					</div>
					<div className="container">
						<CheckedList 
							content={
								<Fragment>
									<li>Auf Basis Ihrer Informationen und unserer Analysen erarbeiten wir die Ziele der Website, definieren die Zielgruppe und erstellen ein Konzept. Auch die Entscheidung für ein bestimmtes Content Management System (CMS) wie Drupal, Magento oder Wordpress fällt in dieser Phase. Die Optimierung bestehender Inhalte bzw. das Texten aussagekräftiger Web-Contents sollte jetzt starten – unsere Expert*innen im Bereich Content Marketing unterstützen Sie gern, auch bei der Bewertung relevanter Keywords.</li>
									<li>Steht der Plan, gehen wir über zur Designphase.</li>
								</Fragment>
							}
							color="var(--color-gigas)"
						/>
					</div>
				</section>

                <section styleName="checklistPart">
					<div className="container-narrow">
						<h2 className="heading-3">2. Design</h2>
					</div>
					<div className="container">
						<CheckedList 
							content={
								<Fragment>
									<li>Die Konzeption bietet den Rahmen für die Gestaltung Ihrer neuen Website. Unsere Webdesigner*innen erstellen Ihnen auf Wunsch zwei oder mehr Webdesign-Entwürfe und visualisieren die Darstellung auf verschiedenen Endgeräten. Bei Bedarf werden die Screendesigns in Feedbackschleifen angepasst. Bereits während der Designphase arbeiten unsere Webdesigner*innen Hand in Hand mit den Entwickler*innen, um die optimale technische Umsetzbarkeit in puncto Geschwindigkeit und SEO sicher zu stellen.</li>
									<li>Auf das Design folgt die Programmierung.</li>
								</Fragment>
							}
							color="var(--color-gigas)"
						/>
					</div>
				</section>

                <section styleName="checklistPart">
					<div className="container-narrow">
						<h2 className="heading-3">3. Programmierung</h2>
					</div>
					<div className="container">
						<CheckedList 
							content={
								<Fragment>
									<li>In der Programmierungsphase schaffen unsere Template-Entwickler*innen interaktive Templates aus den Designentwürfen, die mittels CSS, HTML, PHP, MySQL und verschiedenen Frameworks umgesetzt werden. </li>
									<li>Ist das CMS der Wahl installiert, wird das Design übertragen und für mobile Geräte optimiert. Bereits in dieser Phase wird laufend geprüft, ob die Website auf allen Endgeräten (Smartphone, Tablet, PC etc.), in den gängigen Browsern und auf unterschiedlichen Systemen reibungslos läuft. Unsere Webprojekte werden grundsätzlich nach geltenden Standards programmiert.</li>
								</Fragment>
							}
							color="var(--color-gigas)"
						/>
					</div>
				</section>

                <section styleName="checklistPart">
					<div className="container-narrow">
						<h2 className="heading-3">4. Inhalte einpflegen & finale Prüfung</h2>
					</div>
					<div className="container">
						<CheckedList 
							content={
								<Fragment>
									<li>Ist die neue Website fertig umgesetzt, geht es an das Einpflegen der Inhalte. Die Qualität der Texte und Fotos ist dabei von höchster Relevanz. Das Publishing übernehmen entweder wir für Sie oder Sie machen es selbst. Auf Wunsch schulen wir Ihre Mitarbeiter*innen vorher im Umgang mit dem neuen Content Management System. </li>
									<li>Sind die Inhalte eingepflegt, beginnt die finale Prüfung: Werden Bilder, Videos, Slider etc. auf allen Endgeräten gut angezeigt? Funktionieren alle Formulare, z.B. zur Newsletteranmeldung? Gibt es eine Datenschutzerklärung?</li>
                                    <li>Sind alle Fragen geklärt, steht dem Onlinegang nichts mehr im Wege.</li>
								</Fragment>
							}
							color="var(--color-gigas)"
						/>
					</div>
				</section>

                <section styleName="checklistPart">
					<div className="container-narrow">
						<h2 className="heading-3">5. Onlinegang</h2>
					</div>
					<div className="container">
						<CheckedList 
							content={
								<Fragment>
									<li>Sobald Sie Ihre Freigabe gegeben haben, wird die Website von unserem Entwicklungsserver auf Ihren Live Server übertragen. Nach dem Onlinegang begleiten wir die digitale Transformation Ihres Unternehmens weiterhin und unterstützen Sie u.a. in folgenden Bereichen: Content Marketing & Website-Pflege, Website-Support, Analyse & Kontrolle, PlugIns & Softwareentwicklung, Strategie & Online Marketing.</li>
								</Fragment>
							}
							color="var(--color-gigas)"
						/>
					</div>
				</section>

                <section>
                    <div className="empty-parallax">
						<ParallaxEffect
							image={parallax4}
							alt="Tandem Marketing & Partners"
						/>
					</div>
                    <SectionColor
                        content={
                            <Fragment>
                                <p className="section-text-200" styleName="blue-links">Haben Sie noch Fragen? Kommen Sie gerne auf unsere Webdesign Agentur zu! Wir freuen uns, Ihre Website responsiv und suchmaschinenoptimiert zu konzipieren: <a href="/contacts">Jetzt Kontakt aufnehmen</a>.</p>
                            </Fragment>
                        }
                        textColor="var(--color-white)"
                        backgroundColor="var(--color-gigas)"
                    />
                </section>

				<FeedbackHomePage />
			</main>
			
			<Footer />
		</div>
	);
}
