import React, { Fragment } from "react";
import { Link } from 'react-router-dom';

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { ParallaxEffect } from "components/ParallaxEffect";
import { Article } from "components/Article";
import { IconList } from "components/IconList";
import { ColorSteps } from "components/ColorSteps";
import { AboutPageStrengths } from "components/AboutPage-OurStrengths";
import { data as WDStrengthsData } from "components/AboutPage-OurStrengths/WDPageStrenghsData";
import { FeedbackHomePage } from "components/FeedbackHomePage";
import { Footer } from "components/Footer";
import { LineChartWebDesign } from "components/Charts";
import { CommonMetaTags } from "components/CommonMetaTags";

import heroImg from "assets/images/hero-webdesign.jpg";
import parallaxPeople from "assets/images/webdesign-parralax-people.jpg";
import parallaxImageWatch from "assets/images/parallax-webdesign-watch.jpg";
import parallaxImagePhone from "assets/images/parallax-webdesign-telephone.jpg";
// import scheme from "assets/images/webdesign-cms-d-01.svg";

import waIcon1 from "assets/images/wd-icon-1.svg";
import waIcon2 from "assets/images/wd-icon-2.svg";
import waIcon3 from "assets/images/wd-icon-3.svg";
import waIcon4 from "assets/images/wd-icon-4.svg";
import waIcon5 from "assets/images/wd-icon-5.svg";
import waIcon6 from "assets/images/wd-icon-6.svg";


import "./WebdesignPage.css";

export const WebdesignPage = () => {

	return (
		<div className='page-wrapper'>
			<CommonMetaTags 
				title={'Webdesign aus Stuttgart - schnell, zuverlässig, professional'}
				description={'Webdesign Stuttgart: Wir realisieren professionell und zuverlässig Ihre Webpräsenz - von einfachen CMS-Lösungen bis hin zu komplexen E-Commerce Portalen.'}
				url={'https://www.tandem-m.de/webdesign-cms.html'}
				image={heroImg}
			/>

			<Header />
			
			<main>
				<Hero
					src={heroImg}
					alt="Webdesign CMS"
					width={1920}
					height={800}
					content={
						<HeroContent 
							title={<span>Webdesign aus Stuttgart</span>}
							text="Webdesign CMS"
						/>
					}
				/>

				<section>
					<h2 className="visuallyHidden">Webdesign aus Stuttgart</h2>
					<SectionWhite 
						title="Webdesign aus Stuttgart: Überzeugen auf den ersten Blick"
						text="Sie suchen eine Webdesign Agentur in Stuttgart und deutschlandweit? Tandem Marketing & Partners bietet professionelles Webdesign."
						textColor="var(--color-gigas)"
					/>
					<div>
						<SectionColor 
							content={
								<Fragment>
									<p className="section-text-200">Dabei berücksichtigen wir die wichtigsten Faktoren für eine erfolgreiche Webpräsenz: Wir bieten Nutzerfreundlichkeit, Suchmaschinenoptimierung (SEO), professionelle Layouts und großartige Web-Texte. Gemeinsam mit Tandem Marketing & Partners machen Sie Kunden aus Ihren Website-Besuchern.</p>
								</Fragment>
							}
							textColor="var(--color-white)"
							backgroundColor="var(--color-gigas)"
						/>
					</div>
				</section>

				<section>
					<ColorSteps
						hiddenTitle="Webdesign aus Stuttgart"
						parallax={
							<ParallaxEffect
								image={parallaxPeople}
								alt="Tandem Marketing & Partners"
								children={
									<div className="section container">
										<b className="heading-2 parallax-text">
											Was ist Webdesign?
										</b>
									</div>
								}
							/>
						}
						textTop={"Webdesign ist mehr als nur die Optik einer Website. Bereits die Konzeption, Ziele, Navigation und der Seitenaufbau sind wichtige Bestandteile des Webdesigns. Dazu kommen Bilder, Farbgebung, Schriftarten, Grafiken und Videos. Gelungenes Webdesign ist ein harmonisches Zusammenspiel aus Grafikdesign, User Experience Design und Interface Design. Weitere relevante Faktoren sind die Suchmaschinenoptimierung und der Aufbau der Inhalte."}
						textCenter={"Wichtig ist, dass die Onlinepräsenz auf allen Endgeräten optimal zur Geltung kommt, gut funktioniert und positive Gefühle hervorruft."}
						textBottom={""}
						bgColor={"var(--color-gigas)"}
					/>
				</section>

				<section>
					<SectionColor 
						content={
							<Fragment>
								<p className="heading-3">Warum Webdesign?</p>
								<p className="section-text-200">Nutzer entscheiden innerhalb weniger Sekunden, ob sie auf einer Website bleiben oder lieber zum Wettbewerber klicken. In dieser kurzen Zeitspanne punktet das Webdesign: Wirken Layout, Bilder und Farben vertrauenswürdig?</p>
								<p className="section-text-200">Eine attraktive Website ist Aushängeschild Ihres Unternehmens und oft der Erstkontakt zu potenziellen Kunden. Als Webdesign Agentur aus Stuttgart helfen wir Ihnen, potenzielle Kunden von Ihrem Unternehmen zu überzeugen.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
				</section>

				<section styleName="iconList" className="section">
					<div className="container">
						<p className="heading-3">Wir bieten:</p>
					</div>
					<IconList
						listData={[
							{
								"iconSrc" : waIcon1,
								"label" : "Konzeption auf Basis Ihrer Unternehmensziele"
							},
							{
								"iconSrc" : waIcon2,
								"label" : "Nutzerorientiertes Webdesign"
							},
							{
								"iconSrc" : waIcon3,
								"label" : "Suchmaschinen-optimierung"
							},
							{
								"iconSrc" : waIcon4,
								"label" : "Responsive Webdesign"
							},
							{
								"iconSrc" : waIcon5,
								"label" : "Content Marketing"
							},
							{
								"iconSrc" : waIcon6,
								"label" : "Online Marketing"
							}
						]}
					/>
				</section>

				<section>
					<ParallaxEffect
						image={parallaxImagePhone}
						alt="Tandem Marketing & Partners"
						children={
							<div className="section container-narrow">
								<b className="heading-2 parallax-text"
									style={{minHeight: "auto"}}
								>
									Was macht gutes Webdesign aus?
								</b>
							</div>
						}
					/>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Bei den meisten Arten von Design ist die Unterscheidung zwischen ‘gut’ und ‘schlecht’ eine Geschmacksfrage. Beim Webdesign ist dies anders: Gutes Webdesign bietet Nutzern genau das Erlebnis, das sie suchen. Es bringt Nutzer zum “Konvertieren”, das heißt, sie führen eine Handlung aus, die ihnen die Website nahelegt (z.B. Produkt kaufen, Newsletter abonnieren, weitere Inhalte abrufen). Die Interaktivität auf einer Seite gehört zu den von Google gemessenen Core Web Vitals und ist seit 2021 Rankingfaktor. Folglich ist eine nutzerorientierte Webgestaltung auch in puncto SEO relevant.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
				
					{/* <SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Damit Webdesign die Conversion Rate (Konversionsrate) erhöht, sollte es einige Punkte erfüllen: Zum einen muss modernes Webdesign auf dem Mobile-First-Prinzip basieren. Das bedeutet, dass die Website auf allen Endgeräten gut angezeigt wird. In unserer Internetagentur Tandem Marketing & Partners entwerfen und entwickeln wir responsive Websites, die durchwegs hohe Geschwindigkeit und Leistung zeigen. Denn mobil schlecht abrufbare Homepages werden von Google mit schlechteren Positionen bestraft.</p>
								<img src={scheme} alt="webdesign scheme" />
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-gigas)"
					/> */}

					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">Damit Webdesign die Conversion Rate (Konversionsrate) erhöht, sollte es einige Punkte erfüllen: Zum einen muss modernes Webdesign auf dem Mobile-First-Prinzip basieren. Das bedeutet, dass die Website auf allen Endgeräten gut angezeigt wird. In unserer Internetagentur Tandem Marketing & Partners entwerfen und entwickeln wir responsive Websites, die durchwegs hohe Geschwindigkeit und Leistung zeigen. Denn mobil schlecht abrufbare Homepages werden von Google mit schlechteren Positionen bestraft.</p>
								<div styleName="line-chart-wrapper">
									<LineChartWebDesign />
								</div>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-gigas)"
					/>

					<SectionColor 
						content={
							<Fragment>
								<p>Zum anderen erwarten Nutzer echten Mehrwert und setzen auf einfache Bedienung, ansprechende Optik und eine überschaubare Struktur — kurz: Usability beziehungsweise Nutzerfreundlichkeit.<br/>Neben dem einheitlichen Design sind deshalb auch eine einfache Navigationsführung sowie klare Struktur von entscheidender Bedeutung. Die Website muss übersichtlich und einfach handhabbar sein sowie eindeutige Call-to-Actions (Handlungsaufforderungen) liefern, damit Besucher innerhalb kürzester Zeit zu den gewünschten Informationen gelangen. Dabei ist auch wichtig, dass eine Balance herrscht zwischen Text und anderen Content-Elementen.</p>
							</Fragment>
						}
						textColor="var(--color-dark)"
						backgroundColor="var(--color-white)"
					/>
				</section>

				<section>
					<Article 
						title="Webdesign Stuttgart: Der Projektablauf"
						content={
							<Fragment>
								<p>Unsere Webdesign Agentur bietet Ihnen als professioneller Dienstleister Leistungen rund um Konzeption, Gestaltung, Programmierung und Pflege von Onlineshops, Webplattformen, Homepages oder Apps. Darüber hinaus verbreiten wir Ihre Botschaft über digitale Kommunikationskanäle wie Social Media, Blogs oder Google Ads.</p>
								<p><b>Ihr Vorteil:</b> Mit einem gelungenen Webdesign finden sich Kunden auf Ihrer Webseite schnell zurecht und fühlen sich wohl. Wecken Sie Emotionen mit Ihrem Webauftritt und sprechen künftig Kunden auch auf einer psychologisch (werbe-)wirksamen Ebene an. Wenn sich auch viele Websites grundsätzlich ähneln, sind es meist doch die kleinen Feinheiten, die den Unterschied ausmachen.</p>
							</Fragment>
						}
						buttonUrl="/projekte/52-watch-de.html"
						buttonCaption="Fall zeigen"
						img={<img 
							src={parallaxImageWatch}
							alt=""
							width={1920} 
							height={800}
							loading="lazy"
						/>}
						color="var(--color-gigas)"
					/>
				</section>

				<section styleName="wdStrengths">
					<AboutPageStrengths
						header={'Starten Sie mit uns in die Digitalisierung – von der Planung bis zum erfolgreichen Onlinegang: '}
						data={WDStrengthsData}
						hrColor={"var(--color-gigas)"}
					/>
				</section>

				<section>
					<SectionColor 
						content={
							<Fragment>
								<p className="section-text-200">
									<span>Tandem Marketing & Partners macht Ihr Unternehmen bereit für den digitalen Wandel: </span>
									<Link to={'/werbeagentur-stuttgart/contact.html'}>
										<b>Kontaktieren Sie uns, um mehr zu erfahren</b>
									</Link>
									.
								</p>
							</Fragment>
						}
						textColor="var(--color-white)"
						backgroundColor="var(--color-gigas)"
					/>
				</section>

				<FeedbackHomePage />

			</main>
			
			<Footer />
		</div>
	);
}
