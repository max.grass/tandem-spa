import React from "react";
import { Icon } from "components/Icon";
import { Hr } from "components/Hr";

import "./Strengths.css";

export const Strengths = () => {

    return (
        <section className="section">
            <div className="section-m container-narrow grid-hr" styleName="offers">
            <p styleName="offers-text" className="heading-3">Unsere Stärken:</p>
            <Hr color="var(--color-medium-teal-blue)"/>
            <div>
              <ul styleName="offers-list">
                <li>
                  <div styleName="offers-icon">
                    <Icon
                      name="lamp"
                      figure="triangle-bottom"
                      color="var(--color-medium-teal-blue)"
                    />
                  </div>
                  <div styleName="offers-title">
                    <h3 className="heading-4">Konzept und Storytelling</h3>
                    <p styleName="offers-descr">
                      Jede Präsentation erzählt eine spannende Geschichte, die Ihre Unternehmenswerte und Vision lebendig vermittelt. Durch Storytelling schaffen wir emotionale Verbindungen, die die Präsentation nicht nur informativ, sondern auch inspirierend machen.
                    </p>
                  </div>
                </li>
                <li>
                  <div styleName="offers-icon">
                    <Icon
                      name="cursor-in-square"
                      figure="triangle-bottom"
                      color="var(--color-medium-teal-blue)"
                    />
                  </div>
                  <div styleName="offers-title">
                    <h3 className="heading-4">Design</h3>
                    <p styleName="offers-descr">
                      Wir gestalten Ihre Präsentation in Ihrem Corporate Design und verleihen ihr das gewisse Etwas, das sie von Ihren Wettbewerbern abhebt. Unsere Designs reichen von schlicht und elegant bis hin zu dynamisch und interaktiv, inklusive Animationen. Unser Fokus: Ihre Kernbotschaften und die Aufmerksamkeit des Publikums.
                    </p>
                  </div>
                </li>
                <li>
                  <div styleName="offers-icon">
                    <Icon
                      name="pen"
                      figure="triangle-bottom"
                      color="var(--color-medium-teal-blue)"
                    />
                  </div>
                  <div styleName="offers-title">
                    <h3 className="heading-4">Datenvisualisierung</h3>
                    <p styleName="offers-descr">
                      Komplexe Daten und Informationen werden durch ansprechend gestaltete Charts und Grafiken verständlich und visuell ansprechend präsentiert. Wir nutzen moderne Tools, um Ihre Daten effektiv und einprägsam darzustellen.
                    </p>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </section>
    );
};