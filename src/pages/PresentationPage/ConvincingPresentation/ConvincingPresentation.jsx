import React, { Fragment } from "react";
import { Icon } from "components/Icon";
import { Hr } from "components/Hr";
import { ListOrderUnorder } from "components/ListOrderUnorder";

import "./ConvincingPresentation.css";

export const ConvincingPresentation = () => {

    return (
        <section className="section">
            <h2 className="container-narrow heading-3">
                Grundelemente einer
                <br/>
                überzeugenden Präsentation
            </h2>
            <div className="container-narrow grid-hr" styleName="advantages">
                <Hr color="var(--color-medium-teal-blue)" />
                <ListOrderUnorder
                    isOrdered={true}
                    icon={
                    <Icon
                        name="checked-empty"
                        width="50px"
                        height="50px"
                        color="var(--color-dark)"
                    />
                    }
                    items={[
                        <Fragment>
                            <h3 className="heading-4">Storytelling</h3>
                            <p>Jede erfolgreiche Unternehmenspräsentation baut auf dem Prinzip des Storytelling auf. Dabei wird eine narrative Struktur verwendet, um die Inhalte interessant und einprägsam zu gestalten. Eine gute Geschichte erzeugt emotionale Verbindungen und macht die Präsentation nicht nur informativ, sondern auch inspirierend.</p>
                        </Fragment>,
                        <Fragment>
                            <h3 className="heading-4">Design</h3>
                            <p>Das visuelle Design der Präsentation muss professionell und markenspezifisch sein. Es soll die Unternehmensidentität (CI) widerspiegeln und konsistent über alle Folien hinweg sein. Wir setzen Farben, Schriften und Bilder gezielt ein, um die Aufmerksamkeit des Publikums zu lenken und die wichtigsten Botschaften optimal hervorzuheben.</p>
                        </Fragment>,
                        <Fragment>
                            <h3 className="heading-4">Darstellung von Daten</h3>
                            <p>Daten und Statistiken lassen sich am besten durch Grafiken, Diagramme und Infografiken darstellen. Eine klare Visualisierung hilft Ihrem Publikum, die wichtigsten Informationen schnell zu verstehen und erleichtert das Verständnis für Ihr Geschäftsmodell und die Marktposition Ihres Unternehmens.</p>
                        </Fragment>,
                        <Fragment>
                            <h3 className="heading-4">Technologien und Tools</h3>
                            <p>Moderne Präsentationstools wie Powerpoint oder Google Slides bieten umfangreiche Möglichkeiten, interaktive und dynamische Präsentationen zu erstellen. Animationen und multimediale Inhalte können dazu beitragen, eine lebendige und einnehmende Präsentation zu gestalten, die das Interesse des Publikums bis zum Ende aufrechterhält.</p>
                        </Fragment>,
                        <Fragment>
                            <h3 className="heading-4">Zielgruppenanalyse</h3>
                            <p>Gemeinsam mit Ihnen und durch gezielte Analysen gewinnen wir ein tiefes Verständnis für Ihre Zielgruppe und entwickeln Präsentationsinhalte, die genau auf deren Interessen und Bedürfnisse abgestimmt sind.</p>
                        </Fragment>,
                        <Fragment>
                            <h3 className="heading-4">Klarheit und Kürze</h3>
                            <p>Wir vermeiden eine Überladung mit Informationen. Jede Folie sollte einen klaren Fokus haben und auf das Wesentliche reduziert sein.</p>
                        </Fragment>
                    ]}
                />
            </div>
        </section>
    );
};