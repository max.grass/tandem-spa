import React, { Fragment } from "react";
import { Link } from 'react-router-dom';

import { Header } from "components/Header";
import { Hero, HeroContent } from "components/Hero";
import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";
import { ColorSteps } from "components/ColorSteps";
import { ParallaxEffect } from "components/ParallaxEffect";
import { Strengths } from "./Strengths";
import { WeOffer } from "./WeOffer/WeOffer";
import { ConvincingPresentation } from "./ConvincingPresentation";
import { Footer } from "components/Footer";
import { CommonMetaTags } from "components/CommonMetaTags";
import { Blog } from "components/Blog";
import { FeedbackHomePage } from "components/FeedbackHomePage";

import heroImg from "assets/images/presentation-page/hero-presentation.jpg";
import graphImg from "assets/images/presentation-page/presentation-parallax-graph.jpg";
import parallaxImg from "assets/images/presentation-page/combination-pic-word.jpg";
import parallaxKruschina from "assets/images/presentation-page/kruschina-parallax-pic.jpg";
import successfulPresentationImg from "assets/images/presentation-page/successful-presentation.jpg";
import presentationHallImg from "assets/images/presentation-page/presentation-hall.jpg";
import designedPresentationImg from "assets/images/presentation-page/designed-presentation.jpg";

import "./PresentationPage.css";

export const PresentationPage = () => {

    return (
        <div styleName="presentation-wrapper" className='page-wrapper'>
            <CommonMetaTags 
                title={'Starke Unternehmenspräsentationen – wir entwickeln sie'}
                description={'Tandem Marketing & Partners ist Ihr Experte für starke Unternehmenspräsentationen: Freuen Sie sich auf überzeugende visuelle Elemente & eine klare Struktur.'}
                url={'https://www.tandem-m.de/marketing-stuttgart/unternehmenspraesentation.html'}
                image={heroImg}
            />

            <Header />

            <main>
                <Hero
                    src={heroImg}
                    alt="Unternehmenspräsentationen"
                    width={1920}
                    height={800}
                    content={
                        <HeroContent
                            title={<span className="heroTitle">Unternehmenspräsentationen:<br/>Mehr als Daten und Fakten</span>}
                            text="Firmenpräsentationen"
                        />
                    }
                />

                <section>
                    <h2 className="visuallyHidden">Ihr Partner für beeindruckende Firmenpräsentationen</h2>
                    <SectionWhite
                        title="Ihr Partner für beeindruckende Firmenpräsentationen"
                        text="Tandem Marketing & Partners ist Ihr Experte für maßgeschneiderte Unternehmenspräsentationen: Ob ein bestimmtes Projekt, Ihr Unternehmen oder Ihren Jahresbericht – wir schneiden jede Präsentation genau auf Ihre Zielgruppe zu."
                        textColor="var(--color-medium-teal-blue)"
                    />
                    <div>
                        <SectionColor
                            content={
                                <Fragment>
                                    <p className="section-text-400">Freuen Sie sich auf starke visuelle Elemente und eine klare Struktur.</p>
                                </Fragment>
                            }
                            textColor="var(--color-white)"
                            backgroundColor="var(--color-medium-teal-blue)"
                        />
                    </div>
                </section>
                
                <section styleName="big-parallax">
                    <h2 className="visuallyHidden">Uhren als Investition</h2>
                    <ParallaxEffect
                        image={graphImg}
                        alt="Uhren als Investition"
                    />
                </section>

                <Strengths />

                <section styleName="cambridge-parallax">
                    <h2 className="visuallyHidden">„Die Kombination aus Bildern und Worten verbessert das Verständnis und die Merkfähigkeit um bis zu 60 %.“</h2>
                    <ParallaxEffect
                        image={parallaxImg}
                        alt="Tandem Marketing & Partners"
                        children={
                            <div className="container">
                                <span className="parallax-text" >
                                    <span className="heading-3">
                                        „Die Kombination aus Bildern und Worten<br/> verbessert das Verständnis und die Merkfähigkeit <br/>um bis zu 60 %.“
                                    </span>
                                    <br/>
                                    <a
                                        className="description-link link"
                                        href="https://www.cambridge.org/core/services/aop-cambridge-core/content/view/S148180350000186X"
                                        target="_blank"
                                    >
                                        Cambridge University
                                    </a>
                                </span>
                            </div>
                        }
                    />
                </section>

                <WeOffer />

                <section>
                    <SectionColor
                        content={
                            <Fragment>
                                <p className="section-text-400">
                                    Unsere professionellen PowerPoint-Präsentationen informieren, begeistern und überzeugen. Erfahren Sie, wie wir Ihre Unternehmenspräsentation zu einem wirkungsvollen Kommunikationsmittel machen können – <Link className="link" to="/werbeagentur-stuttgart/contact.html">kontaktieren Sie uns</Link>!
                                </p>
                            </Fragment>
                        }
                        textColor="var(--color-white)"
                        backgroundColor="var(--color-medium-teal-blue)"
                    />
                </section>

                <section styleName="big-parallax">
                    <h2 className="visuallyHidden">KRUSCHINA Unternehmensgruppe</h2>
                    <ParallaxEffect
                        image={parallaxKruschina}
                        alt="KRUSCHINA Unternehmensgruppe"
                    />
                </section>

                <ColorSteps
                    hiddenTitle="Suchmaschinenoptimierung"
                    parallax={
                        <ParallaxEffect
                            image={successfulPresentationImg}
                            alt="Tandem Marketing & Partners"
                            children={
                                <div className="section container">
                                    <b className="heading-2 parallax-text" >
                                        Wie wichtig ist eine gelungene Firmenpräsentation?
                                    </b>
                                </div>
                            }
                        />
                    }
                    textTop={"In der heutigen Geschäftswelt ist eine effektive Unternehmenspräsentation entscheidend für den Erfolg. Dabei geht es um viel mehr, als pure Informationen zu vermitteln. Es geht darum, dass potenzielle Kunden, Investorinnen und Partner an Ihren Lippen hängen. Denn Sie – und Ihre Firmenpräsentation – erzählen eine Geschichte, die überzeugt."}
                    textCenter={
                        <Fragment>
                            <p className="cite">
                                „91 % der Menschen fühlen sich sicherer mit einer gut gestalteten Präsentation.“
                                <br/>
                                <a
                                    className="description-link link"
                                    href="https://www.harvardbusiness.org/powerful-and-effective-presentation-skills-more-in-demand-now-than-ever/"
                                    target="_blank"
                                >
                                    Harvard Business Publishing
                                </a>
                            </p>
                        </Fragment>
                    }
                    textBottom={"Unternehmenspräsentationen sind ein zentrales Kommunikationsmittel für Firmen jeder Größe. Damit können Sie die Geschichte Ihres Unternehmens, seine Mission, Vision und Werte sowie wichtige Erfolge und Geschäftsdaten ansprechend und verständlich präsentieren. Eine gut durchdachte Präsentation kann die Wahrnehmung des Unternehmens signifikant verbessern und als wirkungsvolles Marketingtool dienen."}
                    bgColor={"var(--color-medium-teal-blue)"}
                />

                <section styleName="big-parallax">
                    <h2 className="visuallyHidden">Tandem Marketing & Partners Presentation</h2>
                    <ParallaxEffect
                        image={presentationHallImg}
                        alt="Tandem Marketing & Partners Presentation"
                    />
                </section>

                <ConvincingPresentation />

                <section styleName="strengths">
                    <h2 className="visuallyHidden">Uhren als Investition</h2>
                    <ParallaxEffect
                        image={designedPresentationImg}
                        alt="Tandem Marketing & Partners"
                        children={
                            <div className="container">
                                <b className="parallax-text heading-3" >
                                    Eine gut gestaltete Unternehmenspräsentation ist ein unverzichtbares Werkzeug im Marketingmix jeder Firma.
                                </b>
                            </div>
                        }
                    />
                </section>

                <section>
                    <SectionColor
                        content={
                            <Fragment>
                                <p className="section-text-200">
                                    Sie trägt nicht nur zur Imagepflege und Markenbildung bei, sondern kann auch entscheidend bei der Gewinnung von Neukundinnen, Investoren und Partnerinnen sein. Durch Storytelling, professionelles Design und effektive Datenvisualisierung machen wir Ihre Präsentation zu einem überzeugenden Kommunikationsmittel, das die Stärken und Werte Ihres Unternehmens erfolgreich vermittelt.
                                </p>
                            </Fragment>
                        }
                        textColor="var(--color-white)"
                        backgroundColor="var(--color-medium-teal-blue)"
                    />
                </section>

                <section styleName="section-text">
                    <h3 className="heading-3">Werbeagentur Stuttgart: Starke Partner für Unternehmenspräsentationen</h3>
                    <p>Unsere Marketing-Manager beraten Sie gerne und finden die optimale Lösung für Ihr Unternehmen.</p>
                </section>
            </main>

            <Blog />
            <FeedbackHomePage />
            <Footer />
        </div>
    );
}
