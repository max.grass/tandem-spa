import React, { Fragment } from "react";
import { Icon } from "components/Icon";
import { Hr } from "components/Hr";
import { ListOrderUnorder } from "components/ListOrderUnorder";

import "./WeOffer.css";

export const WeOffer = () => {

    return (
      <section className="section">
        <div className="container-narrow">
          <h2 className="heading-3">Was wir bieten</h2>
        </div>
        <div className="container-narrow grid-hr" styleName="advantages">
          <Hr color="var(--color-medium-teal-blue)" />
          <ListOrderUnorder
            isOrdered={false}
            icon={
              <Icon
                name="checked"
                width="50px"
                height="50px"
                color="var(--color-dark)"
              />
            }
            items={[
              <Fragment>
                <h3 className="heading-4">Unternehmenspräsentationen (Powerpoint)</h3>
                <p>für Geschäftsberichte, Jahreskonferenzen, Investorenmeetings und mehr.</p>
              </Fragment>,
              <Fragment>
                <h3 className="heading-4">Lifestyle-Präsentationen</h3>
                <p>für Firmenevents, die Ihre Marke in einem lebensnahen Kontext präsentieren. Diese Präsentationen erwecken Ihre Marke zum Leben und fördern eine tiefere Verbindung mit dem Publikum.</p>
              </Fragment>,
              <Fragment>
                <h3 className="heading-4">Animationsgestütztes Design</h3>
                <p>das nicht nur informiert, sondern auch inspiriert. Unsere animierten Designs sind darauf ausgelegt, eine bleibende Wirkung zu hinterlassen.</p>
              </Fragment>
            ]}
          />
        </div>
      </section>
    );
};