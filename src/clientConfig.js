export const clientConfig = {
  siteUrl: 'https://www.tandem-m.de/',
  //siteUrl: 'http://dev.tandem2.loc/',
  gCaptchaPubKey: '6LfJfFwUAAAAAA8uQS2_CAknzkNrCAA5-JHsV0XE',
  gMapKey: 'AIzaSyD7XyUlzm7ZctafbHBRvMEj1OKjBksX8eU',
  gtmId: 'GTM-KGS3Z5',
  gaId: 'UA-10984341-1',
  formApiUrl: 'wp-json/contact-form-7/v1/contact-forms/2291/feedback' //2291 - prod, 1396 - dev form ID in backend
};
