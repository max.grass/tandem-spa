import { HomePage } from 'pages/HomePage';
import { ContactPage } from 'pages/ContactPage';
import { AboutPage } from 'pages/AboutPage';
// import { ApproachPage } from 'pages/ApproachPage';
import { MarketingPage } from 'pages/MarketingPage';
import { SeoPage } from 'pages/SeoPage';
import { SocPage } from 'pages/SocPage';
import { WebAnalPage } from 'pages/WebAnalPage';
import { PresentationPage } from 'pages/PresentationPage';
import { ContentMarketingPage } from 'pages/ContentMarketingPage';
import { MailPage } from 'pages/MailPage';
import { DatenschutzPage } from "pages/DatenschutzPage";
import { ImpressumPage } from "pages/ImpressumPage";
// import { JobsPage } from "pages/JobsPage";
import { DrupalPage } from "pages/DrupalPage";
import { MagentoWebshopPage } from "pages/MagentoWebshopPage";
import { WarumMagentoPage } from "pages/WarumMagentoPage";
import { CMSPage } from "pages/CMSPage";
import { ECommercePage } from "pages/ECommerce";
import { B2b } from "pages/B2b";
import { PWA } from "pages/PWA";
import { ImageSourcesPage } from "pages/ImageSourcesPage";
import { WatchDe } from "pages/ReferencePages/WatchDe";
import { SchoolAcademie } from "pages/ReferencePages/SchoolAcademie";
import { MettlerGmbh } from "pages/ReferencePages/MettlerGmbh";
import { Herhsortt } from "pages/ReferencePages/Herhsortt";
import { Drivelock } from "pages/ReferencePages/Drivelock";
import { EightMan } from "pages/ReferencePages/EightMan";
import { Mylede } from "pages/ReferencePages/Mylede";
import { Mymed24 } from "pages/ReferencePages/Mymed24";
import { OnemalX } from "pages/ReferencePages/OnemalX";
import { Gailing } from "pages/ReferencePages/Gailing";
import { Tabacum } from "pages/ReferencePages/Tabacum";
import { Kruschina } from "pages/ReferencePages/Kruschina";
import { Pps } from "pages/ReferencePages/Pps";
import { BlogListPage } from "pages/BlogListPage";
import { BlogArticlePage } from "pages/BlogArticlePage";
import { WebdesignPage } from "pages/WebdesignPage";
import { UxPage } from "pages/UxPage";
import { UsabilityPage } from "pages/UsabilityPage";
import { ResponsivePage } from "pages/ResponsivePage";
import { RelaunchPage } from "pages/RelaunchPage";
import { ReferencePage } from "pages/ReferencePage";
import { FourZeroFour } from "pages/FourZeroFour";

export const routes = [
  {
    path: '/',
    exact: true,
    component: HomePage,
  },
  {
    path: '/werbeagentur-stuttgart/contact.html',
    exact: true,
    component: ContactPage,
  },
  {
    path: '/werbeagentur-stuttgart.html',
    exact: true,
    component: AboutPage,
  },
  // {
  //   path: '/werbeagentur-stuttgart/approach.html',
  //   exact: true,
  //   component: ApproachPage,
  // },
  {
    path: '/marketing-stuttgart.html',
    exact: true,
    component: MarketingPage,
  },
  {
    path: '/marketing-stuttgart/suchmaschinenoptimierung.html',
    exact: true,
    component: SeoPage,
  },
  {
    path: '/marketing-stuttgart/social-media-marketing.html',
    exact: true,
    component: SocPage,
  },
  {
    path: '/marketing-stuttgart/e-mail-marketing.html',
    exact: true,
    component: MailPage,
  },
  {
    path: '/web-analytics.html',
    exact: true,
    component: WebAnalPage,
  },
  {
    path: '/marketing-stuttgart/unternehmenspraesentation.html',
    exact: true,
    component: PresentationPage,
  },
  {
    path: '/pr.html',
    exact: true,
    component: ContentMarketingPage,
  },
  {
    path: '/webdesign-cms/drupal.html',
    exact: true,
    component: DrupalPage,
  },
  {
    path: '/bildquellen.html',
    exact: true,
    component: ImageSourcesPage,
  },
  {
    path: '/magento-webshop.html',
    exact: true,
    component: MagentoWebshopPage,
  },
  {
    path: '/magento-webshop/warum-magento.html',
    exact: true,
    component: WarumMagentoPage,
  },
  {
    path: '/webdesign-cms/content-management-system.html',
    exact: true,
    component: CMSPage,
  },
  {
    path: '/marketing-stuttgart/e-commerce.html',
    exact: true,
    component: ECommercePage,
  },
  {
    path: '/b2b-commerce.html',
    exact: true,
    component: B2b,
  },
  {
    path: '/pwa-react.html',
    exact: true,
    component: PWA,
  },
  {
    path: '/projekte/52-watch-de.html',
    exact: true,
    component: WatchDe,
  },
  {
    path: '/projekte/48-mr-gruppe.html',
    exact: true,
    component: MettlerGmbh,
  },
  {
    path: '/projekte/84-hershortt.html',
    exact: true,
    component: Herhsortt,
  },
  {
    path: '/projekte/75-myle-de.html',
    exact: true,
    component: Mylede,
  },
  {
    path: '/projekte/49-die-deutsche-schulakademie.html',
    exact: true,
    component: SchoolAcademie,
  },
  {
    path: '/projekte/32-drivelock.html',
    exact: true,
    component: Drivelock,
  },
  {
    path: '/projekte/eight-man.html',
    exact: true,
    component: EightMan,
  },
  {
    path: '/projekte/85-mymed-24.html',
    exact: true,
    component: Mymed24,
  },
  {
    path: '/projekte/36-1malx.html',
    exact: true,
    component: OnemalX
  },
  {
    path: '/projekte/gailing.html',
    exact: true,
    component: Gailing
  },
  {
    path: '/projekte/tabacum.html',
    exact: true,
    component: Tabacum
  },
  {
    path: '/projekte/pps.html',
    exact: true,
    component: Pps
  },
  {
    path: '/projekte/kruschina.html',
    exact: true,
    component: Kruschina
  },
  {
    path: '/webdesign-cms.html',
    exact: true,
    component: WebdesignPage,
  },
  {
    path: '/ux.html',
    exact: true,
    component: UxPage,
  },
  {
    path: '/webdesign-cms/usability.html',
    exact: true,
    component: UsabilityPage,
  },
  {
    path: '/webdesign-cms/responsives-webdesign.html',
    exact: true,
    component: ResponsivePage,
  },
  {
    path: '/website-relaunch.html',
    exact: true,
    component: RelaunchPage
  },
  {
    path: '/referenzen.html',
    exact: true,
    component: ReferencePage
  },
  {
    path: '/blog.html',
    exact: true,
    component: BlogListPage,
  },
  {
    path: '/blog/:slug',
    exact: true,
    component: BlogArticlePage,
  },
  {
    path: '/impressum.html',
    exact: true,
    component: ImpressumPage,
  },
  // {
  //   path: '/jobs.html',
  //   exact: true,
  //   component: JobsPage,
  // },
  {
    path: '/datenschutz.html',
    exact: true,
    component: DatenschutzPage,
  },
  {
    path: '*',
    exact: false,
    component: FourZeroFour,
  }
];
