import './assets/global.css';

import React from 'react';
import ReactDom from 'react-dom';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { ScrollToTop } from 'components/ScrollToTop/';

import { routes } from './routes';

if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register('./service-worker.js').then(registration => {
      console.log('SW registered: ', registration);
    }).catch(registrationError => {
      console.log('SW registration failed: ', registrationError);
    });
  });
}

ReactDom.render(
  <BrowserRouter>
    <Switch>
      {routes.map((route, idx) => <Route key={idx} {...route} />)}
    </Switch>
    <ScrollToTop />
  </BrowserRouter>,
  document.getElementById('root'),
);
