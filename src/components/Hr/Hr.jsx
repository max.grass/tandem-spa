import React, {useRef, useEffect} from "react";
import PropTypes from "prop-types";
import gsap from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";

import "./Hr.css";

export const Hr = (props) => {
  const { color } = props;
  const style = {
    "--color": color
  };

  const hrRef = useRef();
  gsap.registerPlugin(ScrollTrigger);

  useEffect(() => {
    gsap.from(hrRef.current, {
      scrollTrigger: {
        trigger: hrRef.current,
        start: "top bottom-=10%",
      },
      opacity: 0,
      width: 0,
      duration: 1,
      ease: "power2.out"
    })
  }, []);

  return <hr styleName="root" style={style} ref={hrRef}/>
};

Hr.propTypes = {
  color: PropTypes.string,
};

Hr.defaultProps = {
  color: "var(--color-cherry)",
}

