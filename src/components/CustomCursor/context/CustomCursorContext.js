import { useState, createContext } from 'react';

const CustomCursorContext = createContext({ cursorType: 'default' });

export const CustomCursorProvider = CustomCursorContext.Provider;

export default CustomCursorContext;