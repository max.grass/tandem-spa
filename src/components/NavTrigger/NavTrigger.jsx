import './NavTrigger.css';

import React from 'react';
import PropTypes from 'prop-types';

export const NavTrigger = (props) => {
  const { isOpen, triggerClick } = props;
  const rootClass= isOpen ? 'root root-open' : 'root';
  
  const handleTriggerClick = () => {
    if (typeof triggerClick === 'function') {
      triggerClick()
    }
  };

  return (
    <button 
      styleName={rootClass}
      type='button'
      onClick={handleTriggerClick} 
      aria-label='Navigation umschalten'
    >
      <span styleName='navBar navBarTop'/>
      <span styleName='navBar navBarMiddle'/>
      <span styleName='navBar navBarBottom'/>
    </button>
  );
}

NavTrigger.propTypes = {
  isOpen: PropTypes.bool,
  triggerClick: PropTypes.func,
};