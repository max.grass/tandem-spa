import "./AccordionTabs.css";

import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';
import MaterialTabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const TabPanel = (props) => {
  const { children, value, index } = props;

  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
    >
      {value === index && (
        <Box p={3.5}>
          {children}
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'grid',
    gridTemplateColumns: '3fr 5fr',
    color: 'inherit',
    backgroundColor: 'inherit',
    transition: 'backgroundColor 0.5s'
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
  },
  tabsFlexContainer: {
    alignItems: 'flex-start',
  },
  tabsIndicator: {
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
  },
  tab: {
    padding: '28px 12px;',
    maxWidth: '100%',
    minWidth: 'auto',
    width: '100%',
    fontFamily: 'Open Sans, Arial, sans-serif',
    fontWeight: 700,
    fontSize: '1.5rem',
    lineHeight: '1.5',
    textTransform: 'initial',
    color: 'rgba(255, 255, 255, 0.5)',
    '& span:first-child': {
      position: 'relative',
      transition: 'all 0.3s ease-out',
      left: 0
    },
  },
  tabSelected: {
    color: 'rgb(255, 255, 255)',
  },
  tabWrapper: {
    alignItems: 'flex-start',
    textAlign: 'left'
  },
  typographyText: {
    marginBottom: '2rem',
    fontFamily: 'Open Sans, Arial, sans-serif',
    fontWeight: 400,
    fontSize: '1.2rem',
    lineHeight: 2,
    textAlign: 'left',
    '& ul' : {
      listStyle: 'none',
      padding: 0,
      margin: 0
    }
  }
}));

export const TabsAbout = (props) => {
  const { backgroundChange } = props;
  const classes = useStyles();
  const [value, setValue] = useState('#404297');

  const handleChange = (event, newValue) => {
    setValue(newValue);
    if (typeof backgroundChange === 'function') {
      backgroundChange(newValue);
    }
  };

  return (
    <div className={classes.root}>
      <MaterialTabs
        classes={{ 
          root: classes.tabs,
          flexContainer: classes.tabsFlexContainer,
          indicator: classes.tabsIndicator,
        }}
        orientation='vertical'
        variant='scrollable'
        value={value}
        onChange={handleChange}
        aria-label='Vertical tabs example'
        
      >
        <Tab
          classes={{ 
            root: classes.tab,
            selected: classes.tabSelected,
            wrapper: classes.tabWrapper,
          }} 
          label='Beratung & Konzeption' {...a11yProps(0)} 
          value='#404297'
        />
        <Tab
          classes={{ 
            root: classes.tab,
            selected: classes.tabSelected,
            wrapper: classes.tabWrapper,
          }}
          label='Gestaltung & Design' {...a11yProps(1)} 
          value='#57327D'
        />
        <Tab
          classes={{ 
            root: classes.tab,
            selected: classes.tabSelected,
            wrapper: classes.tabWrapper,
          }}
          label='Entwicklung & Programmierung' {...a11yProps(2)}
          value='#67266B'
        />
        <Tab
          classes={{ 
            root: classes.tab,
            selected: classes.tabSelected,
            wrapper: classes.tabWrapper, 
          }}
          label='Marketing & PR' {...a11yProps(3)}
          value='#761B59'
        />
      </MaterialTabs>
      <TabPanel value={value} index={'#404297'}>
        <Typography classes={{ body1: classes.typographyText }}>
          <ul>
            <li>Markt- und Konkurrenzanalysen,</li>
            <li>Zielgruppenanalyse,</li>
            <li>Wirtschaftlichkeitsprüfung einzelner Maßnahmen (ROI / ROS),</li>
            <li>Konzeption von Webprojekten,</li>
            <li>Optimierung bestehender Konzepte,</li>
            <li>Website-Check (Usability, Webanalyse, Performance etc.)</li>
          </ul>
        </Typography>
        <Link styleName="button" to={"/web-analytics.html"}>Mehr erfahren</Link>
      </TabPanel>
      <TabPanel value={value} index={'#57327D'}>
        <Typography classes={{ body1: classes.typographyText }}>
          <ul>
            <li>Corporate Design,</li>
            <li>Responsive Webdesign (Websites, Portale, Webshops),</li>
            <li>Redesigns (Modernisierung, Usability-Optimierung etc.),</li>
            <li>Mediengestaltung (Flyer, Portfolios, Broschüren, Kataloge, Schilder u.v.m.)</li>
          </ul>
        </Typography>
        <Link styleName="button" to={"/webdesign-cms.html"}>Mehr erfahren</Link>
      </TabPanel>
      <TabPanel value={value} index={'#67266B'}>
        <Typography classes={{ body1: classes.typographyText }}>
          <ul>
            <li>Implementierung von Webshops,</li>
            <li>Implementierung von Content,</li>
            <li>Management Systemen (CMS),</li>
            <li>Integration von ERP-Systemen,</li>
            <li>Responsive Websites (für alle Endgeräte optimiert),</li>
            <li>Mobile Commerce,</li>
            <li>Optimierung der Ladezeit (Performance)</li>
          </ul>
        </Typography>
        <Link styleName="button" to={"/magento-webshop.html"}>Mehr erfahren</Link>
      </TabPanel>
      <TabPanel value={value} index={'#761B59'}>
        <Typography classes={{ body1: classes.typographyText }}>
          <ul>
            <li>PR-Konzeption,</li>
            <li>Mediaplanung,</li>
            <li>Content Marketing/Social Media Marketing,</li>
            <li>Suchmaschinen-Marketing (SEA, SEO, SMO),</li>
            <li>E-Mail-Marketing,</li>
            <li>Affiliate-Marketing,</li>
            <li>Performance Marketing,</li>
            <li>Display-Advertising</li>
          </ul>
        </Typography>
        <Link styleName="button" to={"/marketing-stuttgart.html"}>Mehr erfahren</Link>
      </TabPanel>
    </div>
  );
}
