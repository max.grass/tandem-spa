import "./AccordionTabs.css";

import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

import { makeStyles } from '@material-ui/core/styles';
import MaterialTabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const TabPanel = (props) => {
  const { children, value, index } = props;

  return (
    <div
      role='tabpanel'
      hidden={value !== index}
      id={`vertical-tabpanel-${index}`}
      aria-labelledby={`vertical-tab-${index}`}
    >
      {value === index && (
        <Box p={3.5}>
          {children}
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `vertical-tab-${index}`,
    'aria-controls': `vertical-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'grid',
    gridTemplateColumns: '3fr 5fr',
    color: 'inherit',
    backgroundColor: 'inherit',
    transition: 'backgroundColor 0.5s'
  },
  tabs: {
    borderRight: `1px solid ${theme.palette.divider}`,
  },
  tabsFlexContainer: {
    alignItems: 'flex-start',
  },
  tabsIndicator: {
    backgroundColor: 'rgba(255, 255, 255, 0.5)',
  },
  tab: {
    padding: '28px 12px;',
    maxWidth: '100%',
    minWidth: 'auto',
    width: '100%',
    fontFamily: 'Open Sans, Arial, sans-serif',
    fontWeight: 700,
    fontSize: '1.5rem',
    lineHeight: '1.5',
    textTransform: 'initial',
    color: 'rgba(255, 255, 255, 0.5)',
    '& span:first-child': {
      position: 'relative',
      transition: 'all 0.3s ease-out',
      left: 0
    },
  },
  tabSelected: {
    color: 'rgb(255, 255, 255)',
  },
  tabWrapper: {
    alignItems: 'flex-start',
  },
  typographyText: {
    marginBottom: '2rem',
    fontFamily: 'Open Sans, Arial, sans-serif',
    fontWeight: 400,
    fontSize: '1rem',
    lineHeight: 2,
    textAlign: 'left',
  }
}));

export const Tabs = (props) => {
  const { backgroundChange } = props;
  const classes = useStyles();
  const [value, setValue] = useState('#404297');

  const handleChange = (event, newValue) => {
    setValue(newValue);
    if (typeof backgroundChange === 'function') {
      backgroundChange(newValue);
    }
  };

  return (
    <div className={classes.root}>
      <MaterialTabs
        classes={{ 
          root: classes.tabs,
          flexContainer: classes.tabsFlexContainer,
          indicator: classes.tabsIndicator,
        }}
        orientation='vertical'
        variant='scrollable'
        value={value}
        onChange={handleChange}
        aria-label='Vertical tabs example'
        
      >
        <Tab
          classes={{ 
            root: classes.tab,
            selected: classes.tabSelected,
            wrapper: classes.tabWrapper,
          }} 
          label='E-Commerce Lösungen' {...a11yProps(0)} 
          value='#404297'
        />
        <Tab
          classes={{ 
            root: classes.tab,
            selected: classes.tabSelected,
            wrapper: classes.tabWrapper,
          }}
          label='Web-Entwicklung' {...a11yProps(1)} 
          value='#57327D'
        />
        <Tab
          classes={{ 
            root: classes.tab,
            selected: classes.tabSelected,
            wrapper: classes.tabWrapper,
          }}
          label='Web-Design &amp; UX' {...a11yProps(2)}
          value='#67266B'
        />
        <Tab
          classes={{ 
            root: classes.tab,
            selected: classes.tabSelected,
            wrapper: classes.tabWrapper, 
          }}
          label='Analytics' {...a11yProps(3)}
          value='#761B59'
        />
        <Tab
          classes={{ 
            root: classes.tab,
            selected: classes.tabSelected,
            wrapper: classes.tabWrapper,
          }} 
          label='Online-Marketing' {...a11yProps(4)}
          value='#8E0A3F'
        />
      </MaterialTabs>
      <TabPanel value={value} index={'#404297'}>
        <Typography classes={{ body1: classes.typographyText }}>
          Wir bieten moderne, zielgruppenorientierte E-Commerce Lösungen, die genau zu Ihren Bedürfnissen passen: Ob SaaS, Magento Webshop oder Progressive Web App.
        </Typography>
        <Typography classes={{ body1: classes.typographyText }}>
          Überlassen Sie uns Konzeption, Design und die Wahl der richtigen Technologie.
        </Typography>
        <Link styleName="button" to={"/marketing-stuttgart/e-commerce.html"}>Mehr erfahren</Link>
      </TabPanel>
      <TabPanel value={value} index={'#57327D'}>
        <Typography classes={{ body1: classes.typographyText }}>
          Hinter Tandem Marketing & Partners steht ein starkes Team aus Frontend- und Backend-Entwicklern. Gemeinsam mit ihnen können wir nahezu jedes Projekt erfolgreich umsetzen.
        </Typography>
        <Typography classes={{ body1: classes.typographyText }}>
          Ob Webportal, Onlineshop, responsive Website oder Progressive Web App – wir verwirklichen Ihre Vision.
        </Typography>
        <Link styleName="button" to="/pwa-react.html">Mehr erfahren</Link>
      </TabPanel>
      <TabPanel value={value} index={'#67266B'}>
        <Typography classes={{ body1: classes.typographyText }}>
          Wir kreieren Onlinepräsenzen, die glücklich machen: Eine positive User Experience erreichen Sie, wenn alle Elemente der Website ein harmonisches Gesamtbild ergeben und den Erwartungen der Nutzer entsprechen.
        </Typography>
        <Typography classes={{ body1: classes.typographyText }}>
          Wichtige Voraussetzungen dafür sind eine gelungene Konzeption und klare Ziele. Wir unterstützen Sie dabei!
        </Typography>
        <Link styleName="button" to="/webdesign-cms.html">Mehr erfahren</Link>
      </TabPanel>
      <TabPanel value={value} index={'#761B59'}>
        <Typography classes={{ body1: classes.typographyText }}>
          Web Analytics sind ein wichtiges Werkzeug, um den Erfolg einer Website sowie aller digitalen Werbemaßnahmen zu kontrollieren.
        </Typography>
        <Typography classes={{ body1: classes.typographyText }}>
          Für Sie behalten wir aktuelle Online-Trends im Blick und unterstützen Sie bei der strategischen Ausrichtung Ihres Geschäfts.
        </Typography>
        <Link styleName="button" to="/web-analytics.html">Mehr erfahren</Link>
      </TabPanel>
      <TabPanel value={value} index={'#8E0A3F'}>
        <Typography classes={{ body1: classes.typographyText }}>
          Was interessiert Ihre Zielgruppe wirklich? Wir finden es heraus und entwickeln eine passgenaue Online Marketing Strategie für Ihr Unternehmen.
        </Typography>
        <Typography classes={{ body1: classes.typographyText }}>
          Online Werbung (SEA), Content-Strategien (SEO) und reichweitenstarke Ideen für Social Media gehören zu unserem täglich Brot.
        </Typography>
        <Link styleName="button" to="/marketing-stuttgart.html">Mehr erfahren</Link>
      </TabPanel>
    </div>
  );
}
