import "./AccordionTabs.css";

import React, { Fragment, useState } from "react";
import { Link } from 'react-router-dom';

import { makeStyles } from "@material-ui/core/styles";
import MaterialAccordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import { Hr } from "components/Hr";

const useStyles = makeStyles((theme) => ({
  accordionRoot: {
    color: 'inherit',
    backgroundColor: 'inherit',
    border: 0,
    boxShadow: 'none',
  },
  accordionSummary: {
    padding: 0,
    color: 'rgba(255, 255, 255, 0.5)',
    backgroundColor: 'inherit',
  },
  accordionSummaryExpanded: {
    color: 'rgba(255, 255, 255)',
  },
  accordionSummaryContent: {
    margin: '10px 0',
  },
  expandIcon: {
    color: '#fff',
  },
  icon : {
    width: 28,
    height: 28,
  },
  typographyTitle: {
    fontFamily: 'Open Sans, Arial, sans-serif',
    fontWeight: 700,
    fontSize: '1.25rem',
    lineHeight: 1.5,
    color: 'inherit',
  },
  typographyBody1: {
    marginBottom: '2rem',
    fontFamily: 'Open Sans, Arial, sans-serif',
    fontWeight: 400,
    fontSize: '1rem',
    lineHeight: 2,
    textAlign: 'left',
  },
  accordionDetails: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    padding: 0,
    color: 'inherit',
    backgroundColor: 'inherit',
  },
}));

export const Accordion = (props) => {
  const { backgroundChange } = props;
  const classes = useStyles();
  const [expanded, setExpanded] = useState('e-commerce');

  const handleChange = (panel, color) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
    if (typeof backgroundChange === 'function') {
      backgroundChange(color);
    }
  };

  return (
    <Fragment>
      <MaterialAccordion
        classes={{ root: classes.accordionRoot }}
        expanded={expanded === 'e-commerce'} 
        onChange={handleChange('e-commerce', '#404297')}>
        <AccordionSummary
          classes={{ 
            root: classes.accordionSummary,
            expanded: classes.accordionSummaryExpanded,
            content: classes.accordionSummaryContent,
            expandIcon: classes.expandIcon,
          }}
          expandIcon={<ExpandMoreIcon classes={{ root: classes.icon }} />}
          aria-controls="e-commerce-content"
          id="e-commerce-header"
        >
          <Typography classes={{ root: classes.typographyTitle }}>
            E-Commerce Lösungen
          </Typography>
        </AccordionSummary>
        <AccordionDetails classes={{ root: classes.accordionDetails }}>
          <div styleName="hr-wrapper">
            <Hr />
          </div>
          <Typography classes={{ body1: classes.typographyBody1 }}>
            Wir bieten moderne, zielgruppenorientierte E-Commerce Lösungen, die genau zu Ihren Bedürfnissen passen: Ob SaaS, Magento Webshop oder Progressive Web App.
          </Typography>
          <Typography classes={{ body1: classes.typographyBody1 }}>
            Überlassen Sie uns Konzeption, Design und die Wahl der richtigen Technologie.
          </Typography>
          <Link styleName="button" to="/marketing-stuttgart/e-commerce.html">Mehr erfahren</Link>
        </AccordionDetails>
      </MaterialAccordion>

      <MaterialAccordion
        classes={{ root: classes.accordionRoot }}
        expanded={expanded === 'web-development'} 
        onChange={handleChange('web-development', '#57327D')}>
        <AccordionSummary
           classes={{ 
            root: classes.accordionSummary,
            expanded: classes.accordionSummaryExpanded,
            content: classes.accordionSummaryContent,
            expandIcon: classes.expandIcon,
          }}
          expandIcon={<ExpandMoreIcon classes={{ root: classes.icon }} />}
          aria-controls="web-development-content"
          id="web-development-header"
        >
          <Typography classes={{ root: classes.typographyTitle }}>
            Web-Entwicklung
          </Typography>
        </AccordionSummary>
        <AccordionDetails classes={{ root: classes.accordionDetails }}>
          <div styleName="hr-wrapper">
            <Hr />
          </div>
          <Typography classes={{ body1: classes.typographyBody1 }}>
            Hinter Tandem Marketing & Partners steht ein starkes Team aus Frontend- und Backend-Entwicklern. Gemeinsam mit ihnen können wir nahezu jedes Projekt erfolgreich umsetzen. 
          </Typography>
          <Typography classes={{ body1: classes.typographyBody1 }}>
            Ob Webportal, Onlineshop, responsive Website oder Progressive Web App – wir verwirklichen Ihre Vision.
          </Typography>
          <Link styleName="button" to="/pwa-react.html">Mehr erfahren</Link>
        </AccordionDetails>
      </MaterialAccordion>

      <MaterialAccordion
        classes={{ root: classes.accordionRoot }}
        expanded={expanded === 'web-design'} 
        onChange={handleChange('web-design', '#67266B')}>
        <AccordionSummary
           classes={{ 
            root: classes.accordionSummary,
            expanded: classes.accordionSummaryExpanded,
            content: classes.accordionSummaryContent,
            expandIcon: classes.expandIcon,
          }}
          expandIcon={<ExpandMoreIcon classes={{ root: classes.icon }} />}
          aria-controls="web-design-content"
          id="web-design-header"
        >
          <Typography classes={{ root: classes.typographyTitle }}>
            Web-Design &amp; UX
          </Typography>
        </AccordionSummary>
        <AccordionDetails classes={{ root: classes.accordionDetails }}>
          <div styleName="hr-wrapper">
            <Hr />
          </div>
          <Typography classes={{ body1: classes.typographyBody1 }}>
            Wir kreieren Onlinepräsenzen, die glücklich machen: Eine positive User Experience erreichen Sie, wenn alle Elemente der Website ein harmonisches Gesamtbild ergeben und den Erwartungen der Nutzer entsprechen.
          </Typography>
          <Typography classes={{ body1: classes.typographyBody1 }}>
            Wichtige Voraussetzungen dafür sind eine gelungene Konzeption und klare Ziele. Wir unterstützen Sie dabei!
          </Typography>
          <Link styleName="button" to="/webdesign-cms.html">Mehr erfahren</Link>
        </AccordionDetails>
      </MaterialAccordion>

      <MaterialAccordion
        classes={{ root: classes.accordionRoot }}
        expanded={expanded === 'analytics'} 
        onChange={handleChange('analytics', '#761B59')}>
        <AccordionSummary
           classes={{ 
            root: classes.accordionSummary,
            expanded: classes.accordionSummaryExpanded,
            content: classes.accordionSummaryContent,
            expandIcon: classes.expandIcon,
          }}
          expandIcon={<ExpandMoreIcon classes={{ root: classes.icon }} />}
          aria-controls="analytics-content"
          id="analytics-header"
        >
          <Typography classes={{ root: classes.typographyTitle }}>Analytics</Typography>
        </AccordionSummary>
        <AccordionDetails classes={{ root: classes.accordionDetails }}>
          <div styleName="hr-wrapper">
            <Hr />
          </div>
          <Typography classes={{ body1: classes.typographyBody1 }}>
            Web Analytics sind ein wichtiges Werkzeug, um den Erfolg einer Website sowie aller digitalen Werbemaßnahmen zu kontrollieren.
          </Typography>
          <Typography classes={{ body1: classes.typographyBody1 }}>
            Für Sie behalten wir aktuelle Online-Trends im Blick und unterstützen Sie bei der strategischen Ausrichtung Ihres Geschäfts.
          </Typography>
          <Link styleName="button" to="/web-analytics.html">Mehr erfahren</Link>
        </AccordionDetails>
      </MaterialAccordion>

      <MaterialAccordion
        classes={{ root: classes.accordionRoot }}
        expanded={expanded === 'online-marketing'} 
        onChange={handleChange('online-marketing', '#8E0A3F')}>
        <AccordionSummary
           classes={{ 
            root: classes.accordionSummary,
            expanded: classes.accordionSummaryExpanded,
            content: classes.accordionSummaryContent,
            expandIcon: classes.expandIcon,
          }}
          expandIcon={<ExpandMoreIcon classes={{ root: classes.icon }} />}
          aria-controls="nline-marketing-content"
          id="online-marketing-header"
        >
          <Typography classes={{ root: classes.typographyTitle }}>
            Online Marketing
          </Typography>
        </AccordionSummary>
        <AccordionDetails classes={{ root: classes.accordionDetails }}>
          <div styleName="hr-wrapper">
            <Hr />
          </div>
          <Typography classes={{ body1: classes.typographyBody1 }}>
            Was interessiert Ihre Zielgruppe wirklich? Wir finden es heraus und entwickeln eine passgenaue Online Marketing Strategie für Ihr Unternehmen.
          </Typography>
          <Typography classes={{ body1: classes.typographyBody1 }}>
            Online Werbung (SEA), Content-Strategien (SEO) und reichweitenstarke Ideen für Social Media gehören zu unserem täglich Brot.
          </Typography>
          <Link styleName="button" to="/marketing-stuttgart.html">Mehr erfahren</Link>
        </AccordionDetails>
      </MaterialAccordion>
    </Fragment>
  );
}