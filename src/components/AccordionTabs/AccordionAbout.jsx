import "./AccordionTabs.css";

import React, { Fragment, useState } from "react";
import { Link } from 'react-router-dom';

import { makeStyles } from "@material-ui/core/styles";
import MaterialAccordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import { Hr } from "components/Hr";

const useStyles = makeStyles((theme) => ({
  accordionRoot: {
    color: 'inherit',
    backgroundColor: 'inherit',
    border: 0,
    boxShadow: 'none',
  },
  accordionSummary: {
    padding: 0,
    color: 'rgba(255, 255, 255, 0.5)',
    backgroundColor: 'inherit',
  },
  accordionSummaryExpanded: {
    color: 'rgba(255, 255, 255)',
  },
  accordionSummaryContent: {
    margin: '10px 0',
  },
  expandIcon: {
    color: '#fff',
  },
  icon : {
    width: 28,
    height: 28,
  },
  typographyTitle: {
    fontFamily: 'Open Sans, Arial, sans-serif',
    fontWeight: 700,
    fontSize: '1.25rem',
    lineHeight: 1.5,
    color: 'inherit',
  },
  typographyBody1: {
    marginBottom: '2rem',
    fontFamily: 'Open Sans, Arial, sans-serif',
    fontWeight: 400,
    fontSize: '1rem',
    lineHeight: 2,
    textAlign: 'left',
  },
  accordionDetails: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    padding: 0,
    color: 'inherit',
    backgroundColor: 'inherit',
  },
}));

export const AccordionAbout = (props) => {
  const { backgroundChange } = props;
  const classes = useStyles();
  const [expanded, setExpanded] = useState('e-commerce');

  const handleChange = (panel, color) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
    if (typeof backgroundChange === 'function') {
      backgroundChange(color);
    }
  };

  return (
    <Fragment>
      <MaterialAccordion
        classes={{ root: classes.accordionRoot }}
        expanded={expanded === 'e-commerce'} 
        onChange={handleChange('e-commerce', '#404297')}>
        <AccordionSummary
          classes={{ 
            root: classes.accordionSummary,
            expanded: classes.accordionSummaryExpanded,
            content: classes.accordionSummaryContent,
            expandIcon: classes.expandIcon,
          }}
          expandIcon={<ExpandMoreIcon classes={{ root: classes.icon }} />}
          aria-controls="e-commerce-content"
          id="Beratung & Konzeption"
        >
          <Typography classes={{ root: classes.typographyTitle }}>
          Beratung & Konzeption
          </Typography>
        </AccordionSummary>
        <AccordionDetails classes={{ root: classes.accordionDetails }}>
          <ul>
            <li>Markt- und Konkurrenzanalysen,</li>
            <li>Zielgruppenanalyse,</li>
            <li>Wirtschaftlichkeitsprüfung einzelner Maßnahmen (ROI / ROS),</li>
            <li>Konzeption von Webprojekten,</li>
            <li>Optimierung bestehender Konzepte,</li>
            <li>Website-Check (Usability, Webanalyse, Performance etc.)</li>
          </ul>
          <Link styleName="button" to={"/web-analytics.html"}>Mehr erfahren</Link>
        </AccordionDetails>
      </MaterialAccordion>

      <MaterialAccordion
        classes={{ root: classes.accordionRoot }}
        expanded={expanded === 'web-development'} 
        onChange={handleChange('web-development', '#57327D')}>
        <AccordionSummary
           classes={{ 
            root: classes.accordionSummary,
            expanded: classes.accordionSummaryExpanded,
            content: classes.accordionSummaryContent,
            expandIcon: classes.expandIcon,
          }}
          expandIcon={<ExpandMoreIcon classes={{ root: classes.icon }} />}
          aria-controls="web-development-content"
          id="Gestaltung & Design-header"
        >
          <Typography classes={{ root: classes.typographyTitle }}>
            Gestaltung & Design
          </Typography>
        </AccordionSummary>
        <AccordionDetails classes={{ root: classes.accordionDetails }}>
          <ul>
            <li>Corporate Design,</li>
            <li>Responsive Webdesign (Websites, Portale, Webshops),</li>
            <li>Redesigns (Modernisierung, Usability-Optimierung etc.),</li>
            <li>Mediengestaltung (Flyer, Portfolios, Broschüren, Kataloge, Schilder u.v.m.)</li>
          </ul>
          <Link styleName="button" to={"/webdesign-cms.html"}>Mehr erfahren</Link>
        </AccordionDetails>
      </MaterialAccordion>

      <MaterialAccordion
        classes={{ root: classes.accordionRoot }}
        expanded={expanded === 'web-design'} 
        onChange={handleChange('web-design', '#67266B')}>
        <AccordionSummary
           classes={{ 
            root: classes.accordionSummary,
            expanded: classes.accordionSummaryExpanded,
            content: classes.accordionSummaryContent,
            expandIcon: classes.expandIcon,
          }}
          expandIcon={<ExpandMoreIcon classes={{ root: classes.icon }} />}
          aria-controls="web-design-content"
          id="web-design-header"
        >
          <Typography classes={{ root: classes.typographyTitle }}>
            Entwicklung & Programmierung'
          </Typography>
        </AccordionSummary>
        <AccordionDetails classes={{ root: classes.accordionDetails }}>
          <ul>
            <li>Implementierung von Webshops,</li>
            <li>Implementierung von Content,</li>
            <li>Management Systemen (CMS),</li>
            <li>Integration von ERP-Systemen,</li>
            <li>Responsive Websites (für alle Endgeräte optimiert),</li>
            <li>Mobile Commerce,</li>
            <li>Optimierung der Ladezeit (Performance)</li>
          </ul>
          <Link styleName="button" to={"/magento-webshop.html"}>Mehr erfahren</Link>
        </AccordionDetails>
      </MaterialAccordion>

      <MaterialAccordion
        classes={{ root: classes.accordionRoot }}
        expanded={expanded === 'analytics'} 
        onChange={handleChange('analytics', '#761B59')}>
        <AccordionSummary
           classes={{ 
            root: classes.accordionSummary,
            expanded: classes.accordionSummaryExpanded,
            content: classes.accordionSummaryContent,
            expandIcon: classes.expandIcon,
          }}
          expandIcon={<ExpandMoreIcon classes={{ root: classes.icon }} />}
          aria-controls="analytics-content"
          id="analytics-header"
        >
          <Typography classes={{ root: classes.typographyTitle }}>Marketing & PR</Typography>
        </AccordionSummary>
        <AccordionDetails classes={{ root: classes.accordionDetails }}>
          <ul>
            <li>PR-Konzeption,</li>
            <li>Mediaplanung,</li>
            <li>Content Marketing/Social Media Marketing,</li>
            <li>Suchmaschinen-Marketing (SEA, SEO, SMO),</li>
            <li>E-Mail-Marketing,</li>
            <li>Affiliate-Marketing,</li>
            <li>Performance Marketing,</li>
            <li>Display-Advertising</li>
          </ul>
          <Link styleName="button" to={"/marketing-stuttgart.html"}>Mehr erfahren</Link>
        </AccordionDetails>
      </MaterialAccordion>
    </Fragment>
  );
}