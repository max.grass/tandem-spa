import './AccordionTabs.css';

import React, { useState, useRef } from 'react';

import { Accordion } from './Accordion';
import { AccordionAbout } from './AccordionAbout';
import { Tabs } from './Tabs';
import { TabsAbout } from './TabsAbout';

export const AccordionTabs = props => {
  const {
    target
  } = props;

  let header = useRef(null);
  let title = useRef(null);

  const [backgroundColor, setBackgroundColor] = useState("var(--color-gigas)")

  const handleBackgroundChange = (color) => {
    setBackgroundColor(color);
  }

  return (
    <section>
      <div styleName="content" style={{backgroundColor: backgroundColor }}>
        <div styleName="accordion">
          <div className="container-narrow">
            {target == "main" ? (
              <Accordion backgroundChange={handleBackgroundChange} />
            ) : (
              <AccordionAbout backgroundChange={handleBackgroundChange} />
            )}
          </div>
        </div>
        <div styleName='tabs'>
          <div className={target == "main" ? 'container-narrow' : 'container'}>
            {target == "main" ? (
              <Tabs backgroundChange={handleBackgroundChange} />
            ) : (
              <TabsAbout backgroundChange={handleBackgroundChange} />
            )}
          </div>
        </div>
      </div>
    </section>
  );
}