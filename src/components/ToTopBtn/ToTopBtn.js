
import React, { useState } from "react";
import { Link, useLocation } from "react-router-dom";

import './ToTopBtn.css';

export const ToTopBtn = () => {

    const [visible, setVisible] = useState(false);
    const location = useLocation()
    const isContacts = location.pathname == '/contact';

    const toggleVisible = () => {
        const scrolled = document.documentElement.scrollTop;
        if (scrolled > 300){
            setVisible(true)
        } 
        else if (scrolled <= 300){
            setVisible(false)
        }
    };

    const scrollToTop = () =>{
        window.scrollTo({
            top: 0, 
            behavior: 'smooth'
        });
    };

    window.addEventListener('scroll', toggleVisible);

    return (
        <div
            styleName={`root ${visible ? 'shown': ''}`}
        >
            { !isContacts ? (
                <Link
                    styleName="contacts"
                    aria-label='Navigation contacts'
                    to={'/werbeagentur-stuttgart/contact.html'}
                >
                </Link>
            ) : null }
            <button
                styleName="top"
                aria-label='Navigation top'
                onClick={() => scrollToTop()}
            >

            </button>
        </div>
    )
}