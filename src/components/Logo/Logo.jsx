import React from 'react';
import { Link } from 'react-router-dom';

import svg from './logo.svg';
import './Logo.css';

export const Logo = () => {

  return (
    <Link styleName='root' to='/'>
      <img width='256' height='60' src={svg} alt='Tandem-Marketing- und Partner-Logo'/>
    </Link>
  );
}