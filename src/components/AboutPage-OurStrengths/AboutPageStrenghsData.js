import React from "react";

import {
  IconConsulting,
  IconDesign,
  IconProgramming,
  IconMarketing,
} from "components/Icons";

export const data = [
  {
    title: "Beratung & Konzeption",
    content: [
      "Markt- und Konkurrenzanalysen",
      "Zielgruppenanalyse",
      "Wirtschaftlichkeitsprüfung einzelner Maßnahmen (ROI / ROS)",
      "Konzeption von Webprojekten",
      "Optimierung bestehender Konzepte",
      "Website-Check (Usability, Webanalyse, Performance etc.)",
    ],
    figureClass: "circle",
    icon: <IconConsulting 
      primaryColor="var(--color-blue-marguerite)"
      secondaryColor="var(--color-white)"
      style={{"gridArea": "icon"}}
    />
  },
  {
    title: "Gestaltung & Design",
    content: [
      "Corporate Design",
      "Webdesign (Websites, Portale, Webshops)",
      "Responsive Webdesign",
      "Redesigns (Modernisierung, Usability-Optimierung etc.)",
      "Mediengestaltung (Flyer, Portfolios, Broschüren, Kataloge, Schilder u.v.m.)",
    ],
    figureClass: "triangle",
    icon: <IconDesign 
      primaryColor="var(--color-blue-marguerite)"
      secondaryColor="var(--color-white)"
    />
  },
  {
    title: "Entwicklung & Programmierung",
    content: [
      "Implementierung von Webshops",
      "Implementierung von Content",
      "Management Systemen (CMS)",
      "Integration von ERP-Systemen",
      "Responsive Websites (für alle Endgeräte optimiert)",
      "Mobile Commerce",
      "Optimierung der Ladezeit (Performance)",
    ],
    figureClass: "square",
    icon: <IconProgramming
      primaryColor="var(--color-blue-marguerite)"
      secondaryColor="var(--color-white)"
    />
  },
  {
    title: "Marketing & PR",
    content: [
      "PR-Konzeption",
      "Mediaplanung",
      "Content Marketing",
      "Social Media Marketing",
      "Suchmaschinen-Marketing (SEA, SEO, SMO)",
      "E-Mail-Marketing",
      "Affiliate-Marketing",
      "Performance Marketing",
      "Display-Advertising",
    ],
    figureClass: "circle",
    icon: <IconMarketing primaryColor="var(--color-blue-marguerite)"
    secondaryColor="var(--color-white)"/>,
  },
];
