import React from "react";

import {
    UtilityIcon1,
    UtilityIcon2
} from "components/Icons";

export const data = [
  {
    title: "Analyse:",
    content: [
      "tiefgehende Analyse des Conversion-Pfads, um diesen zu optimieren",
      "qualifizierten Usability-Test Ihrer Website, um mehr Kunden zu begeistern",
      "Review Ihrer Google Ads, um mehr Kunden zu erreichen"
    ],
    figureClass: "circle",
    icon: <UtilityIcon1/>
  },
  {
    title: "Optimierungsvorschläge",
    content: [
      "Tipps zur Verbesserung von Inhalten: Texte, Bilder, Videos ",
      "Tipps zur optischen Verbesserung: Layout, User Interface, Aufzählungslisten, Buttondesigns, Webformulare und weitere Conversion-Elemente (auch für mobile Endgeräte)",
      "Optimierungsvorschläge für Werbeanzeigen bzw. -formate",
      "Vorschläge und Ideen, wie Sie Ihre Conversion-Rate verbessern können"
    ],
    figureClass: "triangle",
    icon: <UtilityIcon2/>
  }
];
