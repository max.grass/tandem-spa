import React from "react";

import {
    UXIcon1,
    UXIcon2,
    UXIcon3,
    UXIcon4,
    UXIcon5,
    UXIcon6,
    UXIcon7
} from "components/Icons";

export const data = [
  {
    title: "Verwendbar: Kann man es benutzen?",
    content: [
      "Eine der grundlegendsten Anforderungen an eine gute UX besteht darin, das zu erreichen, was Nutzer*innen sich vorgenommen haben. Wenn sie das Produkt nicht wie gewünscht verwenden können, ist es für sie unbrauchbar."
    ],
    figureClass: "circle",
    icon: <UXIcon1/>
  },
  {
    title: "Auffindbar: Findet man die Inhalte?",
    content: [
      "Es ist auch wichtig, die benötigten Informationen zu finden: Ist das Navigationsmenü einfach und intuitiv? Befindet sich die Suchleiste dort, wo sie erwartet wird? Wenn Nutzer*innen darüber nachdenken müssen, wie sie das Gesuchte finden, fehlt die UX. Dies gilt auch für die ursprüngliche Suche nach einem Produkt – sei es über eine Suchmaschine oder auf anderem Weg."
    ],
    figureClass: "circle",
    icon: <UXIcon2/>
  },
  {
    title: "Nützlich: Dient es den Kundenbedürfnissen?",
    content: [
      "Ein Produkt kann wunderschön gestaltet und einfach zu bedienen sein. Wenn es jedoch kein Bedürfnis deckt, werden Nutzer*innen sich nicht dafür interessieren."
    ],
    figureClass: "circle",
    icon: <UXIcon3/>
  },
  {
    title: "Erwünscht: Möchten Kund*innen es nutzen?",
    content: [
      "Wenn das Design eines Produkts intuitiv und angenehm zu bedienen ist, werden Ihre Kund*innen es verwenden wollen. Oder etwa nicht? Selbst das nützlichste und funktionalste Produkt bietet eine schlechte Benutzererfahrung, wenn es langweilig ist oder keine Anreize bietet, es zu verwenden."
    ],
    figureClass: "circle",
    icon: <UXIcon4/>
  },
  {
    title: "Wertvoll: Finden Ihre Kund*innen es wertvoll?",
    content: [
      "Spart das Produkt Zeit oder Geld? Hilft es Benutzer*innen, persönliche oder berufliche Ziele zu erreichen? Was auch immer der Maßstab ist, ein Produkt sollte einen Mehrwert bieten. Dabei zählt die subjektive Empfindung der Kund*innen."
    ],
    figureClass: "circle",
    icon: <UXIcon5/>
  },
  {
    title: "Glaubwürdig: Vertrauen die Kund*innen Ihnen?",
    content: [
      "Vertrauen ist sehr wichtig. Wenn Nutzer*innen einem Anbieter nicht vertrauen, geben sie auch keine Kreditkarteninformationen oder andere persönliche Daten an, zum Beispiel um einen Kauf zu tätigen."
    ],
    figureClass: "circle",
    icon: <UXIcon6/>
  },
  {
    title: "Barrierefrei: Haben Benutzer*innen Zugriff auf Ihre Inhalte?",
    content: [
      "Eine Voraussetzung für großartige UX ist, dass alle Nutzer*innen die angebotenen Waren oder Dienstleistungen verwenden könne. Auch Benutzer*innen mit Behinderungen sollten ihr Ziel effektiv, effizient und zufriedenstellend erreichen können."
    ],
    figureClass: "circle",
    icon: <UXIcon7/>
  },
];
