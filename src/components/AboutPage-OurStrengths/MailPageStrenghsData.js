import React from "react";

import {
  IconCheckWindow,
  IconGear,
  IconRoundCheck,
  IconPieChart,
  IconEmail,
} from "components/Icons";

export const data = [
  {
    title: "… die passende E-Mail-Marketing-Software zu finden",
    content: [
      "Wir beraten Sie bei der Wahl des richtigen Tools und beziehen dabei Kosten, Abrechnungsmodell, Versandkapazität und Unternehmenssitz (Stichwort: Datenschutz) ein",
      "Wir setzen bei unseren Kunden derzeit auf das deutsche E-Mail Marketing-Tool Cleverreach und Mautic, eine open source Software zur Marketing Automation. So lassen sich Empfängergruppen segmentieren und Kampagnen optimal auf die Bedürfnisse der Zielgruppe abstimmen",
    ],
    figureClass: "triangle",
    icon: <IconCheckWindow 
      primaryColor="var(--color-cherry-dark)"
      secondaryColor="var(--color-white)"
      style={{"gridArea": "icon"}}
    />
  },
  {
    title: "… Ihre Empfängerliste aufzubauen",
    content: [
      "Auf Wunsch integrieren wir Newslettermodule auf Ihrer Webseite oder in Ihrem Onlineshop, sodass sich interessierte Kund*innen einfach anmelden können",
      "Der Double-Opt-In – die doppelte Einverständniserklärung – stellt die rechtsgültige Anmeldung sicher",
      "Gerade im E-Commerce können Gutscheine beim Aufbau des Verteilers helfen. Auch durch hochwertige Inhalte wie Checklisten oder Whitepapers interessieren Sie Ihre Zielgruppe für Ihr Angebot",
      "Gemeinsam erarbeiten wir eine individuelle Strategie zum Aufbau Ihres Verteilers",
    ],
    figureClass: "circle",
    icon: <IconGear 
      primaryColor="var(--color-cherry-dark)"
      secondaryColor="var(--color-white)"
    />
  },
  {
    title: "… nutzerorientierte E-Mail-Kampagnen zu erstellen",
    content: [
      "Unsere Designer*innen und Entwickler *innen erstellen eine benutzerdefinierte Newletter-Vorlage, die Ihrem Corporate Design und den Bedürfnissen Ihrer Zielgruppe entspricht",
      "Besonders wichtig ist dabei die Optimierung für mobile Endgeräte, da mittlerweile mehr als 50 Prozent der E-Mails auf Smartphones angezeigt werden"
    ],
    figureClass: "square",
    icon: <IconRoundCheck
      primaryColor="var(--color-cherry-dark)"
      secondaryColor="var(--color-white)"
    />
  },
  {
    title: "… zu Planen und Automatisieren",
    content: [
      "Unsere E-Mail-Marketing-Expert*innen analysieren Ihre Abonnent*innen und planen auf dieser Basis umfassende Mailing-Kampagnen",
      "Je besser wir Ihr Unternehmen, Ihre Ziele und die Zielgruppe kennenlernen, desto effizienter werden die Mailings: So lassen sich Abonnentengruppen nach Vorlieben, Geografie oder gekauften Produkten segmentieren und mit personalisierten und für sie relevanten Inhalten versorgen",
      "Durch automatisierte Kampagnen können Sie Ihren Abonnenten ohne zusätzlichen Aufwand sofort einen Mehrwert bieten",
    ],
    figureClass: "circle",
    icon: <IconPieChart primaryColor="var(--color-cherry-dark)"
    secondaryColor="var(--color-white)"/>,
  },
  {
    title: "… zu Analysieren und Optimieren",
    content: [
      "Wir behalten die Leistung von laufenden E-Mail-Marketing-Kampagnen über Analysetools stets im Blick – für das Wachstum und den Erfolg Ihres Unternehmens",
      "Die wichtigsten Kennzahlen im E-Mail Marketing sind die Zustellrate, Öffnungsrate, Klickrate und Conversion Rate. Auch Abmeldungen und Bounces liefern wertvolle Informationen über den Erfolg von Kampagnen",
      "Ein Boost für die Conversion Rate sind z.B. E-Mails mit Rabatten und Sonderangeboten oder Erinnerungen für Warenkorbabbrecher*innen",
    ],
    figureClass: "triangle",
    icon: <IconEmail primaryColor="var(--color-cherry-dark)"
    secondaryColor="var(--color-white)"/>,
  },
];
