import React from "react";

import {
    RelaunchIcon1,
    RelaunchIcon2,
    RelaunchIcon3,
    RelaunchIcon4
} from "components/Icons";

export const data = [
  {
    title: "Design, Funktionen, Vor- und Nachteile",
    content: [],
    figureClass: "circle",
    icon: <RelaunchIcon1/>
  },
  {
    title: "Zahlenwerte",
    content: [
      "Google Analytics",
      "Search Console",
      "SEMRush, SE Ranking"
    ],
    figureClass: "triangle",
    icon: <RelaunchIcon2/>
  },
  {
    title: "Porträt der Zielgruppe",
    content: [
      "Technologien, Geräte",
      "Präferenzen",
      "Verhalten"
    ],
    figureClass: "square",
    icon: <RelaunchIcon3/>
  },
  {
    title: "Technisches Audit",
    content: [
      "Server",
      "CMS",
      "Ladezeit"
    ],
    figureClass: "circle",
    icon: <RelaunchIcon4/>
  }
];
