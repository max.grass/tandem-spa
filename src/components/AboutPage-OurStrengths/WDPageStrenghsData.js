import React from "react";

import {
  IconCheckWindow,
  IconGear,
  IconRoundCheck,
  IconPieChart,
  IconEmail,
} from "components/Icons";

export const data = [
  {
    title: "Konzeption",
    content: [
      "Sie wünschen sich eine kreative, ansprechende und erfolgreiche Webpräsenz? Auf Basis Ihrer Informationen und unserer Analysen erarbeiten wir die Ziele der Website, definieren die Zielgruppe und erstellen ein Konzept. Auch die Entscheidung für ein bestimmtes Content Management System (CMS) wie Drupal, Magento oder Wordpress fällt in dieser Phase. Die Optimierung bestehender Inhalte bzw. das Texten aussagekräftiger Web-Contents sollte jetzt starten – unsere Expert*innen im Bereich Content Marketing unterstützen Sie gern, auch bei der Bewertung relevanter Keywords.",
      "Steht der Plan, gehen wir über zur Designphase"
    ],
    figureClass: "triangle",
    icon: <IconCheckWindow 
      primaryColor="var(--color-gigas)"
      secondaryColor="var(--color-white)"
      style={{"gridArea": "icon"}}
    />
  },
  {
    title: "Design",
    content: [
      "Die Konzeption bietet den Rahmen für die Gestaltung Ihrer neuen Website. Unsere Webdesigner*innen erstellen Ihnen auf Wunsch zwei oder mehr Webdesign-Entwürfe. Bei Bedarf werden die Screendesigns in Feedbackschleifen angepasst. Bereits während der Designphase arbeiten unsere Webdesigner*innen Hand in Hand mit den Entwickler*innen, um die optimale technische Umsetzbarkeit in puncto Geschwindigkeit und SEO sicher zu stellen.",
      "Auf das Design folgt die Programmierung."
    ],
    figureClass: "circle",
    icon: <IconGear 
      primaryColor="var(--color-gigas)"
      secondaryColor="var(--color-white)"
    />
  },
  {
    title: "Programmierung",
    content: [
      "In der Programmierungsphase schaffen unsere Template-Entwickler*innen interaktive Templates aus den Designentwürfen, die mittels CSS, HTML, PHP, MySQL und verschiedenen Frameworks umgesetzt werden. Ist das CMS der Wahl installiert, wird das Design übertragen und für mobile Geräte optimiert. Bereits in dieser Phase wird laufend geprüft, ob die Website auf allen Endgeräten (Smartphone, Tablet, PC etc.), in den gängigen Browsern und auf unterschiedlichen Systemen reibungslos läuft. Unsere Webprojekte werden grundsätzlich nach geltenden Standards programmiert."
    ],
    figureClass: "square",
    icon: <IconRoundCheck
      primaryColor="var(--color-gigas)"
      secondaryColor="var(--color-white)"
    />
  },
  {
    title: "Inhalte einpflegen & finale Prüfung",
    content: [
      "Ist die neue Website fertig umgesetzt, geht es an das Einpflegen der Inhalte. Die Qualität der Texte und Fotos ist dabei von höchster Relevanz. Das Publishing übernehmen entweder wir für Sie oder Sie machen es selbst. Auf Wunsch schulen wir Ihre Mitarbeiter*innen vorher im Umgang mit dem neuen Content Management System.",
      "Sind die Inhalte eingepflegt, beginnt die finale Prüfung: Werden Bilder, Videos, Slider etc. auf allen Endgeräten gut angezeigt? Funktionieren alle Formulare, z.B. zur Newsletteranmeldung? Gibt es eine Datenschutzerklärung?",
      "Sind alle Fragen geklärt, steht dem Onlinegang nichts mehr im Wege."
    ],
    figureClass: "circle",
    icon: <IconPieChart primaryColor="var(--color-gigas)"
    secondaryColor="var(--color-white)"/>,
  },
  {
    title: "Onlinegang",
    content: [
      "Sobald Sie Ihre Freigabe gegeben haben, wird die Website von unserem Entwicklungsserver auf Ihren Live Server übertragen. Nach dem Onlinegang begleiten wir die digitale Transformation Ihres Unternehmens weiterhin und unterstützen Sie u.a. in folgenden Bereichen:",
      "- Content Marketing & Website-Pflege",
      "- Website-Support",
      "- Analyse & Kontrolle",
      "- PlugIns & Softwareentwicklung",
      "- Strategie & Online Marketing"
    ],
    figureClass: "triangle",
    icon: <IconEmail primaryColor="var(--color-gigas)"
    secondaryColor="var(--color-white)"/>,
  },
];
