import React from "react";

import { Hr } from "components/Hr";
import { ListWithIcons } from "components/ListWithIcons";

import "./AboutPageStrengths.css";

export const AboutPageStrengths = (props) => {
  
  const { header, data, hrColor } = props;

  return (
    <section styleName="root">
      <div styleName="container">
        <h2 styleName="title">{header}</h2>
        <div styleName="hr-wrapper">
          <Hr color={hrColor}/>
        </div>
        <div styleName="list">
          <ListWithIcons 
            data={data}
            paddingTop="116px"
            textColor= "var(--color-dark)"
            backgroundColor="var(--color-white)"
            iconSize="90px"
            iconTop="15px"
            iconLeft="0"
            figureSize="80px"
            figureColor="var(--color-hint-of-red)"
          />
        </div>
      </div>
    </section>
  );
}