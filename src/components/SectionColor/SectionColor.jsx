import React from "react";
import PropTypes from "prop-types";

import "./SectionColor.css";

export const SectionColor = (props) => {
  const { textColor, backgroundColor, content } = props;
  const style = {
    "--text-color": textColor,
    "--background-color": backgroundColor,
  }

  return (
    <div styleName="root" style={style}>
      <div className="container-narrow">
        {content}
      </div>
    </div>
  );
};

SectionColor.propTypes = {
  textColor: PropTypes.string,
  backgroundColor: PropTypes.string,
  content: PropTypes.node.isRequired,
};

SectionColor.defaultProps = {
  textColor: "var(--color-white)",
  backgroundColor: "var(--color-cherry)",
};
