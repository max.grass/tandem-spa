import ppsPerson from './photos/ppsPerson.jpg';
import ppsPreview from './photos/ppsPreview.jpg';
import ppsLogo from './photos/logo-pps-color.svg';

import watchPerson from './photos/watchPerson.jpg';
import watchPreview from './photos/watchPreview.jpg';
import watchLogo from './photos/watchLogo.svg';

import mettlerPerson from './photos/mettlerPerson2.jpg';
import mettlerPreview from './photos/mettlerPreview.jpg';
import mettlerLogo from './photos/mettlerLogo.png';

import gailingPerson from './photos/gailingPerson.jpg';
import gailingPreview from './photos/gailingPreview.jpg';
import gailingLogo from './photos/gailingLogo.png';

import tabacumPreview from './photos/tabacumPreview.jpg';
import tabacumLogo from './photos/tabacumLogo.png';
import tabacumPerson from './photos/tabacumPerson.jpg';

import myLePerson from './photos/myLePerson.jpg';
import myLeLogo from './photos/myLeLogo.svg';
import myLePreview from './photos/myLePreview.jpg';

import kruschinaPerson from './photos/kruschinaPreview.jpg';
import kruschinaLogo from './photos/kruschinaLogo.svg';
import kruschinaPreview from './photos/kruschinaPreview.jpg';

export const data = [
    {
        "personPhoto": watchPerson,
        "personName": "Ralf Häffner",
        "personTitle": "Inhaber",
        "projectLink": "/projekte/52-watch-de.html",
        "previewPhoto": watchPreview,
        "logo": watchLogo,
        "textColumn": 2,
        "text": "<p>Seit über 13 Jahren arbeiten wir partnerschaftlich zusammen und haben während dieser Zeit bemerkenswerte Fortschritte erzielt. Wir haben nicht nur den technologischen Wandel von Perl auf M1 erfolgreich durchgeführt, sondern auch mehrere Upgrades und Relaunches erfolgreich umgesetzt. Ein herausragender Meilenstein war die gemeinsame Umsetzung der PWA-Erweiterung inklusive Magento 2-Upgrade in Zusammenarbeit mit Tandem.</p><p>Die Zusammenarbeit mit Andreas Schmidt und seinem Tandem-Team bereitet uns Freude. Ihre Fachkompetenz und ihr Gespür für zukünftige Trends haben uns beeindruckt. Gemeinsam haben wir einen fortschrittlichen Onlineshop gestaltet, der sowohl im Design als auch in der technischen Umsetzung wegweisend ist.</p><p>Unser Backend wurde durch maßgeschneiderte Erweiterungen perfekt auf unsere Anforderungen angepasst. Tandem hat eine individuelle Lösung geschaffen, die wir von keiner anderen Agentur zugetraut hätten.</p><p>Neben der Optimierung unserer Webseite und unseres Backends betreut Tandem auch all unsere Marketing-Aktivitäten. Wöchentliche Blogbeiträge, Videoinhalte, Newsletter, Inhalte für Plattformen wie Youtube und Instagram sowie sämtliche Designprojekte werden zeitnah und zur vollsten Zufriedenheit umgesetzt.</p><p>Die Zusammenarbeit mit Andreas Schmidt und seinem Team erfüllt uns mit Freude und wir freuen uns auf kommende Projekte!</p>"
    },
    {
        "personPhoto": ppsPerson,
        "personName": "Markus Göbbels",
        "personTitle": "PPS CTO",
        "projectLink": "/projekte/pps.html",
        "previewPhoto": ppsPreview,
        "logo": ppsLogo,
        "textColumn": 3,
        "text": "Mit Tandem haben wir einen innovativen und kompetenten Partner gewinnen können.<br>In sehr vielen gemeinsamen Projekten konnten wir eine starke Vertrauensbasis auf Augenhöhe entwickeln.<br>Auch die zukunftsorientierte Entwicklung unserer Onlinedienste wird stark durch die Kreativität von Tandem beflügelt."
    },
    {
        "personPhoto": mettlerPerson,
        "personName": "Michael Mettler",
        "personTitle": "Geschäftsführer (CEO)",
        "projectLink": "/projekte/48-mr-gruppe.html",
        "previewPhoto": mettlerPreview,
        "logo": mettlerLogo,
        "textColumn": 2,
        "text": "Wir arbeiten seit über 10 Jahren mit Tandem Marketing zusammen. Neben kreativen Ansätzen für unseren digitalen Unternehmensauftritt, überzeugen Herr Schmidt und sein Team mit weitsichtigen Ideen zur Digitalisierung unserer Geschäftsprozesse. "
    },
    {
        "personPhoto": gailingPerson,
        "personName": "Sabrina & Norman Gailing",
        "personTitle": "Geschäftsführer",
        "projectLink": "/projekte/gailing.html",
        "previewPhoto": gailingPreview,
        "logo": gailingLogo,
        "textColumn": 3,
        "text": "Die Zusammenarbeit mit Tandem hat uns kontinuierlich durch kreative und zukunftsorientierte Strategien beeindruckt. Dank ihrer professionellen Betreuung unseres Webshops und ihrer raschen Reaktionszeit sind sie für uns ein essenzieller Partner geworden. Im Bereich des Online-Marketings unterstützt uns Tandem umfassend – von SEO über E-Mail Marketing und Videografie bis hin zu Social Media Strategien."
    },
    {
        "personPhoto": tabacumPerson,
        "personName": "Siegfried Schäuble",
        "personTitle": "Tabacum - La Casa del Habanos",
        "projectLink": "",
        "previewPhoto": tabacumPreview,
        "logo": tabacumLogo,
        "textColumn": 1,
        "text": "Mit Tandem habe ich einen kompetenten und zuverlässigen Partner gefunden. Mein Webshop wird nach meinen Wünschen und Ansprüchen immer bestens betreut. Bei Problemen erhalten wir immer schnelle Antwort und Lösungen. Wir freuen uns weiterhin auf so gute Betreuung und Zusammenarbeit."
    },
    {
        "personPhoto": myLePerson,
        "personName": "Marion Mohr",
        "personTitle": "Vorstandsmitglied BDS LE e.V.",
        "projectLink": "/projekte/75-myle-de.html",
        "previewPhoto": myLePreview,
        "logo": myLeLogo,
        "textColumn": 2,
        "text": "Vor 7 Jahren hat der Stadtverband mit Tandem den Schritt zu einem digitalen Online-Marktplatz für die lokalen Betriebe unserer Großen Kreisstadt Leinfelden-Echterdingen gewagt. Seit der ersten Stunde haben wir mit dem Team um Herrn Schmidt eine professionelle Agentur zur Entwicklung und Programmierung an der Seite. Herr Schmidt war für uns mit seinem extrem fundierten technischen Wissen essentiell. Seine Erfahrung im Bereich Digitaler Marktplatz ist unbezahlbar. Tandem ist uns als Fullservice-Dienstleister für digitale Medien ein starker Partner, ohne den wir unseren Marktplatz nie hätten in der Form und Schlagkraft realisieren und seit über 6 Jahren erfolgreich betreiben können."
    },
    // {
    //     "personPhoto": kruschinaPerson,
    //     "personName": "...",
    //     "personTitle": "...",
    //     "projectLink": "/projekte/kruschina.html",
    //     "previewPhoto": kruschinaPreview,
    //     "logo": kruschinaLogo,
    //     "textColumn": 1,
    //     "text": "..."
    // }
]
