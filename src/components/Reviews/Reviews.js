import React from 'react';
import { Link } from 'react-router-dom';

import { data } from "./data";
import "./Reviews.css";

const renderReview = (item, index) => {
    return (    
        <div
            key={`review-${index}`}
            styleName={`reviewWrp ${!index ? 'reviewWrpWatch': ''}`}
        >
            <div styleName="descrWrp" style={{gridColumn: item.textColumn}}>
                <img styleName="logo" src={item.logo}/>
                <p dangerouslySetInnerHTML={{__html: item.text}} />
            </div>
            <div styleName="personWrp">
                <img styleName="personPhoto" src={item.personPhoto}/>
                <div styleName="personInfo">
                    <p styleName="personName">{item.personName}</p>
                    <p styleName="personTitle">{item.personTitle}</p>
                </div>
            </div>
            {item.projectLink &&
            <Link
                to={item.projectLink}
                styleName="linkWrp"
            >
                <img styleName="preview" src={item.previewPhoto}/>
                <span styleName="linkText">
                    <span>Projekt ansehen</span>
                </span>
            </Link>
            }
            {!item.projectLink &&
            <div styleName="linkWrp">
                <img styleName="preview" src={item.previewPhoto}/>
            </div>
            }
        </div>
    );
}

export const Reviews = (props) => {

    return (
        <div styleName='root'>
            <div className='container'>
                <h2 className='heading-3' styleName='title'>Unsere Kunden über uns</h2>
            </div>
            { data.map((item, i) => renderReview(item, i) ) }
        </div>
    )
}
