import React from "react";
import PropTypes from "prop-types";

import sprite from "assets/images/sprite.svg";

import "./Icon.css";

export const Icon = (props) => {
  const { 
    name,
    color,
    secondaryColor,
    width,
    height,
    figure,
    figureColor,
  } = props;
  
  const style = {
    "--width": width,
    "--height": height,
    "--primary-color": color,
    "--secondary-color": secondaryColor,
    "--figure-color": figureColor, 
  };

  const figureClass = !figure 
    ? "no-figure"
    : figure;

  return (
    <figure styleName={`root ${figureClass}`} style={style}>
      <svg>
        <use xlinkHref={`${sprite}#${name}`}></use>
      </svg>
    </figure>
  );
}

Icon.propTypes = {
  name: PropTypes.string.isRequired,
  color: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
  figure: PropTypes.oneOf([
    "square",
    "circle",
    "triangle-bottom",
    "triangle-top",
    "triangle-top-right",
    null
  ]),
  figureColor: PropTypes.string,
};

Icon.defaultProps = {
  name: "lamp",
  color: "var(--color-cherry)",
  secondaryColor: "var(--color-white)",
  width: "88px",
  height: "88px",
  figure: null,
  figureColor: "var(--color-hint-of-red)"
};
