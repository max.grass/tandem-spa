import React from "react";

import {
  IconConsulting,
  IconConception,
  IconDesign,
  IconProgramming,
  IconSupport
} from "components/Icons";

export const samplesData = [
  {
    title: "Beratung",
    content: "Vor Projektstart beraten wir Sie eingehend, gehen auf Ihre Wünsche und Bedürfnisse ein und empfehlen Ihnen erste Lösungsmöglichkeiten.",
    figureClass: "circle",
    icon: <IconConsulting />,
  },
  {
    title: "Konzeption",
    content: "Nach dem Briefing sowie der eingehenden Konkurrenz- und Marktanalyse entwickeln wir ein stimmiges Konzept, das optimal auf Ihre Zielgruppe ausgerichtet ist.",
    figureClass: "square",
    icon: <IconConception />,
  },
  {
    title: "Design",
    content: "Die komplexe Gestaltung erfolgt dabei in einzelnen Schritten – von den ersten Skizzen auf Papier bis hin zur onlinetauglichen Digitalisierung.",
    figureClass: "triangle",
    icon: <IconDesign />,
  },
  {
    title: "Programmieren",
    content: "Unsere Entwickler wenden ausschließlich zukunftsfähige Web-Technologien an, die in moderne Content Management Systeme (CMS) implementiert werden.",
    figureClass: "square",
    icon: <IconProgramming />,
  },
  {
    title: "Support",
    content: "Nach dem erfolgreichen Abschluss eines Webprojekts stehen wir unseren Kunden auf Wunsch weiterhin in den Bereichen Entwicklung, Marketing und Support zur Seite.",
    figureClass: "circle",
    icon: <IconSupport />,
  },
];