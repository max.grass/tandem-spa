import React from "react";
import PropTypes from "prop-types";

import { ItemWithIcon, ItemWithIconType } from "./ItemWithIcon";

import { samplesData } from "./samplesData";
import "./ListWithIcons.css";

export const ListWithIcons = (props) => {
  const {
    data,
    textColor,
    backgroundColor,
    iconSize,
    iconTop,
    iconLeft,
    figureColor
  } = props;

  const itemProps = {
    textColor,
    iconSize,
    iconTop,
    iconLeft,
    figureColor
  };

  const style = {
    "--text-color": textColor,
    "--background-color": backgroundColor,
  };

  return(
    <ul styleName="root" style={style}>
      {data.map((item, idx) => <ItemWithIcon key={idx} {...item} {...itemProps}/>)}
    </ul>
  );
};

ListWithIcons.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape(ItemWithIconType)),
  paddingTop: PropTypes.string,
  textColor: PropTypes.string,
  backgroundColor: PropTypes.string,
  iconPrimaryColor: PropTypes.string,
  iconSecondaryColor: PropTypes.string,
  iconSize: PropTypes.string,
  iconTop: PropTypes.string,
  iconLeft: PropTypes.string,
  figureSize: PropTypes.string,
  figureColor: PropTypes.string,
};

ListWithIcons.defaultProps = {
  data: samplesData,
  paddingTop: "116px",
  textColor: "var(--color-dark)",
  backgroundColor: "var(--color-white)",
  iconPrimaryColor: "var(--color-white)",
  iconSecondaryColor: "var(--color-blue-marguerite)",
  iconSize: "88px",
  iconTop: "16px",
  iconLeft: "24px",
  figureSize: "80px",
  figureColor: "var(--color-hint-of-red)",
}