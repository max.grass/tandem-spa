import React from "react";
import PropTypes from "prop-types";

import "./ItemWithIcon.css";

export const ItemWithIcon = (props) => {
  const {
    title,
    content,
    paddingTop,
    icon,
    iconSize,
    iconTop,
    iconLeft,
    figureClass,
    figureSize,
    figureColor
  } = props;

  const rootClass = `root root-${figureClass}`;

  const style = {
    "--padding-top": paddingTop,
    "--icon-size": iconSize,
    "--icon-top": iconTop,
    "--icon-left": iconLeft,
    "--figure-size": figureSize,
    "--figure-color": figureColor,
  };

  if (typeof content  === "string") {
    return (
      <li styleName={rootClass} style={style}>
        <h3 styleName="title">{title}</h3>
        <p styleName="text">{content}</p>
        <span styleName="icon">{icon}</span>
      </li>
    );
  } else if (typeof content  === "object") {
    return (
      <li styleName={rootClass} style={style}>
        <h3 styleName="title">{title}</h3>
        <ul>
          {content.map((text, idx) => {
            return <li key={idx} styleName="text">{text}</li>
          })}
        </ul>
        <span styleName="icon">{icon}</span>
      </li>
    );
  } else return null;
};

export const ItemWithIconType = {
  title: PropTypes.string.isRequired,
  content: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string),
  ]).isRequired,
  paddingTop: PropTypes.string,
  icon: PropTypes.oneOfType([
    PropTypes.node,
    PropTypes.instanceOf(null)
  ]).isRequired,
  iconSize: PropTypes.string,
  iconTop: PropTypes.string,
  iconLeft: PropTypes.string,
  figureClass: PropTypes.oneOf([
    "square",
    "circle",
    "triangle",
  ]),
  figureSize: PropTypes.string,
  figureColor: PropTypes.string,
};

ItemWithIcon.propTypes = ItemWithIconType;

ItemWithIcon.defaultProps = {
  paddingTop: "116px",
  iconPrimaryColor: "var(--color-white)",
  iconSecondaryColor: "var(--color-white)",
  iconSize: "88px",
  iconTop: "16px",
  iconLeft: "24px",
  figureClass: "square",
  figureSize: "80px",
  figureColor: "var(--color-hint-of-red)",
};