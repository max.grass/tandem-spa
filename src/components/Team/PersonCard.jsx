import React from "react";
import PropTypes from "prop-types";

import "./PersonCard.css";

export const PersonCard = (props) => {
  const { name, desc, imgSrc, imgWidth, imgHeight, imgAlt } = props;
  
  return (
    <li styleName="root">
      <div styleName="text-wrapper">
        <h3 styleName="name">{name}</h3>
        <p styleName="desc">{desc}</p>
      </div>
      <div styleName="image-wrapper">
        <img styleName="image"
          src={imgSrc} 
          width={imgWidth}
          height={imgHeight}
          alt={imgAlt}
          loading="lazy"
        />
      </div>
    </li>
  );
};

PersonCard.propTypes = {
  name: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
  imgSrc: PropTypes.string.isRequired,
  imgWidth: PropTypes.number,
  imgHeight: PropTypes.number,
};

PersonCard.defaultProps = {
  name: "Sandra Jurisch",
  desc: "Markt- und Konkurrenzanalysen",
  imgSrc: "https://via.placeholder.com/375x258.png",
  imgWidth: 375,
  imgHeight: 258,
  imgAlt: "Foto der Mitarbeiterin Sandra Jurisch"
}
