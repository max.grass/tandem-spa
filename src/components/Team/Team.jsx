import React from "react";
import { PersonCard } from "./PersonCard";
import { StatsCard } from "./StatsCard";

import { data } from "./samplesData";
import "./Team.css";

export  const Team = () => {
  return (
    <section className="section">
      <div className="container">
        <h2 styleName="title">Unsere Team</h2>
      </div>
      <ul styleName="list">
        {data.map((item, idx) => {
          const { 
            type,
            name,
            desc,
            imgSrc,
            imgWidth,
            imgHeight,
            imgAlt,
            term,
            count
          } = item;
          
          const personProps = { name, desc, imgSrc, imgWidth, imgHeight, imgAlt};
          const statsProps = { term, count };

          if (type === "person") {
            return <PersonCard idx={idx} {...personProps} />
          } else if (type === "stats") {
            return <StatsCard idx={idx} {...statsProps}/>
          }
        })}
      </ul>
    </section>
  );
}