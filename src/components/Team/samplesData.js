
import photo1 from "assets/images/team-photo-1.png";
import photo2 from "assets/images/team-photo-2.png";
import photo3 from "assets/images/team-photo-3.png";
import photo4 from "assets/images/team-photo-4.png";
import photo5 from "assets/images/team-photo-5.png";
import photo6 from "assets/images/team-photo-6.png";

export const data = [
  {
    type: "person",
    name: "Sandra Jurisch",
    desc: "Markt- und Konkurrenzanalysen",
    imgSrc: photo1,
    imgWidth: 640,
    imgHeight: 440,
    imgAlt: "Foto der Mitarbeiterin Sandra Jurisc",
  },
  {
    type: "person",
    name: "Julia Fiesel",
    desc: "Markt- und Konkurrenzanalysen",
    imgSrc: photo2,
    imgWidth: 640,
    imgHeight: 440,
    imgAlt: "Foto der Mitarbeiterin Julia Fiesel",
  },
  {
    type: "stats",
    term: "Tassen Grüntee getrunken",
    count: 1142,
  },
  {
    type: "stats",
    term: "Keywords analysiert",
    count: 432,
  },
  {
    type: "person",
    name: "Sandra Jurisch",
    desc: "Markt- und Konkurrenzanalysen",
    imgSrc: photo3,
    imgWidth: 640,
    imgHeight: 440,
    imgAlt: "Foto der Mitarbeiterin Sandra Jurisc",
  },
  {
    type: "person",
    name: "Sandra Jurisch",
    desc: "Markt- und Konkurrenzanalysen",
    imgSrc: photo4,
    imgWidth: 640,
    imgHeight: 440,
    imgAlt: "Foto der Mitarbeiterin Sandra Jurisc",
  },
  {
    type: "person",
    name: "Sandra Jurisch",
    description: "Markt- und Konkurrenzanalysen",
    imgSrc: photo5,
    imgWidth: 640,
    imgHeight: 440,
    imgAlt: "Foto der Mitarbeiterin Sandra Jurisc",
  },
  {
    type: "stats",
    term: "Geistesblitze gehabt",
    count: 363,
  },
  {
    type: "person",
    name: "Sandra Jurisch",
    description: "Markt- und Konkurrenzanalysen",
    imgSrc: photo6,
    imgWidth: 640,
    imgHeight: 440,
    imgAlt: "Foto der Mitarbeiterin Sandra Jurisc",
  },
];