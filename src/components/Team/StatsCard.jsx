import React, { useRef, useEffect } from "react";
import PropTypes from "prop-types";
import gsap from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";

import "./StatsCard.css";

export const StatsCard = (props) => {
  const { term, count } = props;
  const counterRef = useRef();
  gsap.registerPlugin(ScrollTrigger);

  useEffect(() => {
    gsap.from(counterRef.current, {
      scrollTrigger: {
        trigger: counterRef.current,
        start: "top bottom-=10%",
      },
      textContent: 0,
      duration: 7,
      ease: "power1.in",
      snap: { textContent: 1 },
      stagger: 1.0
    })
  }, []);
  
  return (
    <li styleName="root">
      <div>
        <h3 styleName="term">{term}</h3>
        <p styleName="count" ref={counterRef}>{count}</p>
      </div>
    </li>
  );
};

export const PersonCardPropTypes = {
  term: PropTypes.string.isRequired,
  count: PropTypes.number.isRequired,
};

StatsCard.propTypes = PersonCardPropTypes;

StatsCard.defaultProps = {
  term: "Geistesblitze gehabt",
  count: 363,
}