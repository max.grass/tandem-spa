import React, {useRef, useEffect, useContext} from "react";
import PropTypes from "prop-types";
import gsap from 'gsap';
import { OurStepsTween } from "components/OurSteps";

import classes from "./OurSteps.css";

export const OurSteps = (props) => {
	const {listData, columnGap} = props;

	const style = {
		"--column-gap": columnGap,
	}

	const trustRef = useRef();
	const trustRef2 = useRef();
  	const trustSelector = gsap.utils.selector(trustRef);
	const trustSelector2 = gsap.utils.selector(trustRef2);


	const body = useRef();
	const additinal = useRef();

	useEffect(() => {
		body.current.addEventListener('scroll', function(){
			additinal.current.scrollLeft = body.current.scrollLeft;
		})

		additinal.current.addEventListener('scroll', function(){
			body.current.scrollLeft = additinal.current.scrollLeft;
		})
	}, [body, additinal]);


	useEffect(() => {
		let tl = gsap.timeline();

		tl.add(OurStepsTween(trustSelector("li"), trustRef.current));
		tl.add(OurStepsTween(trustSelector2("li"), trustRef2.current));
	}, []);
	
	const listContent = listData.map(({ iconSrc, label,  customWidth, figure}) => {
		const figureClass = (figure)
			? (figure === 'square')
				? 'figure'
				:`figure icon${figure}`
			: null;

		const figureStyle = figure ? {
			"--figure-color": "var(--color-hint-of-red)"
		} : null;

		return (
			<li
				key={label}
				className={classes.li}
				style={customWidth ? {width: customWidth} : null}
			>

				{iconSrc ? (
						figure
						? (
							<div styleName={figureClass} style={figureStyle}>
								<img
									className={classes.liIcon}
									src={iconSrc}
									loading="lazy"
								/>
							</div>
						)
						: (
							<img
								className={classes.liIcon}
								src={iconSrc}
								loading="lazy"
							/>
						)
				):null}

				<p
					className={classes.liText}
				>
					{label}
				</p>
			</li>
		);
	});

	const listDescription = listData.map(({ description,  customWidth }, index) => {
		return (
			<li
				key={index}
				className={classes.li}
				style={customWidth ? {width: customWidth} : null}
				dangerouslySetInnerHTML={{__html: description}}
			></li>
		);
	});

	return (
		<div className={classes.listWrp}>
			<div className={classes.topline} ref={body}>
				<ul className={classes.list} ref={trustRef} style={style}>
					{listContent}
				</ul>
			</div>
			<div className={classes.bottomline} ref={additinal}>
				<ul className={classes.list} ref={trustRef2} style={style}>
					{listDescription}
				</ul>
			</div>
		</div>
	);
};

OurSteps.propTypes = {
	listData: PropTypes.array.isRequired
};

OurSteps.defaultProps = {
	columnGap: "2rem",
}
