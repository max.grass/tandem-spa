import React, {Fragment} from "react";
// import PropTypes from "prop-types";

import { IconList } from "components/IconList";

import classes from "./MarketingScheme.css";

import iconEmail from "assets/images/marketing-list-1.svg";
import iconSEM from "assets/images/marketing-list-2.svg";
import iconBanner from "assets/images/marketing-list-3.svg";
import iconVideo from "assets/images/marketing-list-4.svg";
import iconAffiliate from "assets/images/marketing-list-5.svg";
import iconSocial from "assets/images/marketing-list-6.svg";

export const MarketingScheme = (props) => {
	const {} = props;

	return (
		<div className={classes.wrp}>
			<p className={classes.text}>Wo es noch vor einigen Jahren eine überschaubare Anzahl an Medienkanälen gab, sind es jetzt unzählbar viele – und fast täglich kommen neue hinzu. Ihre Zielgruppe dort zu erreichen, wo sie sich gerade aufhält, ist die Herausforderung heutiger Kommunikationskonzepte: Auf dem Weg zur Arbeit, am Smartphone, im Büro am PC, beim Abrufen von E-Mails, in den Sozialen Medien oder in der Pause beim Durchblättern von Printmedien.</p>
			<IconList
				listData={[
					{
						"iconSrc" : iconEmail,
						"label" : "E-Mail-Marketing"
					},
					{
						"iconSrc" : iconSEM,
						"label" : "SEM",
						"sublabel": "SEA / SEO"
					},
					{
						"iconSrc" : iconBanner,
						"label" : "Banner-Marketing"
					},
					{
						"iconSrc" : iconVideo,
						"label" : "Video-Marketing"
					},
					{
						"iconSrc" : iconAffiliate,
						"label" : "Affiliate-Marketing"
					},
					{
						"iconSrc" : iconSocial,
						"label" : "Social-Media Marketing"
					}
				]}
			/>
		</div>
	);
};

// MarketingScheme.propTypes = {
//   image: PropTypes.string.isRequired,
//   alt: PropTypes.string,
//   strength: PropTypes.number,
//   children: PropTypes.node.isRequired,
// };