import React, {useState, useEffect, useContext} from "react";
import Slider from 'react-slick';
import CustomCursorContext from 'components/CustomCursor/context/CustomCursorContext'
import PropTypes from "prop-types";
import gsap from 'gsap';
import { IconListTween } from "components/IconList";

import classes from "./VideoPlayer.css";

export const VideoPlayerDeprecated = (props) => {

	const [showVideo, startVideo] = useState(false);
	
	const {
		poster,
		playColor,
		videoUid
	} = props;

	const video = showVideo ? (
		<iframe
			className={classes.video}
			src={`https://www.youtube.com/embed/${videoUid}?controls=0&autoplay=1&rel=0&playlist=${videoUid}&loop=1&mute=1`}
			title="YouTube video player" frameBorder="0"
			allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
		></iframe>
	) : null;

	return (
		<div className={classes.root}
			style={{backgroundImage: `url(${poster})`}}
		>
			{ video ? video : (
				<button
					className={classes.playBtnbtn}
					style={{backgroundColor: playColor}}
					onClick={() => startVideo(true)}
				></button>
			)}
		</div>
	);
};
