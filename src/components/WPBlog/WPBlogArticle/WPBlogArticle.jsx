import React, {useEffect, useState} from 'react';
import {clientConfig} from "components/../clientConfig";
import axios from "axios";
import {Helmet} from "react-helmet";

import { Blog } from 'components/Blog';

import './WPBlogArticle.css';

import preloader from "assets/images/rolling-preloader.svg";


export const ArticleInfo = (props) => {
	const {title, figure_color, category, categoryLink, date, view} = props;
	return (
		<div className="page-header-content">
			<div className="page-title-wrapper">

			</div>
		</div>
	)
}

export const WPBlogArticle = (props) => {
	const slug = props.slug;

	const [loading, setLoading] = useState(true);
	const [postData, setPostData] = useState(null);
	const [error, setError] = useState(null);

	useEffect( () => {
		const wordPressSiteURL = clientConfig.siteUrl;

		axios.get( `${wordPressSiteURL}wp-json/trae/v1/blog/?slug=${slug}` )
			.then( response => {
				setLoading(false);
				
				if (200 === response.data.status) {
					setPostData(response.data.post_data);
				} else {
					setError('No posts found');
				}
				
			})
			.catch( error => {
				setError(error.response.data.message);
				setLoading(false);
			});
	}, []);
	
	if (loading || !postData) {
		return (
			<div className="container">
				<div className="rolling-loader">
					<img src={preloader}/>
				</div>
			</div>
		)
	}

	const style = {
		"--bg-color": postData.figure_color,
	}

	const metaTitle = postData.meta?.title ? postData.meta.title : postData.title
	const metaDescription = postData.meta.description;
	const metaImg = postData.attachment_image.img_src[0];
	const metaUrl = `https://www.tandem-m.de/blog/${postData.slug}.html`;

	return (
		<article styleName="root">
			
			<Helmet>
				<title>{metaTitle}</title>
				<meta name="description" content={metaDescription} />
				<link rel="canonical" href={metaUrl} />

				<meta property="og:url" content={metaUrl} />
				<meta property="og:type" content="website" />
				<meta property="og:title" content={metaTitle} />
				<meta property="og:description" content={metaDescription} />
				<meta property="og:image" content={metaImg} />

				<meta name="twitter:card" content="summary_large_image" />
				<meta property="twitter:domain" content="tandem-m.de" />
				<meta property="twitter:url" content={metaUrl} />
				<meta name="twitter:title" content={metaTitle	} />
				<meta name="twitter:description" content={metaDescription} />
				<meta name="twitter:image" content={metaImg} />
			</Helmet>

			<div styleName="poster-wrapper">
				<picture>
					{postData.attachment_image.img_src[4] && <source srcSet={postData.attachment_image.img_src[4]} width={postData.attachment_image.img_src[1]} height={postData.attachment_image.img_src[2]}/>}
					<img styleName="poster" loading="lazy" src={postData.attachment_image.img_src[0]} alt={postData.title} width={postData.attachment_image.img_src[1]} height={postData.attachment_image.img_src[2]}/>
				</picture>
			</div>
			<div styleName="header-wrp">
				<div styleName="header-inner" style={style}>
					<div className="container-narrow">
						<p styleName="header" className="section-text-700"
							dangerouslySetInnerHTML={{__html: postData.title}}
						></p>
						<p styleName="date">{postData.date}</p>
					</div>
				</div>
			</div>
			<div className="container-narrow">
				<div styleName="content"
					dangerouslySetInnerHTML={{__html: postData.content}}
				></div>
			</div>
			<div className="container" styleName="navigation">
				{postData.prev !== null && <a styleName="navigation-button" href={'/blog/' + postData.prev}>Zurück</a>}
				{postData.next !== null && <a styleName="navigation-button" href={'/blog/' + postData.next}>Weiter</a>}
			</div>
			<Blog />
		</article>
	)
}
