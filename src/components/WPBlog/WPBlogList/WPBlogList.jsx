import React, { useState, useEffect } from "react";
import axios from "axios";
import {clientConfig} from "components/../clientConfig";
import {WPBlogArticleList} from "components/WPBlog/WPBlogArtilceList";
import {WPBlogPagination} from "components/WPBlog/WPBlogPagination";
import {WPBlogCategoryList} from "components/WPBlog/WPBlogCategoryList";

import preloader from "assets/images/rolling-preloader.svg";


export const WPBlogList = (props) => {
  const {postType, categoryId, pageNo} = props;

  const [initialLoad, setInitialLoad] = useState( true );
  const [currentPage, setCurrentPage] = useState( pageNo );
  const [currentCategory, setCurrentCategory] = useState( categoryId );
  
  const [loading, setLoading] = useState(true);
  const [postData, setPostData] = useState(null);
  const [foundPost, setFoundPosts] = useState(null);
  const [pageCount, setPageCount] = useState(null);
  const [error, setError] = useState(null);

  // reset page if category changed
  useEffect( () => { 
    if (!initialLoad) setCurrentPage(1);
  }, [currentCategory])

  useEffect( () => {
    const wordPressSiteURL = clientConfig.siteUrl;

    /**
     * we expected
     * status 200
     * post_data - array of posts
     * found_posts - total count of posts
     * page_count - for pagination
     *
    */

    const categoryQuery = currentCategory ? `&cat_id=${currentCategory}` : '';

    axios.get( `${wordPressSiteURL}wp-json/trae/v1/${postType}?page_no=${currentPage}${categoryQuery}` )
      .then( response => {
        setLoading(false);

        if (200 === response.data.status) {
          setPostData(response.data.post_data);
          setFoundPosts(response.data.found_posts);
          setPageCount(response.data.page_count);
        } else {
          setError('No posts found');
        }

      })
      .catch( error => {
        setError(error.response.data.message);
        setLoading(false);
      });

      setInitialLoad(false);
  }, [currentPage, currentCategory])

  if (loading) {
    return (
      <div className="container">
        <div className="rolling-loader">
          <img src={preloader}/>
        </div>
      </div>
    )
  }

  return (
    <React.Fragment>
      <div className="container">
        <WPBlogCategoryList currentCategory={currentCategory} setCurrentCategory={setCurrentCategory}/>
        <WPBlogArticleList posts={postData} />
        <WPBlogPagination 
          pageNo={currentPage}
          currentCategory={currentCategory}
          maxPages={7} totalPages={pageCount}
          setPageNo={setCurrentPage}
        />
      </div>

    </React.Fragment>
  )
}

