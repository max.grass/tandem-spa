/**
 * category List Component
 */

import React, {useEffect, useState, Fragment} from 'react';
import { Link } from "react-router-dom";
import {clientConfig} from "components/../clientConfig";
import axios from "axios";
import './WPBlogCategoryList.css';

export const WPBlogCategoryListItems = (props) => {
  const {data, currentCategory, setCurrentCategory} = props;


  return (
    <ul styleName={`blog-category-list`}>
      <li key={`tax-cat-0`}>
        {!currentCategory ?
          <span styleName={`blog-category-link`}>Alle</span>
          :
          <Link 
            styleName={`blog-category-link`}
            to={
              {search: ''}
            }
            onClick={() => setCurrentCategory(0)}
          >Alle</Link>
        }
      </li>
      { data && data.length > 0 && data.map( (item) => {
        return (
          <li key={`tax-cat-${item.id}`}>
            {
              currentCategory == item.id ?
              <span 
                styleName={`blog-category-link`}
                dangerouslySetInnerHTML={{__html:item.title}} />
              :
              <Link 
                styleName={`blog-category-link`} 
                to={
                  {search: `?cat_id=${item.id}`}
                }
                onClick={() => setCurrentCategory(item.id)}
                dangerouslySetInnerHTML={{__html:item.title}}
              />
            }
          </li>
        )
      })}
    </ul>
  )
}

export const WPBlogCategoryList = (props) => {
  const {currentCategory, setCurrentCategory} = props;

  const [loading, setLoading] = useState(false);
  const [data, setData] = useState(null);
  const [error, setError] = useState(null);

  useEffect( () => {
    const wordPressSiteURL = clientConfig.siteUrl;

    axios.get( `${wordPressSiteURL}wp-json/trae/v1/tax/blogcategories`)
      .then( response => {
        setLoading(false);

        if (200 === response.data.status) {
          setData(response.data.terms);
        } else {
          setError('No categories found');
        }
      })
      .catch( error => {
        setError(error.response.data.message);
        setLoading(false);
      });

  }, []);

  return (
    <Fragment>
      <p styleName="header">Kategorien</p>
      <WPBlogCategoryListItems data={data} currentCategory={currentCategory} setCurrentCategory={setCurrentCategory} />
    </Fragment>
  )

}
