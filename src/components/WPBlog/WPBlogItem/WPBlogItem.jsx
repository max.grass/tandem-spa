/**
 * Blog Item Component
 */

import React from 'react';
import { Link } from "react-router-dom";
import './WPBlogItem.css';

export const WPBlogItem = (props) => {
  const {id, title, slug, excerpt, attachment_image, figure, figure_color } = props.data;

  const figureStyle = {
    "--figure-color": figure_color
  };
  const figureClass = figure !== null ? `figure-icon ${figure}` : ''

  return(
    <article styleName="blog-item">
      <div styleName="blog-item-wrapper">
        <figure styleName="blog-item-image">
          <img loading="lazy" src={attachment_image.img_src[0]} srcSet={attachment_image.srcset} width={attachment_image.img_src[1]} height={attachment_image.img_src[2]} alt={title} />
        </figure>
        <div styleName="blog-item-content">
          <h3 styleName="blog-item-title figure-wrapper" >
            {figure && <div className={figureClass} style={figureStyle} />}
            <span dangerouslySetInnerHTML={{__html: title}} />
          </h3>
          <div styleName="blog-item-intro" dangerouslySetInnerHTML={{__html: excerpt}} />
        </div>
        <div styleName="blog-item-link">
          <Link to={`/blog/${slug}`}><span className="visuallyHidden" dangerouslySetInnerHTML={{__html:title}}/></Link>
        </div>
      </div>
    </article>
  )
}

