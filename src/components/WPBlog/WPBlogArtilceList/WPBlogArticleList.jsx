import React from 'react';

// import {WPBlogItem} from "components/WPBlog/WPBlogItem";
import { ArticlePreview } from 'components/ArticlePreview';

import "./WPBlogArticleList.css";


export const WPBlogArticleList = (props) => {
  const {posts} = props;

  return  (
    <div styleName="blog-list">
      {posts && posts.length > 0 && posts.map( post => {
        // return <WPBlogItem key={post.id} data={post} />
        return <ArticlePreview 
                key={post.id}
                title={post.title}
                text={post.excerpt}
                href={post.slug}
                attachment={post.attachment_image}
                src={post.attachment_image.img_src[0]}
                width={post.attachment_image.img_src[1]}
                height={post.attachment_image.img_src[2]}
                webp={post.attachment_image.img_src[4]}
                figure={post.figure}
                color={post.figure_color}
              />
      })}
    </div>
  )
}
