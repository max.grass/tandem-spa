import React from "react";
import { Link } from "react-router-dom";
import './WPBlogPagination.css';

/**
 *
 * @constructor
 *
 * create pagination list
 *
 * @param currentPage
 * @param maxPages
 * @param totalPages
 *
 * Example
 *  mP (maxPages min 5) - max pagination items to show  (must be odd for symmetry)
 *  x - current page (currentPage)
 *  totalPages - test;
 *  tP - totalPages - total pages (example: 100)
 *  delta = 3 - minimal page number moving
 *
 *  if mP = 7
 *  1 2 3 4 5 ... 100 // x = 1 ... 4  (mP - delta)
 *  1 ... 4 5 6 ... 100 // x = 5
 *  1 ... 4 5 6 7 8 ... 100 // x = 6; mp = 9
 *  1 ... 96 97 98 99 100 // x = 96 ... 100  (tP + delta - mP)
 *
 *  1 ... x-1 x x+1 ... tP // x is active if  mP - delta < x < tP - mP + delta
 *
 */

const paginationPageList = (currentPage, maxPages, totalPages) => {
  const pages = [];
  const delta = 3
  const offset = maxPages - delta;

  if (totalPages === 1) {
    return pages;
  }

  if (totalPages <= maxPages && totalPages > 1) {
    for (let i = 1; i <= totalPages; i++) {
      pages.push(i);
    }
    return pages;
  }

  if (currentPage < offset) {
    for (let i = 1; i <= offset + 1; i++) {
      pages.push(i);
    }
    pages.push('...');
    pages.push(totalPages);

    return pages;
  }

  if (currentPage > totalPages - offset) {
    pages.push(1);
    pages.push('...');

    for (let i = totalPages - offset - 1; i <= totalPages; i++) {
      pages.push(i);
    }
    return pages;
  }

  if (currentPage >= offset || currentPage <= (totalPages - offset)) {
    const d = Math.floor((maxPages - 4) / 2)

    pages.push(1);
    pages.push('...');

    for (let i = d; i >= 1; i--) {
      pages.push(currentPage - i);
    }

    pages.push(currentPage);

    for (let i = 1; i <= d; i++) {
      pages.push(currentPage + i);
    }

    pages.push('...');
    pages.push(totalPages);

    return pages;
  }
}



const PageItem = (props) => {
  const {pageNo, currentCategory, isActive, setPageNo} = props;

  const changePage = pageNo => {
    setPageNo(pageNo);
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });
  }


  const PageItemElement = () => {
    if (isActive) {
      return <span styleName={'pagination-item'}>{pageNo}</span>
    } else {
      let search = '';
      
      if (pageNo > 1) {
        search += '?page_no=' + pageNo ;
      }

      if (currentCategory) {

        (pageNo > 1) ? (search += '&') : (search += '?');
        
        search += 'cat_id='+currentCategory;
      }

      return (
        <Link 
          styleName={'pagination-item'}
          to={
            {search: search}
          }
          onClick={() => changePage(pageNo)}
        >
          { pageNo }
        </Link>
      )
    }
  }

  return (
    <PageItemElement />
  )

}

export const WPBlogPagination = (props) => {
  const {pageNo, currentCategory, maxPages, totalPages, setPageNo} = props;
  const paginationList = paginationPageList(pageNo, maxPages, totalPages);

  return(
    <div className="pagination-wrapper">
      <ul styleName="pagination">
        { paginationList && paginationList.map( (item, index) => {
          return (
            <li key={`page-${index}`}>
              <PageItem pageNo={item} currentCategory={currentCategory} setPageNo={setPageNo} isActive={pageNo == item}/>
            </li>
          )
        }) }
      </ul>
    </div>
  )
}
