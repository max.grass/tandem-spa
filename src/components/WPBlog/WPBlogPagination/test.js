function t (currentPage, maxPages, totalPages) {
  maxPages = maxPages < 5 ? 5 : maxPages;
  const pages = [];
  const delta = 3
  const offset = maxPages - delta;

  if (totalPages === 1) {
    return pages;
  }

  if (totalPages <= maxPages && totalPages > 1) {
    for (let i = 1; i <= totalPages; i++) {
      pages.push(i);
    }
    return pages;
  }

  if (currentPage < offset) {
    for (let i = 1; i <= offset + 1; i++) {
      pages.push(i);
    }
    pages.push('...');
    pages.push(totalPages);

    return pages;
  }

  if (currentPage > totalPages - offset) {
    pages.push(1);
    pages.push('...');

    for (let i = totalPages - offset - 1; i <= totalPages; i++) {
      pages.push(i);
    }
    return pages;
  }

  if (currentPage >= offset || currentPage <= (totalPages - offset)) {
    const d = Math.floor((maxPages - 4) / 2)

    pages.push(1);
    pages.push('...');

    for (let i = d; i >= 1; i--) {
      pages.push(currentPage - i);
    }

    pages.push(currentPage);

    for (let i = 1; i <= d; i++) {
      pages.push(currentPage + i);
    }

    pages.push('...');
    pages.push(totalPages);

    return pages;
  }
}

