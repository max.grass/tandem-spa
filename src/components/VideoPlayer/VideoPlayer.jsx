import React, {useState, useRef} from "react";
import YouTube  from "react-youtube";

import classes from "./VideoPlayer.css";

export const VideoPlayer = (props) => {
	const [isPlaying, setIsPlaying] = useState(false);

	const {
		poster,
		playColor,
		videoUid
	} = props;

	const playerRef = useRef(null);

	const opts = {
			playerVars: {
				autoplay: 1,
				playsinline: 1,
				mute: 1,
				disablekb: 1,
				controls: 0,
				rel: 0,
				showinfo: 0
			},
			host: "https://www.youtube-nocookie.com",
		};

	const play = (evt) => {
		setIsPlaying(true);
	}

	const stop = () => {
		setIsPlaying(false);
	}

	const end = (evt) => {
		evt.target.playVideo();
	}

	const playVideo = (event) => {
		const player = playerRef.current.getInternalPlayer();

		if (player) {
			player.playVideo();
			setIsPlaying(true);
		}
	}

	const rootClasses = isPlaying ? classes.root + ' ' + classes.playing : classes.root

	return (
		<div className={rootClasses}>
			<YouTube
				ref={playerRef}
				iframeClassName={classes.video}
				videoId={videoUid}
				width={560}
				height={315}
				opts={opts}
				onPlay={play}
				onPause={stop}
				onEnd={end}
			/>
			<div className={classes.poster}>
				<img src={poster} alt="video preview"/>
				<button
					className={classes.playBtnbtn}
					style={{backgroundColor: playColor}}
					onClick={playVideo}
				></button>
			</div>
		</div>
	);
};
