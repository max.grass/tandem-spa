import React, { useState } from 'react';

import './cookiePopup.css';

export const CookiePopup = props => {

    const {
        allParams,
        saveParamsAndClosePanel,
	  } = props;

    const [active, setActive] = useState(0);
    const handleClick = e => {
        
        e.preventDefault();

        const index = parseInt(e.target.id, 0);
        if (index !== active) {
            setActive(index);
        }
    };

    const handleSubmit = (values) => {
        saveParamsAndClosePanel( JSON.stringify(Object.keys(values)) )
    };

    return (
        <div styleName="cookie-popup">
            <form
                styleName="cookie-popup-form"
                onSubmit={handleSubmit}
            >
                <h3 styleName="cookie-popup-title">Datenschutz-Einstellungen</h3>
                <div styleName="cookie-popup-content">
                    <div styleName="cookie-popup-switcher">
                        <button
                            styleName={(active === 0) ? 'is-active' : 'not-active' }
                            onClick={handleClick}
                            id={0}
                        >Erforderlich</button>
                        <button
                            styleName={(active === 1) ? 'is-active' : 'not-active' }
                            onClick={handleClick}
                            id={1}
                        >Statistik</button>
                        <button
                            styleName={(active === 2) ? 'is-active' : 'not-active' }
                            onClick={handleClick}
                            id={2}
                        >Marketing</button>
                    </div>
                    <div
                        styleName="cookie-popup-tab-content"
                        style={ (active === 0) ? {} :  {display:'none'}}
                    >
                        <div styleName="cookie-popup-checkbox">
                            <input type="checkbox"
                                id="common"
                                name="common"
                                disabled={true}
                                checked={true}
                            />
                            <label htmlFor="common">Erforderlich</label>
                        </div>
                        <p styleName="cookie-popup-text">Diese Services und Technologien sind für den Betrieb von Tandem Marketing unbedingt erforderlich und ermöglichen beispielsweise sicherheitsrelevante Funktionalitäten. Außerdem können wir mit ihnen ebenfalls erkennen, ob Sie in Ihrem Profil eingeloggt bleiben möchten, um Ihnen unsere Dienste bei einem erneuten Besuch unserer Seite schneller zur Verfügung zu stellen.</p>
                    </div>
                    <div
                        styleName="cookie-popup-tab-content"
                        style={ (active === 1) ? {} :  {display:'none'}}
                    >
                        <div styleName="cookie-popup-checkbox">
                            <input type="checkbox"
                                   id="statistics"
                                   name="statistics"
                            />
                            <label htmlFor="statistics">Statistik</label>
                        </div>

                        <p styleName="cookie-popup-text">Um unsere Angebote weiter zu verbessern, erfassen wir anonymisierte Daten für Statistiken und Analysen. Mithilfe dieser Daten können wir beispielsweise die Besucherzahlen und den Effekt bestimmter Seiten unseres Web-Auftritts ermitteln und unsere Inhalte optimieren.</p>
                    </div>
                    <div
                        styleName="cookie-popup-tab-content"
                        style={ (active === 2) ? {} :  {display:'none'}}
                    >
                        <div styleName="cookie-popup-checkbox">
                            <input type="checkbox"
                                   id="marketing"
                                   name="marketing"
                            />
                            <label htmlFor="marketing">Marketing</label>
                        </div>
                        <p styleName="cookie-popup-text">Diese Services und Technologien werden genutzt, um Ihnen personalisierte Inhalte, passend zu Ihren Interessen anzuzeigen. Somit können wir Ihnen Angebote präsentieren, die für Sie besonders relevant sind.</p>
                    </div>
                </div>
                <div styleName="cookie-actions">
                    <button
                        className="button"
                        styleName="settings-cookie"
                    >Einstellungen</button>
                    <button
                        type='button'
                        className="button"
                        styleName="accept-cookie"
                        onClick={() => saveParamsAndClosePanel(allParams)}
                    >Alle akzeptieren</button>
                </div>
            </form>
        </div>
    )
}
