import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import './cookieWarning.css';

import { CookiePopup } from './cookiePopup';


const ALL_PARAMS = '["common","statistics","marketing"]';

const setCookie = (name, value, options) => {
    options = options || {};

	var expires = options.expires;

	if (typeof expires == "number" && expires) {
		var d = new Date();
		d.setTime(d.getTime() + expires * 1000);
		expires = options.expires = d;
	}
	if (expires && expires.toUTCString) {
		options.expires = expires.toUTCString();
	}

	value = encodeURIComponent(value);

	var updatedCookie = name + "=" + value;

	for (var propName in options) {
		updatedCookie += "; " + propName;
		var propValue = options[propName];
		if (propValue !== true) {
			updatedCookie += "=" + propValue;
		}
	}

	document.cookie = updatedCookie;
}

const getCookie = (name) => {
    const matches = document.cookie.match(new RegExp(
		"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	));
	return matches ? decodeURIComponent(matches[1]) : undefined;
}

const CookieWarning = () => {
    
    const [showPanel, setShowPanel] = useState(getCookie('user_cookie_group') ? false : true);
    const [showPopup, setShowPopup] = useState(false);

    const saveParamsAndClosePanel = (data) => {
        setCookie('user_cookie_group', data, {expires: 60 * 60 * 24 * 14} );
        setShowPanel(false);

        window.location.reload();
    }

   return showPanel && (
        <>
            <div styleName="cookie-notice">
                <div styleName="cookie-container">
                    <div styleName="cookie-description">
                        <h3>Um unser Angebot auf Sie abzustimmen</h3>
                        <p>Tandem Marketing & Partners benötigt für einzelne Datennutzungen Ihre Einwilligung, um die Funktion der Website zu gewährleisten und Ihnen unter anderem Informationen zu Ihren Interessen anzuzeigen. Mit Klick auf "Zustimmen" geben Sie Ihre Einwilligung dazu. Ausführliche Informationen erhalten Sie in unserer Datenschutzerklärung.
                          <br/>Sie haben jederzeit die Möglichkeit Ihre Zustimmung in der Datenschutzerklärung zurück zu nehmen.
                          <br/><Link to="/datenschutz">Zur Datenschutzerklärung</Link>
                        </p>
                    </div>
                    <div styleName="cookie-actions">
                        <button
                            className="button"
                            styleName="settings-cookie"
                            onClick={() => setShowPopup(true)}
                        >Einstellungen</button>
                        <button
                            className="button"
                            styleName="accept-cookie"
                            onClick={() => saveParamsAndClosePanel(ALL_PARAMS)}
                        >Zustimmen</button>
                    </div>
                </div>
            </div>
            {showPopup ? (
                <CookiePopup
                    saveParamsAndClosePanel={saveParamsAndClosePanel}
                    allParams={ALL_PARAMS}
                />)  : null}
        </>
    );
};

export default CookieWarning;
