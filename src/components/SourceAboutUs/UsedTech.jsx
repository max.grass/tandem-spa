import React, {useEffect} from 'react';
import Slider from 'react-slick';
import {sourceAboutUsTween} from 'components/Animation';

import xovi from './xovi.png';
import idg from './idg-expertennetzwerk.png';
import computerwoche from './computerwoche.png';
import channelPartner from './channel-partner.png';
import eStrategy from './e-strategy.png';
import ds from './deutsche-startups.png';

import './SourceAboutUs.css';
import '../../assets/slick/slick.css';

const Slide = (props) => {
  const { title, src, width, height } = props;

  return(
    <div styleName='slide'>
      <h3 styleName='title'>{title}</h3>
      <img src={src} width={width} height={height} alt={title} title={title} loading="lazy" />
    </div>
  );
}

export const UsedTech = (props) => {
  const { techs } = props;

  const settings = {
    arrows: false,
    dots: false,
    infinite: false,
    variableWidth: true,
    lazyload: true,
    slidesToScroll: 1,
    swipe: true,
    touchThreshold: 20,
    swipeToSlide: true,
    slidesToShow: 6,

    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 5,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 4,
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 375,
        settings: {
          slidesToShow: 2,
        }
      }
    ]
  };

  useEffect(() => {
    sourceAboutUsTween('#sourceAboutUsWrapper .slick-track > div', '#sourceAboutUsRoot');
  }, []);

  return (
    <section id='sourceAboutUsRoot'>
      <div styleName='container'>
        <h2 styleName='sectionTitle'>Technologien</h2>
        <div styleName='sliderWrapper' id='sourceAboutUsWrapper'>
          <Slider {...settings}>
            {
                techs.map(({ src, width, height, title}, i) => {

                    return <Slide key={`usedtech-$i`} title={title} src={src} width={width} height={height}/>
                })
            }
          </Slider>
        </div>
      </div>
    </section>
  );
}
