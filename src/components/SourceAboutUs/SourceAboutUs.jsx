import React, {useEffect} from 'react';
import Slider from 'react-slick';
import {sourceAboutUsTween} from 'components/Animation';

import xovi from './xovi.png';
import idg from './idg-expertennetzwerk.png';
import computerwoche from './computerwoche.png';
import channelPartner from './channel-partner.png';
import eStrategy from './e-strategy.png';
import ds from './deutsche-startups.png';

import './SourceAboutUs.css';
import '../../assets/slick/slick.css';

const Slide = (props) => {
  const { title, src, width, height, link } = props;

  return(
    <div styleName='slide'>
      <h3 styleName='title'>{title}</h3>
      <a href={link} target="_blank">
        <img src={src} width={width} height={height} alt={title} loading="lazy" />
      </a>
    </div>
  );
}

export const SourceAboutUs = () => {
  const settings = {
    arrows: false,
    dots: false,
    infinite: false,
    variableWidth: true,
    swipe: true,
    touchThreshold: 20,
    swipeToSlide: true,
    slidesToScroll: 1,
    slidesToShow: 6,
    responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 5,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 4,
        }
      },
      {
        breakpoint: 576,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 375,
        settings: {
          slidesToShow: 2,
        }
      }
    ]
  };

  useEffect(() => {
    sourceAboutUsTween('#sourceAboutUsWrapper .slick-track > div', '#sourceAboutUsRoot');
  }, []);

  return (
    <section id='sourceAboutUsRoot'>
      <div styleName='container'>
        <h2 styleName='sectionTitle'>Tandem Marketing ist unter anderem bekannt aus:</h2>
        <div styleName='sliderWrapper' id='sourceAboutUsWrapper'>
          <Slider {...settings}>
            <Slide 
              title='XOVI' link={'https://www.xovi.de/author/julia-albrecht/'}
              src={xovi} width='75' height='59'
            />
            <Slide 
              title='IDG-Expertennetzwerk' link={'https://www.channelpartner.de/ap/julia-albrecht,1261'}
              src={idg} width='160' height='29'
            />
            <Slide 
              title='Computerwoche' link={'http://www.computerwoche.de/ap/julia-albrecht,1261'}
              src={computerwoche} width='142' height='46'
            />
            <Slide 
              title='Channel Partner' link={'http://www.channelpartner.de/ap/julia-albrecht,1261'}
              src={channelPartner} width='168' height='23'
            />
            <Slide 
              title='eStrategy' link={'http://www.estrategy-magazin.de/2015/customer-self-service-wie-sie-ihren-kunden-online-besten-self-service-bieten.html'}
              src={eStrategy} width='125' height='28'
            />
            <Slide 
              title='Deutsche startups' link={'http://www.deutsche-startups.de/2015/09/10/wir-lueften-das-geheimnis-einer-perfekten-startup-website'}
              src={ds} width='119' height='46'
            />
          </Slider>
        </div>
      </div>
    </section>
  );
}
