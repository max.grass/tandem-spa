import React, {Fragment} from "react";
// import PropTypes from "prop-types";

import classes from "./CheckedList.css";

export const CheckedList = (props) => {
	const {color, content} = props;
    const style = {
        "--color": color,
      }

	return (
		<ul className={classes.list} style={style}>
			{content}
		</ul>
	);
};

// CheckedList.propTypes = {
//   image: PropTypes.string.isRequired,
//   alt: PropTypes.string,
//   strength: PropTypes.number,
//   children: PropTypes.node.isRequired,
// };