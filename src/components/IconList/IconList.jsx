import React, {useRef, useEffect, useContext} from "react";
import Slider from 'react-slick';
import CustomCursorContext from 'components/CustomCursor/context/CustomCursorContext'
import PropTypes from "prop-types";
import gsap from 'gsap';
import { IconListTween } from "components/IconList";

import '../../assets/slick/slick.css';
import '../../assets/slick/slick-theme.css';
import classes from "./IconList.css";

export const IconList = (props) => {
	const {listData} = props;

	const trustRef = useRef();
  	const trustSelector = gsap.utils.selector(trustRef);

	const { cursorType, setCursorType } = useContext(CustomCursorContext);

	// useEffect(()=> {
	// 	trustRef.current.addEventListener('mouseenter', () => {
	// 	setCursorType('slider');
	// 	return () => {};
	// 	});
	//
	// 	trustRef.current.addEventListener('mouseleave', () => {
	// 	setCursorType('default');
	// 	return () => {};
	// 	});
	// }, []);

	const settings = {
		arrows: false,
		dots: false,
		centerMode: false,
		infinite: false,
		variableWidth: true,
		swipe: true,
		touchThreshold: 20,
		swipeToSlide: true
	};

	useEffect(() => {
		let tl = gsap.timeline();

		tl.add(IconListTween(trustSelector('[class*="IconList-li-"]'), trustRef.current));
	}, []);
	
	const listContent = listData.map(({ iconSrc, label,  sublabel}) => {
		return (
			<div
				key={label}
				className={classes.li}
			>
				{iconSrc ? (
					<img
						className={classes.liIcon}
						src={iconSrc}
						loading="lazy"
					/>
				):null}
				<p
					className={classes.liText}
				>
					{label}
					{sublabel ? <span>{sublabel}</span> : null}
				</p>
			</div>
		);
	});

	return (
		<div className={classes.listWrp}>
			<div className={classes.list} ref={trustRef}>
				<Slider {...settings}>
					{listContent}
				</Slider>
			</div>
		</div>
	);
};

IconList.propTypes = {
	listData: PropTypes.array.isRequired
};
