import React from "react";
import PropTypes from "prop-types";

import { ImageListItem, ImageListItemPropTypes } from "./ImageListItem";

import "./ImageList.css";

export const ImageList = (props) => {
  const {
    images,
    rowGap,
    rowGapDesktop,
    columnGap,
    columnGapDesktop,
  } =  props;

  const style = {
    "--row-gap": rowGap,
    "--row-gap-desktop": rowGapDesktop,
    "--column-gap": columnGap,
    "--column-gap-desktop": columnGapDesktop,
  };

  return (
    <ul styleName="root" style={style}>
      {images.map((image, idx) => <ImageListItem key={idx} {...image} />)}
    </ul>
  );
};

ImageList.propTypes = {
  images: PropTypes.array.isRequired,
  rowGap: PropTypes.string,
  rowGapDesktop: PropTypes.string,
  columnGap: PropTypes.string,
  columnGapDesktop: PropTypes.string,
};

ImageList.defaultProps = {
  rowGap: "20px",
  rowGapDesktop: "30px",
  columnGap: "30px",
  columnGapDesktop: "50px",
};
