import gsap from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";

export const ImageListTween = (selector, trigger) => {
  gsap.registerPlugin(ScrollTrigger);
  gsap.from(selector, {
    scrollTrigger: {
      trigger: trigger,
      start: "top bottom-=10%",
      end: "bottom bottom",
    },
    opacity: 0,
    x: -50,
    ease: "power1.out",
    stagger: 0.5,
  });
};
