import React from "react";
import PropTypes from "prop-types";

export const ImageListItem = (props) => {
  const {
    src,
    alt,
    width,
    height,
    loading
  } = props;

  const imgProps = { src, alt, width, height, loading };
  const style={ width, height };

  return (
    <li style={style}>
      <img {...imgProps} />
    </li>
  );
}

ImageListItem.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  loading: PropTypes.string
};
