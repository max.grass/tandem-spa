import React, { Fragment} from  "react";
import { ResponsiveContainer,  LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, LabelList } from "recharts";

import "./LineChartWebDesign.css";

const data = [
  { year: 2015, mobile: 35, desktop: 65 },
  { year: 2016, mobile: 45, desktop: 55 },
  { year: 2017, mobile: 55, desktop: 45 },
  { year: 2018, mobile: 52, desktop: 48 },
  { year: 2019, mobile: 50, desktop: 50 },
  { year: 2020, mobile: 60, desktop: 40 },
];

const CustomizedXAxisTick = ({ x, y, payload }) => {
  return (
    <g transform={`translate(${x},${y})`}>
      <text
        x={0}
        y={0}
        dx={12}
        dy={16}
        textAnchor="end"
        fill="#fff"
        style={{
          "fontWeight": "bold",
          "fontFamily": "Open Sans",
          "fontSize": 10,
          "lineHeight": 2,
        }}
      >
        {payload.value}
      </text>
    </g>
  );
};

const CustomizedYAxisTick = ({ x, y, payload }) => {
  let path = "";
  let fill = "";
  let width = 0;
  let height = 0;

  switch (payload.value) {
    case 35:
      path = 'm28.932 18.92-5.852-5.65V3.518C23.08 1.577 21.45 0 19.444 0H3.635C1.63 0 0 1.577 0 3.518v32.964C0 38.423 1.63 40 3.635 40h24.71c.572 0 1.07-.394 1.19-.934l2.263-10.876c.68-3.402-.377-6.862-2.866-9.27zM3.635 2.35H19.46c.664 0 1.207.526 1.207 1.168v15.11c-1.478-1.416-3.877-1.416-5.34.014a3.595 3.595 0 0 0-.015 5.183l4.63 4.671c-1.01.73-2.307 2-3.242 4.073H2.414V3.52c0-.643.543-1.169 1.221-1.169zM2.414 36.482V34.92h13.5a15.919 15.919 0 0 0-.392 2.73H3.62c-.663 0-1.206-.526-1.206-1.168zm26.986-8.76-2.067 9.928h-9.382c.18-2.497.92-4.57 2.172-6.088 1.04-1.255 2.097-1.693 2.172-1.737.8-.278 1.071-1.27.438-1.913-5.733-5.766-5.702-5.737-5.718-5.751a1.305 1.305 0 0 1 0-1.884 1.408 1.408 0 0 1 1.931 0c2.565 2.54 1.313 1.314 5.793 5.65.467.453 1.237.453 1.72 0a1.162 1.162 0 0 0 0-1.664l-3.38-3.241v-4.438l4.134 4c1.915 1.854 2.73 4.51 2.187 7.139z';
      fill="#f7fbfc";
      width = 32;
      height = 40;
      break;
    case 50:
      path = 'M13 56.585v1.108L4.273 54.49v-1.091L13 50.193v1.108L5.653 53.91v.068L13 56.585Zm-3.41-.409V51.71h.938v4.466h-.937Zm-.527-8.212H13v1.006H6.455V48h1.022v-.086a1.87 1.87 0 0 1-.8-.698c-.205-.313-.308-.716-.308-1.21 0-.444.091-.832.273-1.164.179-.333.452-.591.818-.776.364-.184.824-.277 1.38-.277H13v1.006H8.91c-.515 0-.916.134-1.203.4-.29.268-.434.634-.434 1.1 0 .321.07.608.209.86.139.25.342.448.609.593.267.145.59.218.972.218Zm-2.608-8.86h.852v3.392h-.852v-3.392Zm-1.569 2.404v-1.006h6.239c.284 0 .497-.041.64-.124a.64.64 0 0 0 .28-.324c.046-.133.069-.274.069-.422 0-.11-.006-.201-.017-.272l-.034-.17.903-.205c.026.068.051.163.077.285.028.122.042.277.042.465 0 .284-.06.562-.183.835a1.66 1.66 0 0 1-.558.673c-.25.176-.566.264-.946.264H4.886Zm8.25-6.59c0 .63-.139 1.174-.417 1.631a2.803 2.803 0 0 1-1.176 1.053c-.506.244-1.094.366-1.765.366-.67 0-1.261-.122-1.772-.366a2.89 2.89 0 0 1-1.202-1.031c-.29-.444-.435-.96-.435-1.551 0-.341.057-.678.17-1.01.114-.333.3-.635.555-.908.253-.273.588-.49 1.005-.652.418-.162.932-.243 1.543-.243h.426v5.045h-.87V33.23c-.368 0-.698.074-.988.221a1.67 1.67 0 0 0-.686.623 1.742 1.742 0 0 0-.251.946c0 .4.1.747.298 1.04.196.29.452.512.767.668.315.157.653.235 1.014.235h.58c.494 0 .913-.086 1.257-.256.34-.173.6-.413.78-.72.176-.307.264-.663.264-1.07 0-.264-.037-.503-.11-.716a1.534 1.534 0 0 0-.342-.558 1.55 1.55 0 0 0-.57-.362l.272-.972c.33.103.62.275.87.516.246.241.44.54.579.895.136.355.204.754.204 1.197ZM13 30.677H6.455v-1.006H13v1.005Zm-7.636-.512a.715.715 0 0 1-.2.507.643.643 0 0 1-.482.209.643.643 0 0 1-.482-.21.715.715 0 0 1-.2-.506.7.7 0 0 1 .2-.503.638.638 0 0 1 .482-.213c.187 0 .348.071.481.213a.7.7 0 0 1 .2.503Zm-1.091-3.342H13v1.005H4.273v-1.005ZM13 21.607H6.455V20.6H13v1.006Zm-7.636-.511a.715.715 0 0 1-.2.507.643.643 0 0 1-.482.208.643.643 0 0 1-.482-.208.715.715 0 0 1-.2-.508.7.7 0 0 1 .2-.502.637.637 0 0 1 .482-.213c.187 0 .348.07.481.213a.7.7 0 0 1 .2.503Zm3.699-3.342H13v1.005H6.455v-.971h1.022v-.086a1.869 1.869 0 0 1-.8-.698c-.205-.313-.308-.716-.308-1.21 0-.444.091-.832.273-1.164.179-.332.452-.59.818-.776.364-.184.824-.277 1.38-.277H13v1.006H8.91c-.515 0-.916.134-1.203.4-.29.268-.434.634-.434 1.1 0 .321.07.608.209.86.139.25.342.448.609.593.267.145.59.218.972.218Zm2.3-13.821h-.46c-.318 0-.609-.065-.873-.196a1.572 1.572 0 0 1-.64-.58c-.161-.255-.242-.565-.242-.929 0-.369.08-.679.243-.929.159-.25.372-.439.639-.566.264-.128.555-.192.873-.192h.46c.319 0 .611.065.879.196.264.128.477.318.639.57.159.25.238.558.238.921 0 .37-.08.68-.238.934a1.579 1.579 0 0 1-.64.575 1.97 1.97 0 0 1-.877.196Zm-.46-.87h.46c.265 0 .502-.062.712-.187.208-.125.311-.34.311-.648 0-.298-.103-.508-.31-.63a1.366 1.366 0 0 0-.712-.188h-.46c-.265 0-.5.06-.708.18-.21.118-.315.332-.315.638 0 .299.105.513.315.644.207.128.443.192.707.192ZM6.37 8.28h-.46c-.318 0-.61-.065-.873-.196a1.572 1.572 0 0 1-.64-.58c-.162-.255-.243-.564-.243-.928 0-.37.081-.68.243-.93.16-.249.372-.438.64-.566.264-.128.555-.192.873-.192h.46c.319 0 .611.066.878.196.264.128.477.319.64.571.158.25.238.557.238.92 0 .37-.08.681-.239.934a1.578 1.578 0 0 1-.639.575 1.97 1.97 0 0 1-.878.196Zm-.46-.869h.46c.265 0 .502-.062.712-.187.207-.125.311-.341.311-.648 0-.298-.104-.509-.311-.63a1.366 1.366 0 0 0-.712-.188h-.46c-.264 0-.5.06-.707.179-.21.119-.316.332-.316.639 0 .298.105.513.316.643.207.128.443.192.707.192ZM13 7.956l-8.727-6V.984l8.727 6v.972Z';
      fill="#ffffff";
      width = 18;
      height = 66;
      break;
    case 65:
      path = 'M57.189 0H4.81C2.161 0 0 2.146 0 4.777v36.466c0 2.631 2.161 4.777 4.811 4.777H24.59v2.646c0 2.66-2.191 4.835-4.87 4.835-.697 0-1.26.559-1.26 1.25 0 .69.563 1.249 1.26 1.249h22.56c.697 0 1.26-.559 1.26-1.25 0-.69-.563-1.249-1.26-1.249-2.679 0-4.87-2.175-4.87-4.835V46.02H57.19c2.65 0 4.811-2.146 4.811-4.777V4.777C62 2.146 59.853 0 57.189 0zM4.81 2.499h52.392c1.259 0 2.295 1.029 2.295 2.278v30.102H2.517V4.777A2.288 2.288 0 0 1 4.81 2.499zM36.73 53.5H25.27a7.29 7.29 0 0 0 1.836-4.835V46.02h7.787v2.646A7.29 7.29 0 0 0 36.73 53.5zm20.46-9.98H4.81c-1.258 0-2.294-1.029-2.294-2.278v-3.866h56.981v3.866a2.301 2.301 0 0 1-2.31 2.278z" style="strokeWidth:0;stroke-linecap:butt;stroke-linejoin:miter;fill:#5658ca" vector-effect="non-scaling-stroke"';
      fill="#5658ca";
      width = 62;
      height = 56;
      break;
    default:
      path = "";
  }

  return (
    <svg x = {x - width} y = {y - (height / 2)} version="1.2" xmlns="http://www.w3.org/2000/svg" overflow="visible" preserveAspectRatio="none" width={width} height={height} fill={fill}>
      <path d={path} />
    </svg>
  );
};

const CustomizedLabel = ({ x, y, value, index, data, counterName, circleColor, counterColor } ) => {
  const counter = data[index][counterName];
  const radius = 7;

  const styleValue = {
    "fontWeight": 700,
    "fontFamily": "Poppins",
    "fontSize": 12,
  };

  const styleCounter = {
    "fontWeight": 700,
    "fontFamily": "Open Sans",
    "fontSize": 8,
  };

  return (
    <g>
      {counter && 
        <Fragment>
          <circle cx={x + 15} cy={y - 15} r={radius} fill={circleColor} />
          <text x={x + 15} y={y - 14} fill={counterColor} textAnchor="middle" dominantBaseline="middle" style={styleCounter}>
            {counter}
          </text>
        </Fragment>
      }
      <text x={x} y={y} fill="#fff" textAnchor="middle" dominantBaseline="middle" style={styleValue}>
        {value}
      </text>
    </g>
  );
};

// const CustomTooltip = ({ active, payload, label } ) => {
//   if (active && payload && payload.length) {
//     return (
//       <div styleName="custom-tooltip">
//         <p styleName="custom-tooltip-text">{`Im Jahr ${label} nutzten ${payload[0].value} Prozent der Internetnutzer in Deutschland Desktops`}</p>
//         <p styleName="custom-tooltip-text">{`Im Jahr ${label} nutzten ${payload[1].value} Prozent der Internetnutzer in Deutschland mobile Geräte.`}</p>
//         {/* <p styleName="custom-tooltip-text">{`${label}: ${payload[0].value} % der Internetnutzer in Deutschland Desktops`}</p>
//         <p styleName="custom-tooltip-text">{`${label}: ${payload[1].value} % der Internetnutzer in Deutschland mobile Geräte.`}</p> */}
//       </div>
//     );
//   }

//   return null;
// };

export const LineChartWebDesign = () => {
  return (
    <div styleName="root">
    <ResponsiveContainer>
      <LineChart width={930} height={400} data={data} margin ={{ left: 50, right: 50 }}>
        <Line 
          type="monotone"
          dataKey="desktop"
          stroke="#5658ca"
          strokeWidth="3"
          dot={{ r: 20, style: { "fill": "#404297"} }}
          activeDot={false}
        >
          <LabelList
            dataKey="desktop"
            content={
              <CustomizedLabel
                data={data}
                counterName="desktopCounter"
                circleColor="#5658ca"
                counterColor="#ffffff"
              />
            }
          />
        </Line>

        <Line 
          type="monotone"
          dataKey="mobile"
          stroke="#dadada"
          strokeWidth="3"
          dot={{ r: 20, style: { "fill": "#404297"} }}
          activeDot={false}
        >
          <LabelList 
            dataKey="mobile" 
            content={
              <CustomizedLabel 
                data={data} 
                counterName="mobileCounter"
                circleColor="#f7fbfc"
                counterColor="#404297"
              />
            }
          />
        </Line>

        <CartesianGrid 
          stroke="#5658ca"
          horizontal={false} 
        />

        <XAxis 
          dataKey="year"
          tick={CustomizedXAxisTick}
          padding={{ left: 30, right: 30 }}
          style={{ "stroke": "#5658ca" }}
        />

        <YAxis
          type="number" domain={[20, 'dataMax']}
          tick={CustomizedYAxisTick}
          axisLine={false}
          tickLine={false}
          ticks={[35, 40, 45, 50, 55, 60, 65, 80]}
        />

        {/* <Tooltip content={<CustomTooltip />} /> */}
        <Tooltip  />
      </LineChart>
    </ResponsiveContainer>
    </div>
  );
}
