import React from 'react';
import { BarChart, Bar,  XAxis, CartesianGrid, Tooltip, LabelList } from 'recharts';

const data = [
  {
    date: "June 2017",
    amount: 18,
    fill: "#f7f3f4",
  },
  {
    date: "May 2018",
    amount: 19,
    fill: "#ede4e6",
  },
  {
    date: "Dec. 2019",
    amount: 32,
    fill: "#e0ccd1",
  },
  {
    date: "July 2020",
    amount: 55,
    fill: "#8e0a3f",
  },
];

const renderCustomizedLabel = (props) => {
  const { x, y, width, height, value } = props;
  return (
    <g>
      <text 
        x={x + width / 2} 
        y={height + y - 37} fill="#fff" 
        textAnchor="middle" 
        dominantBaseline="middle"
        style={{
          "fontWeight": "700",
          "fontSize": 16,
          "fontFamily": "Poppins",
        }}
      >
        {value}%
      </text>
    </g>
  );
};

const renderCustomAxisTick = ({ x, y, payload }) => {
  return (
    <g>
      <text 
        x={x} y={y + 18}
        textAnchor="middle" 
        dominantBaseline="middle"
        style={{
          "fontWeight": "400",
          "fontSize": 12,
          "fontFamily": "Open Sans",
          "fill": "#a3a3a3",
        }}
      >
        {payload.value.split(' ')[0]}
      </text>
      <text 
        x={x} y={y + 32}
        textAnchor="middle" 
        dominantBaseline="middle"
        style={{
          "fontWeight": "400",
          "fontSize": 12,
          "fontFamily": "Open Sans",
          "fill": "#a3a3a3",
        }}
      >
        {payload.value.split(' ')[1]}
      </text>
    </g>
  );
};

export const TinyBarChartEcommerce = () => {
  return (
    <BarChart
      width={400}
      height={300}
      data={data}
      margin={{
        top: 5,
        right: 30,
        left: 20,
        bottom: 20,
      }}
    >
      <CartesianGrid strokeDasharray="3 3" />
      <XAxis dataKey="date" tick={renderCustomAxisTick} tickLine={false} />
      <Bar dataKey="amount" fill={data.fill} minPointSize={5} >
        <LabelList dataKey="amount" content={renderCustomizedLabel} />
      </Bar>
      <Tooltip />
    </BarChart>
  );
}
