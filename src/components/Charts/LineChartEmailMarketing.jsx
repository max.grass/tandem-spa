import React, { Fragment} from  "react";
import { ResponsiveContainer,  LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, LabelList } from "recharts";

import "./LineChartEmailMarketing.css";

const data = [
  { year: 2017, amount: 269 },
  { year: 2018, amount: 281.1 },
  { year: 2019, amount: 293.6 },
  { year: 2020, amount: 306.4 },
  { year: 2021, amount: 319.6 },
  { year: 2022, amount: 333.2 },
  { year: 2023, amount: 347.3 },
  { year: 2024, amount: 361.6 },
];

const CustomizedXAxisTick = ({ x, y, payload }) => {
  return (
    <g transform={`translate(${x},${y})`}>
      <text
        x={0}
        y={0}
        dx={12}
        dy={16}
        textAnchor="end"
        fill="#fff"
        style={{
          "fontWeight": "400",
          "fontFamily": "Open Sans",
          "fontSize": 12,
          "lineHeight": 2,
        }}
      >
        {payload.value < 2022 ? payload.value : payload.value + '*'}
      </text>
    </g>
  );
};

const CustomizedLabel = ({ x, y, value } ) => {
  const radius = 7;

  const styleValue = {
    "fontWeight": 700,
    "fontFamily": "Poppins",
    "fontSize": 12,
  };

  return (
    <g>
      <text x={x} y={y} fill="#fff" textAnchor="middle" dominantBaseline="middle" style={styleValue}>
        {value}
      </text>
    </g>
  );
};

export const LineChartEmailMarketing = () => {
  return (
    <div styleName="root">
    <ResponsiveContainer>
      <LineChart width={930} height={400} data={data} margin ={{ left: 50, right: 50 }}>
        <Line 
          type="monotone"
          dataKey="amount"
          stroke="#f7fbfc"
          strokeWidth="3"
          dot={{ r: 20, style: { "fill": "#770c36"} }}
          activeDot={false}
        >
          <LabelList
            dataKey="amount"
            content={
              <CustomizedLabel
              />
            }
          />
        </Line>

        <CartesianGrid 
          stroke="#8e0a3f"
          horizontal={false} 
        />

        <XAxis 
          dataKey="year"
          tick={CustomizedXAxisTick}
          padding={{ left: 30, right: 30 }}
          style={{ "stroke": "#f7fbfc" }}
        />

        <YAxis
          dataKey="amount"
          axisLine={false}
          tickLine={false}
          ticks={[0, 100, 200, 300, 400]}
          style={{ "stroke": "#d6588a" }}
        />

        {/* <Tooltip content={<CustomTooltip />} /> */}
        <Tooltip itemStyle={{color: "#64062e"}} />
      </LineChart>
    </ResponsiveContainer>
    </div>
  );
}