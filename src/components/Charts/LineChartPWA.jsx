import React, { Fragment} from  "react";
import { ResponsiveContainer,  LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, LabelList } from "recharts";

import "./LineChartPWA.css";

const data = [
  { year: 2015, mobile: 54, desktop: 78, desktopCounter: "+1", mobileCounter: null },
  { year: 2016, mobile: 59, desktop: 79, desktopCounter: "+1", mobileCounter: "+5" },
  { year: 2017, mobile: 64, desktop: 81, desktopCounter: "+2", mobileCounter: "+5" },
  { year: 2018, mobile: 68, desktop: 84, desktopCounter: "+3", mobileCounter: "+4" },
  { year: 2019, mobile: 74, desktop: 86, desktopCounter: "+2", mobileCounter: "+6" },
  { year: 2020, mobile: 80, desktop: 88, desktopCounter: "+2", mobileCounter: "+6" },
];

const CustomizedXAxisTick = ({ x, y, payload }) => {
  return (
    <g transform={`translate(${x},${y})`}>
      <text
        x={0}
        y={0}
        dx={12}
        dy={16}
        textAnchor="end"
        fill="#fff"
        style={{
          "fontWeight": "bold",
          "fontFamily": "Open Sans",
          "fontSize": 10,
          "lineHeight": 2,
        }}
      >
        {payload.value}
      </text>
    </g>
  );
};

const CustomizedYAxisTick = ({ x, y, payload }) => {
  let path = "";
  let fill = "";
  let width = 0;
  let height = 0;

  switch (payload.value) {
    case 54:
      path = 'm28.932 18.92-5.852-5.65V3.518C23.08 1.577 21.45 0 19.444 0H3.635C1.63 0 0 1.577 0 3.518v32.964C0 38.423 1.63 40 3.635 40h24.71c.572 0 1.07-.394 1.19-.934l2.263-10.876c.68-3.402-.377-6.862-2.866-9.27zM3.635 2.35H19.46c.664 0 1.207.526 1.207 1.168v15.11c-1.478-1.416-3.877-1.416-5.34.014a3.595 3.595 0 0 0-.015 5.183l4.63 4.671c-1.01.73-2.307 2-3.242 4.073H2.414V3.52c0-.643.543-1.169 1.221-1.169zM2.414 36.482V34.92h13.5a15.919 15.919 0 0 0-.392 2.73H3.62c-.663 0-1.206-.526-1.206-1.168zm26.986-8.76-2.067 9.928h-9.382c.18-2.497.92-4.57 2.172-6.088 1.04-1.255 2.097-1.693 2.172-1.737.8-.278 1.071-1.27.438-1.913-5.733-5.766-5.702-5.737-5.718-5.751a1.305 1.305 0 0 1 0-1.884 1.408 1.408 0 0 1 1.931 0c2.565 2.54 1.313 1.314 5.793 5.65.467.453 1.237.453 1.72 0a1.162 1.162 0 0 0 0-1.664l-3.38-3.241v-4.438l4.134 4c1.915 1.854 2.73 4.51 2.187 7.139z';
      fill="#f7fbfc";
      width = 32;
      height = 40;
      break;
    case 78:
      path = 'M48.8843,10.39546c0.53719,-0.8156 0.84022,-1.79709 0.84022,-2.86152c0,-2.86152 -2.30028,-5.19773 -5.12397,-5.19773c-1.32231,0 -2.53444,0.51148 -3.44353,1.35473c-4.14601,-2.41916 -8.84298,-3.69094 -13.66391,-3.69094c-15.16529,0 -27.49311,12.55196 -27.49311,27.99309c0,6.41422 2.16253,12.62108 6.1157,17.59763c-0.53719,0.8156 -0.84022,1.79709 -0.84022,2.86152c0,2.86152 2.30028,5.19773 5.12397,5.19773c1.32231,0 2.53444,-0.51148 3.44353,-1.35473c4.14601,2.41916 8.85675,3.70476 13.66391,3.70476c15.16529,0 27.49311,-12.55196 27.49311,-27.99309c-0.01377,-6.42804 -2.17631,-12.6349 -6.1157,-17.61145zM42.35537,35.76203c-0.39945,-1.72797 -1.65289,-3.13799 -3.27824,-3.71859c0.08264,-1.32708 0.12397,-2.68181 0.12397,-4.03653c0,-2.66798 -0.15152,-5.26685 -0.44077,-7.76895h12.69972c0.77135,2.48827 1.18457,5.11479 1.18457,7.76895c0,2.70945 -0.41322,5.30832 -1.18457,7.76895h-9.10468zM27.49311,53.63614c-2.25895,0 -4.58678,-2.58504 -6.34986,-7.10541c-0.96419,-2.4468 -1.70799,-5.30832 -2.21763,-8.41866h13.4573c0.3719,1.61738 1.4876,2.94446 2.96143,3.59417c-1.74931,7.28511 -4.79339,11.92989 -7.85124,11.92989zM10.38567,43.24068c-0.93664,0 -1.81818,0.26265 -2.57576,0.70501c-1.40496,-1.79709 -2.54821,-3.76006 -3.42975,-5.84745h12.17631c0.53719,3.42829 1.34986,6.5801 2.41047,9.27573c0.92287,2.35004 1.99725,4.25771 3.18182,5.66774c-2.45179,-0.53913 -4.80716,-1.45149 -6.99725,-2.70945c0.23416,-0.59442 0.35813,-1.23031 0.35813,-1.89385c-0.01377,-2.86152 -2.30028,-5.19773 -5.12397,-5.19773zM12.63085,20.22414c0.39945,1.72797 1.65289,3.13799 3.27824,3.71859c-0.08264,1.32708 -0.12397,2.68181 -0.12397,4.03653c0,2.66798 0.15152,5.26685 0.44077,7.76895h-12.69972c-0.77135,-2.48827 -1.18457,-5.11479 -1.18457,-7.76895c0,-2.70945 0.41322,-5.30832 1.18457,-7.76895h9.10468zM27.49311,2.35004c2.25895,0 4.58678,2.58504 6.34986,7.10541c0.96419,2.4468 1.70799,5.30832 2.21763,8.41866h-13.4573c-0.3719,-1.61738 -1.4876,-2.94446 -2.96143,-3.59417c1.74931,-7.28511 4.79339,-11.92989 7.85124,-11.92989zM44.60055,12.74549c0.93664,0 1.81818,-0.26265 2.57576,-0.70501c1.40496,1.79709 2.54821,3.76006 3.42975,5.84745h-12.17631c-0.53719,-3.42829 -1.34986,-6.5801 -2.41047,-9.27573c-0.92287,-2.35004 -1.99725,-4.25771 -3.18182,-5.66774c2.45179,0.53913 4.80716,1.45149 6.99725,2.70945c-0.23416,0.59442 -0.35813,1.23031 -0.35813,1.89385c0.01377,2.86152 2.30028,5.19773 5.12397,5.19773zM36.84573,27.99309c0,1.27178 -0.04132,2.52975 -0.11019,3.78771c-2.14876,0.26265 -3.8843,1.88003 -4.36639,3.98124h-13.78788c-0.30303,-2.47445 -0.45455,-5.08714 -0.45455,-7.76895c0,-1.27178 0.04132,-2.52975 0.11019,-3.78771c2.14876,-0.26265 3.8843,-1.88003 4.36639,-3.98124h13.78788c0.30303,2.48827 0.45455,5.10096 0.45455,7.76895zM17.61708,16.20143c1.52893,0 2.78237,1.27178 2.78237,2.84769c0,1.57591 -1.23967,2.84769 -2.78237,2.84769c-0.09642,0 -0.19284,0 -0.27548,-0.01382h-0.01377c-1.39118,-0.15206 -2.47934,-1.36855 -2.47934,-2.83387c0,-1.56208 1.23967,-2.84769 2.7686,-2.84769zM37.36915,39.78474c-0.16529,0 -0.34435,-0.01382 -0.49587,-0.05529c0,0 0,0 -0.01377,0c-1.29477,-0.24883 -2.27273,-1.41002 -2.27273,-2.7924c0,-1.57591 1.23967,-2.84769 2.78237,-2.84769c1.5427,0 2.78237,1.27178 2.78237,2.84769c-0.01377,1.56208 -1.25344,2.84769 -2.78237,2.84769zM44.60055,4.70007c1.52893,0 2.78237,1.27178 2.78237,2.84769c0,1.57591 -1.23967,2.84769 -2.78237,2.84769c-1.5427,0 -2.78237,-1.27178 -2.78237,-2.84769c0,-1.57591 1.25344,-2.84769 2.78237,-2.84769zM22.13499,2.93063c-1.99725,2.37768 -3.66391,6.1101 -4.79339,10.92076c-2.30028,0.12441 -4.2011,1.79709 -4.69697,4.00889h-8.26446c3.16804,-7.49247 9.76584,-13.14638 17.75482,-14.92965zM10.38567,51.2861c-1.52893,0 -2.78237,-1.27178 -2.78237,-2.84769c0,-1.57591 1.23967,-2.84769 2.78237,-2.84769c1.5427,0 2.78237,1.27178 2.78237,2.84769c-0.01377,1.57591 -1.25344,2.84769 -2.78237,2.84769zM32.85124,53.05554c1.99725,-2.37768 3.66391,-6.1101 4.79339,-10.92076c2.30028,-0.12441 4.2011,-1.79709 4.69697,-4.00889h8.25069c-3.15427,7.49247 -9.75207,13.14638 -17.74105,14.92965z';
      fill="#5481dc";
      width = 62;
      height = 56;
      break;
    default:
      path = "";
  }

  return (
    <svg x = {x - width} y = {y - (height / 2)}  version="1.2" xmlns="http://www.w3.org/2000/svg" overflow="visible" preserveAspectRatio="none" width={width} height={height} fill={fill}>
      <path d={path} />
    </svg>
  );
};

const CustomizedLabel = ({ x, y, value, index, data, counterName, circleColor, counterColor } ) => {
  const counter = data[index][counterName];
  const radius = 7;

  const styleValue = {
    "fontWeight": 700,
    "fontFamily": "Poppins",
    "fontSize": 12,
  };

  const styleCounter = {
    "fontWeight": 700,
    "fontFamily": "Open Sans",
    "fontSize": 8,
  };

  return (
    <g>
      {counter && 
        <Fragment>
          <circle cx={x + 15} cy={y - 15} r={radius} fill={circleColor} />
          <text x={x + 15} y={y - 14} fill={counterColor} textAnchor="middle" dominantBaseline="middle" style={styleCounter}>
            {counter}
          </text>
        </Fragment>
      }
      <text x={x} y={y} fill="#fff" textAnchor="middle" dominantBaseline="middle" style={styleValue}>
        {value}
      </text>
    </g>
  );
};

const CustomTooltip = ({ active, payload, label } ) => {
  if (active && payload && payload.length) {
    return (
      <div styleName="custom-tooltip">
        <p styleName="custom-tooltip-year">{label}</p>
        <p styleName="custom-tooltip-text">{`Internetnutzer (DE) ${payload[0].value}`}</p>
        <p styleName="custom-tooltip-text">{`Davon Mobile Nutzer ${payload[1].value}`}</p>
        {/* <p styleName="custom-tooltip-text">{`${label}: ${payload[0].value} % der Internetnutzer in Deutschland Desktops`}</p>
        <p styleName="custom-tooltip-text">{`${label}: ${payload[1].value} % der Internetnutzer in Deutschland mobile Geräte.`}</p> */}
      </div>
    );
  }

  return null;
};

export const LineChartPWA = () => (
  <div styleName="root">
    <ResponsiveContainer>
      <LineChart width={930} height={400} data={data} margin={{ left: 50, right: 50 }}>
        <Line
          type="monotone"
          dataKey="desktop"
          stroke="#5481dc"
          strokeWidth="3"
          dot={{ r: 20, style: { "fill": "#0254a6" } }}
          activeDot={false}
        >
          <LabelList
            dataKey="desktop"
            content={<CustomizedLabel
              data={data}
              counterName="desktopCounter"
              circleColor="#5481dc"
              counterColor="#ffffff" />} />
        </Line>

        <Line
          type="monotone"
          dataKey="mobile"
          stroke="#dadada"
          strokeWidth="3"
          dot={{ r: 20, style: { "fill": "#0254a6" } }}
          activeDot={false}
        >
          <LabelList
            dataKey="mobile"
            content={<CustomizedLabel
              data={data}
              counterName="mobileCounter"
              circleColor="#f7fbfc"
              counterColor="#0254a6" />} />
        </Line>

        <CartesianGrid
          stroke="#2e67b8"
          horizontal={false} />

        <XAxis
          dataKey="year"
          tick={CustomizedXAxisTick}
          padding={{ left: 30, right: 30 }}
          style={{ "stroke": "#2e67b8" }} />

        <YAxis
          tick={CustomizedYAxisTick}
          axisLine={false}
          tickLine={false}
          ticks={[0, 54, 78, 100]} />

        <Tooltip content={<CustomTooltip />} />
        {/* <Tooltip /> */}
      </LineChart>
    </ResponsiveContainer>
  </div>
)