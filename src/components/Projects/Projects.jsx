import React from "react";

import { ProjectCard } from "./ProjectCard";

//import { data } from "./samplesData";
import "./Projects.css";

export const Projects = (props) => {
  
  const {
    data,
    title,
    referenzePage
  } = props;

  return (
    <section className="section" style={referenzePage ? {paddingBottom: 0} : null}>
      <div className="container">
        <h2 styleName="title">{title}</h2>
      </div>
      <ul styleName="list">
        {data.map((item, idx) => <ProjectCard key={`project-${idx}`} idx={idx} {...item}/>)}
      </ul>
    </section>
  );
}
