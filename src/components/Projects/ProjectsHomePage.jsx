import React from "react";

import { ProjectCard } from "./ProjectCard";

import { data } from "./homepageProjectsData";
import "./Projects.css";

export const Projects = () => {
  return (
    <section className="section">
      <div className="container">
        <h2 styleName="title">Zeit, unsere Arbeit zu zeigen</h2>
      </div>
      <ul styleName="list">
        {data.map((item, idx) => <ProjectCard key={`project-${idx}`} idx={idx} {...item}/>)}
      </ul>
    </section>
  );
}
