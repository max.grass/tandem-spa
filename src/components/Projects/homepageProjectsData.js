
import project_mr_gruppe from "assets/images/project-mettler.jpg";
import project_watch_de from "assets/images/project-watch-de-2.jpg";
import project_hershortt from "assets/images/project-hershortt.jpg";
import project_myle_de from "assets/images/project-myle-de.jpg";

export const data = [
  {
    title: "Mettler GmbH",
    desc: "Familiengeführtes Handwerksunternehmen mit zwei Standorten in der Region Stuttgart",
    text: "",
    imgSrc: project_mr_gruppe,
    imgWidth: 1200,
    imgHeight: 632,
    imgAlt: "Projekt Mettler",
    imgPosition: "50% 100%",
    url: "/projekte/48-mr-gruppe.html",
    backgroundColor: "#4e544b",
    buttonColor: "#787f7f",
  },
  {
    title: "watch.de",
    desc: "Internetshop für exklusive Markenuhren und Schmuck",
    text: "",
    imgSrc: project_watch_de,
    imgWidth: 2000,
    imgHeight: 1126,
    imgAlt: "Projekt Watch.de",
    imgPosition: "65% 100%",
    url: "/projekte/52-watch-de.html",
    backgroundColor: "var(--color-fungreen)",
    buttonColor: "var(--color-tropical-rain-forest)",
  },
  {
    title: "Hershortt",
    desc: "Designermode zu Outlet-Preisen",
    text: "",
    imgSrc: project_hershortt,
    imgWidth: 1920,
    imgHeight: 800,
    imgAlt: "Projekt Hershortt",
    imgPosition: "70% 100%",
    url: "/projekte/84-hershortt.html",
    backgroundColor: "#422818",
    buttonColor: "#724a2d",
  },
  {
    title: "myle.de",
    desc: "Lokaler Online-Marktplatz für Leinfelden-Echterdingen",
    text: "",
    imgSrc: project_myle_de,
    imgWidth: 1200,
    imgHeight: 677,
    imgAlt: "Projekt Myle.de",
    imgPosition: "60% 100%",
    url: "/projekte/75-myle-de.html",
    backgroundColor: "#283237",
    buttonColor: "#66787d",
  },
];
