import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import "./ProjectCard.css";

export const ProjectCard = (props) => {
  const {
    title,
    desc,
    text,
    imgSrc,
    imgWidth,
    imgHeight,
    imgAlt,
    imgPosition,
    url,
    backgroundColor,
    buttonColor,
  } = props;

  return (
    <li styleName="root">
      <Link styleName="link" to={url}>
        <div styleName="main-content">
          <h3 styleName="header">{title}</h3>
          <p styleName="desc">{desc}</p>
          <div styleName="image-wrapper">
            <img styleName="image"
              loading="lazy"
              style={{"objectPosition": imgPosition}}
              src={imgSrc} 
              width={imgWidth}
              height={imgHeight}
              alt={imgAlt}
            />
          </div>
        </div>
        {/* <div styleName="hover-content" style={{"backgroundColor": backgroundColor}}>
          <p styleName="text">{text}</p>
          <span styleName="button" style={{"backgroundColor": buttonColor}}>Mehr lesen</span>
        </div> */}
      </Link>
    </li>
  );
};

ProjectCard.propTypes = {
  name: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
  imgSrc: PropTypes.string.isRequired,
  imgWidth: PropTypes.number,
  imgHeight: PropTypes.number,
  imgPosition: PropTypes.string,
  url: PropTypes.string.isRequired,
  backgroundColor: PropTypes.string,
  buttonColor: PropTypes.string,
};

ProjectCard.defaultProps = {
  name: "Sandra Jurisch",
  desc: "Markt- und Konkurrenzanalysen",
  imgSrc: "https://via.placeholder.com/375x258.png",
  imgWidth: 375,
  imgHeight: 258,
  imgAlt: "Foto der Mitarbeiterin Sandra Jurisch",
  imgPosition: "top center"
}
