import React, { Fragment, useState } from "react";
import PropTypes from 'prop-types';

import { makeStyles } from "@material-ui/core/styles";
import MaterialAccordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { Hr } from "components/Hr";

import "./Accordion.css";

const useStyles = makeStyles(() => ({
  root: {
    color: 'inherit',
    backgroundColor: 'inherit',
    border: 0,
    boxShadow: 'none',
  },
  details: {
    display: 'flex',
    flexDirection: 'column',
    // rowGap: '2rem',
    alignItems: 'flex-start',
    padding: 0,
    color: 'inherit',
    backgroundColor: 'inherit',
  },
  summary: {
    padding: 0,
    color: 'rgba(255, 255, 255, 0.5)',
    backgroundColor: 'inherit',
  },
  summaryExpanded: {
    color: 'inherit',
  },
  summaryContent: {
    margin: '0.75rem 0',
  },
  expandIcon: {
    color: 'inherit',
  },
  icon : {
    width: "1.75rem",
    height: "1.75rem",
  },
  typographyTitle: {
    fontFamily: 'var(--font-family-open-sans)',
    fontWeight: 700,
    fontSize: '1.25rem',
    lineHeight: 1.5,
    color: 'inherit',
  },
  typographyBody1: {
    fontFamily: 'var(--font-family-open-sans)',
    fontWeight: 400,
    fontSize: '1rem',
    lineHeight: 2,
    textAlign: 'left',
  },
 
}));

export const Accordion = (props) => {
  const {
    id,
    color,
    bgColor,
    title,
    content

  } = props;
  const classes = useStyles();
  const [expanded, setExpanded] = useState(0);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  const style = {
    "--color": color,
    "--bgColor": bgColor,
  };

  return (
    <div styleName="root" style={style}>
      <MaterialAccordion classes={{ root: classes.root }}
        expanded={expanded === id} 
        onChange={handleChange(id)}>
        <AccordionSummary
          classes={{ 
            root: classes.summary,
            expanded: classes.summaryExpanded,
            content: classes.summaryContent,
            expandIcon: classes.expandIcon,
          }}
          expandIcon={<ExpandMoreIcon classes={{ root: classes.icon }}/>}
        >
          <Typography classes={{ root: classes.typographyTitle }}>
            {title}
          </Typography>
        </AccordionSummary>
        <AccordionDetails classes={{ root: classes.details }}>
          <Hr color={color}/>
          {content}
        </AccordionDetails>
      </MaterialAccordion>
    </div>
  );
}

Accordion.propTypes = {
  id: PropTypes.number.isRequired,
  color: PropTypes.string,
  bgColor: PropTypes.string,
  title: PropTypes.string.isRequired,
  content: PropTypes.node.isRequired,
};

Accordion.defaultProps = {
  id: 0,
  color: "var(--color-white)",
  bgColor: "var(--color-flamingo)",
};