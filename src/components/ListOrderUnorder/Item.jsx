import React from "react";
import PropTypes from "prop-types";

import { Icon } from "components/Icon";

import "./Item.css";

export const Item = (props) => {
  const { content, icon, textColor, counter } = props;

  const style = {
    "--color": textColor,
  }

  const text = typeof content === "string"
    ? <p styleName="text">{content}</p>
    : content;

  return (
    <li styleName="root" style={style}>
      <div styleName="icon-wrapper" data-counter={counter}>{icon}</div>
      <div styleName="text-wrapper">{text}</div>
    </li>
  );
};

export const itemPropTypes = {
  content: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]).isRequired,
  icon: PropTypes.node,
  textColor: PropTypes.string,
  counter: PropTypes.number,
};

export const itemDefaultProps = {
  icon: <Icon
    name="checked"
    width="50px"
    height="50px"
    color="var(--color-dark)"
  />,
  textColor: "var(--color-dark)",
  counter: null,
};

Item.propTypes = itemPropTypes;
Item.defaultProps = itemDefaultProps;
