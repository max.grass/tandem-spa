import gsap from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";

export const ListOrderUnorderTween = (selector, trigger) => {
  gsap.registerPlugin(ScrollTrigger);
  gsap.from(selector, {
    scrollTrigger: {
      trigger: trigger,
      start: "top bottom-=20%",
    },
    opacity: 0,
    x: -50,
    ease: "power1.easeInOut",
    stagger: 0.5,
  });
};
