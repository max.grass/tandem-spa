import React, { Fragment } from "react";
import PropTypes from 'prop-types';

import { Item, itemPropTypes, itemDefaultProps } from "./Item";

import "./ListOrderUnorder.css";

export const ListOrderUnorder = (props) => {
  const { items, isOrdered, icon, textColor } = props;

  const content = items.map((item, idx) => {
    const counter = isOrdered ? idx + 1  : null;

    return (<Item 
      key={idx} 
      icon={icon}
      textColor={textColor}
      content={item}
      counter={counter}
    />);
  });

  return (
    <Fragment>
      {isOrdered
        ? <ol styleName="root">{content}</ol>
        : <ul styleName="root">{content}</ul>
      }
    </Fragment>
  );
};

ListOrderUnorder.propTypes = {
  isOrdered: PropTypes.bool,
  items: PropTypes.arrayOf(
    itemPropTypes.content
  ),
  icon: itemPropTypes.icon,
  textColor: itemPropTypes.textColor
};

ListOrderUnorder.defaultProps = {
  isOrdered: false,
  icon: itemDefaultProps.icon,
  textColor: itemDefaultProps.textColor,
}
