import React, { useRef, useEffect } from "react";
import PropTypes from "prop-types";
import gsap from "gsap";

import "./HeroContent.css";

export const HeroContent = (props) => {
  const { title, text } = props;

  const titleRef = useRef();
  const textRef = useRef();

  useEffect(() => {
    const tl = gsap.timeline();
    
    tl.from(titleRef.current, 
      {
        y: 100,
        color: "var(color-dark)",
        opacity: 0,
        ease: "ease-in-out",
        duration: 1.3,
      })
    .from(textRef.current, 
      {
        y: 100,
        color: "var(color-dark)",
        opacity: 0,
        ease: "ease-in-out",
        duration: 1.3,
      })
  }, []);

  return (
    <div styleName="container">
      <h1 styleName="title" ref={titleRef}>
        {title}
      </h1>
      <p styleName="text" ref={textRef}>{text}</p>
    </div>
  )
}

HeroContent.propTypes = {
  title: PropTypes.node.isRequired,
  text: PropTypes.string.isRequired,
}