import React, { Fragment, useRef, useEffect } from "react";
import PropTypes from "prop-types";
import gsap from 'gsap';
import ScrollTrigger from 'gsap/ScrollTrigger';

import './Hero.css';

export const Hero = (props) => {
  const {
    src,
    alt,
    width,
    height,
    content
  } = props;
  
  const imageProps = { src, alt, width, height };
  const imgRef = useRef();
  gsap.registerPlugin(ScrollTrigger);

  useEffect(() => {
    gsap.to(imgRef.current, {
      scrollTrigger: {
        trigger: imgRef.current,
        start: 'top top',
        scrub: true,
      },
      scale: 1.2,
    });
  }, []);

  return (
    <div styleName="root">
      <img styleName="image" ref={imgRef} {...imageProps} />
      { typeof content === "string"
        ? <div className="container">
            <h2 styleName="title">{content}</h2>
          </div>
        : <Fragment>{content}</Fragment>
      }
    </div>
  );
};

Hero.propTypes = {
  src: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  content: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node,
  ]).isRequired
};

Hero.defaultProps = {
  src: "https://4dimension.net/images/so-simple-sample-image-3.jpg",
  alt: "Hero Image Placeholder",
  width: 1900,
  height: 800,
  content: "Hero heading",
};