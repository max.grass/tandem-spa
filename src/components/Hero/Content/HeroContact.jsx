import React,  { useRef, useEffect } from "react";
import gsap from "gsap";

import "./HeroContact.css";

export const HeroContact = () => {
  const titleRef = useRef();

  useEffect(() => {
    const tl = gsap.timeline();
    
    tl.from(titleRef.current, 
      {
        y: 100,
        color: "var(color-dark)",
        opacity: 0,
        ease: "ease-in-out",
        duration: 1.3,
      })
  }, []);

  return (
    <div styleName="container">
      <h2 styleName="title" ref={titleRef}>Kontaktieren Sie uns</h2>
    </div>
  )
}
