import React, { useRef, useEffect } from "react";
import gsap from 'gsap';

import { ImageList } from "components/ImageList";
import { ImageListTween } from "components/ImageList";

import logoTesa from 'assets/images/logo-tesa-w.svg';
import logoWatchDe from 'assets/images/logo-watch-de-w.svg';
import logoRitterSport from 'assets/images/logo-ritter-sport-w.svg';
import logoDdsa from 'assets/images/logo-ddsa-w.svg';
import logoSolarWinds from 'assets/images/logo-solarwinds-w.svg';
import logoRoberBoschStiftung from 'assets/images/logo_rober_bosch_stiftung.svg';
import "./HeroHome.css";

export const HeroHome = () => {
  const titleRef = useRef();
  const underlineRef = useRef();
  const listRef = useRef();

  const titleSelector = gsap.utils.selector(titleRef);
  const listSelector = gsap.utils.selector(listRef);

  const images = [
    {
      src: logoTesa,
      alt: "Tesa Logo",
      width: 81,
      height: 22,
      loading: 'lazy'
    },
    {
      src: logoWatchDe,
      alt: "Watch.de Logo",
      width: 75,
      height: 44,
      loading: 'lazy'
    },
    {
      src: logoRitterSport,
      alt: "Ritter Sport Logo",
      width: 44,
      height: 44,
      loading: 'lazy'
    },
    {
      src: logoDdsa,
      alt: "Die Deutsche Schulakademie Logo",
      width: 114,
      height: 24,
      loading: 'lazy'
    },
    {
      src: logoSolarWinds,
      alt: "SolarWinds Logo",
      width: 107,
      height: 24,
      loading: 'lazy'
    },
    {
      src: logoRoberBoschStiftung,
      alt: "Rober Bosch Stiftung Logo",
      width: 108,
      height: 44,
      loading: 'lazy'
    },
  ];

  useEffect(() => {
    let tl = gsap.timeline();

    tl
      .set(underlineRef.current, { width: 0 })
      .from(titleSelector("span"), 
      {
        y: 50,
        color: 'black',
        opacity: 0,
        ease: 'ease-out',
        // duration: 1.5,
        duration: 1,
        stagger: 0.2,
      })
      .to(underlineRef.current, {
        width: "100%",
        ease: 'ease-in-out',
        duration: 1,
      })
      .add(ImageListTween(listSelector("li"), listRef.current));
  }, []);

  return (
    <div styleName="root">
      <div styleName="title-wrapper">
        <h2 styleName="title" ref={titleRef}>
          <span>Wir sind eine Full-Service </span>
          <span styleName="title-underlined">
            Webagentur
            <span styleName="line" ref={underlineRef}></span>
          </span> 
          <span> f&uuml;r&nbsp;komplexe </span>
          <span>Webl&ouml;sungen</span>
        </h2>
      </div>
      <div styleName="customers-wrapper">
        <p styleName="customers-text">Trusted by</p>
        <div ref={listRef}>
          <ImageList
            images={images}
            rowGap="20px"
            rowGapDesktop="30px"
            columnGap="43px"
            columnGapDesktop="70px"
          />
        </div>
      </div>
    </div>
  )
}
