import React, { useRef, useEffect } from "react";
import gsap from "gsap";

import "./HeroBlog.css";

export const HeroBlog = () => {
  const titleRef = useRef();
  const textRef = useRef();

  useEffect(() => {
    const tl = gsap.timeline();

    tl.from(titleRef.current,
      {
        y: 100,
        color: "var(color-dark)",
        opacity: 0,
        ease: "ease-in-out",
        duration: 1.3,
      })
      .from(textRef.current,
        {
          y: 100,
          color: "var(color-dark)",
          opacity: 0,
          ease: "ease-in-out",
          duration: 1.3,
        })
  }, []);

  return (
    <div styleName="container">
      <h2 styleName="title" ref={titleRef}>
        Letzte Beiträge
      </h2>
    </div>
  )
}
