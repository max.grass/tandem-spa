import React,  { useRef, useEffect } from "react";
import gsap from "gsap";

import "./HeroContact.css";

export const HeroPage = (props) => {
  const titleRef = useRef();
  const title = props.title;
  const h1 = props.h1;

  useEffect(() => {
    const tl = gsap.timeline();
    
    tl.from(titleRef.current, 
      {
        y: 100,
        color: "var(color-dark)",
        opacity: 0,
        ease: "ease-in-out",
        duration: 1.3,
      })
  }, []);

  return (
    <div styleName="container">
      {!h1 && <h2 styleName="title" ref={titleRef}>{title}</h2>}
      {h1 && <h1 styleName="title" ref={titleRef}>{title}</h1>}
    </div>
  )
}
