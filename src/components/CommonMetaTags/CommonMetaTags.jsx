import React from "react";

import { Helmet } from "react-helmet";

export const CommonMetaTags = (props) => {
  const { 
    title,
    description,
    image,
    url
  } = props;
  
  return (
    <Helmet>
      <title>{title}</title>
      <meta name="description" content={description} />
      <link rel="canonical" href={url} />

      <meta property="og:url" content={url} />
      <meta property="og:type" content="website" />
      <meta property="og:title" content={title} />
      <meta property="og:description" content={description} />
      {image && (<meta property="og:image" content={'https://www.tandem-m.de' + image} />)}

      <meta name="twitter:card" content="summary_large_image" />
      <meta property="twitter:domain" content="tandem-m.de" />
      <meta property="twitter:url" content={url} />
      <meta name="twitter:title" content={title} />
      <meta name="twitter:description" content={description} />
      {image && (<meta name="twitter:image" content={'https://www.tandem-m.de' + image} />)}
    </Helmet>
  );
}