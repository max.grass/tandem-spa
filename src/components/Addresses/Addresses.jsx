import React from "react";
import PropTypes from "prop-types";

import { IconStuttgart } from "./IconStuttgart";
import { IconBerlin } from "./IconBerlin";

import "./Addresses.css";

export const Addresses = (props) => {
  const { textColor, figureSize, figureColor } = props;

  const itemStyle = {
    "--text-color": textColor,
    "--figure-size": figureSize,
    "--figure-color": figureColor,
  };

  return (
    <ul styleName="root">
      <li styleName="item item-triangle" style={itemStyle}>
        <IconStuttgart fill={textColor} />
        <p styleName="title">Büro Stuttgart, Business Center</p>
        <p styleName="text">
          Leinfelder Straße 64
          <br/>
          70771 Leinfelden-Echterdingen
          <br/>
          <small styleName="textSmall">
            (wenige Minuten von Stuttgart Flughafen)
          </small>
        </p>
      </li>
      <li styleName="item item-circle" style={itemStyle}>
        <IconBerlin fill={textColor} />
        <p styleName="title">Büro Berlin</p>
        {/* <p styleName="text">Greifenhagener Straße 26
          <br/>
          10437 Berlin</p> */}
      </li>
    </ul>
  );
};

Addresses.propTypes = {
  textColor: PropTypes.string,
  figureSize: PropTypes.string,
  figureColor: PropTypes.string,
};

Addresses.defaultProps = {
  textColor: "var(--color-dark)",
  figureSize: "80px",
  figureColor: "var(--color-lipstick)",
};
