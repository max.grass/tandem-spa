import "./Footer.css";

import React, { Fragment, lazy, Suspense } from "react";
import PropTypes from "prop-types";
import { FooterTop } from "./FooterTop";
import { FooterBottom } from "./FooterBottom";
import { ToTopBtn } from "../ToTopBtn";
import usePageTracking  from "../UsePageTracking";
const CookieWarning  = lazy(() => import("../CookieWarning"));

export const Footer = (props) => {
  const { content } = props;

  usePageTracking();

  return (
    <>
      <div styleName="root">
        
        {content === "full" && (
          <Fragment>
            <FooterTop />
            <FooterBottom />
          </Fragment>
        )}
        
        {content === "top" && (
          <FooterTop />
        )}

        {content === "bottom" && (
          <FooterBottom />
        )}
        <Suspense fallback={<div>Loading...</div>}>
          <CookieWarning />
        </Suspense>

      </div>
      <ToTopBtn />
    </>
  );
};

Footer.propTypes = {
  content: PropTypes.oneOf(["full", "top", "bottom"]),
};

Footer.defaultProps = {
  content: "full",
};
