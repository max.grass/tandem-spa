import React from "react";
import { Addresses } from "components/Addresses";

import "./FooterTop.css";

export const FooterTop = () => {
  return (
    <div styleName="root">
      <div styleName="container">
        <Addresses textColor="var(--color-white)"/>
      </div>
    </div>
  );
}