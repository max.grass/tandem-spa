import React from "react";
import {Link} from "react-router-dom";

import "./FooterBottom.css";
import Pdf from '../../static/pdfs/TMP_AGB.pdf';

export const FooterBottom = () => {

  const date = new Date ();

  return (
    <div styleName="root">
      <div className="container">
        <ul styleName="links-list">
          <li styleName="link-item">
            <Link to="/werbeagentur-stuttgart.html">Werbeagentur Stuttgart</Link>
          </li>
          <li styleName="link-item">
            <Link to="/magento-webshop.html">Magento Webshop</Link>
          </li>
          <li styleName="link-item">
            <Link to="/webdesign-cms.html">Webdesign Stuttgart</Link>
          </li>
          <li styleName="link-item">
            <Link to="/pwa-react.html">PWA React</Link>
          </li>
          <li styleName="link-item">
            <Link to="/marketing-stuttgart.html">Marketing</Link>
          </li>
          <li styleName="link-item">
            <Link to="/marketing-stuttgart/social-media-marketing.html">Social Media Marketing</Link>
          </li>
          <li styleName="link-item link-item-mobile">
            <Link to="/blog.html">Blog</Link>
          </li>
          {/*<li styleName="link-item"><Link to="#">Corporate Design</Link></li>*/}
          <li styleName="link-item link-item-mobile">
            <Link to="/referenzen.html">Referenzen</Link>
          </li>
          <li styleName="link-item link-item-mobile">
            <Link to="/impressum.html">Impressum</Link>
          </li>
          {/* <li styleName="link-item link-item-mobile">
            <Link to="/jobs.html">Jobs</Link>
          </li> */}
          <li styleName="link-item">
            <a href={ Pdf } target = "_blank">Unsere AGB</a>
          </li>
          <li styleName="link-item">
            <Link to="/datenschutz.html">Datenschutz</Link>
          </li>
        </ul>
        <p styleName="copyright">&copy; 2007 - {date.getFullYear()} Tandem Marketing &amp; Partners</p>
      </div>
    </div>
  );
}
