import React, { Fragment, useState } from 'react';

import IconButton from '@material-ui/core/IconButton';
import ChatIcon from '@material-ui/icons/Chat';

import { Messenger } from 'components/test/Messenger';

import './Chat.css';

export const Chat = () => {
  const [chatIsVisible, setChatIsVisible] = useState(false);

  const toggleChatVisibility = () => {
    setChatIsVisible((prevState) => !prevState);
  }

  return (
    <Fragment>
      <div styleName='triggerWrapper'>
        <IconButton
          aria-label='Open chat'
          color='secondary'
          onClick={toggleChatVisibility}
        >
          <ChatIcon />
        </IconButton>

      </div>
      {chatIsVisible && <Messenger />}
    </Fragment>

  );
}