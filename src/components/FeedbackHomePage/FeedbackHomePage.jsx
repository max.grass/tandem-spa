import React, { useRef, useEffect } from "react";

import { FeedbackForm } from "components/FeedbackForm";
import { Hr } from "components/Hr";
import { PhoneNumber } from "components/PhoneNumber";
import { FeedbackFormTween } from "components/Animation";

import "./FeedbackHomePage.css";

export const FeedbackHomePage = () => {
  let formContentRef = useRef(null);
  
  useEffect(() => {
    FeedbackFormTween(formContentRef);
  }, []);

  return (
    <section styleName="root">
      <div styleName="container">
        <h2 className="heading-3">Kontaktieren Sie uns</h2>
        <Hr />
        <div styleName="form-content" ref={(el) => (formContentRef = el)}>
          <p styleName="text">Haben Sie Fragen, Anregungen, Bedarf an Informationsmaterial oder möchten ein Angebot anfordern? Dann rufen Sie uns an oder verwenden das Kontaktformular.</p>
          <div styleName="phone-wrapper">
            <PhoneNumber
              text="Telefonnummer:"
              phoneLink="tel:+49071194552157"
              phoneCaption="+49 (0) 711 9455215"
            />
          </div>
          <FeedbackForm />
        </div>
      </div>
    </section>
  );
}
