import React from "react";
import PropTypes from "prop-types";

import "./ColorSteps.css";

export const ColorSteps = (props) => {
	const { hiddenTitle, parallax, textTop, textCenter, textBottom, bgColor } = props;
	const style = {
		"--bg-color": bgColor,
	}

	return (
		<section styleName="root" style={style}>
			<h2 className="visuallyHidden">{hiddenTitle}</h2>
			{parallax}
			<div styleName="content">
					{typeof textTop === "string" && <p styleName="top">{textTop}</p>}
					{typeof textTop === "object" && <div styleName="top">{textTop}</div>}
				<div styleName="middle">
					{typeof textCenter === "string" && <p>{textCenter}</p>}
					{typeof textCenter === "object" && textCenter}
				</div>
			</div>
			{textBottom ? <div styleName="bottom" className="section-text-200">{textBottom}</div> : null }
		</section>
	);
};

ColorSteps.propTypes = {
	hiddenTitle: PropTypes.string.isRequired,
	parallax: PropTypes.object.isRequired, 
	textTop: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.node
	]).isRequired,
	textCenter: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.node
	]).isRequired,
	textBottom: PropTypes.node,
	bgColor: PropTypes.string,
}

ColorSteps.defaultProps = {
	bgColor: "var(--color-cherry)",
}
