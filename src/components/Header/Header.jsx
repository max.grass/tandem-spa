import './Header.css';

import React from 'react';

import { MainNav } from 'components/MainNav';

export const Header = () => {
  return (
    <header styleName='root'>
      <MainNav />
    </header>
  )
}