import gsap from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";

gsap.registerPlugin(ScrollTrigger); 

// Displaying customer logos in the Customers component one by one
export const customerListTween = (item1, item2, item3, item4, item5) => {
  gsap.fromTo([item1, item2, item3, item4, item5],
    {
      opacity: 0,
      x: -20
    },
    {
      opacity: 1,
      x: 0,
      delay: 0.5,
      ease: 'power1.out',
      stagger: 0.2,
    }
  );
};

// Displaying technologies logos in the Tech component one by one
export const techListTween = (section, item1, item2, item3, item4, item5, item6, item7) => {
  gsap.from([item1, item2, item3, item4, item5, item6, item7], {
    scrollTrigger: {
      trigger: section,
      start: 'top bottom-=10%',
      end: 'bottom bottom',
      // toggleActions: 'play none none reverse',
    },
    opacity: 0,
    x: -20,
    duration: 0.5,
    ease: 'power1.out',
    stagger: 0.3,
  });
};

// Displaying technologies logos in the Tech component one by one
export const sourceAboutUsTween = (selector, trigger) => {
  gsap.from(selector, {
    scrollTrigger: {
      trigger: trigger,
      start: 'top bottom-=10%',
      end: 'bottom bottom',
    },
    opacity: 0,
    x: -20,
    duration: 0.5,
    ease: 'power1.out',
    stagger: 0.3,
  });
};

// Feedback Form Animation
export const FeedbackFormTween = (element) => {
  gsap.from(element, {
    scrollTrigger: {
      trigger: element,
      start: 'top bottom-=10%',
      end: 'bottom bottom'
    },
    x: -1000,
    duration: 1,
    ease: 'power2.out'
  });
}

// Displaying technologies logos in the Tech component one by one
export const aboutWorkIconConception = (selector, trigger) => {
  gsap.to(selector, {
    scrollTrigger: {
      trigger: trigger,
      start: 'top bottom-=10%',
      end: 'bottom bottom',
    },
    duration: 1,
    rotation: 360,
    transformOrigin: "50% 50%"
  });
};


/////////////////////////
// svg animation examples
/////////////////////////

// transforms on SVG content 
export const svgExample1 = (element) => {
  gsap.to(element, {
    scrollTrigger: {
      trigger: element,
      start: 'bottom center',
      end: 'top top',
      scrub: true
    },
    duration: 1, 
    x: 100, 
    y: 100, 
    scale: 0.5, 
    rotation: 180, 
    skewX: 45
  });
};

// Rotation with transformsOrigin
export const rotationWithTransformOrigin = (element) => {
  gsap.to(element, {
    scrollTrigger: {
      trigger: element,
      start: 'top bottom',
      end: 'bottom top',
      scrub: true
      
    },
    duration: 1, 
    rotation: 360, 
    transformOrigin: "50% 50%"
  });
};

// AboutWork Round Items
// export const floatingItems = (elements) => {
//   gsap.to(elements, {
//     x: 'random(-5, 5, 5)',
//     y: 'random(-5, 5, 5)',
//     repeat: -1,
//     yoyo: true,
//     repeatRefresh: true
//   })
// };