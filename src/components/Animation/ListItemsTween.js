import gsap from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";

// Moves list items from left to right by 50px
export const ListItemsTween = (sectionElement, stagger) => {
  gsap.registerPlugin(ScrollTrigger);
  const sectionSelector = gsap.utils.selector(sectionElement);
  
  const staggerValue = !stagger ? 0.5 : stagger;

  let tl = gsap.timeline({
    scrollTrigger: {
      trigger: sectionElement,
      start: "top bottom-=20%",
    },
  });
  
  tl.from(sectionSelector("li"), {
    opacity: 0,
    x: -50,
    ease: "power1.easeInOut",
    stagger: staggerValue,
  });
};
