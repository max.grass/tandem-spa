import gsap from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";

// Fills with a colored background by increasing the radius of the background circle
export const RoundBackgroundTween = (roundElement, sectionElement, secondaryColor) => {
  gsap.registerPlugin(ScrollTrigger);

  const sectionSelector = gsap.utils.selector(sectionElement);

  let tl = gsap.timeline({
    scrollTrigger: {
      trigger: sectionElement,
      start: "top bottom-=30%",
    }
  });

  tl.set(sectionElement, {
    "--bgColor": "var(--color-white)",
  })
  .set(sectionSelector("figure"), {
    "--secondary-color": "var(--color-white)",
  })
  .to(roundElement, {
    duration: 9,
    "--radius": 10000,
  })
  .set(sectionSelector("figure"), {
    "--secondary-color": secondaryColor 
  },"<1");
};
