import React from "react";
import PropTypes from "prop-types";

import { Button } from "components/Button";

import "./Article.css";

export const Article = (props) => {
  const {
    title,
    content,
    img,
    buttonUrl,
    buttonCaption,
    color,
    narrowText
  } = props;

  return (
    <section styleName="root">
      <div styleName="container">
        {
          content ? (
            <div styleName={`content ${narrowText ? "contentNarrow" : ""}`}>
              <h3 className="section-text-400">{title}</h3>
              {content}
            </div>
          ) : null
        }
        <div styleName="button-wrapper">
          <Button 
            caption={buttonCaption}
            url={buttonUrl}
            color={color}
          />
        </div>
      </div>
      <div styleName="img-wrapper">
        {img}
      </div>
    </section>
  );
}

Article.propTypes = {
  title: PropTypes.string,
  content: PropTypes.node,
  img: PropTypes.node.isRequired,
  buttonUrl: PropTypes.string,
  buttonCaption: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ]),
  color: PropTypes.string,
};

Article.defaultProps = {
  content: null,
  buttonUrl: "/",
  buttonCaption: "Fälle zeigen",
  color: "var(--color-сurious-blue)",
}