import './FeedbackForm.css';

import React, { Fragment, useRef, useState } from 'react';
import { Link } from 'react-router-dom';
import Recaptcha from 'react-recaptcha';

import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import SvgIcon from '@material-ui/core/SvgIcon';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import CheckIcon from '@material-ui/icons/Check';
import Button from '@material-ui/core/Button';
import FormHelperText from '@material-ui/core/FormHelperText';

import axios from "axios";
import {clientConfig} from "components/../clientConfig";
import {FormEvent} from "react";

const useStyles = makeStyles((theme) => ({
  field_name: {
    gridArea: 'name',
  },
  field_requirements: {
    gridArea: 'requirements',
  },
  field_phone: {
    gridArea: 'phone',
  },
  field_email: {
    gridArea: 'email',
  },
  field_message: {
    gridArea: 'message',
  },
  field_agreement: {
    gridArea: 'agreement',
  },
  field_agreement_label: {
    display: 'grid',
    gridTemplateColumns: '28px 1fr',
    marginRight: 0,
    marginLeft: '10px',
    alignItems: 'start',
    gap: '0 15px',
  },
  inputRoot: {
    paddingTop: '11px',
    paddingBottom: '11px',
    fontFamily: 'Open Sans, Arial, sans-serif',
    fontWeight: 300,
    color: '#333',
  },
  inputUnderline: {
    '&:hover:not(:disabled):before': {
      borderBottom: '1px solid #8e0a3f',
      transition: 'border-bottom-color 0.3s ease-out'
    },
    '&:after': {
      borderBottom: '2px solid #8e0a3f',
    },
  },
  labelRoot: {
    fontFamily: 'Open Sans, Arial, sans-serif',
    fontWeight: 600,
    fontSize: '14px',
    color: '#333',
    '&.focused': {
      color: '#8e0a3f',
    }
  },
  labelFocused: {
    color: '#8e0a3f !important',
  },
  inputSelect: {
    '&:focus': {
      backgroundColor: 'transparent',
    }
  },
  formControlLabelRoot: {
    display: 'grid',
    gridTemplateColumns: '26px 1fr',
    columnGap: '13px',
    alignItems: 'flex-start',
    marginLeft: 0,
    marginRight: 0,
  },
  checkboxRoot: {
    color: '#333',
    padding: 0,
  },
  checkboxRootChecked: {
    color: '#333 !important',
  },
  button: {
    gridArea: 'button',
    paddingTop: '17px',
    paddingBottom: '17px',
    fontFamily: 'Poppins, Arial, sans-serif',
    fontWeight: 600,
    fontSize: '20px',
    color: '#ffffff !important',
    backgroundColor: '#8e0a3f !important',
    borderRadius: 0,
    '&:not(:disabled):hover': {
      backgroundColor: '#be0d54 !important',
    },
    '&:not(:disabled):active': {
      backgroundColor: '#be0d54 !important',
    },
    '&:disabled': {
      opacity: 0.7,
      cursor: 'default',
    },
  },
  form_response: {
    opacity: 0,
    transition: 'opacity 0.2s ease'
  }
}));

export const FeedbackForm = () => {
  const classes = useStyles();
  const [name, setName] = useState('');
  const [requirements, setRequirements] = useState('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');
  const [message, setMessage] = useState('');
  const [terms, setTerms] = useState(false);
  const [checkedAgreement, setCheckedAgreement] = useState(false);
  const [recaptchaToken, setRecaptchaToken] = useState('');
  const [answer, setAnswer] = useState({});
  const [fieldWarning, setFieldWarning] = useState({});
  const [submitText, setSubmitText] = useState('Senden');
  const [submitDisable, setSubmitDisable] = useState(false);

  const handleInputChange = (event) => {
    const fieldName = event.target.name;
    const fieldValue = event.target.value;

    if (fieldName === 'yourname') {
      setName(fieldValue);
    } else if (fieldName === 'requirements') {
      setRequirements(fieldValue);
    } else if (fieldName === 'phone') {
      setPhone(fieldValue);
    } else if (fieldName === 'email') {
      setEmail(fieldValue);
    } else if (fieldName === 'message') {
      setMessage(fieldValue);
    } else if (fieldName === 'checkedAgreement') {
      setTerms((prevState) => !prevState);
      setCheckedAgreement(event.target.checked);
    }
  };

  const recaptchaLoaded = (e) => {
    //console.log('captcha successfully loaded');
  }

  const recaptchaVerify = (e) => {
    //console.log('recaptcha ver: ', e);
  }

  const submitForm = (event) => {
    event.preventDefault();

    setSubmitText('Senden...');
    setSubmitDisable(true);

    axios.post( clientConfig.siteUrl + clientConfig.formApiUrl,
      new FormData(event.target),
      {
        headers: {
          'Content-Type': 'multipart/form-data; boundary=X-TANDEM-BOUNDARY',
        }
      }
    ).then( response => {
      setAnswer(response.data);


      if (['mail_failed', 'acceptance_missing', 'validation_failed'].indexOf(response.data.status) > -1 ) {
        const invalidFields = {};
        response.data?.invalid_fields && response.data.invalid_fields.forEach(item => {
          const fieldName = item.error_id.replace('-ve-', '');
          invalidFields[fieldName] = item.message;
        })
        setFieldWarning(invalidFields);
      }

      if (response.data.status === 'mail_sent') {
        setName('');
        setRequirements('');
        setPhone('');
        setEmail('');
        setMessage('');
        setTerms((prevState) => !prevState);
        setCheckedAgreement(false);
        setFieldWarning({});
      }

    })
    .catch( error => {
      console.log(error);
    })
    .finally( () => {
      setSubmitText('Senden');
      setSubmitDisable(false);
    });
}

  const updateRecaptchaToken = (token) => {
    setRecaptchaToken(token);
  }

  return (
    <Fragment>
      
      <form styleName='root' encType="multipart/form-data" method="post" onSubmit={submitForm}>
        <TextField classes={{ root: classes.field_name }}
          InputLabelProps={{
            classes: {
              root: classes.labelRoot,
              focused: classes.labelFocused,
            }
          }}
          InputProps={{ 
            classes: {
              root: classes.inputRoot,
              underline: classes.inputUnderline
            } 
          }}
          id="name"
          name="yourname"
          label="Name"
          value={name}
          onChange={handleInputChange}
          error={fieldWarning?.yourname && true}
          helperText={fieldWarning?.yourname}
          required
        />

        <FormControl className={classes.field_requirements} required>
          <InputLabel
            classes={{
              root: classes.labelRoot,
              focused: classes.labelFocused,
            }}
            id="label-requirements"
          >
            Ich möchte
          </InputLabel>
          <Select classes={{ select: classes.inputSelect }}
            input={
              <Input
                classes={{
                  root: classes.inputRoot,
                  underline: classes.inputUnderline
                }}
              />
            }
            labelId="label-requirements"
            id="requirements"
            name="requirements"
            value={requirements}
            onChange={handleInputChange}
            IconComponent={KeyboardArrowDownIcon}
          >
            <MenuItem value=""><em>None</em></MenuItem>
            <MenuItem value="Ein kostenloses Angebot anfordern">Ein kostenloses Angebot anfordern</MenuItem>
            <MenuItem value="Kostenlose Beratung">Kostenlose Beratung</MenuItem>
            <MenuItem value="Suchmaschinenoptimierung">Suchmaschinenoptimierung</MenuItem>
            <MenuItem value="Newsletter-Applikation">Newsletter-Applikation</MenuItem>
            <MenuItem value="Web Analytics">Web Analytics</MenuItem>
            <MenuItem value="E-Commerce / Onlineshop">E-Commerce / Onlineshop</MenuItem>
            <MenuItem value="Partner werden">Partner werden</MenuItem>
            <MenuItem value="Mich um einen Job bewerben">Mich um einen Job bewerben</MenuItem>
            <MenuItem value="Anderes Anliegen">Anderes Anliegen</MenuItem>
          </Select>
        </FormControl>
    
        <TextField classes={{ root: classes.field_email }}
          InputLabelProps={{
            classes: {
              root: classes.labelRoot,
              focused: classes.labelFocused,
            }
          }}
          InputProps={{ 
            classes: {
              root: classes.inputRoot,
              underline: classes.inputUnderline
            } 
          }}
          id="email"
          name="email"
          label="E-Mail"
          placeholder="mymail@mailbox.com"
          value={email}
          onChange={handleInputChange}
          error={fieldWarning?.email && true}
          helperText={fieldWarning?.email}
          required
        />
      
        <TextField classes={{ root: classes.field_phone }}
          InputLabelProps={{
            classes: {
              root: classes.labelRoot,
              focused: classes.labelFocused,
            }
          }}
          InputProps={{ 
            classes: {
              root: classes.inputRoot,
              underline: classes.inputUnderline
            } 
          }}
          id="phone"
          name="phone"
          label="Telefon"
          value={phone}
          onChange={handleInputChange}
          error={fieldWarning?.phone && true}
          helperText={fieldWarning?.phone}
          required
        />

        <TextField classes={{ root: classes.field_message }}
          InputLabelProps={{
            classes: {
              root: classes.labelRoot,
              focused: classes.labelFocused,
            }
          }}
          InputProps={{ 
            classes: {
              root: classes.inputRoot,
              underline: classes.inputUnderline
            } 
          }}
          id="message"
          name="message"
          label="Ihre Nachricht"
          value={message}
          onChange={handleInputChange}
          multiline
          minrows={4}
          error={fieldWarning?.message && true}
          helperText={fieldWarning?.message}
          required
        />

        <FormControl classes={{ root: classes.field_agreement }}
          error={answer?.status === "acceptance_missing" && true}
        >
          <FormControlLabel classes={{ root: classes.field_agreement_label }}
            control={
              <Checkbox classes={{
                  root: classes.checkboxRoot,
                  checked: classes.checkboxRootChecked
                }}
                icon={
                  <SvgIcon version="1.2" xmlns="http://www.w3.org/2000/svg" overflow="visible" preserveAspectRatio="none" width="28" height="28">
                    <path d="M1 1h26v26H1V1z" vectorEffect="non-scaling-stroke" stroke="#333" fill="#fff"/>
                  </SvgIcon>
                }
                checkedIcon={<CheckIcon fontSize='medium' />}
                checked={terms}
                onChange={handleInputChange}
                value={checkedAgreement}
                name='checkedAgreement'
                styleName={answer?.status === 'acceptance_missing' && `checkbox-warning`}
              />
            }
            label={
              <span styleName='agreementText'>
                Ja, ich habe die <Link styleName="agreementLink" to="/datenschutz.html">Datenschutzerklärung</Link> gelesen und bin mit der darin beschriebenen Verarbeitung meiner personenbezogenen Daten einverstanden *
              </span>
            }
          />
        </FormControl>
        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f2291-o1" />
        <FormControl styleName='field_captcha'
                     error={fieldWarning?.recaptcha && true}
        >
          <Recaptcha
            sitekey={clientConfig.gCaptchaPubKey}
            onChange={updateRecaptchaToken}
            hl='de'
            render='explicit'
            onloadCallback={recaptchaLoaded}
            verifyCallback={recaptchaVerify}
          />
          {fieldWarning?.recaptcha && <FormHelperText>{fieldWarning?.recaptcha}</FormHelperText>}
        </FormControl>

        <div styleName={answer?.status} className={`form-response`}><span>{answer?.message}</span></div>

        <Button className={classes.button}
          styleName='button'
          type='submit'
          variant='contained'
          color='primary'
          disabled={submitDisable}
        >
          {submitText}
        </Button>
      </form>
    </Fragment>
  );
}
