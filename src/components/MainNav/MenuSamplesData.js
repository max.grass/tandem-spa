export const menuData = [
  {
    name: 'E-Commerce',
    link: '#',
    isActive: false,
    dropdownItems: [
      {name: 'E-Commerce', link: '/marketing-stuttgart/e-commerce.html'},
      {name: 'Magento Webshop', link: '/magento-webshop.html'},
      {name: 'Warum Magento', link: '/magento-webshop/warum-magento.html'},
      {name: 'B2B Commerce', link: '/b2b-commerce.html'},
      {name: 'PWA React', link: '/pwa-react.html'},
    ],
  },
  {
    name: 'Web-Design \u0026 UX',
    link: '#',
    isActive: false,
    dropdownItems: [
      {name: 'Webdesign', link: '/webdesign-cms.html'},
      {name: 'UX', link: '/ux.html'},
      {name: 'Usability', link: '/webdesign-cms/usability.html'},
      {name: 'Responsives Webdesign', link: '/webdesign-cms/responsives-webdesign.html'},
      {name: 'Website Relaunch', link: '/website-relaunch.html'},
    ],
  },
  {
    name: 'Web-Entwicklung',
    link: '#',
    isActive: false,
    dropdownItems: [
      // {name: 'Webportale', link: '#'},
      {name: 'CMS', link: '/webdesign-cms/content-management-system.html'},
      {name: 'Magento', link: '/magento-webshop/warum-magento.html'},
      {name: 'Drupal', link: '/webdesign-cms/drupal.html'},
      // {name: 'Frameworks', link: '#'},
      {name: 'PWA React', link: '/pwa-react.html'},
    ],
  },
  {
    name: 'Marketing \u0026 PR',
    link: '#',
    isActive: false,
    dropdownItems: [
      {name: 'Marketing', link: '/marketing-stuttgart.html'},
      {name: 'Content Marketing', link: '/pr.html'},
      {name: 'Suchmachinenoptimierung', link: '/marketing-stuttgart/suchmaschinenoptimierung.html'},
      {name: 'Social media marketing', link: '/marketing-stuttgart/social-media-marketing.html'},
      {name: 'Email marketing', link: '/marketing-stuttgart/e-mail-marketing.html'},
      {name: 'Web-Analytics', link: '/web-analytics.html'},
      {name: 'Unternehmenspräsentation', link: '/marketing-stuttgart/unternehmenspraesentation.html'},
    ],
  },
  {
    name: 'Referenzen',
    link: '/referenzen.html',
    isActive: false,
    dropdownItems: null,
  },
  {
    name: 'Über uns',
    link: '#',
    isActive: false,
    dropdownItems: [
      {name: 'Über uns', link: '/werbeagentur-stuttgart.html'},
      // {name: 'Herangehensweise', link: '/werbeagentur-stuttgart/approach.html'},
      {name: 'Kontakt', link: '/werbeagentur-stuttgart/contact.html'},
    ],
  },
  {
    name: 'Blog',
    link: '/blog.html',
    isActive: false,
    dropdownItems: null,
  },
];

