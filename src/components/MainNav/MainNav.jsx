import React, { useState, useEffect } from 'react';

import { Logo } from 'components/Logo';
import { NavTrigger } from 'components/NavTrigger';
import { MenuAccordion } from './MenuAccordion';
import { MenuList } from './MenuList';

import { menuData } from './MenuSamplesData.js'
import './MainNav.css';

export const MainNav = () => {
  const [mobileMenuIsOpen, setMobileMenuIsOpen ] = useState(false);
  const [menuItems, setMenuItems] = useState(menuData)
  const rootClass= mobileMenuIsOpen ? 'root root-open' : 'root';

  const handleMenuToggle = () => {
    setMobileMenuIsOpen((prevState) => !prevState);
  }
  
  const handleEsc = (event) => {
    if (event.keyCode === 27) {
      setMenuItems(items =>
        items.map(item => ({
          ...item,
          isActive: false
        }))
      )
    }
  }

  useEffect(() => {
    window.addEventListener("keydown", (e) => handleEsc(e));
    return () => {
      window.removeEventListener("keydown", (e) => handleEsc(e) );
    };
  }, []);

  const handleItemActivate = (name) => {
    
    if (name === 'close_dropdown') {
      setMenuItems(items =>
        items.map(item => ({
          ...item,
          isActive: false
        }))
      )
    } else {
      setMenuItems(items =>
        items.map(item => ({
          ...item,
          isActive: item.name === name
        }))
      )
    }
  }

  return (
    <nav styleName={rootClass}>
      <div styleName='container'>
        <Logo />
        <NavTrigger isOpen={mobileMenuIsOpen} triggerClick={handleMenuToggle}/>
        
        {mobileMenuIsOpen 
          ? <MenuAccordion menuItems={menuItems} />
          : <MenuList
              menuItems={menuItems}
              itemClick={handleItemActivate}
            />
        }

        {/* <MainMenu items={menuItems} isOpen={menuIsOpen} itemClick={handleItemActive} /> */}
      </div>
    </nav>
  );
}