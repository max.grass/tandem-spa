import './MainNav.css';

import React, { useState } from 'react';
import { Link } from 'react-router-dom';

import { makeStyles } from '@material-ui/core/styles';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

const useStyles = makeStyles((theme) => ({
  accordionRoot: {
    color: '#ffffff',
    backgroundColor: '#1e1e20',
    boxShadow: 'none',
  },
  summaryRoot: {
    padding: 0,
  },
  summaryContent: {
    margin: '15px 0',
  },
  summaryExpanded: {
    margin: '15px 0',
  },
  summaryIcon: {
    color: '#ffffff'
  },
  menuLink: {
    fontFamily: 'Poppins, Arial, sans-serif',
    fontWeight: 600,
    fontSize: '24px',
    lineHeight: 1.5,
    color: '#ffffff',
    textDecoration: 'none',
  },
  dropdownLink: {
    fontFamily: 'Open Sans, Arial, sans-serif',
    fontWeight: 400,
    fontSize: '16px',
    lineHeight: 1.5,
    color: '#f7f3f4',
    textDecoration: 'none',
    borderBottom: '1px solid #8e0a3f',
    transition: 'border 0.2s ease-out',
    '&:hover': {
      borderBottom: '1px solid #be0d54',
    }
  }
}));

export const MenuAccordion = (props) => {
  const classes = useStyles();
  const { menuItems } = props;
  const [ expanded, setExpanded ] = useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };

  return (
    <div styleName='accordion'>
      {menuItems.map((item, idx) => (
        <Accordion
          key={idx}
          classes={{ root: classes.accordionRoot }}
          square={true}
          expanded={
            item.dropdownItems 
            ? expanded === item.name
            : null
          }
          onChange={
            item.dropdownItems 
              ? handleChange(item.name)
              : null
            }
        >
          <AccordionSummary
            classes={{
              root: classes.summaryRoot,
              content: classes.summaryContent,
              expanded: classes.summaryExpanded,
            }}
            expandIcon={ 
              item.dropdownItems 
              ? <ExpandMoreIcon className={classes.summaryIcon}/>
              : null
            }
            aria-controls={`${item.name}-content`}
            id={`${item.name}-header`}
          >
            <Typography>
              <Link className={classes.menuLink} to={item.link}>{item.name}</Link>
            </Typography>
          </AccordionSummary>
          {item.dropdownItems && item.dropdownItems.map((dropdownItem, idx) => (
            <AccordionDetails key={idx} >
              <Typography>
                <Link className={classes.dropdownLink} to={dropdownItem.link}>{dropdownItem.name}</Link>
              </Typography>
            </AccordionDetails>
          ))}
        </Accordion>
      ))}
    </div>
  );
}
