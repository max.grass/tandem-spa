import './MainNav.css';

import React from 'react';

import { Link, useLocation } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';

const useStyles = makeStyles((theme) => ({
  buttonRoot: {
    padding: '30px',
  },
  buttonColorPrimary: {
    color: '#fff',
  },
}));

export const MenuList = (props) => {
  const { menuItems, itemClick } = props;
  const classes = useStyles();
  
  const handleClick = (event) => {
    if (typeof itemClick === 'function') {
      itemClick(event.target.name);
    }
  };

  const location = useLocation();
  
  return (
    <ul styleName='list'>
      {menuItems.map((item, idx) => (
        <li styleName={item.isActive ? 'item_active' : null} key={idx}>
          <Link
            styleName='link'
            to={item.link}
            name={item.name}
            onClick={handleClick}
          >
            {item.name}
          </Link>
          {item.dropdownItems && 
            <div styleName='dropdown_wrapper'
              name='close_dropdown'
              onClick={handleClick}
            >
              <div styleName='dropdown_container'>
                <p styleName='dropdown_title'>{item.name}</p>
                <ul styleName='dropdown_list'>
                  {item.dropdownItems.map((dropdownItem, idx) => (
                    <li styleName='dropdown_item' key={idx}>
                      <Link 
                        styleName={ (location.pathname.indexOf(dropdownItem.link) != -1) ? 'linkActive' : 'link'}
                        to={dropdownItem.link}
                      >{dropdownItem.name}</Link>
                    </li>
                  ))}
                </ul>
                <IconButton 
                  classes={{
                    root:classes.buttonRoot,
                    colorPrimary: classes.buttonColorPrimary 
                  }}
                  name='close_dropdown'
                  aria-label='close dropdown menu'
                  color='primary'
                  size='medium'
                  onClick={handleClick}
                >
                  <CloseIcon size='large' />
                </IconButton>
              </div>
            </div>
          }
        </li>
      ))}
    </ul>
  );
}