import React from "react";
import PropTypes from "prop-types";
import GoogleMapReact from 'google-map-react';
import {clientConfig} from "../../clientConfig";

import "./GoogleMap.css";

const GoogleMapPin = () => (<div styleName="pin"></div>);

export const GoogleMap = (props) => {
  const { key, center_point, zoom, point_berlin, point_stuttgart } = props;

  return (
    <div styleName="root">
      <GoogleMapReact
        bootstrapURLKeys={key}
        defaultCenter={center_point}
        defaultZoom={zoom}
      >
        <GoogleMapPin {...point_berlin} />
        <GoogleMapPin {...point_stuttgart} />
      </GoogleMapReact>
    </div>
  );
}

GoogleMap.propTypes = {
  key: PropTypes.shape({
    key: PropTypes.string.isRequired,
    language: PropTypes.string,
  }).isRequired,
  center_point: PropTypes.shape({
    lat: PropTypes.number.isRequired,
    lng: PropTypes.number.isRequired,
  }).isRequired,
  zoom: PropTypes.number,
  point_berlin: PropTypes.shape({
    lat: PropTypes.number.isRequired,
    lng: PropTypes.number.isRequired,
  }).isRequired,
  point_stuttgart: PropTypes.shape({
    lat: PropTypes.number.isRequired,
    lng: PropTypes.number.isRequired,
  }).isRequired,
};

GoogleMap.defaultProps = {
  key: {
    key: clientConfig.gMapKey,
    language: "de",
  },
  center_point: {
    lat: 50.6215525,
    lng: 11.2883066,
  },
  zoom: 5,
  point_berlin: {
    lat: 52.550928,
    lng: 13.418282,
  },
  point_stuttgart: {
    lat: 48.692177,
    lng: 9.1583312,
  },
}
