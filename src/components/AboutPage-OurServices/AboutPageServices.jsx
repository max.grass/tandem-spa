import React, { Fragment } from "react";

import { SectionWhite } from "components/SectionWhite";
import { SectionColor } from "components/SectionColor";

import "./AboutPageServices.css";

export const AboutPageServices = () => {
  return (
    <section>
      <h2 className="visuallyHidden">Unsere Dienstleistungen</h2>
      <SectionWhite 
        title="Ihr Fullservice-Dienstleister für digitale Medien"
        text="Zu unseren Stärken gehört die Konzeption von Webportalen und Webshops, Leistungen rund um E-Commerce sowie die Betreuung aller Online-Marketing-Kanäle."
      />
      <SectionColor 
        content={
          <Fragment>
            <p styleName="text">Sie haben Ihren Kunden etwas Wichtiges zu sagen – wir setzen es in erfolgreiche Werbemaßnahmen auf allen digitalen Kanälen um: Das Tandem-Team besteht aus Projektleitern, Programmierern, Designern, Redakteuren und Marketing-Experten, die Ihnen in allen Leistungsbereichen kompetent zur Seite stehen. Um unseren Kunden optimale Ergebnisse zu ermöglichen, setzen wir moderne Technologien ein und gehen jedes Projekt voller Motivation, Leidenschaft und Einsatzbereitschaft an.</p>
            <p styleName="text">Als junge, dynamische Werbeagentur sind wir spezialisiert auf digitale Medien und entwickeln Lösungen in den Bereichen E-Commerce, Public Relations (PR), Social Media, Grafikdesign und vieles mehr.</p>
          </Fragment>
        }
        textColor="var(--color-white)"
        backgroundColor="#770c36"
      />
    </section>
  );
}