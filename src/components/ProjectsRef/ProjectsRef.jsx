import React from "react";

import { ProjectCardRef } from "./ProjectCardRef";

import "./ProjectsRef.css";

export const ProjectsRef = (props) => {
  
  const {
    data, 
    title,
    referenzePage
  } = props;

  return (
    <div className="section" style={referenzePage ? {paddingBottom: 0} : null}>
        <ul className="container" styleName="list">
            {data.map((item, idx) => <ProjectCardRef key={`project-${idx}`} idx={idx} {...item}/>)}
        </ul>
    </div>
  );
}
