import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import "./ProjectCardRef.css";

export const ProjectCardRef = (props) => {
  const {
    title,
    desc,
    text,
    imgSrc,
    imgWidth,
    imgHeight,
    imgAlt,
    imgPosition,
    url,
    position,
    label,
    labelColor
  } = props;

  return (
    <li styleName={position ? 'root position-' + position : 'root'}>
      <Link styleName="link" to={url}>
        <div styleName="image-wrapper" style={{maxWidth: imgWidth + 'px'}}>
          <img styleName="image"
            loading="lazy"
            style={{"objectPosition": imgPosition}}
            src={imgSrc}
            width={imgWidth}
            height={imgHeight}
            alt={imgAlt}
          />
        </div>
        <div styleName="main-content">
          <h3 styleName="header"><span>{title}</span>{label && <span styleName="label" style={{backgroundColor: labelColor}}>{label}</span>}</h3>
          <p styleName="desc">{desc}</p>
        </div>
      </Link>
    </li>
  );
};

ProjectCardRef.propTypes = {
  name: PropTypes.string.isRequired,
  desc: PropTypes.string.isRequired,
  imgSrc: PropTypes.string.isRequired,
  imgWidth: PropTypes.number,
  imgHeight: PropTypes.number,
  imgPosition: PropTypes.string,
  url: PropTypes.string.isRequired,
  label: PropTypes.string,
  labelColor: PropTypes.string
};

ProjectCardRef.defaultProps = {
  name: "Sandra Jurisch",
  desc: "Markt- und Konkurrenzanalysen",
  imgSrc: "https://via.placeholder.com/375x258.png",
  imgWidth: 375,
  imgHeight: 258,
  imgAlt: "Foto der Mitarbeiterin Sandra Jurisch",
  imgPosition: "top center",
  label: "",
  labelColor: ""
}
