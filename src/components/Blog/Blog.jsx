import './Blog.css';
import '../../assets/slick/slick.css';
import '../../assets/slick/slick-theme.css';

import React, { useEffect, useRef, useContext, useState } from 'react';
import Slider from 'react-slick';
import axios from "axios";
import CustomCursorContext from 'components/CustomCursor/context/CustomCursorContext';
import { ArticlePreview } from 'components/ArticlePreview';
import {clientConfig} from "components/../clientConfig";

import preloader from "assets/images/rolling-preloader.svg";

export const Blog = () => {
  const sliderRef = useRef(null);
  const { cursorType, setCursorType } = useContext(CustomCursorContext);
  const [loading, setLoading] = useState(true);
  const [postData, setPostData] = useState(null);

  // useEffect(()=> {
  //   sliderRef.current.addEventListener('mouseenter', () => {
  //     setCursorType('slider');
  //     return () => {};
  //   });

  //   sliderRef.current.addEventListener('mouseleave', () => {
  //     setCursorType('default');
  //     return () => {};
  //   });
  // }, []);

  const settings = {
    arrows: false,
    dots: true,
    infinite: true,
    variableWidth: true,
  };

  useEffect( () => {
    const wordPressSiteURL = clientConfig.siteUrl;

    /**
     * we expected
     * status 200
     * post_data - array of posts
     * found_posts - total count of posts
     * page_count - for pagination
     *
    */

    axios.get( `${wordPressSiteURL}wp-json/trae/v1/articles?page_no=1` )
      .then( response => {
        setLoading(false);

        if (200 === response.data.status) {
          setPostData(response.data.post_data);
        } else {
          setError('No posts found');
        }

      })
      .catch( error => {
        setError(error.response.data.message);
        setLoading(false);
      });
  }, [])


  if (loading || !postData) {
    return (
      <div className="container">
        <div className="rolling-loader">
          <img src={preloader}/>
        </div>
      </div>
    )
  }

  return (
    <section className='section-m' styleName='root'>

      <div styleName='container'>
        <h2 styleName='sectionTitle'>Blog</h2>
        <div styleName='sliderWrapper' ref={sliderRef}>
          <Slider {...settings}>
            {
              postData.map( post => {
                return <ArticlePreview
                          key={post.id}
                          title={post.title}
                          text={post.excerpt}
                          href={post.slug}
                          src={post.attachment_image.img_src[0]}
                          width={post.attachment_image.img_src[1]}
                          height={post.attachment_image.img_src[2]}
                          webp={post.attachment_image.img_src[4]}
                          figure={post.figure}
                          color={post.figure_color}
                          attachment={post.attachment_image}
                        />
              })
            }
          </Slider>
        </div>
      </div>
    </section>
  );
}

