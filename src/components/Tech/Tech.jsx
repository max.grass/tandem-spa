import './Tech.css';

import React, {useRef, useEffect} from 'react';

import {techListTween} from 'components/Animation';

import logoDrupal from './drupal.svg';
import logoMagento from './magento.svg';
import logoSymfony from './symfony.svg';
import logoIOS from './ios.svg';
import logoAndroid from './android.svg';
import logoPwa from './pwa.svg';
import logoReact from './react.svg';


export const Tech = () => {
  let item1 = useRef(null);
  let item2 = useRef(null);
  let item3 = useRef(null);
  let item4 = useRef(null);
  let item5 = useRef(null);
  let item6 = useRef(null);
  let item7 = useRef(null);
  let section = useRef(null);

  useEffect(() => {
    techListTween(section, item1, item2, item3, item4, item5, item6, item7);
  }, []);

  return (
    <section styleName='root' ref={(el) => (section = el)}>
      <h2 className='visuallyHidden'>Technologien, die wir verwenden:</h2>
      <div styleName='container'>
        <ul styleName='list'>
          <li styleName='itemDrupal' ref={(el) => (item1 = el)}>
            <img src={logoDrupal} width='175' height='44' alt='Drupal-Logo' loading="lazy"/>
            <p className='visuallyHidden'>Drupal - Open-Source-CMS.</p>
          </li>
          <li styleName='itemMagento' ref={(el) => (item2 = el)}>
            <img src={logoMagento} width='131' height='44' alt='Magento-Logo' loading="lazy"/>
            <p className='visuallyHidden'>Magento - E-Commerce-Plattformen.</p>
          </li>
          <li styleName='itemSymfony' ref={(el) => (item3 = el)}>
            <img src={logoSymfony} width='225' height='56' alt='Symfony-Logo' loading="lazy"/>
            <p className='visuallyHidden'>Symfony ist ein Satz wiederverwendbarer PHP-Komponenten und ein PHP-Framework zum Erstellen von Webanwendungen, APIs, Microservices und Webservices.</p>
          </li>
          <li styleName='itemIOS' ref={(el) => (item4 = el)}>
            <img src={logoIOS} width='69' height='44' alt='IOS-Logo' loading="lazy"/>
            <p className='visuallyHidden'>App-Entwicklung für Apple IOS.</p>
          </li>
          <li styleName='itemAndroid' ref={(el) => (item5 = el)}>
            <img src={logoAndroid} width='157' height='24' alt='Android-Logo' loading="lazy"/>
            <p className='visuallyHidden'>Entwicklung von Android-Anwendungen.</p>
          </li>
          <li styleName='itemPwa' ref={(el) => (item6 = el)}>
            <img src={logoPwa} width='81' height='32' alt='PWA-Logo' loading="lazy"/>
            <p className='visuallyHidden'>Progressive Web Apps</p>
          </li>
          <li styleName='itemReact' ref={(el) => (item7 = el)}>
            <img src={logoReact} width='56' height='56' alt='React-Logo' loading="lazy"/>
            <p className='visuallyHidden'>React</p>
          </li>
        </ul>
      </div>
    </section>
  )
}
