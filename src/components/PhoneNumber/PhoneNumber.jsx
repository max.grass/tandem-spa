import React from "react";
import PropTypes from "prop-types";

import "./PhoneNumber.css";

export const PhoneNumber = (props) => {
  const { text, phoneLink, phoneCaption } = props;
  
  return (
    <div styleName="root">
      <p styleName="text">{text}</p>
      <a styleName="link" href={phoneLink}>{phoneCaption}</a>
    </div>
  );
}

PhoneNumber.propTypes = {
  text: PropTypes.string,
  phoneLink: PropTypes.string,
  phoneCaption: PropTypes.string,
};

PhoneNumber.defaulpProps = {
  text: "Telefonnummer:",
  phoneLink: "tel:+49071194552157",
  phoneCaption: "+49 (0) 711 94552157",
};
