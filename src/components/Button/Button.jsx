import React from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

import arrow from "assets/images/arrow-right.svg";

import "./Button.css";

export const Button = (props) => {
  const { url, caption, color } = props;

  const style = color ? { "--bg-color": color} : null;

  return (
    <Link to={url} styleName="root" style={style}>
      {caption}
    </Link>
  );
}

Button.propTypes = {
  url: PropTypes.string.isRequired,
  caption: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.object
  ]),
  color: PropTypes.string,
};

Button.defaultProps = {
  url: "/",
  caption: "Fälle zeigen",
}