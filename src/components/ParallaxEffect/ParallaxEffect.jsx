import React from "react";
import PropTypes from "prop-types";
import { Parallax } from 'react-parallax';

import defaultClasses from "./ParallaxEffect.css";

export const ParallaxEffect = (props) => {
  const { 
    image,
    alt,
    strength,
    children 
  } = props;

  return (
    <Parallax
      className={defaultClasses.container}
      bgImage={image}
      bgImageAlt={alt}
      bgImageStyle={{ objectFit: "cover" }}
      strength={strength}
      children={children}
    />
  );
};

ParallaxEffect.propTypes = {
  image: PropTypes.string.isRequired,
  alt: PropTypes.string,
  strength: PropTypes.number,
  children: PropTypes.node,
};

ParallaxEffect.defaultProps = {
  strength: 200,
  children: null
};