import React from 'react';

import './ArticlePreview.css';
	
export const ArticlePreview = (props) => {
	const { title, text, src, href, width, height, figure, color, attachment } = props;

  const contentClass = (!figure || (figure === 'square') )
    ? 'textContent' 
    :`textContent icon${figure}`;

  const contentStyle = {
    "--figure-color": color
  };

  
  return(
    <div styleName='root'>
      <div styleName={contentClass} style={contentStyle}>
        <a styleName='link' href={'/blog/' + href}>
          <h3 styleName='title' dangerouslySetInnerHTML={{__html: title}}></h3>
        </a>
        <p styleName='text' dangerouslySetInnerHTML={{__html: text}}></p>
      </div>
      <figure styleName='imageWrapper'>
        <a styleName='imageLink' href={'/blog/' + href}>
            <picture>
                {attachment.img_src[4] && <source srcSet={attachment.img_src[4]} width={attachment.img_src[1]} height={attachment.img_src[2]}/>}
                <img loading="lazy" src={attachment.img_src[0]} alt={title} width={width} height={height} srcSet={attachment.img_srcset} sizes={attachment.img_sizes}/>
            </picture>
        </a>
      </figure>
    </div>
  );
}
