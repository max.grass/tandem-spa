import React, {Fragment} from "react";
// import PropTypes from "prop-types";

import classes from "./CheckedList.css";

export const CheckedList = (props) => {
	const {color, title, content} = props;
    const style = {
        "--color": color,
      }

	return (
		<div>
			{ title && <h4 className={'heading-5'}>{title}</h4>}
			<ul className={classes.list} style={style}>
				{content}
			</ul>
		</div>

	);
};
