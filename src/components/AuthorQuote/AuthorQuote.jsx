import React, {Fragment} from 'react';
import { SectionColor } from "components/SectionColor";

import classes from './AuthorQuote.css';



export const AuthorQuote = (props) => {
    const {
        photo,
        text,
        authorName,
        authorPosition,
        backgroundColor,
        textColor
    } = props;


    return (
        <SectionColor textColor={textColor} backgroundColor={backgroundColor}
        content={
            <Fragment>
                <div className={classes.quote}>
                    <div className={classes.image}>
                        <img src={photo} alt={authorName} width={240} height={240} loading="lazy" />
                    </div>
                    <div className={classes.text}>{text}</div>
                    <div className={classes.author}>
                        <svg width="60" height="46" viewBox="0 0 60 46" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M18.0882 17.5668C19.8529 17.5668 22 18.655 24.5294 20.8315C27.1176 22.9491 28.4118 25.9785 28.4118 29.9197C28.4118 33.8021 26.9706 37.3609 24.0882 40.5962C21.2059 43.8315 17.7647 45.4491 13.7647 45.4491C9.76471 45.4491 6.47059 44.3021 3.88235 42.0079C1.29412 39.7138 0 36.0373 0 30.9785C0 25.9197 2.32353 20.1256 6.97059 13.5962C11.6765 7.06675 17.0588 2.86087 23.1176 0.978516C25.1176 2.03734 26.2059 3.65499 26.3824 5.83146C24.2059 7.53734 22.2941 9.65499 20.6471 12.1844C19.0588 14.655 18.2059 16.4491 18.0882 17.5668ZM49.6765 17.5668C51.4412 17.5668 53.5882 18.655 56.1176 20.8315C58.7059 22.9491 60 25.9785 60 29.9197C60 33.8021 58.5588 37.3609 55.6765 40.5962C52.7941 43.8315 49.3529 45.4491 45.3529 45.4491C41.3529 45.4491 38.0588 44.3021 35.4706 42.0079C32.9412 39.7138 31.6765 36.5668 31.6765 32.5668C31.6765 28.5668 32.7059 24.3903 34.7647 20.0373C36.8824 15.6256 39.7353 11.655 43.3235 8.12558C46.9118 4.59616 50.7353 2.21381 54.7941 0.978516C56.7941 2.09616 57.8824 3.71381 58.0588 5.83146C55.8235 7.53734 53.8824 9.65499 52.2353 12.1844C50.6471 14.655 49.7941 16.4491 49.6765 17.5668Z" fill="currentColor"/>
                        </svg>
                        <div className={classes.authorWrapper}>
                            <div className={classes.name}>{authorName}</div>
                            <div className={classes.position}>{authorPosition}</div>
                        </div>
                    </div>
                </div>
            </Fragment>
        } />
    )
}
