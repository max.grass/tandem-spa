import { useEffect} from "react";
import { useLocation } from "react-router-dom";
import ReactGA from "react-ga";
import TagManager from 'react-gtm-module'
import { clientConfig } from "../../clientConfig";

const getCookie = (name) => {
    const matches = document.cookie.match(new RegExp(
		"(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
	));
	return matches ? decodeURIComponent(matches[1]) : undefined;
}

const usePageTracking = () => {
  const location = useLocation();
  
  useEffect(() => {
    
    const cookie = getCookie('user_cookie_group'),
        agree = cookie ? JSON.parse(cookie).indexOf('statistics') : null;
        
    if (agree && (agree != -1) ) {
        
        ReactGA.initialize(clientConfig.gaId, {
            gaOptions: {
                anonymizeIp: true
            }
        });
        ReactGA.pageview(location.pathname + location.search);


        const tagManagerArgs = {
          gtmId: clientConfig.gtmId
        }
        TagManager.initialize(tagManagerArgs)
    }

  }, [location]);
};

export default usePageTracking;