/**
 *
 * Loader Component
 */

import React from 'react';

export const Loader = () => {
  return (
    <div styleName="loader">
      <div />
      <div />
    </div>
  )
}
