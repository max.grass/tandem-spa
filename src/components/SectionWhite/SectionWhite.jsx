import React from "react";
import PropTypes from "prop-types";
import { Hr } from "components/Hr";

import "./SectionWhite.css";

export const SectionWhite = (props) => {
  const { title, text, textColor } = props;
  const style = {
    "--text-color": textColor,
  }

  return (
    <div className="section">
      <div className="container-narrow" styleName="container" style={style}>
        <b className="heading-2">{title}</b>
        <div styleName="hr-wrapper">
          <Hr color={textColor}/>
        </div>
        <p styleName="text">{text}</p>
      </div>
    </div>
  );
};

SectionWhite.propTypes = {
  title: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  textColor: PropTypes.string,
}

SectionWhite.defaultProps = {
  textColor: "var(--color-cherry)",
}
