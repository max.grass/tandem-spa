import React, {useRef, useEffect} from "react";
import gsap from 'gsap';
import {sourceAboutUsTween} from 'components/Animation';

import classes from "./MarkedLogos.css";

import galogo from 'assets/images/logos/galogo.svg';

export const MarkedLogos = (props) => {

	const {listData} = props;

	const trustRef = useRef();
	const trustSelector = gsap.utils.selector(trustRef);

	useEffect(() => {
		sourceAboutUsTween(trustSelector('li'), trustRef.current);
	}, []);
	
	const listContent = listData.map(({ iconSrc, label}) => {
		return (
			<li 
				key={label}
				className={classes.li}
			>
				<img
					className={classes.liIcon}
					src={iconSrc}
					loading="lazy"
				/>
				<p
					className={classes.liText}
				>
					{label}
				</p>
			</li>
		);
	});

	return (
		<div className={classes.listWrp}>
			<ul className={classes.list} ref={trustRef}>
				{listContent}
			</ul>
		</div>
	);
};

// MarkedLogos.propTypes = {
	// listData: PropTypes.array.isRequired
// };
