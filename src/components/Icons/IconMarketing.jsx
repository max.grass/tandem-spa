import React from "react";
import PropTypes from "prop-types";

export const IconMarketing = (props) => {
  const { width, height, primaryColor, secondaryColor } = props;

  return (
    <svg width={width} height={height} fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M81.2 29.4C83 33.9 84 38.8 84 44c0 22.1-17.9 40-40 40S4 66.1 4 44 21.9 4 44 4c5.2 0 10.1 1 14.6 2.8" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
      <path d="M65.3 29.1 47.4 47l-2.1-2.1 18.2-18.2C58.7 21.4 51.7 18 44 18c-14.4 0-26 11.6-26 26s11.6 26 26 26 26-11.6 26-26c0-5.6-1.7-10.7-4.7-14.9Z" fill={primaryColor}/>
      <path d="M44 54c5.523 0 10-4.477 10-10s-4.477-10-10-10-10 4.477-10 10 4.477 10 10 10Z" fill={secondaryColor}/>
      <path d="m64.3 23.7-20 20M73.8 25.6l-9.6-1.8-1.8-9.6L72.7 4l1.7 9.6 9.6 1.7-10.2 10.3Z" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
    </svg>
  );
};

IconMarketing.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  primaryColor: PropTypes.string,
  secondaryColor: PropTypes.string,
};

IconMarketing.defaultProps = {
  width: 88,
  height: 88,
  primaryColor: "var(--color-white)",
  secondaryColor: "var(--color-blue-marguerite)"
};