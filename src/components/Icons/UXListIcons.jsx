import React from "react";

export const UXIcon1 = () => {

  return (
    <svg width="88" height="88" viewBox="0 0 88 88" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path d="M13 53C17.9706 53 22 48.9706 22 44C22 39.0294 17.9706 35 13 35C8.02944 35 4 39.0294 4 44C4 48.9706 8.02944 53 13 53Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
        <path d="M75 53C79.9706 53 84 48.9706 84 44C84 39.0294 79.9706 35 75 35C70.0294 35 66 39.0294 66 44C66 48.9706 70.0294 53 75 53Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
        <path d="M27.96 80C32.9306 80 36.96 75.9706 36.96 71C36.96 66.0294 32.9306 62 27.96 62C22.9894 62 18.96 66.0294 18.96 71C18.96 75.9706 22.9894 80 27.96 80Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
        <path d="M60.04 80C65.0106 80 69.04 75.9706 69.04 71C69.04 66.0294 65.0106 62 60.04 62C55.0694 62 51.04 66.0294 51.04 71C51.04 75.9706 55.0694 80 60.04 80Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
        <path d="M44 53C48.9706 53 53 48.9706 53 44C53 39.0294 48.9706 35 44 35C39.0294 35 35 39.0294 35 44C35 48.9706 39.0294 53 44 53Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
        <path d="M60.04 26C65.0106 26 69.04 21.9706 69.04 17C69.04 12.0294 65.0106 8 60.04 8C55.0694 8 51.04 12.0294 51.04 17C51.04 21.9706 55.0694 26 60.04 26Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
        <path d="M27.96 26C32.9306 26 36.96 21.9706 36.96 17C36.96 12.0294 32.9306 8 27.96 8C22.9894 8 18.96 12.0294 18.96 17C18.96 21.9706 22.9894 26 27.96 26Z" fill="#8E0A3F" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
        <path d="M51 16.7188H36.95" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
        <path d="M70.88 35.9998L64.44 24.8398" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
        <path d="M64.44 63.16L70.88 52" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
        <path d="M37 71.2812H51.05" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
        <path d="M17.12 52L23.56 63.16" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
        <path d="M23.56 24.8398L17.12 35.9998" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
    </svg>
  );
};

export const UXIcon2 = () => {

    return (
        <svg width="88" height="88" viewBox="0 0 88 88" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M13 53C17.9706 53 22 48.9706 22 44C22 39.0294 17.9706 35 13 35C8.02944 35 4 39.0294 4 44C4 48.9706 8.02944 53 13 53Z" fill="#8E0A3F" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M75 53C79.9706 53 84 48.9706 84 44C84 39.0294 79.9706 35 75 35C70.0294 35 66 39.0294 66 44C66 48.9706 70.0294 53 75 53Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M27.96 80C32.9306 80 36.96 75.9706 36.96 71C36.96 66.0294 32.9306 62 27.96 62C22.9894 62 18.96 66.0294 18.96 71C18.96 75.9706 22.9894 80 27.96 80Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M60.04 80C65.0106 80 69.04 75.9706 69.04 71C69.04 66.0294 65.0106 62 60.04 62C55.0694 62 51.04 66.0294 51.04 71C51.04 75.9706 55.0694 80 60.04 80Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M44 53C48.9706 53 53 48.9706 53 44C53 39.0294 48.9706 35 44 35C39.0294 35 35 39.0294 35 44C35 48.9706 39.0294 53 44 53Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M60.04 26C65.0106 26 69.04 21.9706 69.04 17C69.04 12.0294 65.0106 8 60.04 8C55.0694 8 51.04 12.0294 51.04 17C51.04 21.9706 55.0694 26 60.04 26Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M27.96 26C32.9306 26 36.96 21.9706 36.96 17C36.96 12.0294 32.9306 8 27.96 8C22.9894 8 18.96 12.0294 18.96 17C18.96 21.9706 22.9894 26 27.96 26Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M51 16.7188H36.95" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M70.88 35.9998L64.44 24.8398" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M64.44 63.16L70.88 52" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M37 71.2812H51.05" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M17.12 52L23.56 63.16" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M23.56 24.8398L17.12 35.9998" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
        </svg>
    );
  };

  export const UXIcon3 = () => {

    return (
        <svg width="88" height="88" viewBox="0 0 88 88" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M13 53C17.9706 53 22 48.9706 22 44C22 39.0294 17.9706 35 13 35C8.02944 35 4 39.0294 4 44C4 48.9706 8.02944 53 13 53Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M75 53C79.9706 53 84 48.9706 84 44C84 39.0294 79.9706 35 75 35C70.0294 35 66 39.0294 66 44C66 48.9706 70.0294 53 75 53Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M27.96 80C32.9306 80 36.96 75.9706 36.96 71C36.96 66.0294 32.9306 62 27.96 62C22.9894 62 18.96 66.0294 18.96 71C18.96 75.9706 22.9894 80 27.96 80Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M60.04 80C65.0106 80 69.04 75.9706 69.04 71C69.04 66.0294 65.0106 62 60.04 62C55.0694 62 51.04 66.0294 51.04 71C51.04 75.9706 55.0694 80 60.04 80Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M44 53C48.9706 53 53 48.9706 53 44C53 39.0294 48.9706 35 44 35C39.0294 35 35 39.0294 35 44C35 48.9706 39.0294 53 44 53Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M60.04 26C65.0106 26 69.04 21.9706 69.04 17C69.04 12.0294 65.0106 8 60.04 8C55.0694 8 51.04 12.0294 51.04 17C51.04 21.9706 55.0694 26 60.04 26Z" fill="#8E0A3F" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M27.96 26C32.9306 26 36.96 21.9706 36.96 17C36.96 12.0294 32.9306 8 27.96 8C22.9894 8 18.96 12.0294 18.96 17C18.96 21.9706 22.9894 26 27.96 26Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M51 16.7188H36.95" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M70.88 35.9998L64.44 24.8398" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M64.44 63.16L70.88 52" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M37 71.2812H51.05" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M17.12 52L23.56 63.16" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M23.56 24.8398L17.12 35.9998" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
        </svg>
    );
  };

  export const UXIcon4 = () => {

    return (
        <svg width="88" height="88" viewBox="0 0 88 88" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M13 53C17.9706 53 22 48.9706 22 44C22 39.0294 17.9706 35 13 35C8.02944 35 4 39.0294 4 44C4 48.9706 8.02944 53 13 53Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M75 53C79.9706 53 84 48.9706 84 44C84 39.0294 79.9706 35 75 35C70.0294 35 66 39.0294 66 44C66 48.9706 70.0294 53 75 53Z" fill="#8E0A3F" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M27.96 80C32.9306 80 36.96 75.9706 36.96 71C36.96 66.0294 32.9306 62 27.96 62C22.9894 62 18.96 66.0294 18.96 71C18.96 75.9706 22.9894 80 27.96 80Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M60.04 80C65.0106 80 69.04 75.9706 69.04 71C69.04 66.0294 65.0106 62 60.04 62C55.0694 62 51.04 66.0294 51.04 71C51.04 75.9706 55.0694 80 60.04 80Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M44 53C48.9706 53 53 48.9706 53 44C53 39.0294 48.9706 35 44 35C39.0294 35 35 39.0294 35 44C35 48.9706 39.0294 53 44 53Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M60.04 26C65.0106 26 69.04 21.9706 69.04 17C69.04 12.0294 65.0106 8 60.04 8C55.0694 8 51.04 12.0294 51.04 17C51.04 21.9706 55.0694 26 60.04 26Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M27.96 26C32.9306 26 36.96 21.9706 36.96 17C36.96 12.0294 32.9306 8 27.96 8C22.9894 8 18.96 12.0294 18.96 17C18.96 21.9706 22.9894 26 27.96 26Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M51 16.7188H36.95" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M70.88 35.9998L64.44 24.8398" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M64.44 63.16L70.88 52" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M37 71.2812H51.05" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M17.12 52L23.56 63.16" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M23.56 24.8398L17.12 35.9998" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
        </svg>
    );
  };

  export const UXIcon5 = () => {

    return (
        <svg width="88" height="88" viewBox="0 0 88 88" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M13 53C17.9706 53 22 48.9706 22 44C22 39.0294 17.9706 35 13 35C8.02944 35 4 39.0294 4 44C4 48.9706 8.02944 53 13 53Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M75 53C79.9706 53 84 48.9706 84 44C84 39.0294 79.9706 35 75 35C70.0294 35 66 39.0294 66 44C66 48.9706 70.0294 53 75 53Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M27.96 80C32.9306 80 36.96 75.9706 36.96 71C36.96 66.0294 32.9306 62 27.96 62C22.9894 62 18.96 66.0294 18.96 71C18.96 75.9706 22.9894 80 27.96 80Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M60.04 80C65.0106 80 69.04 75.9706 69.04 71C69.04 66.0294 65.0106 62 60.04 62C55.0694 62 51.04 66.0294 51.04 71C51.04 75.9706 55.0694 80 60.04 80Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M44 53C48.9706 53 53 48.9706 53 44C53 39.0294 48.9706 35 44 35C39.0294 35 35 39.0294 35 44C35 48.9706 39.0294 53 44 53Z" fill="#8E0A3F" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M60.04 26C65.0106 26 69.04 21.9706 69.04 17C69.04 12.0294 65.0106 8 60.04 8C55.0694 8 51.04 12.0294 51.04 17C51.04 21.9706 55.0694 26 60.04 26Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M27.96 26C32.9306 26 36.96 21.9706 36.96 17C36.96 12.0294 32.9306 8 27.96 8C22.9894 8 18.96 12.0294 18.96 17C18.96 21.9706 22.9894 26 27.96 26Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M51 16.7188H36.95" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M70.88 35.9998L64.44 24.8398" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M64.44 63.16L70.88 52" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M37 71.2812H51.05" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M17.12 52L23.56 63.16" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M23.56 24.8398L17.12 35.9998" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
        </svg>
    );
  };

  export const UXIcon6 = () => {

    return (
        <svg width="88" height="88" viewBox="0 0 88 88" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M13 53C17.9706 53 22 48.9706 22 44C22 39.0294 17.9706 35 13 35C8.02944 35 4 39.0294 4 44C4 48.9706 8.02944 53 13 53Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M75 53C79.9706 53 84 48.9706 84 44C84 39.0294 79.9706 35 75 35C70.0294 35 66 39.0294 66 44C66 48.9706 70.0294 53 75 53Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M27.96 80C32.9306 80 36.96 75.9706 36.96 71C36.96 66.0294 32.9306 62 27.96 62C22.9894 62 18.96 66.0294 18.96 71C18.96 75.9706 22.9894 80 27.96 80Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M60.04 80C65.0106 80 69.04 75.9706 69.04 71C69.04 66.0294 65.0106 62 60.04 62C55.0694 62 51.04 66.0294 51.04 71C51.04 75.9706 55.0694 80 60.04 80Z" fill="#8E0A3F" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M44 53C48.9706 53 53 48.9706 53 44C53 39.0294 48.9706 35 44 35C39.0294 35 35 39.0294 35 44C35 48.9706 39.0294 53 44 53Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M60.04 26C65.0106 26 69.04 21.9706 69.04 17C69.04 12.0294 65.0106 8 60.04 8C55.0694 8 51.04 12.0294 51.04 17C51.04 21.9706 55.0694 26 60.04 26Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M27.96 26C32.9306 26 36.96 21.9706 36.96 17C36.96 12.0294 32.9306 8 27.96 8C22.9894 8 18.96 12.0294 18.96 17C18.96 21.9706 22.9894 26 27.96 26Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M51 16.7188H36.95" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M70.88 35.9998L64.44 24.8398" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M64.44 63.16L70.88 52" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M37 71.2812H51.05" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M17.12 52L23.56 63.16" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M23.56 24.8398L17.12 35.9998" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
        </svg>
    );
  };

  export const UXIcon7 = () => {

    return (
        <svg width="88" height="88" viewBox="0 0 88 88" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M13 53C17.9706 53 22 48.9706 22 44C22 39.0294 17.9706 35 13 35C8.02944 35 4 39.0294 4 44C4 48.9706 8.02944 53 13 53Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M75 53C79.9706 53 84 48.9706 84 44C84 39.0294 79.9706 35 75 35C70.0294 35 66 39.0294 66 44C66 48.9706 70.0294 53 75 53Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M27.96 80C32.9306 80 36.96 75.9706 36.96 71C36.96 66.0294 32.9306 62 27.96 62C22.9894 62 18.96 66.0294 18.96 71C18.96 75.9706 22.9894 80 27.96 80Z" fill="#8E0A3F" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M60.04 80C65.0106 80 69.04 75.9706 69.04 71C69.04 66.0294 65.0106 62 60.04 62C55.0694 62 51.04 66.0294 51.04 71C51.04 75.9706 55.0694 80 60.04 80Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M44 53C48.9706 53 53 48.9706 53 44C53 39.0294 48.9706 35 44 35C39.0294 35 35 39.0294 35 44C35 48.9706 39.0294 53 44 53Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M60.04 26C65.0106 26 69.04 21.9706 69.04 17C69.04 12.0294 65.0106 8 60.04 8C55.0694 8 51.04 12.0294 51.04 17C51.04 21.9706 55.0694 26 60.04 26Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M27.96 26C32.9306 26 36.96 21.9706 36.96 17C36.96 12.0294 32.9306 8 27.96 8C22.9894 8 18.96 12.0294 18.96 17C18.96 21.9706 22.9894 26 27.96 26Z" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M51 16.7188H36.95" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M70.88 35.9998L64.44 24.8398" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M64.44 63.16L70.88 52" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M37 71.2812H51.05" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M17.12 52L23.56 63.16" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
            <path d="M23.56 24.8398L17.12 35.9998" stroke="#8E0A3F" strokeWidth="3" strokeMiterlimit="10"/>
        </svg>
    );
  };
