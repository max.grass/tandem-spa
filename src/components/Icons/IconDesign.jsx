import React from "react";
import PropTypes from "prop-types";

export const IconDesign = (props) => {
  const { width, height, primaryColor, secondaryColor } = props;

  return (
    <svg width={width} height={height} fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M13 22c-5 0-9-4-9-9s4-9 9-9 9 4 9 9-4 9-9 9ZM13 84c-5 0-9-4-9-9s4-9 9-9 9 4 9 9-4 9-9 9ZM75 22c-5 0-9-4-9-9s4-9 9-9 9 4 9 9-4 9-9 9ZM13 21.5v44M75 22v18M22 13h44M22 75h18" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
      <path d="m38 38 16.1 48 10.8-21.1L86 54.1 38 38Z" fill={primaryColor} />
    </svg>
  );
};

IconDesign.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  primaryColor: PropTypes.string,
  secondaryColor: PropTypes.string,
};

IconDesign.defaultProps = {
  width: 88,
  height: 88,
  primaryColor: "var(--color-white)",
  secondaryColor: "var(--color-blue-marguerite)"
};