import React from "react";
import PropTypes from "prop-types";

export const IconGear = (props) => {
  const { width, height, primaryColor, secondaryColor } = props;

  return (
    <svg width={width} height={height} viewBox="0 0 83 83" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M41.5 63.5C53.6503 63.5 63.5 53.6503 63.5 41.5C63.5 29.3497 53.6503 19.5 41.5 19.5C29.3497 19.5 19.5 29.3497 19.5 41.5C19.5 53.6503 29.3497 63.5 41.5 63.5Z" fill={primaryColor}/>
      <path d="M41.5 57.5C32.7 57.5 25.5 50.3 25.5 41.5C25.5 32.7 32.7 25.5 41.5 25.5C50.3 25.5 57.5 32.7 57.5 41.5C57.5 43.7 57.1 45.7 56.3 47.6" stroke="white" strokeWidth="3" strokeMiterlimit="10"/>
      <path d="M54.2 11C52 10.1 49.7 9.4 47.3 9L45.2 1.5H37.7L35.7 9C31 9.8 26.5 11.7 22.6 14.4L15.8 10.5L10.5 15.8L14.4 22.6C11.7 26.5 9.9 31 9 35.7L1.5 37.8V45.3L9 47.3C9.8 52 11.7 56.5 14.4 60.4L10.5 67.2L15.8 72.5L22.6 68.6C26.5 71.3 31 73.2 35.7 74L37.7 81.5H45.2L47.2 74C51.9 73.2 56.4 71.3 60.3 68.6L67.1 72.5L72.4 67.2L68.5 60.4C71.2 56.5 73.1 52 73.9 47.3L81.4 45.2V37.7L73.9 35.7C73.1 31 71.2 26.5 68.5 22.6L72.4 15.8L67.1 10.5" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
    </svg>
  );
};

IconGear.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  primaryColor: PropTypes.string,
  secondaryColor: PropTypes.string,
};

IconGear.defaultProps = {
  width: 88,
  height: 88,
  primaryColor: "var(--color-white)",
  secondaryColor: "var(--color-blue-marguerite)"
};
