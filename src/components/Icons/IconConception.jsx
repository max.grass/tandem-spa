import React from "react";
import PropTypes from "prop-types";

export const IconConception = (props) => {
  const { width, height, primaryColor, secondaryColor } = props;

  return (
    <svg width={width} height={height} fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M18 49c-2.5-4.4-4-9.5-4-15C14 17.4 27.4 4 44 4s30 13.4 30 30c0 4.8-1.1 9.3-3.1 13.3" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
      <path d="M44 56c12.15 0 22-9.85 22-22s-9.85-22-22-22-22 9.85-22 22 9.85 22 22 22Z" fill={primaryColor}/>
      <path d="m46.9 50.5 5.8-17.8c-2.7 1.2-5.6 1.8-8.8 1.8-3.2 0-6.1-.7-8.8-1.8l5.8 17.8" stroke={secondaryColor} strokeWidth="3" strokeMiterlimit="10"/>
      <path d="m17.1 47.3 11.4 21.2M70.9 47.3 66 56.4M60 79V68H28v11l5 5h22l5-5Z" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
    </svg>
  );
};

IconConception.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  primaryColor: PropTypes.string,
  secondaryColor: PropTypes.string,
};

IconConception.defaultProps = {
  width: 88,
  height: 88,
  primaryColor: "var(--color-white)",
  secondaryColor: "var(--color-blue-marguerite)"
};