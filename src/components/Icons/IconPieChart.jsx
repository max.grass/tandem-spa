import React from "react";
import PropTypes from "prop-types";

export const IconPieChart = (props) => {
  const { width, height, primaryColor, secondaryColor } = props;

  return (
    <svg width={width} height={height} viewBox="0 0 88 88" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M44 70C58.3594 70 70 58.3594 70 44C70 29.6406 58.3594 18 44 18C29.6406 18 18 29.6406 18 44C18 58.3594 29.6406 70 44 70Z" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
      <path d="M44 84.0007V70.0007C49.1409 69.9988 54.1659 68.4729 58.4398 65.6159C62.7137 62.7588 66.0448 58.6989 68.0119 53.9492C69.9789 49.1995 70.4938 43.9732 69.4914 38.931C68.489 33.8887 66.0142 29.2568 62.38 25.6207L72.28 15.7207C77.8724 21.3148 81.6806 28.4413 83.2233 36.1994C84.766 43.9576 83.9739 51.9989 80.9471 59.3069C77.9203 66.6149 72.7947 72.8614 66.2183 77.2568C59.6419 81.6521 51.91 83.999 44 84.0007Z" fill={primaryColor}/>
      <path d="M29.76 6.61047C39.0013 3.05889 49.2302 3.05418 58.4747 6.59724C67.7192 10.1403 75.3249 16.9803 79.8253 25.7985C84.3257 34.6167 85.4022 44.7888 82.8474 54.3537C80.2926 63.9187 74.2872 72.1991 65.9891 77.5988C57.6911 82.9985 47.6878 85.1351 37.9078 83.5966C28.1278 82.0581 19.2636 76.9535 13.0241 69.267C6.78463 61.5804 3.61161 51.8561 4.11679 41.9687C4.62197 32.0814 8.76957 22.7311 15.76 15.7205" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
      <path d="M18 44H4" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
    </svg>
  );
};

IconPieChart.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  primaryColor: PropTypes.string,
  secondaryColor: PropTypes.string,
};

IconPieChart.defaultProps = {
  width: 88,
  height: 88,
  primaryColor: "var(--color-white)",
  secondaryColor: "var(--color-blue-marguerite)"
};
