import React from "react";
import PropTypes from "prop-types";

export const IconCheckWindow = (props) => {
  const { width, height, primaryColor, secondaryColor } = props;

  return (
    <svg width={width} height={height} viewBox="0 0 88 88" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M74 4H84V84H4V4H56.83" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
      <path d="M44 30.5C55.8741 30.5 65.5 40.1259 65.5 52C65.5 63.8741 55.8741 73.5 44 73.5C32.1259 73.5 22.5 63.8741 22.5 52C22.5 40.1259 32.1259 30.5 44 30.5Z" fill={primaryColor} stroke={primaryColor}/>
      <path d="M4 20H84" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
      <path d="M10.07 12H13.07" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
      <path d="M17.03 12H20.03" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
      <path d="M24 12H27" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
      <path d="M35 52L41 58L53 46" stroke="white" strokeWidth="3" strokeMiterlimit="10"/>
    </svg>
  );
};

IconCheckWindow.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  primaryColor: PropTypes.string,
  secondaryColor: PropTypes.string,
};

IconCheckWindow.defaultProps = {
  width: 88,
  height: 88,
  primaryColor: "var(--color-white)",
  secondaryColor: "var(--color-blue-marguerite)"
};
