import React from "react";
import PropTypes from "prop-types";

export const IconSupport = (props) => {
  const { width, height, primaryColor, secondaryColor } = props;

  return (
    <svg width={width} height={height} fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M43.5 65.5c12.15 0 22-9.85 22-22s-9.85-22-22-22-22 9.85-22 22 9.85 22 22 22Z" fill={primaryColor}/>
      <path d="M43.5 59.5c-8.8 0-16-7.2-16-16s7.2-16 16-16 16 7.2 16 16c0 2.2-.4 4.2-1.2 6.1" stroke={secondaryColor} strokeWidth="3" strokeMiterlimit="10"/>
      <path d="M56.2 13c-2.2-.9-4.5-1.6-6.9-2l-2.1-7.5h-7.5l-2 7.5c-4.7.8-9.2 2.7-13.1 5.4l-6.8-3.9-5.3 5.3 3.9 6.8c-2.7 3.9-4.5 8.4-5.4 13.1l-7.5 2.1v7.5l7.5 2c.8 4.7 2.7 9.2 5.4 13.1l-3.9 6.8 5.3 5.3 6.8-3.9c3.9 2.7 8.4 4.6 13.1 5.4l2 7.5h7.5l2-7.5c4.7-.8 9.2-2.7 13.1-5.4l6.8 3.9 5.3-5.3-3.9-6.8c2.7-3.9 4.6-8.4 5.4-13.1l7.5-2.1v-7.5l-7.5-2c-.8-4.7-2.7-9.2-5.4-13.1l3.9-6.8-5.3-5.3" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
    </svg>
  );
};

IconSupport.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  primaryColor: PropTypes.string,
  secondaryColor: PropTypes.string,
};

IconSupport.defaultProps = {
  width: 88,
  height: 88,
  primaryColor: "var(--color-white)",
  secondaryColor: "var(--color-blue-marguerite)"
};