import React from "react";
import PropTypes from "prop-types";

export const IconConsulting = (props) => {
  const { width, height, primaryColor, secondaryColor } = props;

  return (
    <svg width={width} height={height} fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M44 74c16.569 0 30-13.431 30-30 0-16.569-13.431-30-30-30-16.569 0-30 13.431-30 30 0 16.569 13.431 30 30 30Z" fill={primaryColor}/>
      <path d="M72.3 15.7c-8-7.9-19.2-12.5-31.5-11.6C20.9 5.7 5.1 22 4.1 41.8 3.4 53.7 8 64.4 15.6 72.1L4 84h39.2C65.3 84 83.8 66.4 84 44.3c0-5.1-.9-10-2.6-14.6" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
      <path d="M29 33h30M29 44h30M29 56h16" stroke={secondaryColor} strokeWidth="3" strokeMiterlimit="10"/>
    </svg>
  );
};

IconConsulting.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  primaryColor: PropTypes.string,
  secondaryColor: PropTypes.string,
};

IconConsulting.defaultProps = {
  width: 88,
  height: 88,
  primaryColor: "var(--color-white)",
  secondaryColor: "var(--color-blue-marguerite)"
};