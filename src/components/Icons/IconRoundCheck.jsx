import React from "react";
import PropTypes from "prop-types";

export const IconRoundCheck = (props) => {
  const { width, height, primaryColor, secondaryColor } = props;

  return (
    <svg width={width} height={height} viewBox="0 0 88 88" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M58.24 6.61047C48.9987 3.05889 38.7698 3.05418 29.5253 6.59724C20.2807 10.1403 12.6751 16.9803 8.17473 25.7985C3.67432 34.6167 2.59776 44.7888 5.1526 54.3537C7.70743 63.9187 13.7128 72.1991 22.0109 77.5988C30.3089 82.9985 40.3122 85.1351 50.0922 83.5966C59.8722 82.0581 68.7364 76.9535 74.9759 69.267C81.2154 61.5804 84.3884 51.8561 83.8832 41.9687C83.378 32.0814 79.2304 22.7311 72.24 15.7205" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
      <path d="M66 36C66 30.1652 63.6822 24.5695 59.5564 20.4437C55.4305 16.3179 49.8348 14 44 14C38.1652 14 32.5695 16.3179 28.4437 20.4437C24.3179 24.5695 22 30.1652 22 36C21.9988 40.3207 23.2698 44.5461 25.6546 48.149C28.0394 51.752 31.4323 54.573 35.41 56.26C30.3259 57.7819 25.7365 60.6253 22.11 64.5C24.916 67.4942 28.3064 69.8809 32.0715 71.5125C35.8367 73.1441 39.8965 73.9859 44 73.9859C48.1035 73.9859 52.1633 73.1441 55.9285 71.5125C59.6936 69.8809 63.084 67.4942 65.89 64.5C62.2635 60.6253 57.6741 57.7819 52.59 56.26C56.5677 54.573 59.9606 51.752 62.3454 48.149C64.7302 44.5461 66.0012 40.3207 66 36Z" fill={primaryColor}/>
      <path d="M35 36L41 42L53 30" stroke="white" strokeWidth="3" strokeMiterlimit="10"/>
    </svg>
  );
};

IconRoundCheck.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  primaryColor: PropTypes.string,
  secondaryColor: PropTypes.string,
};

IconRoundCheck.defaultProps = {
  width: 88,
  height: 88,
  primaryColor: "var(--color-white)",
  secondaryColor: "var(--color-blue-marguerite)"
};
