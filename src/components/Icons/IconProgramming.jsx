import React from "react";
import PropTypes from "prop-types";

export const IconProgramming = (props) => {
  const { width, height, primaryColor, secondaryColor } = props;

  return (
    <svg width={width} height={height} fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M74 4h10v80H4V4h52.8" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
      <path d="M52 74H36c-12.2 0-22-9.8-22-22s9.8-22 22-22h16c12.2 0 22 9.8 22 22s-9.8 22-22 22Z" fill={primaryColor}/>
      <path d="M4 20h80" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
      <path d="M34 64 22 52l12-12M54 64l12-12-12-12M48 40l-8 24" stroke={secondaryColor} strokeWidth="3" strokeMiterlimit="10"/>
      <path d="M10.1 12h3M17 12h3M24 12h3" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
    </svg>
  );
};

IconProgramming.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  primaryColor: PropTypes.string,
  secondaryColor: PropTypes.string,
};

IconProgramming.defaultProps = {
  width: 88,
  height: 88,
  primaryColor: "var(--color-white)",
  secondaryColor: "var(--color-blue-marguerite)"
};