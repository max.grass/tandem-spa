import React from "react";
import PropTypes from "prop-types";

export const IconEmail = (props) => {
  const { width, height, primaryColor, secondaryColor } = props;

  return (
    <svg width={width} height={height} viewBox="0 0 88 88" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path d="M74 84H84V36L44 59.5L4 36V84H56.83" stroke={primaryColor} strokeWidth="3" strokeMiterlimit="10"/>
      <path d="M74 32L44 50L14 32V4H74V32Z" fill={primaryColor}/>
      <path d="M29 15H59" stroke="white" strokeWidth="3" strokeMiterlimit="10"/>
      <path d="M29 24H59" stroke="white" strokeWidth="3" strokeMiterlimit="10"/>
      <path d="M29 34H45" stroke="white" strokeWidth="3" strokeMiterlimit="10"/>
    </svg>
  );
};

IconEmail.propTypes = {
  width: PropTypes.number,
  height: PropTypes.number,
  primaryColor: PropTypes.string,
  secondaryColor: PropTypes.string,
};

IconEmail.defaultProps = {
  width: 88,
  height: 88,
  primaryColor: "var(--color-white)",
  secondaryColor: "var(--color-blue-marguerite)"
};
