const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const WorkboxPlugin = require('workbox-webpack-plugin');
const { Build } = require('@material-ui/icons');
const  { CleanWebpackPlugin }  =  require ('clean-webpack-plugin') ;
const WebpackPwaManifest = require('webpack-pwa-manifest');
const ImageMinimizerPlugin = require("image-minimizer-webpack-plugin");

module.exports = {
  entry: path.resolve(__dirname, 'src', 'index.js'),
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'main.js',
    publicPath: '/',
  },
  resolve: {
    alias: {
      components: path.resolve(__dirname, 'src', 'components'),
      assets: path.resolve(__dirname, 'src', 'assets'),
      pages: path.resolve(__dirname, 'src', 'pages'),
    },
    extensions: ['.js', '.jsx'],
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.s?css$/,
        use: [
          MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              modules: {
                localIdentName: '[name]-[local]-[hash:base64:5]'
              }
            }
          },
          {
            loader: "postcss-loader",
            options: {
              postcssOptions: {
                plugins: [
                  [
                    "autoprefixer",
                  ],
                ]
              },
            },
          }
        ],
      },
      {
        test: /\.(png|jpe?g|svg|gif)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'images',
            },
          },
        ],
      },
      {
        test: /\.(pdf)$/i,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: './',
            },
          },
        ],
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: 'asset/resource',
      },
    ]
  },
  optimization: {
    minimizer: [
      new ImageMinimizerPlugin({
        minimizer: {
          implementation: ImageMinimizerPlugin.squooshMinify,
          options: {
            encodeOptions: {
              mozjpeg: {
                // That setting might be close to lossless, but it’s not guaranteed
                // https://github.com/GoogleChromeLabs/squoosh/issues/85
                quality: 85,
              },
              pngquant: {
                quality: [0.75, 0.9]
              },
              webp: {
                lossless: 1,
              }
            },
          },
        },
        generator: [
          {
            // You can apply generator using `?as=webp`, you can use any name and provide more options
            preset: "webp",
            implementation: ImageMinimizerPlugin.squooshGenerate,
            options: {
              encodeOptions: {
                // Please specify only one codec here, multiple codecs will not work
                webp: {
                  quality: 90,
                },
              },
            },
          },
        ],
      }),
    ],
  },


  plugins: [
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, 'src', 'index.html'),
      filename: 'index.html',
      favicon: "./src/static/icons/favicon.ico"
    }),
    new MiniCssExtractPlugin({
      filename: 'main.css',
    }),
    new WebpackPwaManifest({
      name: 'Tandem Marketing and Partners',
      short_name: 'TMP',
      description: 'Tandem Marketing and Partners - Full-Service Webagentur für komplexe Weblösungen ',
      background_color: '#64062e',
      icons: [
        {
          src: path.resolve('./src/static/icons/favicon.png'),
          sizes: [16, 32, 72, 96, 120, 128, 144, 152, 167, 180, 192, 384, 512],
        },
        {
          src: path.resolve('./src/static/icons/favicon.png'),
          sizes: [120, 152, 167, 180, 1024],
          ios: true
        },
        {
          src: path.resolve('./src/static/icons/favicon.png'),
          size: '512x512',
          purpose: 'maskable'
        }
      ],
    }),
    new WorkboxPlugin.GenerateSW({
      clientsClaim: true,
      skipWaiting: true,
    }),
    new  CleanWebpackPlugin(), 
  ],
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 9002,
    useLocalIp: true,
    historyApiFallback: {
      disableDotRule: true
    },
  },
  //devtool: 'eval-source-map',
}
